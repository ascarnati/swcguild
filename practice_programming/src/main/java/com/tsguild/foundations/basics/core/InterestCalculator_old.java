/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculator_old {

    public static void main(String[] args) {

        double annualRate = 0;
        double currentBalance = 0;
        double quarterBalance = 0;
        double annualBalance = 0;
        int annualDuration = 0;

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Please enter your principal amount: ");
        currentBalance = myScanner.nextDouble();

        System.out.println("Please enter the annual interest rate: ");
        annualRate = myScanner.nextDouble();

        System.out.println("Please enter the duration (in years): ");
        annualDuration = myScanner.nextInt();

        double quarterRate = annualRate / 4;
        int quarterDuration = annualDuration * 4;

        for (int i = 1; i <= quarterDuration; i++) {
            quarterBalance = currentBalance * (1 + (quarterRate / 100));
            currentBalance = quarterBalance;

            if (i % 4 == 0) {
                annualBalance = quarterBalance;
            }
            System.out.print("Year: " + i / 4);
            System.out.println(" Ending Balance: " + annualBalance);
        }

    }
}
