/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core.SimpleCalculator;

/**
 *
 * @author apprentice
 */
public class SimpleCalculator {

    public double addition(double a, double b) {
        return (a + b);
    }

    public double subtraction(double a, double b) {
        return (a - b);
    }

    public double multiplication(double a, double b) {
        return (a * b);
    }

    public double division(double a, double b) {
        return (a / b);
    }

}
