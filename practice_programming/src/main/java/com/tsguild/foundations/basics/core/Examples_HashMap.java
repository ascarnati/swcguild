/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class Examples_HashMap {
    
    public static void main(String[] args) {
        
    // ++CREATION AND ADDING KEY/VALUE MAPPING++
    
    // create a map that maps a country to its population
    // requires you to first import and instantiate the java.util.HashMap
    HashMap<String, Integer> populations = new HashMap<>();
       
    // add the first country
    populations.put("USA", 313000000);
       
    // add the next country
    populations.put("Canada", 34000000);
       
    // add another country
    populations.put("United Kingdom", 63000000);
       
    // add another country
    populations.put("Japan", 127000000);
       
    // ask the map for its size
    System.out.println("Map size is: " + populations.size());
 
    // ++REPLACING A KEY/VALUE MAPPING++
       
    // replace the mapping for population of the USA - original
    // number was too low
    populations.put("USA", 200000000);
       
    // ask the map for its size - it will be 4
    System.out.println("Map size is: " + populations.size());
    
    // ++RETRIEVING A KEY/VALUE MAPPING++
    
    // get the poplation of Japan and print it to the screen
    Integer japanPopulation = populations.get("Japan");
    System.out.println("The population of Japan is: " + 
            japanPopulation);    
    
    // ++ LISTING ALL THE KEYS ++
    
    // get the Set of keys from the map
    // requires you to first import and instantiate the java.util.Set
    Set<String> keys = populations.keySet();
       
    // print the keys to the screen - is the order they are printed
    // what you would expect?  It prints them in random order - Canada, 
    // USA, Japan, UK
    for (String mytempvariable : keys) {
        System.out.println(mytempvariable);
        }    
    
    // print the keys AND THEIR ASSOCIATED VALUES to the screen
    // notice it still prints them in random order
    for (String mytempvariable : keys) {
        System.out.println("The population of " + mytempvariable + 
                " is " + populations.get(mytempvariable));
        }
    // ++ LISTING ALL THE VALUES: AKA VALUE COLLECTION ++
    // get the Collection of values from the Map
    // requires you to first import and instantiate the java.util.Collections
    Collection<Integer> popValues = populations.values();
       
    // list all of the population values - how can we tell which 
    // population value goes with each country?
    for (Integer p : popValues) {
        System.out.println(p);
        }
    
    }
    
}
