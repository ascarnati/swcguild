/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BiggerBetterAdder {

    public static void main(String[] args)  {

        byte games;
        float era;
        int sanchezHomeruns, birdHomeruns, combinedHomeruns;
         
        Scanner inputReader = new Scanner(System.in);
        
        System.out.println("Number of games: ");
        games = inputReader.nextByte();
        
        System.out.println("Earned Run Average: ");
        era = inputReader.nextFloat();
        
        System.out.println("How many home runs by Sanchez: ");
        sanchezHomeruns = inputReader.nextInt();
        
        System.out.println("How many home runs by Bird: ");
        birdHomeruns = inputReader.nextInt();

        combinedHomeruns = sanchezHomeruns + birdHomeruns;
        
        System.out.println("CC Sabbathia will have a sub " + era + " era.");
        System.out.println("Gary Sanchez will hit " + sanchezHomeruns + " "
                + "homeruns.");
        System.out.println("The Baby Bombers will combine for over " 
                + combinedHomeruns + " dingers!");
        System.out.println("And the Yankees will win " + games + " games "
                + "this year!");
        System.out.println("You heard it here first!");
        
    }

    
}
