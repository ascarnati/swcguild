/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculator {

    public static void main(String[] args) {

// ********** PREP VARIABLES ************         
        int reportYear,
                numYears,
                currentYear = 2017;
        double principalAmount = 0.0,
                interestRate = 0.0,
                newPrincipal = 0.0,
                interestEarned = 0.0;
        String stringPrincipalAmount,
                stringInterestRate,
                stringNewPrincipal,
                stringInterestEarned,
                stringNumYears,
                stringReportYear;

        Scanner sc = new Scanner(System.in);
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        
// ******* PROMPT USER FOR INPUT *********
        do {
            System.out.println("Please enter an amount > $0.01"
                    + " to be invested: ");
            stringPrincipalAmount = sc.next();
            principalAmount = Double.parseDouble(stringPrincipalAmount);
        } while (principalAmount < 0.01);

        do {
            System.out.println("Please enter a future year to start investing: ");
            stringReportYear = sc.next();
            reportYear = Integer.parseInt(stringReportYear);
        } while (reportYear < currentYear);

        do {
            System.out.println("Please enter the number of years to invest: ");
            stringNumYears = sc.next();
            numYears = Integer.parseInt(stringNumYears);
        } while (numYears < 1);

        int i = 0;
        newPrincipal = principalAmount;

        do {
            // this is where we will call our method to  do the calculation

            double newNetValue = calcInterest(newPrincipal);
            // ****  Need to prep/calc for our annual reporting
            interestEarned = newNetValue - principalAmount;
            System.out.println(" ");
            System.out.println("***************** " + reportYear
                    + " *****************");
            System.out.println("Financial Report for " + reportYear);
            System.out.println("Initial Principal Value for "
                    + reportYear + " = " + formatter.format(newPrincipal));
            System.out.println("Interest earned for  "
                    + reportYear + " = " + formatter.format(interestEarned));
            System.out.println("New Principal Value for "
                    + reportYear + " = " + formatter.format(newNetValue));
            System.out.println(" ");

            
            //  ****  Need to increment 1 for next year processing     
            i++;
            newPrincipal = newNetValue;
            reportYear = reportYear + 1;
        } while (numYears > i);
    }

    public static double calcInterest(double principal) {
        double newPrincipalCalc = 0.0;
        newPrincipalCalc = principal;
        int i = 1;

        do {
            newPrincipalCalc = newPrincipalCalc * 1.025;
            i++;
        } while (i < 5);

        principal = newPrincipalCalc;

        return principal;
    }
}