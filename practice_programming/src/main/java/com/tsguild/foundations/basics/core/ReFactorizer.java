/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class ReFactorizer {

    private int num;
    private boolean isPrime = true;
    private int factCount;
    private int perfectSum;


    public ReFactorizer(int num) {
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
//                System.out.println(i);
                perfectSum = perfectSum + i;
                isPrime = false;
                factCount = factCount + 1;
            }
        }
    }

    public int getNum() {
        return num;
    }

    //Not including this method because only allowing number to be set thru 
    //constructor above;  Making this property READ ONLY
//   public void setNum(int num) {
//        this.num = num;
//    }
    
    public boolean getIsPrime() {
        return isPrime;
    }

    //in a proper class file, this would be removed if we want isPrime to be 
    //read only
//    public void setIsPrime(boolean isPrime) {
//        this.isPrime = isPrime;
//    }

    public int getFactCount() {
        return factCount;
    }

    
    //in a proper class file, this would be removed if we want factCount to be 
    //read only
//    public void setFactCount(int factCount) {
//        this.factCount = factCount;
//    }

}
