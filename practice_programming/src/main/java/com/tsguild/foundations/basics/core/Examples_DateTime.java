/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author apprentice
 */
public class Examples_DateTime {
    
    public static void main(String[] args) {
    
    // * BASIC FORMATTING EXAMPLES * //
        // declare a variable "LocalDate" 
        LocalDate ld = LocalDate.now();
        System.out.println(ld);
        
        ld = LocalDate.parse("2015-01-01");
        System.out.println(ld);
        
        ld = LocalDate.parse("02/07/2010", DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        System.out.println(ld);
        
        String isoDate = ld.toString();
        System.out.println(isoDate);
        ld = LocalDate.parse(isoDate);
        System.out.println(ld);
        
        String formatted = ld.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        System.out.println(formatted);
        
        formatted = ld.format(DateTimeFormatter.ofPattern("MM=dd=yyyy+=+=+="));
        System.out.println(formatted);

        formatted = ld.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
        System.out.println(formatted);
        
        // * CALCULATING DATES in future or past based upon a date * //
        
        LocalDate past = ld.minusDays(8);
        formatted = past.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
        System.out.println(formatted);
        
        past = ld.minusMonths(3);
        formatted = past.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
        System.out.println(formatted);
     
    // * CALCULATING PERIODS of TIME * //
        
        Period diff = ld.until(past);
        // declare variable of type Period called diff; requires that you
        // import java.time.Period
        System.out.println(diff);
        // ask the Period for the month
        System.out.println(diff.getMonths());
        // ask the Period for the month, but reverse it for a positive number
        diff = past.until(ld);
        System.out.println(diff.getMonths());
        
    // * LOCAL DATETIME * //
    
        // declare a variable LocalDateTime (includes hh:mm:ss)
        // has all the same types of options as LocalDate
    
        LocalDateTime ldt = LocalDateTime.now();
        System.out.println(ldt);  // this line is the DEFAULT ISO format
        formatted = ldt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
        // note the importance of cap M and lower m above
        System.out.println(formatted);
        
    // * CONVERTING B/W MODERN and LEGACY API's * //
        
        // instantiate a date object 
        Date legacyDate = new Date();  //this will print the date for right now
                                       //this is the default legacy format
        System.out.println(legacyDate);
        
        // instantiate a Gregorian Calendar date
        GregorianCalendar legacyCalendar = new GregorianCalendar();
        System.out.println(legacyCalendar);  // this is UNFORMATTED and more useful
                                             // for debugging purposes; not viewer-friendly

        ZonedDateTime zdt = ZonedDateTime.ofInstant(legacyDate.toInstant(), ZoneId.systemDefault());
        ld = zdt.toLocalDate();
        System.out.println(ld);
        
        zdt = legacyCalendar.toZonedDateTime();
        ld = zdt.toLocalDate();
        System.out.println(ld);
    
    }
}
