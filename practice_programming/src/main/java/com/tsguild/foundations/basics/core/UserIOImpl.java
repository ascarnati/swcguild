/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class UserIOImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double readDouble(String prompt) {
        System.out.println(prompt);
        String line = sc.nextLine();
        double dub = Double.parseDouble(line);
        return dub;
        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        String line;
        do {
            System.out.println(prompt);
            line = sc.nextLine();
            // dub = Double.parseDouble(line);
        } while (Double.parseDouble(line) < min || Double.parseDouble(line) > max);
        return Double.parseDouble(line);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float readFloat(String prompt) {
        System.out.println(prompt);
        String line = sc.nextLine();
        float flo = Float.parseFloat(line);
        return flo;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        String line;
        do {
            System.out.println(prompt);
            line = sc.nextLine();
        } while (Float.parseFloat(line) < min || Float.parseFloat(line) > max);

        return Float.parseFloat(line);
//   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int readInt(String prompt) {
        System.out.println(prompt);
        String line = sc.nextLine();
        //  float flo = Integer.parseInt(line);
        return Integer.parseInt(line);
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        String line;
        do {
            System.out.println(prompt);
            line = sc.nextLine();
        } while (Integer.parseInt(line) < min || Integer.parseInt(line) > max);

        return Integer.parseInt(line);
// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long readLong(String prompt) {
        System.out.println(prompt);
        String line = sc.nextLine();
        return Long.parseLong(line);
//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        String line;
        do {
            System.out.println(prompt);
            line = sc.nextLine();
        } while (Long.parseLong(line) < min || Long.parseLong(line) > max);

        return Long.parseLong(line);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        String line = sc.nextLine();
        return line;
// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
