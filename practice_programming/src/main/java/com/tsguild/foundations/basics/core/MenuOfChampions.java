/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.math.BigDecimal;

/**
 *
 * @author apprentice
 */
public class MenuOfChampions {
    
    public static void main(String[] args)  {
        
        //declaring as BigDecimal varialbes and setting the values here 
        BigDecimal soup = new BigDecimal("10.00");
        BigDecimal pizza = new BigDecimal("5.00");
        BigDecimal sushi = new BigDecimal("7.50");
        
        //creating the display here
      
        System.out.println("Welcome to Anthony's Restaurant!");
        System.out.println("Where we only serve MY favorite foods!");
        System.out.println("~~~~~~~~~ Our Menu ~~~~~~~~~");
        
        System.out.println("A steaming bowl of Pho      $" + soup);
        System.out.println("Always fresh slice of Pizza $" + pizza);
        System.out.println("Sushi for me; Sushi for you $" + sushi);        
        
    }
}
