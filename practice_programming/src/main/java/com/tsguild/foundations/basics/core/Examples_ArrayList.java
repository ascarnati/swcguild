/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class Examples_ArrayList {

    // ++CREATION AND ADDING ELEMENTS++

    public static void main(String[] args) {
    // create an ArrayList of String objects
    ArrayList<String> stringList = new ArrayList<>();

    // ask the list how big it is
    System.out.println("List size before adding any Strings: " + 
            stringList.size());

    // add a String object to our list
    stringList.add("My First String");

    // ask the list how big it is
    System.out.println("List size after adding one String: " + 
            stringList.size());

    // add another String object to our list
    stringList.add("My Second String");

    // ask the list how big it is
    System.out.println("List size after adding two Strings: " + 
            stringList.size());
    
    // ++REMOVING ELEMENTS++
    
    // remove the second String object from our list - remember that
    // our indexes start counting at 0 instead of 1
    stringList.remove(1);

    // ask the list how big it is
    System.out.println("List size after removing one String: " + 
            stringList.size());
       
    // remove the remaining String object from our list - remember
    // that the list resizes automatically so if there is only one
    // element in a list it is alway at index 0
    stringList.remove(0);
       
    // ask the list how big it is
    System.out.println("List size after removing last String: " + 
            stringList.size());
               
    // what happens if you try to remove another element? Give it a
    // try...
    //stringList.remove(0);
    // it throws an 'IndexOutOfBoundsException'
    
    //++VISITING ALL ELEMENTS: ENHANCED FOR-LOOP++
    
    // assuming we already created an ArrayList of String objects above
    // we'll just add strings back in to existing ArrayList without
    // creating it here
    // add a String object to our list
    stringList.add("My First String");

    // add another String object to our list
    stringList.add("My Second String");

    // add another String object to our list
    stringList.add("My Third String");

    // add another String object to our list
    stringList.add("My Fourth String");

    // ask the list how big it is
    System.out.println("List size: " + stringList.size());
       
    // print every String in our list with an enhanced for loop
    for (String item : stringList) {
        System.out.println(item);

    }
    // ++USING AN ITERATOR to visit all elements++
    // print every String in our list with an iterator

    // ask the list how big it is
    System.out.println("");
    System.out.println("List size: " + stringList.size());
    
    // ask for the iterator - we must ask for an iterator of Strings
    // If we don't, we'll get an "uncompilable source code - 
    // Erroneous sym type: iter.hasNext"
    Iterator<String> myIter = stringList.iterator();
       
    // get String objects from the list while there are still Strings
    // remaining
    while(myIter.hasNext()) {
        String current = myIter.next();
        System.out.println(current);
    }  
    
    }
}

    
