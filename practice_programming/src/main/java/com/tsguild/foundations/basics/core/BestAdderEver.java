/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class BestAdderEver {
    
    public static void main(String[] args)  {
        //declaring my variables here
         byte games;
         float era;
         int sanchezHomeruns, birdHomeruns, combinedHomeruns;
         
        //setting their values here
        games = 100;
        era = 2.12f;
        sanchezHomeruns = 40;
        birdHomeruns = 25;
        combinedHomeruns = sanchezHomeruns + birdHomeruns;
        
        
        System.out.println("CC Sabbathia will have a sub " + era + " era.");
        System.out.println("Gary Sanchez will hit " + sanchezHomeruns + " "
                + "homeruns.");
        System.out.println("The Baby Bombers will combine for over " 
                + combinedHomeruns + " dingers!");
        System.out.println("And the Yankees will win " + games + " games "
                + "this year!");
        System.out.println("You heard it here first!");
        
    }
}
