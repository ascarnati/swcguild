/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class Mutt extends Dog {
    
    private String breedGuess;
    
    @Override
    public void bark() {
        System.out.println("bow wow, mannnn.");
    }
    
    public Mutt(String size, int id)   {
        super(id, size);
    }

    public String getBreedGuess() {
        return breedGuess;
    }

    public void setBreedGuess(String breedGuess) {
        this.breedGuess = breedGuess;
    }
    
    
}
