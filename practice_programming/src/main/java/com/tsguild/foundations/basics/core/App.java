/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {

        Scanner inputReader = new Scanner(System.in);
        int num;

        System.out.println("What is your number?");
        num = inputReader.nextInt();

        ReFactorizer bob = new ReFactorizer(num);

        boolean isPrime = bob.getIsPrime();

        if (isPrime == true) {
            System.out.println(num + " is a Prime number.");
        } else {
            System.out.println(num + " is not a Prime number.");
        }

    }
}
