/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author apprentice
 */
public class Lambda_Sandbox {

    public static void main(String[] args) {

        ArrayList<String> llamaHerd = new ArrayList<>();
        llamaHerd.add("Felicity");
        llamaHerd.add("BeeBop");
        llamaHerd.add("NoWhite");
        llamaHerd.add("Prism");
        llamaHerd.add("Kaliedescope");
        llamaHerd.add("Floyd");
        llamaHerd.add("Dixie");
        llamaHerd.add("Oreo");
        llamaHerd.add("Samurai");

        // use .size to obtain the size of the ArrayList
        System.out.println("I have this many llamas in my herd: " + llamaHerd.size());

        // iterate over the ArrayList with a for statement and print the name of 
        // each item in the list
        for (String llamaName : llamaHerd) {
            System.out.println(llamaName);
        }

        // anything that inherits a Collection - an ArrayList, a HashMap, e.g. -
        // simply inherit the ability to ALSO be a stream.  You don't need to 
        // set anything extra up to call llamaHerd.stream
        Stream<String> llamaStream = llamaHerd.stream();

        // Filter takes in a predicate
        // so it has ONE parameter, of the same type you're streaming 
        // and it HAS to return a boolean
        llamaHerd.stream().filter(llamaName -> {
            return true;
        });

        llamaHerd.stream().filter(llamaName -> {
            return llamaName.equals("Oreo");
        });

        // TO extract the contents of the stream, we use the .forEach operator
        // but this operator is a TERMINAL operator, which completes the stream
        // you can no longer use this stream
        System.out.println("");
        System.out.println("These are all the llamas named 'Oreo'");
        llamaHerd.stream().filter(llamaName -> llamaName.startsWith("F"))
                //equals("Oreo"))
                .forEach(shouldBeOreo -> System.out.println(shouldBeOreo));
        // .forEach used above takes in a CONSUMER Lambda

        
        // This next code block is unrelated to the Llama stuff above; its
        // using the method but has to go in our PSVM for this example.
        // this block is using the result of the sumStuff method that COLLECTED 
        // a list based upon a stream of an ArrayList
        
        int result = sumStuff((Integer bob) -> {
                                               return true;
                                                });

        System.out.println("");
        System.out.println("The result of summing all the stuff is: " + result);

        // 2nd example of using a Lambda shows returning a conditional value 
        //based upon the entire list
        int result2 = sumStuff((Integer bob) -> {
                                               return bob > 5;
                                                });

        System.out.println("");
        System.out.println("The result of summing all the stuff greater than 5: " + result2);

        // 3rd example of using a Lambda returning a conditional value based 
        // upon the entire list; it also shows a shorthand version of the above
        // ((Integer bob) -> { return bob > 5; }); becomes (bob -> bob > 5);
        int result3 = sumStuff(bob -> bob > 5);

        System.out.println("");
        System.out.println("The result of summing all the stuff greater than 5: " + result2);

    }

    public static int sumStuff(Predicate<Integer> whatToSum) {
        List<Integer> someIntsIHave = new ArrayList<>();
        someIntsIHave.add(1);
        someIntsIHave.add(2);
        someIntsIHave.add(3);
        someIntsIHave.add(4);
        someIntsIHave.add(5);
        someIntsIHave.add(6);
        someIntsIHave.add(7);
        someIntsIHave.add(8);
        someIntsIHave.add(9);
        someIntsIHave.add(10);

        // the code below creates a stream of items, 'collects' it into a 
        // list, so that that list now called "filteredInts" can be used 
        // in an ensuing for loop
        List<Integer> filteredInts
                = someIntsIHave.stream()
                .filter(whatToSum)
                .collect(Collectors.toList());

        // this code operates on the the list above
        int sum = 0;
        for (int x : filteredInts) {
            sum += x;
        }
        return sum;
    }

}
