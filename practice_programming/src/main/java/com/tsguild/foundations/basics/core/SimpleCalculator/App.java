/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core.SimpleCalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String operationType;
        double numA;
        double numB;
        double result;
        String replay;
        boolean playAgain = true;

        System.out.println("Welcome to 'Simple Calculator.'");

        do {

            System.out.println("Please indicate the type of operation you'd like "
                    + "to perform:");
            System.out.println("(a)ddition, (s)ubtraction, (m)ultiplication, "
                    + "or (d)ivision.  Press 'x' to exit.");
            operationType = sc.nextLine();

            if (operationType.equals("x")) {
                System.out.println("Ok.  Thank you.  Goodbye.");
                return;
            }

            System.out.println("What is your first number?");
            numA = sc.nextDouble();
            sc.nextLine();

            System.out.println("What is your second number?");
            numB = sc.nextDouble();
            sc.nextLine();

            SimpleCalculator bob = new SimpleCalculator();

            if (operationType.equals("a")) {
                result = bob.addition(numA, numB);
                System.out.println("Your result is: " + result);
            } else if (operationType.equals("s")) {
                result = bob.subtraction(numA, numB);
                System.out.println("Your result is: " + result);

            } else if (operationType.equals("m")) {
                result = bob.multiplication(numA, numB);
                System.out.println("Your result is: " + result);

            } else if (operationType.equals("d")) {
                result = bob.division(numA, numB);
                System.out.println("Your result is: " + result);
            } else {
                playAgain = false;
            }

            System.out.println("Would you like to perform another operation? (y/n)");
            replay = sc.nextLine();

            if (replay.equals("y")) {
                playAgain = true;
            } else {
                playAgain = false;
                System.out.println("Ok.  Thank you.  Goodbye.");
                return;
            }

        } while (playAgain);

    }
}
