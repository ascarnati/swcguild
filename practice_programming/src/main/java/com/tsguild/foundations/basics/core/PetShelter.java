/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class PetShelter {

    public static void main(String[] args) {
        
        ArrayList<Dog> dogs = new ArrayList<>();
        
//      The following uses the 'ALL ARGS' constructor to add an object to the list
        Dog fido = new Dog();
        fido.setName("Fido");
        fido.setColor("Nondescript brown");
        fido.setSize("Mediumish");
        fido.setWeight(25);
        fido.setId(1);
        
        dogs.add(fido);
        
        System.out.println("I have " + dogs.size() + " many dogs");
        
//      The following uses the DEFAULT ('no args') CONSTRUCTOR to add an object
//      to the list
        Dog fifi = new Dog("Fifi", 4, "Teensy", "White", 2);
        dogs.add(fifi);
        
        System.out.println("I have " + dogs.size() + " many dogs");
        
//      Another way is to use the following which DOESN'T store the object dog
//      in temp memory before adding it to the List
        dogs.add(new Dog("Yosemite", 45, "Mediumish", "White Spotted", 3));
        
        System.out.println("I have " + dogs.size() + " many dogs.");
        
//      YET another way to add to the list is to use the following convention:
        GreatDane zeus = new GreatDane("Damn Large", 4);
        zeus.setWeight(110);
        zeus.setColor("Black and White");
        zeus.setName("Zeus");
        
        dogs.add(zeus);
        
        System.out.println("I have " + dogs.size() + " many dogs.");
        
        Chihuahua killer = new Chihuahua("Ankle biter", 5);
        killer.setWeight(7);
        killer.setColor("tan");
        killer.setName("Killer");
        
        dogs.add(killer);
        
        System.out.println("I have " + dogs.size() + " many dogs.");
        
        Mutt shithead = new Mutt("Medium", 6);
        shithead.setWeight(25);
        shithead.setColor("brown");
        shithead.setName("Shithead");
                
        dogs.add(shithead);
        
        System.out.println("I have " + dogs.size() + " many dogs.");
        
//      iterate through the list and make them all bark
        for( Dog tempDog : dogs)    {
            tempDog.bark();
        }
        
//      iterate through the list and provide the info for each
        for(Dog tempDog : dogs) {
        // DON'T DO THIS: THIS IS PRINTING THE DOG
        // System.out.println(tempDog);
        // instead... do this...
        System.out.println(tempDog.getName());
        }
        
//      find the dog in the list that's a Mutt and cast
//      it as a variable "theMutt"; since I know that the 6th dog is my Mutt
        System.out.println("");
        Mutt theMutt = (Mutt)dogs.get(5);
        
    }
    
}
