/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class arrayMorningPractice {

    public static void main(String[] args) {

        namesOfPets();
        System.out.println("");
        lastNames();
    }

    public static void namesOfPets() {
        String[] petNames = {"Queenie", "Denver", "Fritz", "Blue", "Pegasus",
            "Perseus", "Jimmy TwoPipes", "FluffyPet", "FancyPants", "Athensa",
            "Knoppler", "Marie", "FrooFroo", "Magpie", "Bonnie", "Clyde",
            "Benji", "Mensa", "Pumpernickel", "Zazoo", "Duchesse", "Moof",
            "Prue", "Mitch", "Bagel", "Darkwing Duck", "Bobbafett", "Dr. What",
            "Mochi", "Floyd", "Prism", "Winnie the Poo", "Piglet",
            "Frankenfurter"};

        System.out.println("There are " + petNames.length + " elements.");

        for (int i = 0; i < petNames.length; i++) {
            System.out.println("Index #: " + i + " " + petNames[i]);
        }

    }

    public static void lastNames() {
        String[] lastNames = {"Smith", "Johnson", "Williams", "Jones", "Brown",
            "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson", "Thomas",
            "Jackson", "White", "Harris", "Martin", "Thompson", "Garcia",
            "Martinez", "Robinson", "Clark", "Rodriguez", "Lewis", "Lee",
            "Walker", "Hall", "Allen", "Young", "Hernadez", "King",
            "Wright", "Lopez"};

        System.out.println("There are " + lastNames.length + " elements.");

        for (String name : lastNames) {
            System.out.println("Index #: " + name);

        }

    }
    
}
