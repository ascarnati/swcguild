/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class AllAboutMe {
    public static void main(String[] args)  {
        //declaring variables here
        String myName, food;
        byte pets;
        boolean ateGnocchi;
        int whistleAge;
        
        //setting their values here
        myName = "Anthony Scarnati";
        food = "pizza";
        pets = 1;
        ateGnocchi = true;
        whistleAge = 8;
        
        //and displaying them as output here
        System.out.println("My name is " + myName + ".");
        System.out.println("I love to eat " + food + ".");
        System.out.println("I have " + pets + " pet.");
        System.out.println("It's " + ateGnocchi + "; I have eaten gnocchi.");
        System.out.println("I learned to whistle when I was just " + whistleAge +
                " years old.");
        
    }
}
