/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.NumberFormat;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculatorBigDec {

    public static void main(String[] args) {

// ********** PREP VARIABLES ************         
        int reportYear,
                numYears,
                currentYear = 2017;
        BigDecimal principalAmount,
                interestRate,
                newPrincipal,
                interestEarned,
                minimumAmount = new BigDecimal ("0.01");
        String stringPrincipalAmount,
                stringInterestRate,
                stringNewPrincipal,
                stringInterestEarned,
                stringNumYears,
                stringReportYear;

        Scanner sc = new Scanner(System.in);
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        
// ******* PROMPT USER FOR INPUT *********
        do {
            System.out.println("Please enter an amount > $0.01"
                    + " to be invested: ");
            stringPrincipalAmount = sc.next();
            principalAmount = new BigDecimal(stringPrincipalAmount);
        } while (principalAmount.compareTo(minimumAmount) < 0);

        do {
            System.out.println("Please enter a future year to start investing: ");
            stringReportYear = sc.next();
            reportYear = Integer.parseInt(stringReportYear);
        } while (reportYear < currentYear);

        do {
            System.out.println("Please enter the number of years to invest: ");
            stringNumYears = sc.next();
            numYears = Integer.parseInt(stringNumYears);
        } while (numYears < 1);

        int i = 0;
        newPrincipal = principalAmount;

        do {
            // this is where we will call our method to  do the calculation

            BigDecimal newNetValue = calcInterest(newPrincipal);
            // ****  Need to prep/calc for our annual reporting
            interestEarned = newNetValue.subtract(principalAmount);
            System.out.println(" ");
            System.out.println("***************** " + reportYear
                    + " *****************");
            System.out.println("Financial Report for " + reportYear);
            System.out.println("Initial Principal Value for "
                    + reportYear + " = " + formatter.format(newPrincipal));
            System.out.println("Interest earned for  "
                    + reportYear + " = " + formatter.format(interestEarned));
            System.out.println("New Principal Value for "
                    + reportYear + " = " + formatter.format(newNetValue));
            System.out.println(" ");

            
            //  ****  Need to increment 1 for next year processing     
            i++;
            newPrincipal = newNetValue;
            reportYear = reportYear + 1;
        } while (numYears > i);
    }

    public static BigDecimal calcInterest(BigDecimal principal) {
        BigDecimal newPrincipalCalc;
        newPrincipalCalc = principal;
        int i = 1;

        do {
            BigDecimal quarterRate = new BigDecimal(1.025);
            newPrincipalCalc = newPrincipalCalc.multiply(quarterRate);
            
            i++;
        } while (i < 5);

        principal = newPrincipalCalc;

        return principal;
    }
}