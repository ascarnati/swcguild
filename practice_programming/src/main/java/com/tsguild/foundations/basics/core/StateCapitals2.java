/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.HashMap;
import java.util.Set;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class StateCapitals2 {

    public static void main(String[] args) {

        Capitals alaska = new Capitals("Alaska", "Juneau", "100000", 50);
        Capitals alabama = new Capitals("Alabama", "Montgomery", "200000", 30);
        Capitals arizona = new Capitals("Arizona", "Phoenix", "300000", 70);
        Capitals arkansas = new Capitals("Arkansas", "Little Rock", "400000", 40);

        HashMap<String, Capitals> CapitalsList = new HashMap<>();

        CapitalsList.put("Alaska", alaska);
        CapitalsList.put("Alabama", alabama);
        CapitalsList.put("Arizona", arizona);
        CapitalsList.put("Arkansas", arkansas);

        System.out.println("Map size is: " + CapitalsList.size());
        System.out.println("");

        Set<String> keys = CapitalsList.keySet();

        System.out.println("The capital of " + alaska.getState() + " is " + alaska.getName());
        System.out.println("");

        for (String mytempvariable : keys) {
            System.out.println(mytempvariable);
        }

        System.out.println("");

        for (String mytempvariable : keys) {
            String city = CapitalsList.get(mytempvariable).getName();
            String pop = CapitalsList.get(mytempvariable).getPopulation();
            Double sqMile = CapitalsList.get(mytempvariable).getSquareMileage();

            System.out.println("The capital of " + mytempvariable + " is " + city
                    + ".  Its population is " + pop + ", and its square mileage is "
                    + sqMile);
        }

    }
}
