/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class MyOwnRandomizer {

    public static void main(String[] args) {

        Random randomizer = new Random();
        
        int x = randomizer.nextInt(5);
        
        if (x == 0) {
            System.out.println("Dog");
        } else if (x == 1) {
            System.out.println("Cat");
        } else if (x == 2) {
            System.out.println("Fish");
        } else if (x == 3) {
            System.out.println("Turkey");
        } else if (x == 4) {
            System.out.println("Cow");
        } else if (x == 5) {
            System.out.println("Moose");
        }
//
    }
}
