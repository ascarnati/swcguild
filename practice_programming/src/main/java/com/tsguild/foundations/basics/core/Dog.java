/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class Dog {
    
    private String name;
    private double weight;
    private String size;
    protected String color;
    private int id;

//  this next line is the constructor we're creating rather than use
//  Default Constructor.  We refer to this as the "ALL ARGS CONSTUCTOR"
    public Dog(String name, double weight, String size, String color, int id) {

//  next we need to set the properties of this object (Dog) with the incoming
//  info from the constructor; but we want to set the values for JUST THIS instance
//  of the object Dog... so we use the keyword THIS and the dot.operator
        this.name = name;
        this.weight = weight;
        this.size = size;
        this.color = color;
        this.id = id;
    }
    
//  the user requested that they only need ID and size to create a new Dog object
//  because they don't often have the other info.  So, we're giving them another 
//  overloaded Constructor
    public Dog(int id, String size) {
        this.id = id;
        this.size = size;

//  We can still set values for properties not taken in by this Constructor
//  as follows
        this.name = "UnAssigned";
        this.color = "UnAssigned";
        this.weight = -1;
        
    }

//  We can't create another, slightly different constructor....
//  public Dog(int id, String name) {   }
//  because METHOD OVERLOADING would be broken because
//  Java doesn't see a difference between String size (above) and
//  String name (here) - they have the same METHOD SIGNATURE.

//  So by def by providing the constructor I have written above,  
//  the default constructor (or no args constructor) goes away.
//  But, a lot of times, people WANT those. So we'll create that TOO....
    public Dog() {
//  But remember, this comes with a risk.
//  Of Null Pointer Exceptions
    }

//  HERE ARE MY BEHAVIOR METHODS
    public void bark() {
        System.out.println("Bark!");
    }
    
    public boolean adopt(String owner) {
        return true;
    }
    

//  GETTERS AND SETTERS START HERE    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
