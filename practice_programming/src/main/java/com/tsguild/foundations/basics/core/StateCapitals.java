/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StateCapitals {

    public static void main(String[] args) {

        HashMap<String, String> StateCaps = new HashMap<>();

        StateCaps.put("Alabama", "Montgomery");
        StateCaps.put("Alaska", "Juneau");
        StateCaps.put("Arizona", "Phoenix");
        StateCaps.put("Arkansas", "Little Rock");

        Set<String> States = StateCaps.keySet();

        // establishes a temp variable and uses that to iterate and return
        // each key in the HashMap
        for (String tempvar : States) {
            System.out.println(tempvar);
        }

        System.out.println("");
        
        // establishes a temp variable and uses that to iterate and return 
        // each value in the Collection
        
        Collection<String> Capitals = StateCaps.values();
        
        for (String tempvar : Capitals) {
            System.out.println(tempvar);
        }
        
        for (String tempvar : States) {
            System.out.println("The capital of " + tempvar + " is " + StateCaps.get(tempvar));
        }
        
                
    }

}
