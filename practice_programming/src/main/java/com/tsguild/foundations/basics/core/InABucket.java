/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class InABucket {
    public static void main(String[] args)  {
        // You can declare all KINDS of variables.
        String walrus;
        double piesEaten;
        float weightOfTeaCupPig;
        int grainsOfSand;
        
        // But declaring them just sets up the space for data
        // to use the variable, you have to put data IN it first!
        walrus = "Sir Leroy Jenkins III";
        piesEaten = 42.1;
        weightOfTeaCupPig = 72.5f;
        grainsOfSand = 1000000;
        
        System.out.println("Meet my pet Walrus, " + walrus);
        System.out.print("He was a bit hungry today, and ate this many pies : ");
        System.out.println(piesEaten);
        System.out.println("That's " + weightOfTeaCupPig);
        System.out.println("And now he displaces ");
        System.out.print(grainsOfSand);
    }
}
