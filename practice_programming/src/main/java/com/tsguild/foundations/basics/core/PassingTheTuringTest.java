/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PassingTheTuringTest {

    public static void main(String[] args)  {
        
        String userName;
        String computerName = "Hal3000";
        String faveColor;
        String faveFood;
        
        Scanner inputReader = new Scanner(System.in);
        
        System.out.println("Hello!");
        System.out.println("What is your name?");
        userName = inputReader.nextLine();
        
        System.out.println("It's nice to meet you, " + userName + ".");
        
        System.out.println("My name is " + computerName + ".");
        
        System.out.println("What's your favorite color, " + userName + "?");
        faveColor = inputReader.nextLine();
        
        System.out.println("Fascinating!  My fave color is " + faveColor + 
                " as well.");
        
        
    }
}
