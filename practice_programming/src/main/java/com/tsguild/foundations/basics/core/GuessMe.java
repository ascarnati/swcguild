/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class GuessMe {

    public static void main(String[] args) {

        int mysteryNum = 19;
        int userGuess;

        System.out.println("I've chosen a number.  Betcha can't guess it!");

        Scanner inputReader = new Scanner(System.in);

        System.out.println("Enter your guess:");
        userGuess = inputReader.nextInt();

        if (userGuess == mysteryNum) {
            System.out.println("Wow! Nice guess! That was it!");
        }

        if (userGuess < mysteryNum) {
            System.out.println("Ha, nice try - too low! I chose " + mysteryNum);
        }

        if (userGuess > mysteryNum) {
            System.out.println("Too bad, way too high.  I chose " + mysteryNum);
        }

    }

}
