/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Factorizer {
                
    public static void main(String[] args)    {
          
        //declare variables
        int num;
        boolean isPrime = true;
        int perfectSum = 1;
        int factCount = 2;
        
        // begin get user input here
        Scanner inputReader = new Scanner(System.in);
                
        System.out.print("What number would you like to factor? ");
        num = inputReader.nextInt();
        //next step is needed to bounce output to a new line
        inputReader.nextLine();

        // end user input
        
        System.out.println("The factors of " + num + " are:");
        System.out.println("1");
        
        // begin loop here to determine "factors" of num
        // "num % i == 0" determines if the num divided by the incremental
        // value has a remainder - if so, i is not a factor of num
        // we'll also display the factors at the same time within the loop
        for (int i=2; i < num; i++) {
            if (num % i == 0)    {
                System.out.println(i);
                perfectSum = perfectSum + i;
                isPrime = false;
                factCount = factCount + 1;        
                }
            }
        // end loop
        
        System.out.println("The total number of factors is: " + factCount);        
        
        // is it a Perfect number?
        if (perfectSum == num)   {
            System.out.println(+ num + " is a perfect number.");
            }
        
        else    {    
            System.out.println(+ num + " is not a perfect number.");
            }
        
        // is it a Prime number?
        if (isPrime == true)    {
            System.out.println(+ num + " is a Prime number.");
            }
        
        else    {            
            System.out.println(+ num + " is not a Prime number.");
            }
    
    }
}
