/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class Chihuahua extends Dog {
    
    @Override
//  we added @Override here to signify that we knowingly want to override the 
//  bark method from class Dog;  @Override is simpler and better form than
//  adding a note everytime
    
    public void bark() {
        System.out.println("yip!yip!");
    }
    
    public Chihuahua(String size, int id)   {
        super(id, size);
    }
    
    public Chihuahua(String color, String size) {
        super(); //If I don't write this, it is there automagically
        this.color = color;  //this.color won't work if String color is PRIVATE
                             //in class Dog.  Changing String color in Dog to 
                             //PROTECTED will allow us to set color here
        
        }
}

    
