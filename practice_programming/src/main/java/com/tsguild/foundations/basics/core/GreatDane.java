/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsguild.foundations.basics.core;

/**
 *
 * @author apprentice
 */
public class GreatDane extends Dog implements Rideable  {
    
    @Override
//  we added @Override here to signify that we knowingly want to override the 
//  bark method from class Dog;  @Override is simpler and better form than
//  adding a note everytime
    
    public void bark() {
        System.out.println("WOOF!");
    }
    
    public GreatDane(String size, int id)   {
        super(id, size);
    }
    
    public GreatDane(String color, String size) {
        super(); //If I don't write this, it is there automagically
        this.color = color;  //this.color won't work if String color on class Dog
                             //is PRIVATE.  Changing String color in Dog to 
                             //PROTECTED will allow us to set color here
        
        }

    @Override
    public void ride() {
        this.bark();
        this.bark();
        System.out.println("I have been ridden.  Please give me a biscuit.");
            
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
