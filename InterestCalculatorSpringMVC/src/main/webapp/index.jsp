<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Interest Calculator Spring MVC Web App</h1>
            <hr/>

            <h2>Input Form Page</h2>

            <form method="POST" action="calculateInterest">
                <div class="form-group">
                    <label for="interestRate">
                        Annual Interest Rate:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="interestRate"
                               placeholder="%" />
                    </div>   
                </div>

                <div class="form-group">
                    <label for="principleAmount">
                        Initial Principle:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="principleAmount"
                               placeholder="$$$" />
                    </div>

                </div>
                <div class="form-group">
                    <label for="numYears">
                        Years to Invest:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="numYears"
                               placeholder="years" />
                    </div>                    
                </div>

                <input type="submit" value="Calculate">
            </form>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

