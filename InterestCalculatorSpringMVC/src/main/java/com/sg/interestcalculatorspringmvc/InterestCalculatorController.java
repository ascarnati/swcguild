/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculatorspringmvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static javafx.scene.input.KeyCode.R;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class InterestCalculatorController {
    
    @RequestMapping(value = "/calculateInterest", 
            method = RequestMethod.POST)
    public String calculateInterest(HttpServletRequest request, 
            Map<String, Object> model) {
        
        // *** DECLARE VARIABLES ***
        List<YearlyReport> yearlyResultsList = new ArrayList<>();
        int reportYear = 2017,
                numYears,
                currentYear = 2017;
        BigDecimal principleAmount,
                interestRate,
                newPrinciple,
                interestEarned,
                minimumAmount = new BigDecimal("0.01");

        // *** GET VALUES SUBMITTED IN THE INDEX FORM ***
        // remember to convert the values from strings
        String inputInterestRate = request.getParameter("interestRate");
        interestRate = new BigDecimal(inputInterestRate).setScale(2);

        String inputPrincipleAmount = request.getParameter("principleAmount");
        principleAmount = new BigDecimal(inputPrincipleAmount).setScale(2);

        String inputNumYears = request.getParameter("numYears");
        numYears = Integer.parseInt(inputNumYears);

        // *** PROCESS BUSINESS LOGIC ***
        newPrinciple = principleAmount;


        // need to iterate over the number of years provided by the user
        // and create an object for each year, then, 
        // put each group of calculations into a list with key=year
        for (int i = 0; i <= numYears; i++) {
            YearlyReport year = new YearlyReport(reportYear);

            do {
                BigDecimal newNetValue = calcInterest(newPrinciple, interestRate, numYears);
                interestEarned = newNetValue.subtract(principleAmount);

                //i++;
                newPrinciple = newNetValue;
                        
            } while (numYears < i);
                    
            year.setReportYear(reportYear);
            year.setNewPrinciple(newPrinciple);
            year.setInterestEarned(interestEarned);
            yearlyResultsList.add(year);
            reportYear = reportYear + 1;
        }
        
        // *** PUT the info into the OBJECT map provided by the DispatcherServlet
        // so they'll be available to the result.jsp
        // instead of setting attributes on the request (the Dispatcher will
        // do this for us) ***

        model.put("numYears", numYears);
        model.put("yearlyResultsList", yearlyResultsList);
        
        return "result";
    }
    
        protected BigDecimal calcInterest(BigDecimal principle, BigDecimal interestRate, int numYears) {
        BigDecimal newPrincipleCalc;
        newPrincipleCalc = principle;
        int i = 1;

        do {
            // given the annual interest rate, calc the quarterly rate
            BigDecimal quarter = new BigDecimal(4.00);
            BigDecimal quarterlyIntRate = interestRate.divide(quarter);
            newPrincipleCalc = newPrincipleCalc.multiply(quarterlyIntRate);

            i++;
        } while (i < numYears);

        principle = newPrincipleCalc;

        return principle;
    }
    
}
