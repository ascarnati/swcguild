/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.bigdecimalandenum;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author apprentice
 */
public class BigDecimalMath {
    
    // Rather than have a diff method for each operation, we'll create one 
    // method called calculate tha twill take in 3 params (operator of type
    // "mathOperator" and two operands both of type BigDecimal)
    // need to import java.math.BigDecimal
    
    public BigDecimal calculate(MathOperator operator, BigDecimal operand1,
            BigDecimal operand2) {
        switch (operator) {
                case PLUS:
                    return operand1.add(operand2);
                case MINUS:
                    return operand1.subtract(operand2);
                case MULTIPLY:
                    return operand1.multiply(operand2);
                case DIVIDE:
                    return operand1.divide(operand2, 2, RoundingMode.HALF_UP);
                default: 
                    throw new UnsupportedOperationException("Unknown Math Operator"); 
                // the default condition is unlikely to happen because we 
                // can't have an unknown operator but to satisfy the switch 
                // statement we need to either throw an exception, or we 
                // could put a return null after the switch statement
            }        
    }
}
