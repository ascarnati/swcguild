/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd.controller;

import com.sg.DVD.ui.UserIO;
import com.sg.DVD.ui.UserIOConsoleImpl;

/**
 *
 * @author apprentice
 */
public class DVDController {

    private UserIO io = new UserIOConsoleImpl();

    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;
        while (keepGoing) {
            io.print("Main Menu");
            io.print("1. List DVD's");
            io.print("2. View a DVD");
            io.print("3. Remove a DVD");
            io.print("4. Search DVD by Title");
            io.print("5. Edit a DVD");
            io.print("6. Exit");

            menuSelection = io.readInt("Please select from the above choices", 1, 7);
            switch(menuSelection){
                case 1:
                    io.print("List DVDs");
                    break;
                case 2:
                    io.print("Create DVD");
                    break;
                case 3:
                        io.print("View DVD");
                        break;
                case 4: 
                    io.print("Remove DVD");
                    break;
                case 5:
                    io.print("Search DVD by title");
                    break;
                case 6:
                    io.print("Edit a DVD");
                case 7: 
                    keepGoing=false;
                    break;
                default:
                    io.print("UNKOWN COMMAND");
                
                    
            }

        }
        io.print("GOOD BYE");
    }

}
