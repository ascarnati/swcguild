/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.service;

import com.sg.flops.dao.FlopsOrderDao;
import com.sg.flops.dao.FlopsProductDao;
import com.sg.flops.dao.FlopsTaxesDao;
import com.sg.flops.dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author aScarnati
 */
public class FlopsServiceLayerImplTest {
    
    private FlopsServiceLayer service;
    
    public FlopsServiceLayerImplTest() {
        
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContextTest.xml");
        service = ctx.getBean("serviceLayer", FlopsServiceLayer.class);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAllOrders method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetAllOrders() throws Exception {
    }

    /**
     * Test of getAllOrdersForDate method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testAddGetAllOrdersForDate() throws Exception {
        String tempDate1 = "07/04/2016";
        LocalDate orderDate = LocalDate.parse(tempDate1, DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        String tempDate2 = "08/04/2016";
        LocalDate badOrderDate = LocalDate.parse(tempDate2, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        
        // positive test should return the two orders we created
        assertEquals(2, service.getAllOrdersForDate(orderDate).size());
        
        // negative test should throw Exception for bad date
        try {
            service.getAllOrdersForDate(badOrderDate);
            fail("FlopsOrderNotFoundException expected but not thrown.");
        } catch (FlopsOrderNotFoundException e) {
            return;
        }
        
    }

    /**
     * Test of getAllProducts method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetAllProducts() throws Exception {
    }

    /**
     * Test of addOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testAddOrder() throws Exception {
    }

    /**
     * Test of getOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetOrder() throws Exception {
        
    }

    /**
     * Test of generateQuote method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGenerateQuote() throws Exception {
    }

    /**
     * Test of generateOrderNumber method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGenerateOrderNumber() {
    }

    /**
     * Test of editOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testEditOrder() {
    }

    /**
     * Test of removeOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testRemoveOrder() {
    }

    /**
     * Test of openOrderDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testOpenOrderDao() throws Exception {
    }

    /**
     * Test of closeOrderDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testCloseOrderDao() throws Exception {
    }

    /**
     * Test of openProductDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testOpenProductDao() throws Exception {
    }

    /**
     * Test of openTaxesDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testOpenTaxesDao() throws Exception {
    }

    /**
     * Test of closeProductDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testCloseProductDao() throws Exception {
    }

    /**
     * Test of closeTaxesDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testCloseTaxesDao() throws Exception {
    }
    
}
