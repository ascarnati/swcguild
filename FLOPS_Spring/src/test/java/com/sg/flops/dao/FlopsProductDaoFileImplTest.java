/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Product;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aScarnati 
 */
public class FlopsProductDaoFileImplTest {
    
    private FlopsProductDao dao = new FlopsProductDaoFileImpl();
    
    public FlopsProductDaoFileImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws FlopsPersistenceException {
        List<Product> productsList = dao.getAllProducts();
        for (Product product : productsList) {
            dao.getProduct(product.getProductType());
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addProduct method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testAddGetProduct() throws Exception {
        // add 1 tax record
        Product product1 = new Product("WOOD");
        product1.setLaborCpSf(new BigDecimal("11.00"));
        product1.setMaterialCpSf(new BigDecimal("20.50"));
        
        dao.addProduct("WOOD", product1);
        
        // confirm that the record was created and we can get it back
        assertEquals("WOOD", dao.getProduct("WOOD").getProductType());
        assertEquals("11.00", dao.getProduct("WOOD").getLaborCpSf().toString());
        assertEquals("20.50", dao.getProduct("WOOD").getMaterialCpSf().toString());
        
    }

    /**
     * Test of editProduct method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testEditProduct() throws Exception {
        // add 1 tax record
        Product product1 = new Product("WOOD");
        product1.setLaborCpSf(new BigDecimal("11.00"));
        product1.setMaterialCpSf(new BigDecimal("20.50"));
        
        dao.addProduct("WOOD", product1);
        
        // then change the laborCpSf property for that record
        product1.setLaborCpSf(new BigDecimal("15.00").setScale(2));
        
        // now confirm that the laborCpSf has changed from 11.00 to 15.00
        assertEquals("15.00", dao.getProduct("WOOD").getLaborCpSf().toString());
        
    }

    /**
     * Test of getAllProducts method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testGetAllProducts() throws Exception {
        // add 2 product records
        Product product1 = new Product("WOOD");
        product1.setLaborCpSf(new BigDecimal("11.00"));
        product1.setMaterialCpSf(new BigDecimal("20.50"));
        
        dao.addProduct("WOOD", product1);

        Product product2 = new Product("RUBBER");
        product1.setLaborCpSf(new BigDecimal("5.00"));
        product1.setMaterialCpSf(new BigDecimal("10.50"));
        
        dao.addProduct("RUBBER", product2);
        
        // confirm that there are 2 objects in the list
        assertEquals(2, dao.getAllProducts().size());
        
    }

    /**
     * Test of removeProduct method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testRemoveProduct() throws Exception {
        // add 2 product records
        Product product1 = new Product("WOOD");
        product1.setLaborCpSf(new BigDecimal("11.00"));
        product1.setMaterialCpSf(new BigDecimal("20.50"));
        
        dao.addProduct("WOOD", product1);

        Product product2 = new Product("RUBBER");
        product1.setLaborCpSf(new BigDecimal("5.00"));
        product1.setMaterialCpSf(new BigDecimal("10.50"));
        
        dao.addProduct("RUBBER", product2);
        
        // then, remove record 1
        dao.removeProduct(product1.getProductType());
        
        // make sure there's just 1 item left in the list
        assertEquals(1, dao.getAllProducts().size());
        
        // make sure that we get a null value if we try to get record 1 now
        assertNull(dao.getProduct(product1.getProductType()));
        
    }

    /**
     * Test of openProductDao method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testOpenProductDao() throws Exception {
    }

    /**
     * Test of closeProductDao method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testCloseProductDao() throws Exception {
    }

    /**
     * Test of writeProductInventory method, of class FlopsProductDaoFileImpl.
     */
    @Test
    public void testWriteProductInventory() throws Exception {
    }
    
}
