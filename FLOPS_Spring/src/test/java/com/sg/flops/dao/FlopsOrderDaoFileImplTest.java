/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Order;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aScarnati
 */
public class FlopsOrderDaoFileImplTest {
    
    private FlopsOrderDao dao = new FlopsOrderDaoFileImpl();
    
    public FlopsOrderDaoFileImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws FlopsPersistenceException {
        List<Order> orderList = dao.getAllOrders();
        for (Order order : orderList) {
            dao.getOrder(order.getOrderNumber());
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addOrder and getOrder methods, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testAddGetOrder() throws Exception {
        // add 1 order record
        Order order1 = new Order(00001);
        order1.setCustomerName("Wise");
        order1.setCustomerState("OH");
        order1.setTaxRate(new BigDecimal("6.25"));
        order1.setProductType("Wood");
        order1.setArea(new BigDecimal("100"));
        order1.setLaborCost(new BigDecimal("475"));
        order1.setMaterialCost(new BigDecimal("515"));
        order1.setLaborCpSf(new BigDecimal("4.75"));
        order1.setMaterialCpSf(new BigDecimal("5.15"));
        order1.setTax(new BigDecimal("61.88"));
        order1.setTotal(new BigDecimal("1051.88"));
        
        dao.addOrder(order1);
        
        // confirm that the record was created and we can get it back
        assertEquals("Wise", dao.getOrder(00001).getCustomerName());
        
    }

    /**
     * Test of editOrder method, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testEditOrder() throws Exception {
        // add 1 order record
        Order order1 = new Order(00001);
        order1.setCustomerName("Wise");
        order1.setCustomerState("OH");
        order1.setTaxRate(new BigDecimal("6.25"));
        order1.setProductType("Wood");
        order1.setArea(new BigDecimal("100"));
        order1.setLaborCost(new BigDecimal("475"));
        order1.setMaterialCost(new BigDecimal("515"));
        order1.setLaborCpSf(new BigDecimal("4.75"));
        order1.setMaterialCpSf(new BigDecimal("5.15"));
        order1.setTax(new BigDecimal("61.88"));
        order1.setTotal(new BigDecimal("1051.88"));
        
        dao.addOrder(order1);
        
        // then change the productType for that record
        order1.setProductType("Laminate");
        
        // now confirm that the productType has changed from Wood to Laminate
        assertEquals("Laminate", dao.getOrder(00001).getProductType());
        
    }

    /**
     * Test of getAllOrders method, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testGetAllOrders() throws Exception {
        // add 2 order records
        Order order1 = new Order(00001);
        order1.setCustomerName("Wise");
        order1.setCustomerState("OH");
        order1.setTaxRate(new BigDecimal("6.25"));
        order1.setProductType("Wood");
        order1.setArea(new BigDecimal("100"));
        order1.setLaborCost(new BigDecimal("475"));
        order1.setMaterialCost(new BigDecimal("515"));
        order1.setLaborCpSf(new BigDecimal("4.75"));
        order1.setMaterialCpSf(new BigDecimal("5.15"));
        order1.setTax(new BigDecimal("61.88"));
        order1.setTotal(new BigDecimal("1051.88"));
        
        dao.addOrder(order1);

        Order order2 = new Order(00002);
        order2.setCustomerName("Hope");
        order2.setCustomerState("PA");
        order2.setTaxRate(new BigDecimal("7.00"));
        order2.setProductType("Laminate");
        order2.setArea(new BigDecimal("100"));
        order2.setLaborCost(new BigDecimal("475"));
        order2.setMaterialCost(new BigDecimal("515"));
        order2.setLaborCpSf(new BigDecimal("4.75"));
        order2.setMaterialCpSf(new BigDecimal("5.15"));
        order2.setTax(new BigDecimal("61.88"));
        order2.setTotal(new BigDecimal("1051.88"));
        
        dao.addOrder(order2);
        
        // confirm that there are 2 objects in the list
        assertEquals(2, dao.getAllOrders().size());        
        
    }

    /**
     * Test of removeOrder method, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testRemoveOrder() throws Exception {
        // add 2 order records
        Order order1 = new Order(00001);
        order1.setCustomerName("Wise");
        order1.setCustomerState("OH");
        order1.setTaxRate(new BigDecimal("6.25"));
        order1.setProductType("Wood");
        order1.setArea(new BigDecimal("100"));
        order1.setLaborCost(new BigDecimal("475"));
        order1.setMaterialCost(new BigDecimal("515"));
        order1.setLaborCpSf(new BigDecimal("4.75"));
        order1.setMaterialCpSf(new BigDecimal("5.15"));
        order1.setTax(new BigDecimal("61.88"));
        order1.setTotal(new BigDecimal("1051.88"));
        
        dao.addOrder(order1);

        Order order2 = new Order(00002);
        order2.setCustomerName("Hope");
        order2.setCustomerState("PA");
        order2.setTaxRate(new BigDecimal("7.00"));
        order2.setProductType("Laminate");
        order2.setArea(new BigDecimal("100"));
        order2.setLaborCost(new BigDecimal("475"));
        order2.setMaterialCost(new BigDecimal("515"));
        order2.setLaborCpSf(new BigDecimal("4.75"));
        order2.setMaterialCpSf(new BigDecimal("5.15"));
        order2.setTax(new BigDecimal("61.88"));
        order2.setTotal(new BigDecimal("1051.88"));
        
        dao.addOrder(order2);
        
        // then remove record 1
        dao.removeOrder(order1.getOrderNumber());
        
        // make sure there's just 1 item left in the list
        assertEquals(1, dao.getAllOrders().size());
        
        // make sure that we get a null value if we try to get record 1 now
        assertNull(dao.getOrder(order1.getOrderNumber()));
        
        
    }

    /**
     * Test of openOrderDao method, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testOpenOrderDao() throws Exception {
    }

    /**
     * Test of closeOrderDao method, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testCloseOrderDao() throws Exception {
    }

    /**
     * Test of writeOrderInventory method, of class FlopsOrderDaoFileImpl.
     */
    @Test
    public void testWriteOrderInventory() throws Exception {
    }
    
}
