/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.app;

import com.sg.flops.controller.FlopsController;
import com.sg.flops.dao.FlopsOrderDao;
import com.sg.flops.dao.FlopsOrderDaoFileImpl;
import com.sg.flops.dao.FlopsProductDao;
import com.sg.flops.dao.FlopsProductDaoFileImpl;
import com.sg.flops.dao.FlopsTaxesDao;
import com.sg.flops.dao.FlopsTaxesDaoFileImpl;
import com.sg.flops.service.FlopsServiceLayer;
import com.sg.flops.service.FlopsServiceLayerImpl;
import com.sg.flops.ui.FlopsView;
import com.sg.flops.ui.UserIO;
import com.sg.flops.ui.UserIOConsoleImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author aScarnati
 */
public class App {

    public static void main(String[] args) {
        // Instantiate the UserIO implementation
//        UserIO myIo = new UserIOConsoleImpl();
//
//        // Instantiate the View and wire the UserIO implementation into it
//        FlopsView myView = new FlopsView(myIo);
//
//        // Instantiate the all the DAOs
//        FlopsOrderDao myOrderDao = new FlopsOrderDaoFileImpl();
//        FlopsProductDao myProductDao = new FlopsProductDaoFileImpl();
//        FlopsTaxesDao myTaxesDao = new FlopsTaxesDaoFileImpl();
//
//        // Instantiate the ServiceLayer
//        FlopsServiceLayer myService = new FlopsServiceLayerImpl(myOrderDao, myProductDao, myTaxesDao);
//        
//        // Instantiate the Controller and wire the Service Layer into it
//        FlopsController controller = new FlopsController(myService, myView);
//        
//        // Kick off the Controller
//        controller.run();

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FlopsController controller = ctx.getBean("controller", FlopsController.class);
        controller.run();
        
        //for test environment
        //ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContextTest.xml");

    }
}
