/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Order;
import java.util.List;

/**
 *
 * @author aScarnati 
 */
public interface FlopsOrderDao {
    
    public String getIsTrainingEnvironment() 
            throws FlopsPersistenceException;
    
    Integer getMaxOrderNumber();

    void setMaxOrderNumber(Integer maxOrderNumber);
    
    Order addOrder(Order pendingOrder); 
    
    Order editOrder(Integer orderNumber, Order editedOrder);
    
    Order getOrder(Integer orderNumber);
    
    List<Order> getAllOrders();

    Order removeOrder(Integer orderNumber);
    
    void openOrderDao()
            throws FlopsPersistenceException;

    void loadOrderInventory()
            throws FlopsPersistenceException;
    
    void closeOrderDao()
            throws FlopsPersistenceException;
        
    void writeOrderInventory()
            throws FlopsPersistenceException;
    
}
