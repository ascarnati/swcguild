/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Taxes;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public interface FlopsTaxesDao {
            
    Taxes addTaxes (String taxState, Taxes taxes);
   
    Taxes editTaxes(String taxState, Taxes taxes);

    Taxes getTaxes (String taxState);
    
    List<Taxes> getAllTaxes();

    Taxes removeTaxes (String taxState);
    
    void openTaxesDao() throws FlopsPersistenceException;
    
    void closeTaxesDao() throws FlopsPersistenceException;
            
}
