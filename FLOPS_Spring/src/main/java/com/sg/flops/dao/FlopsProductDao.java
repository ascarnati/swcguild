/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Product;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public interface FlopsProductDao {
    
    // should we have a productID as well?
    
    public Product addProduct(String productType, Product product);
    
    Product editProduct (String productType, Product product);

    Product getProduct (String productType);
    
    List<Product> getAllProducts();

    Product removeProduct (String productType);
    
    void openProductDao() 
            throws FlopsPersistenceException;
    
    void closeProductDao() 
            throws FlopsPersistenceException;

}
