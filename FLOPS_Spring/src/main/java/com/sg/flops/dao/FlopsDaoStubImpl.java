/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class FlopsDaoStubImpl implements FlopsOrderDao {

    Order order1;
    List<Order> ordersList = new ArrayList<>();
    
    public FlopsDaoStubImpl() {
        // add 2 order records
        
        String tempDate1 = "07/04/2016";
        LocalDate orderDate = LocalDate.parse(tempDate1, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
       
        Order order1 = new Order(00001);
        order1.setCustomerName("Wise");
        order1.setCustomerState("OH");
        order1.setTaxRate(new BigDecimal("6.25"));
        order1.setProductType("Wood");
        order1.setArea(new BigDecimal("100"));
        order1.setLaborCost(new BigDecimal("475"));
        order1.setMaterialCost(new BigDecimal("515"));
        order1.setLaborCpSf(new BigDecimal("4.75"));
        order1.setMaterialCpSf(new BigDecimal("5.15"));
        order1.setTax(new BigDecimal("61.88"));
        order1.setTotal(new BigDecimal("1051.88"));
        order1.setOrderDate(orderDate);
        
        ordersList.add(order1);
        
        Order order2 = new Order(00002);
        order2.setCustomerName("Hope");
        order2.setCustomerState("PA");
        order2.setTaxRate(new BigDecimal("7.00"));
        order2.setProductType("Laminate");
        order2.setArea(new BigDecimal("100"));
        order2.setLaborCost(new BigDecimal("475"));
        order2.setMaterialCost(new BigDecimal("515"));
        order2.setLaborCpSf(new BigDecimal("4.75"));
        order2.setMaterialCpSf(new BigDecimal("5.15"));
        order2.setTax(new BigDecimal("61.88"));
        order2.setTotal(new BigDecimal("1051.88"));
        order2.setOrderDate(orderDate);
        
        ordersList.add(order2);
        
    }
    
    
    @Override
    public String getIsTrainingEnvironment() throws FlopsPersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getMaxOrderNumber() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxOrderNumber(Integer maxOrderNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order addOrder(Order pendingOrder) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order editOrder(Integer orderNumber, Order editedOrder) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order getOrder(Integer orderNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Order> getAllOrders() {
        return ordersList;
    }

    @Override
    public Order removeOrder(Integer orderNumber) {
        if (orderNumber.equals((order1.getOrderNumber()))) {
            return order1;
        } else {
            return null;
        }
    }

    @Override
    public void openOrderDao() throws FlopsPersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadOrderInventory() throws FlopsPersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void closeOrderDao() throws FlopsPersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void writeOrderInventory() throws FlopsPersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
