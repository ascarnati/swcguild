/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.ui;

import java.math.BigDecimal;
import java.util.Scanner;

/**
 *
 * @author aScarnati 
 */
public class UserIOConsoleImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public double readDouble(String prompt) {
        double x;
        System.out.println(prompt);
        while (!sc.hasNextDouble()) {
            sc.nextLine();
            System.out.println(prompt);
        }
        x = Double.parseDouble(sc.nextLine());
        return x;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        double x, y;

        while (true) {
            x = readDouble(prompt);
            if (x >= min && x <= max) {
                y = x;
                break;
            }
        }
        return y;
    }

    @Override
    public float readFloat(String prompt) {
        float x;
        System.out.println(prompt);
        while (!sc.hasNextFloat()) {
            sc.nextLine();
            System.out.println(prompt);
        }
        x = Float.parseFloat(sc.nextLine());
        return x;

    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        float x, y;
        while (true) {
            x = readFloat(prompt);
            if (x >= min && x <= max) {
                y = x;
                break;
            }
        }
        return y;
    }

    @Override
    public int readInt(String prompt) {
        int x;
        System.out.println(prompt);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.println(prompt);
        }
        x = Integer.parseInt(sc.nextLine());
        return x;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int x, y;
        while (true) {
            try {
                x = readInt(prompt);
                if (x >= min && x <= max) {
                    y = x;
                    break;
                }

            } catch (NumberFormatException e) {
                System.out.println("Invalid Number.  Try again.");
            }
        }
        return y;
    }

    @Override
    public long readLong(String prompt) {
        long x;
        System.out.println(prompt);
        while (!sc.hasNextLong()) {
            sc.nextLine();
            System.out.println(prompt);
        }
        x = Long.parseLong(sc.nextLine());
        return x;

    }

    @Override
    public long readLong(String prompt, long min, long max) {
        long x, y;
        while (true) {
            x = readLong(prompt);
            if (x >= min && x <= max) {
                y = x;
                break;
            }
        }
        return y;

    }

    @Override
    public String readString(String prompt) {
        String x;
        System.out.println(prompt);
        x = sc.nextLine();
        return x;

    }

    @Override
    public BigDecimal readBigDecimal(String prompt) {
        BigDecimal x = new BigDecimal("0");

        while (true) {
            System.out.println(prompt);
            //try {
                x = new BigDecimal(sc.nextLine());
                break;
            //} catch (NumberFormatException e) {
            //    System.out.println("Invalid number.");
            //}

        }
        return x;

        //       System.out.println(prompt);
        //       while (!sc.hasNextBigDecimal()) {
        //           x = new BigDecimal (sc.nextLine());
        //           System.out.println(prompt);
        //       }
        //       return x;
        //   }
    }
}
