/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.advice;

import com.sg.flops.dao.FlopsAuditDao;
import com.sg.flops.dao.FlopsPersistenceException;
import org.aspectj.lang.JoinPoint;


/**
 *
 * @author aScarnati
 */
public class LoggingAdvice {
    
    FlopsAuditDao auditDao;
    
    public LoggingAdvice(FlopsAuditDao auditDao) {
        this.auditDao = auditDao;
    }
    
    public void createAuditEntry(JoinPoint jp) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";

        for (Object currentArg : args) {
            auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry("(" + auditEntry + ")");
        } catch (FlopsPersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }

    public void createExceptionEntry(Exception ex) {
        String auditEntry = "(" + ex.getClass() + " | " + ex.getMessage() + ")";
        
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (FlopsPersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
        
    }
    
    public void createExceptionAuditEntry (JoinPoint jp, Exception ex) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";
        
        for (Object currentArg : args) {
                auditEntry = auditEntry + currentArg + " | " ;
                //auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry("(" + ex.getClass() + " | " + ex.getMessage() + " | " + auditEntry + ")");         
            // add'l info or formats to print if desired
            // auditDao.writeAuditEntry(auditEntry);
            // auditDao.writeAuditEntry(ex.getMessage());
            // auditDao.writeAuditEntry(ex.getClass() + " | " + auditEntry.toString());
        } catch (FlopsPersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }
    
}
