/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.StaticPage;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class StaticPageDaoTest {

  StaticPageDao staticPageDao;

  public StaticPageDaoTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {

    ApplicationContext ctx
            = new ClassPathXmlApplicationContext("test-applicationContext.xml");

    staticPageDao = ctx.getBean(StaticPageDao.class);

    JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);

    cleaner.execute("delete from StaticPages where 1=1");

  }

  @After
  public void tearDown() {
  }

  /**
   * Test of addStaticPage method, of class StaticPageDao.
   */
  @Test
  public void testAddGetStaticPage() {
    StaticPage sp = new StaticPage();
    sp.setPageType("Pet Bios");
    sp.setTitle("llama");
    sp.setContent("Some awesome content");

    int id = staticPageDao.addStaticPage(sp);
    sp.setStaticPageId(id);

    StaticPage fromDao = staticPageDao.getStaticPageById(sp.getStaticPageId());

    assertEquals(sp, fromDao);
  }

  /**
   * Test of deleteStaticPage method, of class StaticPageDao.
   */
  @Test
  public void testDeleteStaticPage() {
    StaticPage sp = new StaticPage();
    sp.setPageType("Pet Bios");
    sp.setTitle("llama");
    sp.setContent("Some awesome content");

    int id = staticPageDao.addStaticPage(sp);
    sp.setStaticPageId(id);

    StaticPage fromDao = staticPageDao.getStaticPageById(sp.getStaticPageId());

    assertEquals(sp, fromDao);

    staticPageDao.deleteStaticPage(sp.getStaticPageId());
    Assert.assertNull(staticPageDao.getStaticPageById(sp.getStaticPageId()));
  }

  /**
   * Test of updateStaticPage method, of class StaticPageDao.
   */
  @Test
  public void testUpdateStaticPage() {
    StaticPage sp = new StaticPage();
    sp.setPageType("Pet Bios");
    sp.setTitle("llama");
    sp.setContent("Some awesome content");

    int id = staticPageDao.addStaticPage(sp);
    sp.setStaticPageId(id);

    StaticPage fromDao = staticPageDao.getStaticPageById(sp.getStaticPageId());

    assertEquals(sp, fromDao);

    sp.setPageType("Film Roles");
    sp.setTitle("poodle");
    sp.setContent("Some NEW awesome content");
    staticPageDao.updateStaticPage(sp);

    fromDao = staticPageDao.getStaticPageById(sp.getStaticPageId());

    assertEquals(sp, fromDao);
  }

  /**
   * Test of getAllStaticPages method, of class StaticPageDao.
   */
  @Test
  public void testGetAllStaticPages() {
    List<StaticPage> staticPages = staticPageDao.getAllStaticPages();

    assertEquals(0, staticPages.size());

    StaticPage sp1 = new StaticPage();
    sp1.setPageType("Pet Bios");
    sp1.setTitle("llama");
    sp1.setContent("Some awesome content");

    int id1 = staticPageDao.addStaticPage(sp1);
    sp1.setStaticPageId(id1);

    StaticPage sp2 = new StaticPage();
    sp2.setPageType("Film Roles");
    sp2.setTitle("poodle");
    sp2.setContent("Some MORE awesome content");

    int id2 = staticPageDao.addStaticPage(sp2);
    sp2.setStaticPageId(id2);

    staticPages = staticPageDao.getAllStaticPages();

    assertEquals(2, staticPages.size());

    staticPageDao.deleteStaticPage(sp2.getStaticPageId());

    staticPages = staticPageDao.getAllStaticPages();

    assertEquals(1, staticPages.size());
  }
}
