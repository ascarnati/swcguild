/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Comment;
import com.sg.springstinepettraining.model.Hashtag;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class CommentDaoTest {

    HashtagDao hashtagDao;
    CategoryDao categoryDao;
    BlogDao blogDao;
    StaticPageDao staticPageDao;
    CommentDao commentDao;

    public CommentDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        hashtagDao = ctx.getBean(HashtagDao.class);
        categoryDao = ctx.getBean(CategoryDao.class);
        blogDao = ctx.getBean(BlogDao.class);
        staticPageDao = ctx.getBean(StaticPageDao.class);
        commentDao = ctx.getBean(CommentDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);

        cleaner.execute("delete from Blogs_Hashtags where 1=1");
        cleaner.execute("delete from Blogs_Images where 1=1");
        cleaner.execute("delete from Comments where 1=1");
        cleaner.execute("delete from Blogs where 1=1");
        cleaner.execute("delete from Categories where 1=1");
        cleaner.execute("delete from Hashtags where 1=1");
        cleaner.execute("delete from Images where 1=1");
        cleaner.execute("delete from Authorities where 1=1");
        cleaner.execute("delete from Users where 1=1");
        cleaner.execute("delete from StaticPages where 1=1");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addComment method, of class CommentDao.
     */
    @Test
    public void testAddGetComment() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Comment co = new Comment();
        co.setAuthor("Becky");
        co.setComment("This blog was great!");
        co.setBlog(b);

        int coId = commentDao.addComment(co);
        co.setCommentId(coId);

        Comment fromDao = commentDao.getCommentById(co.getCommentId());

        b.setCreateDate(LocalDate.now());
        co.setCreateDate(LocalDate.now());
        assertEquals(co, fromDao);
    }

    /**
     * Test of deleteComment method, of class CommentDao.
     */
    @Test
    public void testDeleteComment() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Comment co = new Comment();
        co.setAuthor("Becky");
        co.setComment("This blog was great!");
        co.setBlog(b);

        int coId = commentDao.addComment(co);
        co.setCommentId(coId);

        Comment fromDao = commentDao.getCommentById(co.getCommentId());

        b.setCreateDate(LocalDate.now());
        co.setCreateDate(LocalDate.now());
        assertEquals(co, fromDao);

        commentDao.deleteComment(coId);
        Assert.assertNull(commentDao.getCommentById(co.getCommentId()));
    }

    /**
     * Test of updateComment method, of class CommentDao.
     */
    @Test
    public void testUpdateComment() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Comment co = new Comment();
        co.setAuthor("Becky");
        co.setComment("This blog was great!");
        co.setBlog(b);

        int coId = commentDao.addComment(co);
        co.setCommentId(coId);

        Comment fromDao = commentDao.getCommentById(co.getCommentId());

        b.setCreateDate(LocalDate.now());
        co.setCreateDate(LocalDate.now());
        assertEquals(co, fromDao);

        co.setAuthor("Anthony");
        commentDao.updateComment(co);

        fromDao = commentDao.getCommentById(co.getCommentId());

        assertEquals(co, fromDao);
    }

    /**
     * Test of getAllComments method, of class CommentDao.
     */
    @Test
    public void testGetAllComments() {
        List<Comment> comments = commentDao.getAllComments();

        assertEquals(0, comments.size());

        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setCreateDate(LocalDate.parse("2017-04-19", DateTimeFormatter.ISO_DATE));
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Comment co = new Comment();
        co.setAuthor("Becky");
        co.setCreateDate(LocalDate.parse("2017-04-25", DateTimeFormatter.ISO_DATE));
        co.setComment("This blog was great!");
        co.setBlog(b);

        int coId = commentDao.addComment(co);
        co.setCommentId(coId);

        Comment co2 = new Comment();
        co2.setAuthor("Becky");
        co2.setCreateDate(LocalDate.parse("2017-04-25", DateTimeFormatter.ISO_DATE));
        co2.setComment("This blog was great!");
        co2.setBlog(b);

        int coId2 = commentDao.addComment(co2);
        co2.setCommentId(coId2);

        comments = commentDao.getAllComments();

        assertEquals(2, comments.size());

    }

    /**
     * Test of getCommentsByBlogId method, of class CommentDao.
     */
    @Test
    public void testGetCommentsByBlogId() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setCreateDate(LocalDate.parse("2017-04-19", DateTimeFormatter.ISO_DATE));
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Comment co = new Comment();
        co.setAuthor("Becky");
        co.setCreateDate(LocalDate.parse("2017-04-25", DateTimeFormatter.ISO_DATE));
        co.setComment("This blog was great!");
        co.setBlog(b);

        int coId = commentDao.addComment(co);
        co.setCommentId(coId);

        Comment co2 = new Comment();
        co2.setAuthor("Becky");
        co2.setCreateDate(LocalDate.parse("2017-04-25", DateTimeFormatter.ISO_DATE));
        co2.setComment("This blog was great!");
        co2.setBlog(b);

        int coId2 = commentDao.addComment(co2);
        co2.setCommentId(coId2);

        List<Comment> commentsByBlog = commentDao.getCommentsByBlogId(b.getBlogId());

        assertEquals(2, commentsByBlog.size());
    }

}
