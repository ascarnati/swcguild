/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class CategoryDaoTest {

    HashtagDao hashtagDao;
    CategoryDao categoryDao;
    BlogDao blogDao;
    StaticPageDao staticPageDao;
    CommentDao commentDao;

    public CategoryDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        hashtagDao = ctx.getBean(HashtagDao.class);
        categoryDao = ctx.getBean(CategoryDao.class);
        blogDao = ctx.getBean(BlogDao.class);
        staticPageDao = ctx.getBean(StaticPageDao.class);
        commentDao = ctx.getBean(CommentDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);

        cleaner.execute("delete from Blogs_Hashtags where 1=1");
        cleaner.execute("delete from Blogs_Images where 1=1");
        cleaner.execute("delete from Comments where 1=1");
        cleaner.execute("delete from Blogs where 1=1");
        cleaner.execute("delete from Categories where 1=1");
        cleaner.execute("delete from Hashtags where 1=1");
        cleaner.execute("delete from Images where 1=1");
        cleaner.execute("delete from Authorities where 1=1");
        cleaner.execute("delete from Users where 1=1");
        cleaner.execute("delete from StaticPages where 1=1");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addCategory method, of class CategoryDao.
     */
    @Test
    public void testAddGetCategory() {
        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Category fromDao = categoryDao.getCategoryById(c.getCategoryId());

        assertEquals(c, fromDao);
    }

    /**
     * Test of deleteCategory method, of class CategoryDao.
     */
    @Test
    public void testDeleteCategory() {
        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Category fromDao = categoryDao.getCategoryById(c.getCategoryId());

        assertEquals(c, fromDao);

        categoryDao.deleteCategory(c.getCategoryId());
        Assert.assertNull(categoryDao.getCategoryById(c.getCategoryId()));
    }

    /**
     * Test of updateCategory method, of class CategoryDao.
     */
    @Test
    public void testUpdateCategory() {
        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Category fromDao = categoryDao.getCategoryById(c.getCategoryId());

        assertEquals(c, fromDao);

        c.setName("poodle");
        categoryDao.updateCategory(c);

        fromDao = categoryDao.getCategoryById(c.getCategoryId());

        assertEquals(c, fromDao);
    }

    /**
     * Test of getAllCategorys method, of class CategoryDao.
     */
    @Test
    public void testGetAllCategorys() {
        List<Category> categories = categoryDao.getAllCategorys();

        assertEquals(0, categories.size());

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Category c2 = new Category();
        c2.setName("poodle");

        int id2 = categoryDao.addCategory(c2);
        c2.setCategoryId(id2);

        categories = categoryDao.getAllCategorys();

        assertEquals(2, categories.size());

        categoryDao.deleteCategory(c2.getCategoryId());

        categories = categoryDao.getAllCategorys();

        assertEquals(1, categories.size());
    }

}
