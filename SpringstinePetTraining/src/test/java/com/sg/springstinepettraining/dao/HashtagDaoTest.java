/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class HashtagDaoTest {

    HashtagDao hashtagDao;
    CategoryDao categoryDao;
    BlogDao blogDao;
    StaticPageDao staticPageDao;
    CommentDao commentDao;

    public HashtagDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        hashtagDao = ctx.getBean(HashtagDao.class);
        categoryDao = ctx.getBean(CategoryDao.class);
        blogDao = ctx.getBean(BlogDao.class);
        staticPageDao = ctx.getBean(StaticPageDao.class);
        commentDao = ctx.getBean(CommentDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);

        cleaner.execute("delete from Blogs_Hashtags where 1=1");
        cleaner.execute("delete from Blogs_Images where 1=1");
        cleaner.execute("delete from Comments where 1=1");
        cleaner.execute("delete from Blogs where 1=1");
        cleaner.execute("delete from Categories where 1=1");
        cleaner.execute("delete from Hashtags where 1=1");
        cleaner.execute("delete from Images where 1=1");
        cleaner.execute("delete from Authorities where 1=1");
        cleaner.execute("delete from Users where 1=1");
        cleaner.execute("delete from StaticPages where 1=1");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addHashtag method, of class HashtagDao.
     */
    @Test
    public void testAddGetHashtag() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id = hashtagDao.addHashtag(h);
        h.setHashtagId(id);

        Hashtag fromDao = hashtagDao.getHashtagById(h.getHashtagId());

        assertEquals(h, fromDao);
    }

    /**
     * Test of deleteHashtag method, of class HashtagDao.
     */
    @Test
    public void testDeleteHashtag() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id = hashtagDao.addHashtag(h);
        h.setHashtagId(id);

        Hashtag fromDao = hashtagDao.getHashtagById(h.getHashtagId());

        assertEquals(h, fromDao);

        hashtagDao.deleteHashtag(h.getHashtagId());
        Assert.assertNull(hashtagDao.getHashtagById(h.getHashtagId()));
    }

    /**
     * Test of updateHashtag method, of class HashtagDao.
     */
    @Test
    public void testUpdateHashtag() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id = hashtagDao.addHashtag(h);
        h.setHashtagId(id);

        Hashtag fromDao = hashtagDao.getHashtagById(h.getHashtagId());

        assertEquals(h, fromDao);

        h.setName("poodle");
        hashtagDao.updateHashtag(h);

        fromDao = hashtagDao.getHashtagById(h.getHashtagId());

        assertEquals(h, fromDao);
    }

    /**
     * Test of getAllHashtags method, of class HashtagDao.
     */
    @Test
    public void testGetAllHashtags() {
        List<Hashtag> hashtags = hashtagDao.getAllHashtags();

        assertEquals(0, hashtags.size());

        Hashtag h1 = new Hashtag();
        h1.setName("llama");

        int id1 = hashtagDao.addHashtag(h1);
        h1.setHashtagId(id1);

        Hashtag h2 = new Hashtag();
        h2.setName("poodle");

        int id2 = hashtagDao.addHashtag(h2);
        h2.setHashtagId(id2);

        hashtags = hashtagDao.getAllHashtags();

        assertEquals(2, hashtags.size());

        hashtagDao.deleteHashtag(h2.getHashtagId());

        hashtags = hashtagDao.getAllHashtags();

        assertEquals(1, hashtags.size());
    }

    /**
     * Test of getHashtagByBlogId method, of class HashtagDao.
     */
    @Test
    public void testGetHashtagByBlogId() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setHashtags(hashtagDao.getAllHashtags());
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setCreateDate(LocalDate.parse("2017-04-19", DateTimeFormatter.ISO_DATE));
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        List<Hashtag> hashtagsByBlog = hashtagDao.getHashtagByBlogId(b.getBlogId());

        assertEquals(1, hashtagsByBlog.size());

    }

    /**
     * Test of addHashtag method, of class HashtagDao.
     */
    @Test
    public void testAddHashtag() {
    }

    /**
     * Test of getHashtagById method, of class HashtagDao.
     */
    @Test
    public void testGetHashtagById() {
    }

}
