/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class BlogDaoTest {

    HashtagDao hashtagDao;
    CategoryDao categoryDao;
    BlogDao blogDao;
    StaticPageDao staticPageDao;
    CommentDao commentDao;

    public BlogDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        hashtagDao = ctx.getBean(HashtagDao.class);
        categoryDao = ctx.getBean(CategoryDao.class);
        blogDao = ctx.getBean(BlogDao.class);
        staticPageDao = ctx.getBean(StaticPageDao.class);
        commentDao = ctx.getBean(CommentDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);

        cleaner.execute("delete from Blogs_Hashtags where 1=1");
        cleaner.execute("delete from Blogs_Images where 1=1");
        cleaner.execute("delete from Comments where 1=1");
        cleaner.execute("delete from Blogs where 1=1");
        cleaner.execute("delete from Categories where 1=1");
        cleaner.execute("delete from Hashtags where 1=1");
        cleaner.execute("delete from Images where 1=1");
        cleaner.execute("delete from Authorities where 1=1");
        cleaner.execute("delete from Users where 1=1");
        cleaner.execute("delete from StaticPages where 1=1");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addBlog method, of class BlogDao.
     */
    @Test
    public void testAddGetBlog() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Blog fromDao = blogDao.getBlogById(b.getBlogId());

        b.setCreateDate(LocalDate.now());
        assertEquals(b, fromDao);

    }

    /**
     * Test of deleteBlog method, of class BlogDao.
     */
    @Test
    public void testDeleteBlog() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Blog fromDao = blogDao.getBlogById(b.getBlogId());

        b.setCreateDate(LocalDate.now());
        assertEquals(b, fromDao);

        blogDao.deleteBlog(b.getBlogId());
        Assert.assertNull(blogDao.getBlogById(b.getBlogId()));

    }

    /**
     * Test of updateBlog method, of class BlogDao.
     */
    @Test
    public void testUpdateBlog() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Blog fromDao = blogDao.getBlogById(b.getBlogId());

        b.setCreateDate(LocalDate.now());
        assertEquals(b, fromDao);

        b.setStatus("Submitted");
        blogDao.updateBlog(b);

        fromDao = blogDao.getBlogById(b.getBlogId());

        assertEquals(b, fromDao);
    }

    /**
     * Test of getAllBlogs method, of class BlogDao.
     */
    @Test
    public void testGetAllBlogs() {
        List<Blog> blogs = blogDao.getAllBlogs();

        assertEquals(0, blogs.size());

        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setCreateDate(LocalDate.parse("2017-04-19", DateTimeFormatter.ISO_DATE));
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        blogs = blogDao.getAllBlogs();

        assertEquals(1, blogs.size());
    }

    /**
     * Test of getBlogsByHashtagId method, of class BlogDao.
     */
    @Test
    public void testGetBlogsByHashtagId() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("Approved");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Blog fromDao = blogDao.getBlogById(b.getBlogId());

        b.setCreateDate(LocalDate.now());
        assertEquals(b, fromDao);

        List<Blog> blogsByHashtagID = blogDao.getBlogsByHashtagId(h.getHashtagId());

        assertEquals(1, blogsByHashtagID.size());
    }

    /**
     * Test of getBlogsByCategoryId method, of class BlogDao.
     */
    @Test
    public void testGetBlogsByCategoryId() {
        Hashtag h = new Hashtag();
        h.setName("llama");

        int id1 = hashtagDao.addHashtag(h);
        h.setHashtagId(id1);

        Category c = new Category();
        c.setName("llama");

        int id = categoryDao.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("approved");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>HTML Test</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);
        b.setHashtags(hashtagDao.getAllHashtags());

        int id2 = blogDao.addBlog(b);
        b.setBlogId(id2);

        Blog fromDao = blogDao.getBlogById(b.getBlogId());

        b.setCreateDate(LocalDate.now());
        assertEquals(b, fromDao);

        List<Blog> blogsByCategory = blogDao.getBlogsByCategoryId(c.getCategoryId());

        assertEquals(1, blogsByCategory.size());
    }

}
