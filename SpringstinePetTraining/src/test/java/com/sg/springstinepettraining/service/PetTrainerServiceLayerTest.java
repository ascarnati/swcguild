/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.service;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class PetTrainerServiceLayerTest {

    PetTrainerServiceLayer petTrainerService;

    public PetTrainerServiceLayerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        petTrainerService
                = ctx.getBean("petTrainerService", PetTrainerServiceLayer.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);

        cleaner.execute("delete from Blogs_Hashtags where 1=1");
        cleaner.execute("delete from Blogs_Images where 1=1");
        cleaner.execute("delete from Comments where 1=1");
        cleaner.execute("delete from Blogs where 1=1");
        cleaner.execute("delete from Categories where 1=1");
        cleaner.execute("delete from Hashtags where 1=1");
        cleaner.execute("delete from Images where 1=1");
        cleaner.execute("delete from Authorities where 1=1");
        cleaner.execute("delete from Users where 1=1");
        cleaner.execute("delete from StaticPages where 1=1");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addBlog method, of class PetTrainerServiceLayer. Test primarily
     * checks that hashtags are not duplicated in dao, and that list of hashtags
     * are added to blog.
     */
    @Test
    public void testAddBlog() {
        Hashtag h1 = new Hashtag();
        h1.setName("servicelayer");
        petTrainerService.addHashtag(h1);

        Hashtag h2 = new Hashtag();
        h2.setName("other");
        petTrainerService.addHashtag(h2);

        assertEquals(2, petTrainerService.getAllHashtags().size());

        Category c = new Category();
        c.setName("llama");

        int id = petTrainerService.addCategory(c);
        c.setCategoryId(id);

        Blog b = new Blog();
        b.setStatus("In Progress");
        b.setTitle("The First Blog");
        b.setAuthor("Becky");
        b.setContent("<p>Testing the #servicelayer is finding #hashtags1.</p>");
        b.setComments("This is where comments go.");
        b.setExpirationDate(LocalDate.parse("2017-05-01", DateTimeFormatter.ISO_DATE));
        b.setPublishDate(LocalDate.parse("2017-04-21", DateTimeFormatter.ISO_DATE));
        b.setCategory(c);

        int id2 = petTrainerService.addBlog(b);
        b.setBlogId(id2);

        Blog fromService = petTrainerService.getBlogById(b.getBlogId());

        assertEquals(2, fromService.getHashtags().size());

        b.setCreateDate(LocalDate.now());
        assertEquals(b, fromService);

        assertEquals(3, petTrainerService.getAllHashtags().size());
    }

    /**
     * Test of deleteBlog method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testDeleteBlog() {
    }

    /**
     * Test of updateBlog method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testUpdateBlog() {
    }

    /**
     * Test of getBlogById method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetBlogById() {
    }

    /**
     * Test of getAllBlogs method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetAllBlogs() {
    }

    /**
     * Test of getBlogsByHashtagId method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetBlogsByHashtagId() {
    }

    /**
     * Test of getBlogsByCategoryId method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetBlogsByCategoryId() {
    }

    /**
     * Test of addCategory method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testAddCategory() {
    }

    /**
     * Test of deleteCategory method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testDeleteCategory() {
    }

    /**
     * Test of updateCategory method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testUpdateCategory() {
    }

    /**
     * Test of getCategoryById method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetCategoryById() {
    }

    /**
     * Test of getAllCategorys method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetAllCategorys() {
    }

    /**
     * Test of addHashtag method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testAddHashtag() {
    }

    /**
     * Test of deleteHashtag method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testDeleteHashtag() {
    }

    /**
     * Test of updateHashtag method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testUpdateHashtag() {
    }

    /**
     * Test of getHashtagById method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetHashtagById() {
    }

    /**
     * Test of getAllHashtags method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetAllHashtags() {
    }

    /**
     * Test of getHashtagByBlogId method, of class PetTrainerServiceLayer.
     */
    @Test
    public void testGetHashtagByBlogId() {
    }

}
