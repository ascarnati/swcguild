<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li><a href="${pageContext.request.contextPath}/">Home</a></li>
      <li><a href="${pageContext.request.contextPath}/displayAllBlogsPage">Blogs</a></li>
      <li class="dropdown">
        <a class ="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">Pet Pages
          <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li class="dropdown">
            <a href="#">Pet Bios</a>
            <ul class="dropdown-menu">
              <c:forEach var="currentPage" items="${pageList}">
                <c:if test="${currentPage.pageType == 'Pet Bios'}">
                  <li><a href="displayStaticPage?staticPageId=${currentPage.staticPageId}">
                      <c:out value="${currentPage.title}"/></a></li>
                    </c:if>
                  </c:forEach>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#">Film Roles</a>
            <ul class="dropdown-menu">
              <c:forEach var="currentPage" items="${pageList}">
                <c:if test="${currentPage.pageType == 'Film Roles'}">
                  <li><a href="displayStaticPage?staticPageId=${currentPage.staticPageId}">
                      <c:out value="${currentPage.title}"/></a></li>
                    </c:if>
                  </c:forEach>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#">Training & Care</a>
            <ul class="dropdown-menu">
              <c:forEach var="currentPage" items="${pageList}">
                <c:if test="${currentPage.pageType == 'Training & Care'}">
                  <li><a href="displayStaticPage?staticPageId=${currentPage.staticPageId}">
                      <c:out value="${currentPage.title}"/></a></li>
                    </c:if>
                  </c:forEach>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#">Fun Stuff</a>
            <ul class="dropdown-menu">
              <c:forEach var="currentPage" items="${pageList}">
                <c:if test="${currentPage.pageType == 'Fun Stuff'}">
                  <li><a href="displayStaticPage?staticPageId=${currentPage.staticPageId}">
                      <c:out value="${currentPage.title}"/></a></li>
                    </c:if>
                  </c:forEach>
            </ul>
          </li>
          <li role="separator" class="divider"></li>
          <li><a href="${pageContext.request.contextPath}/displayAllStaticPages">Browse All Pages</a></li>
        </ul>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a class ="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
           href="${pageContext.request.contextPath}/#">
          <span class="glyphicon glyphicon-log-in"></span> Admin</a>
        <ul class="dropdown-menu">
          <c:if test="${pageContext.request.userPrincipal.name == null}">
            <li><a href="${pageContext.request.contextPath}/displayLoginPage">Login </a></li>
            </c:if>
          <li><sec:authorize access="hasRole('ROLE_USER')"><a href="${pageContext.request.contextPath}/displayBlogEntryPage?user=${pageContext.request.userPrincipal.name}">Create Blog</a></sec:authorize></li>
          <li><sec:authorize access="hasRole('ROLE_ADMIN')"><a href="${pageContext.request.contextPath}/displayAdminPage">Manage Blogs</a></sec:authorize></li>
          <li><sec:authorize access="hasRole('ROLE_ADMIN')"><a href="${pageContext.request.contextPath}/displayCreateStaticPage">Create Pet Page</a></sec:authorize></li>
          <li><sec:authorize access="hasRole('ROLE_ADMIN')"><a href="${pageContext.request.contextPath}/displayAdminStaticPages">Manage Pet Pages</a></sec:authorize></li>
          <li><sec:authorize access="hasRole('ROLE_ADMIN')"><a href="${pageContext.request.contextPath}/displayUserList">Manage Users</a></sec:authorize></li>

          <c:if test="${pageContext.request.userPrincipal.name != null}">
            <li role="separator" class="divider"></li>
            <li><a href="<c:url value="/j_spring_security_logout" />" > Logout</a></li>
            </c:if>
        </ul>
      </li>
    </ul>
  </div>
</nav>