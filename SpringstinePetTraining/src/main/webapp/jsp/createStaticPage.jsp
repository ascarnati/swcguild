<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Blog Entry</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/pets.js"></script>
    <style>
      @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
    </style>
  </head>
  <body>
    <div class="container">
      <h1>Adolphis Springstine's Spectacular Pet Training</h1>
      <%@include file="menu.jsp" %>
      <div class="row col-md-12">
        <h2>Create new Pet Page</h2>
        <br>
        <form class="form-horizontal" role="form" method="POST" action="savePage">
          <div class="form-group col-md-6">
            <label for="add-staticTitle" class="col-md-3 control-label">Title:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="pageTitle" required/>
            </div>
          </div>
          <div class="form-group col-md-6">
            <label for="add-category" class="col-md-3 control-label">Page Type:</label>
            <div class="col-md-9">
              <select class="form-control" name="pageType" required>
                <option value="Pet Bios">Pet Bios</option>
                <option value="Film Roles">Film Roles</option>
                <option value="Training & Care">Training & Care</option>
                <option value="Fun Stuff">Fun Stuff</option>
              </select>
            </div>
          </div>
          <div class="form-group col-md-12">
            <label for="add-staticContent" class="col-md-4 control-label"></label>
            <textarea class="form-control" name="pageContent" id="content" placeholder="content"></textarea>
          </div>
          <div class="form-group col-md-12">
            <input type="submit" class="btn btn-default" value="Save Pet Page"/>
          </div>
        </form>
      </div>
    </div>
    <script type="text/javascript" src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=v4ch1mqjinfuchsoyz81b5oq9gwaxg5p1dhag26ptvtz5rjs'></script>
    <script type="text/javascript">
      tinymce.init({
        selector: '#content',
        theme: 'modern',
        width: 1100,
        height: 450,
        plugins: [
          'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
          'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
          'save table contextmenu directionality emoticons template paste textcolor'
        ],
        content_css: 'css/content.css',
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
      });
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  </body>
</html>