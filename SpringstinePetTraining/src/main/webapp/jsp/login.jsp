<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick-theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pets.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Adolphis Springstine's Spectacular Pet Training</h1>
            <%@include file="menu.jsp" %>
            <div class="row">
                <div class="col-md-12">
                    <h2>Login</h2>

                    <c:if test="${param.login_error == 1}" >
                        <h3>Wrong ID or password!</h3>
                    </c:if>

                    <form class="form-horizontal"
                          role="form"
                          method="post"
                          action="j_spring_security_check">
                        <div class="form-group">
                            <label for="j_username"
                                   class="col-md-4 control-label">Username:</label>
                            <div class="col-md-4">
                                <input type="text"
                                       class="form-control"
                                       name="j_username"
                                       placeholder="Username"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="j_password"
                                   class="col-md-4 control-label">Password:</label>
                            <div class="col-md-4">
                                <input type="password"
                                       class="form-control"
                                       name="j_password"
                                       placeholder="Password"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <input type="submit"
                                       class="btn btn-default"
                                       id="search-button"
                                       value="Sign In"/>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </body>
</html>

