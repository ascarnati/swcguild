<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Blog Entry</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick-theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Adolphis Springstine's Spectacular Pet Training</h1>
            <%@include file="menu.jsp" %>
            <div class="row">
                <h2>${pageContext.request.userPrincipal.name}'s Blogs</h2>
                <table class="table table-hover">
                    <tr>
                        <th class="sort btn col-md-2" data-sort="title">Blog Title</th>
                        <th class="sort btn col-md-2" data-sort="status">Status</th>
                        <th class="sort btn col-md-2" data-sort="author">Created</th>
                        <th class="sort btn col-md-2" data-sort="publishDate">Published</th>
                        <th class="sort btn col-md-2" data-sort="expirationDate">Expired</th>
                    </tr>
                    <tbody class="list">
                        <c:forEach var="currentBlog" items="${blogs}">
                            <tr>
                                <td class="title">
                                    <a href="displayUserBlogDetails?blogId=${currentBlog.blogId}"><c:out value="${currentBlog.title}"/></a>
                                </td>
                                <td class="status">
                                    <c:out value="${currentBlog.status}"/>
                                </td>
                                <td class="author">
                                    <c:out value="${currentBlog.createDate}"/>
                                </td>
                                <td class="publishDate">
                                    <c:out value="${currentBlog.publishDate}"/>
                                </td>
                                <td class="expirationDate">
                                    <c:out value="${currentBlog.expirationDate}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row">
                <h2>Submit a Post</h2>
                <br>
                <form class="form-horizontal" role="form" method="POST" action="saveBlog">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="add-blogTitle" class="col-md-3 control-label">Title:</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="blogTitle" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-blogAuthor" class="col-md-3 control-label">Author: </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="blogAuthor" value="${pageContext.request.userPrincipal.name}" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-category" class="col-md-3 control-label">Category: </label>
                            <div class="col-md-9">
                                <select class="form-control" name="blogCategory">
                                    <c:forEach items="${categories}" var="currentCategory">
                                        <option value="${currentCategory.categoryId}"><c:out value="${currentCategory.name}" /></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-publishDate" class="col-md-4 control-label">Publish Date: </label>
                            <div class="col-md-8">
                                <input class="form-control" id="publishDate" name="publishDate" placeholder="YYYY-MM-DD" type="text" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-expirationDate" class="col-md-4 control-label">Expiration Date: </label>
                            <div class="col-md-8">
                                <input class="form-control" id="expirationDate" name="expirationDate" placeholder="YYYY-MM-DD" type="text" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default col-md-offset-4" value="Save Blog"/>
                        </div>
                    </div>
                    <!-- Input for TinyMCE -->
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="add-blogContent" class="col-md-4 control-label"></label>
                            <textarea class="form-control" name="blogContent" id="blogContent" placeholder="content"></textarea>
                        </div>
                    </div>
                    <!-- end input for TinyMCE -->
                </form>
            </div>
        </div>
        <script type="text/javascript" src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=v4ch1mqjinfuchsoyz81b5oq9gwaxg5p1dhag26ptvtz5rjs'></script>
        <script type="text/javascript">
            tinymce.init({
                selector: '#blogContent',
                theme: 'modern',
                width: 800,
                height: 300,
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
            });
        </script>
        <script src="//listjs.com/assets/javascripts/list.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/pets.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/datepicker.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    </body>
</html>