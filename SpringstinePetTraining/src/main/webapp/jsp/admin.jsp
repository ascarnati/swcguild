<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/list.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pets.js"></script>
        <script src="//listjs.com/assets/javascripts/list.min.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Adolphis Springstine's Pet Training</h1>
            <%@include file="menu.jsp" %>
            <h2>All Submissions...</h2>
            <div id="allSubmissions">
                <div class="row">
                    <div class="col-md-4">
                        <input class="search" placeholder="Enter Search Terms" />
                    </div>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th class="sort btn col-md-2" data-sort="title">Blog Title</th>
                        <th class="sort btn col-md-2" data-sort="status">Status</th>
                        <th class="sort btn col-md-2" data-sort="author">Author</th>
                        <th class="sort btn col-md-2" data-sort="publishDate">Publish Date</th>
                        <th class="sort btn col-md-2" data-sort="expirationDate">Expiration Date</th>
                    </tr>
                    <tbody class="list">
                        <c:forEach var="currentBlog" items="${blogList}">
                            <tr>
                                <td class="title">
                                    <a href="displayBlogDetails?blogId=${currentBlog.blogId}"><c:out value="${currentBlog.title}"/></a>
                                </td>
                                <td class="status">
                                    <c:out value="${currentBlog.status}"/>
                                </td>
                                <td class="author">
                                    <c:out value="${currentBlog.author}"/>
                                </td>
                                <td class="publishDate">
                                    <c:out value="${currentBlog.publishDate}"/>
                                </td>
                                <td class="expirationDate">
                                    <c:out value="${currentBlog.expirationDate}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript">
            var options = {
                valueNames: ['title', 'status', 'author', 'publishDate', 'expirationDate']
            };
            var allSubmissionsList = new List('allSubmissions', options);
        </script>
    </body>
</html>