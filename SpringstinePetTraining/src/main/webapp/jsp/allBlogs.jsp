<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>All Blogs Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick-theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pets.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Adolphis Springstine's Spectacular Pet Training</h1>
            <%@include file="menu.jsp" %>
            <!-- Split button -->
            <div class="btn-group">
                <a href="${pageContext.request.contextPath}/displayAllBlogsPage" type="button" class="btn btn-primary">All Blogs</a>
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/displayAllBlogsPage">All Blogs</a></li>
                    <li role="separator" class="divider"></li>
                        <c:forEach var="currentCategory" items="${categories}">
                        <li><a href="${pageContext.request.contextPath}/blogsByCategory?categoryId=${currentCategory.categoryId}"><c:out value="${currentCategory.name}"/></a></li>
                        </c:forEach>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h2>What's Happening...</h2>
                    <c:forEach var="currentBlog" items="${blogList}">
                        <div class="blogDiv">
                            <h3>
                                <c:out value="${currentBlog.title}"/>
                            </h3>
                            <div class="form-group">
                                <span>
                                    <c:forEach var="currentHashtag" items="${currentBlog.hashtags}">
                                        <a href="${pageContext.request.contextPath}/blogsByHashtag?hashtagId=${currentHashtag.hashtagId}"
                                           class="hashtagButton btn btn-default"><c:out value="${currentHashtag.name}"/></a>
                                    </c:forEach>
                                </span>
                            </div>

                            <h4>
                                by <c:out value="${currentBlog.author}"/>
                            </h4>
                            <h5>
                                created <c:out value="${currentBlog.createDate}"/>
                            </h5>
                            <p>
                                <c:out escapeXml="false" value="${currentBlog.content}"/>
                            </p>

                            <a class="btn btn-default" href="${pageContext.request.contextPath}/displayCommentForm?blogId=${currentBlog.blogId}">
                                Add Comment
                            </a>
                            <a class="btn btn-default" href="${pageContext.request.contextPath}/displayCommentForm?blogId=${currentBlog.blogId}">
                                See Comments
                            </a>
                        </div>
                    </c:forEach>       
                </div>
                <div class="col-md-3">
                    <h2>#Trending</h2>
                    <div class="form-group">
                        <span>
                            <c:forEach var="currentHashtag" items="${hashtags}">
                                <a href="${pageContext.request.contextPath}/blogsByHashtag?hashtagId=${currentHashtag.hashtagId}"
                                   class="hashtagButton btn btn-default"><c:out value="${currentHashtag.name}"/></a>
                            </c:forEach>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>