<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Blog Details Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick-theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pets.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Adolphis Springstine's Spectacular Pet Training</h1>
            <%@include file="menu.jsp" %>
            <div class="row">
                <h2>Blog Details...</h2>
                <div class="blogDiv">
                    <h3>
                        <c:out value="${blog.title}"/>
                    </h3>
                    <h4>
                        by <c:out value="${blog.author}"/>
                    </h4>
                    <h5>
                        created <c:out value="${blog.createDate}"/>
                    </h5>
                    <p>
                        <c:out escapeXml="false" value="${blog.content}"/>
                    </p>
                </div>
            </div>
            <div class="row">
                <form class="form-vertical" role="form" method="POST" action="adminUpdateBlog">
                    <input type="text" class="hidden" id="blogId" name="blogId" value="${blog.blogId}"/>
                    <div class="form-group">
                        <label for="updateStatus" class="col-md-1 control-label">Status: </label>
                        <div class="col-md-2">
                            <select class="form-control" name="updateStatus">
                                <option value="${blog.status}" selected><c:out value="${blog.status}"/></option>
                                <c:forEach items="${statusList}" var="currentStatus">
                                    <option value="${currentStatus}"><c:out value="${currentStatus}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-publishDate" class="col-md-1 control-label">Publish Date: </label>
                        <div class="col-md-2">
                            <input class="form-control" id="publishDate" name="publishDate" value="${blog.publishDate}" placeholder="YYYY-MM-DD" type="text" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-expirationDate" class="col-md-1 control-label">Expiration Date: </label>
                        <div class="col-md-2">
                            <input class="form-control" id="expirationDate" name="expirationDate" value="${blog.expirationDate}" placeholder="YYYY-MM-DD" type="text" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-default col-md-1" value="Save"/>
                    </div>
                    <div class="form-group">
                        <a href="deleteBlog?blogId=${blog.blogId}" class="btn btn-default col-md-1">Delete</a>
                    </div>
                    <div class="form-group">
                        <a href="${pageContext.request.contextPath}/displayAdminPage" class="btn btn-default col-md-1">
                            Back
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/datepicker.js"></script>
        <script type="text/javascript" src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=v4ch1mqjinfuchsoyz81b5oq9gwaxg5p1dhag26ptvtz5rjs'></script>
        <script type="text/javascript">
            tinymce.init({
                selector: '#blogContent',
                theme: 'modern',
                width: 1150,
                height: 300,
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                content_css: 'css/content.css',
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
            });
        </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    </body>
</html>
