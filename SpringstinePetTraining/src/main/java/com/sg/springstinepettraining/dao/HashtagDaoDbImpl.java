/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Hashtag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HashtagDaoDbImpl implements HashtagDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // Mapper
    private static final class HashtagMapper implements RowMapper<Hashtag> {

        @Override
        public Hashtag mapRow(ResultSet rs, int rowNum) throws SQLException {
            Hashtag h = new Hashtag();
            h.setHashtagId(rs.getInt("hashtagId"));
            h.setName(rs.getString("name"));

            return h;
        }
    }

    private static final String SQL_INSERT_HASHTAG
            = "insert into Hashtags (name) values (?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int addHashtag(Hashtag hashtag) {
        jdbcTemplate.update(SQL_INSERT_HASHTAG, hashtag.getName());

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        hashtag.setHashtagId(id);

        return id;
    }

    private static final String SQL_DELETE_HASHTAG
            = "delete from Hashtags where hashtagId = ?;";

    private static final String SQL_DELETE_BLOGS_HASHTAGS
            = "delete from Blogs_Hashtags where hashtagId = ?;";

    @Override
    public void deleteHashtag(int hashtagId) {
        // delete hashtag-blog relationship before deleting hashtag
        jdbcTemplate.update(SQL_DELETE_BLOGS_HASHTAGS, hashtagId);
        // delete hashtag
        jdbcTemplate.update(SQL_DELETE_HASHTAG, hashtagId);
    }

    private static final String SQL_UPDATE_HASHTAG
            = "update Hashtags set name = ? where hashtagId = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateHashtag(Hashtag hashtag) {
        jdbcTemplate.update(SQL_UPDATE_HASHTAG,
                hashtag.getName(),
                hashtag.getHashtagId());
    }

    private static final String SQL_SELECT_HASHTAG
            = "select * from Hashtags where hashtagId = ?;";

    @Override
    public Hashtag getHashtagById(int hashtagId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_HASHTAG,
                    new HashtagMapper(), hashtagId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_SELECT_ALL_HASHTAGS
            = "select * from Hashtags;";

    @Override
    public List<Hashtag> getAllHashtags() {
        return jdbcTemplate.query(SQL_SELECT_ALL_HASHTAGS, new HashtagMapper());
    }

    private static final String SQL_SELECT_ALL_HASHTAGS_BY_APPROVED_BLOGS
            = "select * from Hashtags h "
            + "join Blogs_Hashtags bh on h.hashtagId = bh.hashtagId "
            + "join Blogs b on b.blogId = bh.blogId "
            + "where b.status  = 'approved' "
            + "AND NOW() BETWEEN b.publishDate AND b.expirationDate;";

    @Override
    public List<Hashtag> getAllHashtagsForApprovedBlogs() {
        return jdbcTemplate.query(SQL_SELECT_ALL_HASHTAGS_BY_APPROVED_BLOGS, new HashtagMapper());
    }

    private static final String SQL_SELECT_HASHTAGS_BY_BLOG_ID
            = "select h.* from Hashtags h "
            + "join Blogs_Hashtags bh on bh.hashtagId = h.hashtagId "
            + "where bh.blogId  =  ?;";

    @Override
    public List<Hashtag> getHashtagByBlogId(int blogId) {
        List<Hashtag> hashtags
                = jdbcTemplate.query(SQL_SELECT_HASHTAGS_BY_BLOG_ID,
                        new HashtagMapper(), blogId);

        return hashtags;
    }

}
