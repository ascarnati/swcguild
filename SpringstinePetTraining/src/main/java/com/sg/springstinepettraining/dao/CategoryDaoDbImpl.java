/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CategoryDaoDbImpl implements CategoryDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // Mapper
    private static final class CategoryMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
            Category c = new Category();
            c.setCategoryId(rs.getInt("categoryId"));
            c.setName(rs.getString("name"));

            return c;
        }
    }

    private static final String SQL_INSERT_CATEGORY
            = "insert into Categories (name) values (?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int addCategory(Category category) {
        jdbcTemplate.update(SQL_INSERT_CATEGORY, category.getName());

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        category.setCategoryId(id);

        return id;
    }

    private static final String SQL_DELETE_CATEGORY
            = "delete from Categories where categoryId = ?;";

    private static final String SQL_UPDATE_BLOGS_TO_DELETE_CATEGORY
            = "update Blogs set categoryId = null where categoryId = ?;";

    @Override
    public void deleteCategory(int categoryId) {
        // update blog to remove category
        jdbcTemplate.update(SQL_UPDATE_BLOGS_TO_DELETE_CATEGORY, categoryId);
        // delete category
        jdbcTemplate.update(SQL_DELETE_CATEGORY, categoryId);
    }

    private static final String SQL_UPDATE_CATEGORY
            = "update Categories set name = ? where categoryId = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateCategory(Category category) {
        jdbcTemplate.update(SQL_UPDATE_CATEGORY,
                category.getName(),
                category.getCategoryId());
    }

    private static final String SQL_SELECT_CATEGORY
            = "select * from Categories where categoryId = ?;";

    @Override
    public Category getCategoryById(int categoryId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY,
                    new CategoryMapper(), categoryId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_SELECT_ALL_CATEGORIES
            = "select * from Categories;";

    @Override
    public List<Category> getAllCategorys() {
        return jdbcTemplate.query(SQL_SELECT_ALL_CATEGORIES, new CategoryMapper());
    }

    private static final String SQL_SELECT_ALL_CATEGORIES_WHERE_BLOG_APPROVED
            = "select * from Categories c "
            + "join Blogs b on c.categoryId = b.categoryId "
            + "where b.status  = 'approved' "
            + "AND NOW() BETWEEN b.publishDate AND b.expirationDate;";

    @Override

    public List<Category> getAllCategorysForApprovedBlogs() {
        return jdbcTemplate.query(SQL_SELECT_ALL_CATEGORIES_WHERE_BLOG_APPROVED, new CategoryMapper());
    }

}
