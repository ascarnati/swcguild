/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Category;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CategoryDao {

    public int addCategory(Category category);

    public void deleteCategory(int categoryId);

    public void updateCategory(Category category);

    public Category getCategoryById(int categoryId);

    public List<Category> getAllCategorys();
    
    public List<Category> getAllCategorysForApprovedBlogs();

}
