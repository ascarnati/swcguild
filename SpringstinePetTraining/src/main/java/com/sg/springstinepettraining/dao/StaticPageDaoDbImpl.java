/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.StaticPage;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class StaticPageDaoDbImpl implements StaticPageDao {

  private JdbcTemplate jdbcTemplate;

  public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  // Mapper
  private static final class StaticPageMapper implements RowMapper<StaticPage> {

    @Override
    public StaticPage mapRow(ResultSet rs, int rowNum) throws SQLException {
      StaticPage sp = new StaticPage();
      sp.setStaticPageId(rs.getInt("staticPageId"));
      sp.setPageType(rs.getString("pageType"));
      sp.setTitle(rs.getString("title"));
      sp.setContent(rs.getString("content"));

      return sp;
    }
  }

  private static final String SQL_INSERT_STATICPAGE
          = "INSERT INTO StaticPages (pageType, title, content) VALUES (?,?,?);";

  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public int addStaticPage(StaticPage staticPage) {
    jdbcTemplate.update(SQL_INSERT_STATICPAGE,
            staticPage.getPageType(),
            staticPage.getTitle(),
            staticPage.getContent());

    int id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()",
            Integer.class);

    staticPage.setStaticPageId(id);

    return id;
  }

  private static final String SQL_DELETE_STATICPAGE
          = "DELETE FROM StaticPages WHERE staticPageId = ?;";

  @Override
  public void deleteStaticPage(int staticPageId) {

    jdbcTemplate.update(SQL_DELETE_STATICPAGE, staticPageId);
  }

  private static final String SQL_UPDATE_STATICPAGE
          = "UPDATE StaticPages SET pageType = ?, title = ?, content = ? WHERE staticPageId = ?;";

  @Override
  @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
  public void updateStaticPage(StaticPage staticPage) {
    jdbcTemplate.update(SQL_UPDATE_STATICPAGE,
            staticPage.getPageType(),
            staticPage.getTitle(),
            staticPage.getContent(),
            staticPage.getStaticPageId());
  }

  private static final String SQL_SELECT_STATICPAGE
          = "SELECT * FROM StaticPages WHERE staticPageId = ?;";

  @Override
  public StaticPage getStaticPageById(int staticPageId) {
    try {
      return jdbcTemplate.queryForObject(SQL_SELECT_STATICPAGE,
              new StaticPageMapper(), staticPageId);

    } catch (EmptyResultDataAccessException ex) {
      return null;
    }
  }

  private static final String SQL_SELECT_ALL_STATICPAGES
          = "SELECT * FROM StaticPages;";

  @Override
  public List<StaticPage> getAllStaticPages() {
    return jdbcTemplate.query(SQL_SELECT_ALL_STATICPAGES, new StaticPageMapper());
  }
}
