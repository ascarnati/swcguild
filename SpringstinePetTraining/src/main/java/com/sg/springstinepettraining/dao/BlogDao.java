/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogDao {

    public int addBlog(Blog blog);

    public void deleteBlog(int blogId);

    public void updateBlog(Blog blog);

    public Blog getBlogById(int blogId);

    public List<Blog> getAllBlogs();

    public List<Blog> getBlogsByHashtagId(int hashtagId);

    public List<Blog> getBlogsByCategoryId(int categoryId);

    public List<Blog> getFiveMostRecentBlogs();

    public List<Blog> getAllApprovedBlogs();

    public List<Blog> getBlogsByUser(String user);

}
