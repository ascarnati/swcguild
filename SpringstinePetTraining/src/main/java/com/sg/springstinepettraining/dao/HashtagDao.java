/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Hashtag;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface HashtagDao {

    public int addHashtag (Hashtag hashtag);

    public void deleteHashtag(int hashtagId);

    public void updateHashtag(Hashtag hashtag);

    public Hashtag getHashtagById(int hashtagId);

    public List<Hashtag> getAllHashtags();

    public List<Hashtag> getHashtagByBlogId(int blogId);
    
    public List<Hashtag> getAllHashtagsForApprovedBlogs();

}
