/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Comment;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CommentDao {

  public int addComment(Comment comment);

  public void deleteComment(int commentId);

  public void updateComment(Comment comment);

  public Comment getCommentById(int commentId);

  public List<Comment> getAllComments();

  public List<Comment> getCommentsByBlogId(int blogId);
}
