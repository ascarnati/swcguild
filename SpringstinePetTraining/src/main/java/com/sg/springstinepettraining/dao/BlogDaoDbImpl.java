/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class BlogDaoDbImpl implements BlogDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // Mappers
    private static final class BlogMapper implements RowMapper<Blog> {

        @Override
        public Blog mapRow(ResultSet rs, int rowNum) throws SQLException {
            Blog b = new Blog();
            b.setBlogId(rs.getInt("blogId"));
            b.setStatus(rs.getString("status"));
            b.setTitle(rs.getString("title"));
            b.setAuthor(rs.getString("author"));
            b.setContent(rs.getString("content"));
            b.setComments(rs.getString("comments"));
            b.setCreateDate(rs.getTimestamp("createDate").
                    toLocalDateTime().toLocalDate());
            b.setExpirationDate(rs.getTimestamp("expirationDate").
                    toLocalDateTime().toLocalDate());
            b.setPublishDate(rs.getTimestamp("publishDate").
                    toLocalDateTime().toLocalDate());

            return b;
        }
    }

    private static final class HashtagMapper implements RowMapper<Hashtag> {

        @Override
        public Hashtag mapRow(ResultSet rs, int rowNum) throws SQLException {
            Hashtag h = new Hashtag();
            h.setHashtagId(rs.getInt("hashtagId"));
            h.setName(rs.getString("name"));

            return h;
        }
    }

    private static final class CategoryMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
            Category c = new Category();
            c.setCategoryId(rs.getInt("categoryId"));
            c.setName(rs.getString("name"));

            return c;
        }
    }

    private static final String SQL_INSERT_BLOG
            = "insert into Blogs (status, title, author, content, comments, "
            + "expirationDate, publishDate, categoryId) "
            + "values (?, ?, ?, ?, ?, ?, ?, ?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int addBlog(Blog blog) {
        jdbcTemplate.update(SQL_INSERT_BLOG,
                blog.getStatus(),
                blog.getTitle(),
                blog.getAuthor(),
                blog.getContent(),
                blog.getComments(),
                //blog.getCreateDate().toString(),
                blog.getExpirationDate().toString(),
                blog.getPublishDate().toString(),
                blog.getCategory().getCategoryId());

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        blog.setBlogId(id);

        // add to blog-hashtag bride table
        insertBlogsHashtagsBridge(blog);

        return id;
    }

    private static final String SQL_DELETE_BLOG_HASHTAG
            = "delete from Blogs_Hashtags where blogId = ?;";

    private static final String SQL_DELETE_COMMENTS_BY_BLOG
            = "delete from Comments where blogId = ?;";

    private static final String SQL_DELETE_BLOG
            = "delete from Blogs where blogId = ?;";

    @Override
    public void deleteBlog(int blogId) {
        // delete blog-hashtag relationships for this blog
        jdbcTemplate.update(SQL_DELETE_BLOG_HASHTAG, blogId);
        // delete comments blog is associated with
        jdbcTemplate.update(SQL_DELETE_COMMENTS_BY_BLOG, blogId);
        // delete blog
        jdbcTemplate.update(SQL_DELETE_BLOG, blogId);
    }

    private static final String SQL_UPDATE_BLOG
            = "update Blogs set status = ?, title = ?, author = ?, content = ?, "
            + "comments = ?, createDate = ?, expirationDate = ?, publishDate = ?, "
            + "categoryId = ? where blogId = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateBlog(Blog blog) {
        jdbcTemplate.update(SQL_UPDATE_BLOG,
                blog.getStatus(),
                blog.getTitle(),
                blog.getAuthor(),
                blog.getContent(),
                blog.getComments(),
                blog.getCreateDate().toString(),
                blog.getExpirationDate().toString(),
                blog.getPublishDate().toString(),
                blog.getCategory().getCategoryId(),
                blog.getBlogId());

        // delete blog-hashtag relationships for this blog
        jdbcTemplate.update(SQL_DELETE_BLOG_HASHTAG, blog.getBlogId());

        // add to blog-hashtag bride table
        insertBlogsHashtagsBridge(blog);
    }

    private static final String SQL_SELECT_BLOG
            = "select * from Blogs where blogId = ?;";

    private static final String SQL_SELECT_ALL_HASHTAGS_BY_BLOG_ID
            = "select h.* from Hashtags h "
            + "join Blogs_Hashtags bh on bh.hashtagId = h.hashtagID "
            + "where bh.blogId = ?;";

    @Override
    public Blog getBlogById(int blogId) {
        try {
            Blog blog = jdbcTemplate.queryForObject(SQL_SELECT_BLOG, new BlogMapper(),
                    blogId);

            // query bridge table for list of hashtags
            List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_ALL_HASHTAGS_BY_BLOG_ID,
                    new HashtagMapper(), blogId);
            blog.setHashtags(hashtags);

            // find category of blog
            Category category = findCategoryForBlog(blog);
            blog.setCategory(category);

            return blog;

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_SELECT_ALL_BLOGS
            = "select * from Blogs;";

    @Override
    public List<Blog> getAllBlogs() {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_ALL_BLOGS, new BlogMapper());

        associateBlogsWithCategoryAndHashtags(blogs);

        return blogs;
    }

    private static final String SQL_SELECT_ALL_APPROVED_BLOGS
            = "select * from Blogs where status = 'approved' "
            + "AND NOW() BETWEEN publishDate AND expirationDate "
            + "ORDER BY publishDate;";

    @Override
    public List<Blog> getAllApprovedBlogs() {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_ALL_APPROVED_BLOGS, new BlogMapper());

        associateBlogsWithCategoryAndHashtags(blogs);

        return blogs;
    }

    private static final String SQL_SELECT_APPROVED_BLOGS_BY_HASHTAG_ID
            = "select b.* from Blogs b "
            + "join Blogs_Hashtags bh on b.blogId = bh.blogId "
            + "where bh.hashtagId  =  ? "
            + "and b.status = 'approved' "
            + "AND NOW() BETWEEN b.publishDate AND b.expirationDate "
            + "ORDER BY b.publishDate;";

    @Override
    public List<Blog> getBlogsByHashtagId(int hashtagId) {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_APPROVED_BLOGS_BY_HASHTAG_ID,
                new BlogMapper(), hashtagId);

        associateBlogsWithCategoryAndHashtags(blogs);

        return blogs;
    }

    private static final String SQL_SELECT_APPROVED_BLOGS_BY_CATEGORY_ID
            = "select * from Blogs where categoryId = ? and status = 'approved' "
            + "AND NOW() BETWEEN publishDate AND expirationDate "
            + "ORDER BY publishDate;";

    @Override
    public List<Blog> getBlogsByCategoryId(int categoryId) {
        List<Blog> blogs
                = jdbcTemplate.query(SQL_SELECT_APPROVED_BLOGS_BY_CATEGORY_ID,
                        new BlogMapper(), categoryId);

        associateBlogsWithCategoryAndHashtags(blogs);

        return blogs;
    }

    // Helper Methods 
    private static final String SQL_INSERT_BLOGS_HASHTAGS
            = "insert into Blogs_Hashtags (blogId, hashtagId) values(?, ?);";

    private void insertBlogsHashtagsBridge(Blog blog) {
        final int blogId = blog.getBlogId();
        final List<Hashtag> hashtags = blog.getHashtags();

        // Update the blog-hashtag bridge table with an entry for 
        // each hashtag for this blog
        if (hashtags == null) {

        } else {
            for (Hashtag currentHashtag : hashtags) {
                jdbcTemplate.update(SQL_INSERT_BLOGS_HASHTAGS,
                        blogId, currentHashtag.getHashtagId());
            }
        }
    }

    private static final String SQL_SELECT_CATEGORY_BY_BLOG_ID
            = "select c.* from Categories c "
            + "join Blogs b on c.categoryId = b.categoryId where "
            + "b.blogId = ?;";

    private Category findCategoryForBlog(Blog blog) {
        Category category
                = jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY_BY_BLOG_ID,
                        new CategoryMapper(), blog.getBlogId());
        return category;
    }

    private List<Blog> associateBlogsWithCategoryAndHashtags(List<Blog> blogs) {

        for (Blog currentBlog : blogs) {
            // query bridge table for list of hashtags
            List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_ALL_HASHTAGS_BY_BLOG_ID,
                    new HashtagMapper(), currentBlog.getBlogId());
            currentBlog.setHashtags(hashtags);

            // find category of blog
            Category category = findCategoryForBlog(currentBlog);
            currentBlog.setCategory(category);
        }

        return blogs;
    }

    private static final String SQL_SELECT_BLOGS_BY_DATE_RECENT
            = "SELECT b.* FROM Blogs b WHERE b.status = 'approved' "
            + "AND NOW() BETWEEN b.publishDate AND b.expirationDate "
            + "ORDER BY b.createDate DESC LIMIT 5;";

    @Override
    public List<Blog> getFiveMostRecentBlogs() {
        List<Blog> blogs = jdbcTemplate.query(SQL_SELECT_BLOGS_BY_DATE_RECENT, new BlogMapper());

        associateBlogsWithCategoryAndHashtags(blogs);

        return blogs;
    }

    private static final String SQL_SELECT_BLOGS_BY_USER
            = "SELECT b.* FROM Blogs b WHERE b.author = ? "
            + "ORDER BY b.publishDate;";

    @Override
    public List<Blog> getBlogsByUser(String user) {
        List<Blog> userBlogs = jdbcTemplate.query(SQL_SELECT_BLOGS_BY_USER, new BlogMapper(), user);

        associateBlogsWithCategoryAndHashtags(userBlogs);

        return userBlogs;
    }

}
