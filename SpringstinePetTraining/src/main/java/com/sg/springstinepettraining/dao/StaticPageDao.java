/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.StaticPage;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface StaticPageDao {

  public int addStaticPage(StaticPage staticPage);

  public void deleteStaticPage(int staticPageId);

  public void updateStaticPage(StaticPage staticPage);

  public StaticPage getStaticPageById(int staticPageId);

  public List<StaticPage> getAllStaticPages();

}
