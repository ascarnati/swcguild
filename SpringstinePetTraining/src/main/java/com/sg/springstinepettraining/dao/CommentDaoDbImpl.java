/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.dao;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Comment;
import com.sg.springstinepettraining.model.Hashtag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CommentDaoDbImpl implements CommentDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // Mappers
    private static final class CommentMapper implements RowMapper<Comment> {

        @Override
        public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
            Comment c = new Comment();
            c.setCommentId(rs.getInt("commentId"));
            c.setAuthor(rs.getString("author"));
            c.setCreateDate(rs.getTimestamp("createDate").
                    toLocalDateTime().toLocalDate());
            c.setComment(rs.getString("comment"));

            return c;
        }
    }

    private static final class BlogMapper implements RowMapper<Blog> {

        @Override
        public Blog mapRow(ResultSet rs, int rowNum) throws SQLException {
            Blog b = new Blog();
            b.setBlogId(rs.getInt("blogId"));
            b.setStatus(rs.getString("status"));
            b.setTitle(rs.getString("title"));
            b.setAuthor(rs.getString("author"));
            b.setContent(rs.getString("content"));
            b.setComments(rs.getString("comments"));
            b.setCreateDate(rs.getTimestamp("createDate").
                    toLocalDateTime().toLocalDate());
            b.setExpirationDate(rs.getTimestamp("expirationDate").
                    toLocalDateTime().toLocalDate());
            b.setPublishDate(rs.getTimestamp("publishDate").
                    toLocalDateTime().toLocalDate());

            return b;
        }
    }

    private static final class HashtagMapper implements RowMapper<Hashtag> {

        @Override
        public Hashtag mapRow(ResultSet rs, int rowNum) throws SQLException {
            Hashtag h = new Hashtag();
            h.setHashtagId(rs.getInt("hashtagId"));
            h.setName(rs.getString("name"));

            return h;
        }
    }

    private static final class CategoryMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
            Category c = new Category();
            c.setCategoryId(rs.getInt("categoryId"));
            c.setName(rs.getString("name"));

            return c;
        }
    }

    private static final String SQL_INSERT_COMMENT
            = "insert into Comments (author, comment, blogId) "
            + "values (?, ?, ?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public int addComment(Comment comment) {
        jdbcTemplate.update(SQL_INSERT_COMMENT,
                comment.getAuthor(),
                comment.getComment(),
                comment.getBlog().getBlogId());

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        comment.setCommentId(id);

        return id;
    }

    private static final String SQL_DELETE_COMMENT
            = "delete from Comments where commentId = ?;";

    @Override
    public void deleteComment(int commentId) {
        // delete comment
        jdbcTemplate.update(SQL_DELETE_COMMENT, commentId);
    }

    private static final String SQL_UPDATE_COMMENT
            = "update Comments set author = ?, createDate = ?, comment = ?, "
            + "blogId = ? where commentId = ?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateComment(Comment comment) {
        jdbcTemplate.update(SQL_UPDATE_COMMENT,
                comment.getAuthor(),
                comment.getCreateDate().toString(),
                comment.getComment(),
                comment.getBlog().getBlogId(),
                comment.getCommentId());
    }

    private static final String SQL_SELECT_COMMENT
            = "select * from Comments where commentId = ?;";

    @Override
    public Comment getCommentById(int commentId) {
        try {
            Comment comment = jdbcTemplate.queryForObject(SQL_SELECT_COMMENT,
                    new CommentMapper(), commentId);

            // find blog for comment
            findBlogForComment(comment);

            return comment;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_SELECT_ALL_COMMENTS
            = "select * from Comments;";

    @Override
    public List<Comment> getAllComments() {
        List<Comment> comments = jdbcTemplate.query(SQL_SELECT_ALL_COMMENTS,
                new CommentMapper());

        for (Comment currentComment : comments) {
            findBlogForComment(currentComment);
        }

        return comments;
    }

    private static final String SQL_SELECT_COMMENTS_BY_BLOG_ID
            = "select * from Comments where blogId = ?;";

    @Override
    public List<Comment> getCommentsByBlogId(int blogId) {
        List<Comment> comments = jdbcTemplate.query(SQL_SELECT_COMMENTS_BY_BLOG_ID,
                new CommentMapper(), blogId);

        for (Comment currentComment : comments) {
            findBlogForComment(currentComment);
        }

        return comments;
    }

    private static final String SQL_SELECT_BLOG_BY_COMMENT_ID
            = "select b.* from Blogs b "
            + "join Comments c on c.blogId = b.blogId where "
            + "c.commentId = ?;";

    private static final String SQL_SELECT_ALL_HASHTAGS_BY_BLOG_ID
            = "select h.* from Hashtags h "
            + "join Blogs_Hashtags bh on bh.hashtagId = h.hashtagID "
            + "where bh.blogId = ?;";

    private Blog findBlogForComment(Comment comment) {
        Blog blog = jdbcTemplate.queryForObject(SQL_SELECT_BLOG_BY_COMMENT_ID,
                new BlogMapper(), comment.getCommentId());

        //query bride table for hashtags
        List<Hashtag> hashtags = jdbcTemplate.query(SQL_SELECT_ALL_HASHTAGS_BY_BLOG_ID,
                new HashtagMapper(), blog.getBlogId());
        blog.setHashtags(hashtags);

        // find category of blog
        Category category = findCategoryForBlog(blog);
        blog.setCategory(category);

        comment.setBlog(blog);

        return blog;
    }

    private static final String SQL_SELECT_CATEGORY_BY_BLOG_ID
            = "select c.* from Categories c "
            + "join Blogs b on c.categoryId = b.categoryId where "
            + "b.blogId = ?;";

    private Category findCategoryForBlog(Blog blog) {
        Category category
                = jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY_BY_BLOG_ID,
                        new CategoryMapper(), blog.getBlogId());
        return category;
    }

}
