/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.service;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Comment;
import com.sg.springstinepettraining.model.Hashtag;
import com.sg.springstinepettraining.model.StaticPage;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface PetTrainerServiceLayer {

    public int addBlog(Blog blog);

    public void deleteBlog(int blogId);

    public void updateBlog(Blog blog);

    public Blog getBlogById(int blogId);

    public List<Blog> getAllBlogs();

    public List<Blog> getBlogsByHashtagId(int hashtagId);

    public List<Blog> getBlogsByCategoryId(int categoryId);

    public int addCategory(Category category);

    public void deleteCategory(int categoryId);

    public void updateCategory(Category category);

    public Category getCategoryById(int categoryId);

    public List<Category> getAllCategorys();

    public int addHashtag(Hashtag hashtag);

    public void deleteHashtag(int hashtagId);

    public void updateHashtag(Hashtag hashtag);

    public Hashtag getHashtagById(int hashtagId);

    public List<Hashtag> getAllHashtags();

    public List<Hashtag> getHashtagByBlogId(int blogId);

    public int addStaticPage(StaticPage staticPage);

    public void deleteStaticPage(int staticPageId);

    public void updateStaticPage(StaticPage staticPage);

    public StaticPage getStaticPageById(int staticPageId);

    public List<StaticPage> getAllStaticPages();

    public int addComment(Comment comment);

    public void deleteComment(int commentId);

    public void updateComment(Comment comment);

    public Comment getCommentById(int commentId);

    public List<Comment> getAllComments();

    public List<Comment> getCommentsByBlogId(int blogId);

    public List<Blog> getFiveMostRecentBlogs();

    public List<Blog> getAllApprovedBlogs();

    public List<Hashtag> getAllHashtagsForApprovedBlogs();

    public List<Category> getAllCategorysForApprovedBlogs();

    public List<Blog> getBlogsByUser(String user);
}
