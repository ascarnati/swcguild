/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.service;

import com.sg.springstinepettraining.dao.BlogDao;
import com.sg.springstinepettraining.dao.CategoryDao;
import com.sg.springstinepettraining.dao.CommentDao;
import com.sg.springstinepettraining.dao.HashtagDao;
import com.sg.springstinepettraining.dao.StaticPageDao;
import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Comment;
import com.sg.springstinepettraining.model.Hashtag;
import com.sg.springstinepettraining.model.StaticPage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;

/**
 *
 * @author apprentice
 */
public class PetTrainerServiceLayerImpl implements PetTrainerServiceLayer {

    private BlogDao blogDao;
    private HashtagDao hashtagDao;
    private CategoryDao categoryDao;
    private StaticPageDao staticPageDao;
    private CommentDao commentDao;

    @Inject
    public PetTrainerServiceLayerImpl(BlogDao blogDao, HashtagDao hashtagDao,
            CategoryDao categoryDao, StaticPageDao staticPageDao, CommentDao commentDao) {
        this.blogDao = blogDao;
        this.hashtagDao = hashtagDao;
        this.categoryDao = categoryDao;
        this.staticPageDao = staticPageDao;
        this.commentDao = commentDao;
    }

    @Override
    public int addBlog(Blog blog) {
        // #       - A literal #
        // (       - Start of capture group
        // \\w+    - One or more word characters
        // )       - End of capture group
        Pattern MY_PATTERN = Pattern.compile("#(\\w+)");
        // An engine that performs match operations on a character sequence
        // by interpreting a Pattern.
        Matcher mat = MY_PATTERN.matcher(blog.getContent());

        List<String> hashtags = new ArrayList<String>();

        while (mat.find()) {
            hashtags.add(mat.group(1));
        }

        // create set for all hashtags in Db
        Set<String> currentHashtagSet = new HashSet<>();
        // turn all hashtags in Db to set of Strings to be able to do compare
        for (Hashtag currentHashtag : getAllHashtags()) {
            currentHashtagSet.add(currentHashtag.getName());
        }

        // create a hashtag object if one does not already exist for that string
        for (String currentHashtag : hashtags) {
            if (currentHashtagSet.contains(currentHashtag)) {
                // do nothing
            } else {
                Hashtag h = new Hashtag();
                h.setName(currentHashtag);
                addHashtag(h);
            }
        }

        // create empty list that will hold hashtags to add to blog
        List<Hashtag> listForBlog = new ArrayList<>();

        // create list of unique hashtags for the blog
        Set<String> blogHashtagSet = new HashSet<>();
        for (String h : hashtags) {
            blogHashtagSet.add(h);
        }

        // loop through all hashtags and if name of hashtag equals name in set,
        // then add to listForBlog
        for (Hashtag h : getAllHashtags()) {
            if (blogHashtagSet.contains(h.getName())) {
                listForBlog.add(h);
            }
        }

        // add list of hashtags to blog
        blog.setHashtags(listForBlog);

        return blogDao.addBlog(blog);
    }

    @Override
    public void deleteBlog(int blogId) {
        blogDao.deleteBlog(blogId);
    }

    @Override
    public void updateBlog(Blog blog) {
        // #       - A literal #
        // (       - Start of capture group
        // \\w+    - One or more word characters
        // )       - End of capture group
        Pattern MY_PATTERN = Pattern.compile("#(\\w+)");
        // An engine that performs match operations on a character sequence
        // by interpreting a Pattern.
        Matcher mat = MY_PATTERN.matcher(blog.getContent());

        List<String> hashtags = new ArrayList<String>();

        while (mat.find()) {
            hashtags.add(mat.group(1));
        }

        // create set for all hashtags in Db
        Set<String> currentHashtagSet = new HashSet<>();
        // turn all hashtags in Db to set of Strings to be able to do compare
        for (Hashtag currentHashtag : getAllHashtags()) {
            currentHashtagSet.add(currentHashtag.getName());
        }

        // create a hashtag object if one does not already exist for that string
        for (String currentHashtag : hashtags) {
            if (currentHashtagSet.contains(currentHashtag)) {
                // do nothing
            } else {
                Hashtag h = new Hashtag();
                h.setName(currentHashtag);
                addHashtag(h);
            }
        }

        // create empty list that will hold hashtags to add to blog
        List<Hashtag> listForBlog = new ArrayList<>();

        // create list of unique hashtags for the blog
        Set<String> blogHashtagSet = new HashSet<>();
        for (String h : hashtags) {
            blogHashtagSet.add(h);
        }

        // loop through all hashtags and if name of hashtag equals name in set,
        // then add to listForBlog
        for (Hashtag h : getAllHashtags()) {
            if (blogHashtagSet.contains(h.getName())) {
                listForBlog.add(h);
            }
        }

        // add list of hashtags to blog
        blog.setHashtags(listForBlog);

        blogDao.updateBlog(blog);
    }

    @Override
    public Blog getBlogById(int blogId) {
        return blogDao.getBlogById(blogId);
    }

    @Override
    public List<Blog> getAllBlogs() {
        return blogDao.getAllBlogs();
    }

    @Override
    public List<Blog> getBlogsByHashtagId(int hashtagId) {
        return blogDao.getBlogsByHashtagId(hashtagId);
    }

    @Override
    public List<Blog> getBlogsByCategoryId(int categoryId) {
        return blogDao.getBlogsByCategoryId(categoryId);
    }

    @Override
    public int addCategory(Category category) {
        return categoryDao.addCategory(category);
    }

    @Override
    public void deleteCategory(int categoryId) {
        categoryDao.deleteCategory(categoryId);
    }

    @Override
    public void updateCategory(Category category) {
        categoryDao.updateCategory(category);
    }

    @Override
    public Category getCategoryById(int categoryId) {
        return categoryDao.getCategoryById(categoryId);
    }

    @Override
    public List<Category> getAllCategorys() {
        return categoryDao.getAllCategorys();
    }

    @Override
    public int addHashtag(Hashtag hashtag) {
        return hashtagDao.addHashtag(hashtag);
    }

    @Override
    public void deleteHashtag(int hashtagId) {
        hashtagDao.deleteHashtag(hashtagId);
    }

    @Override
    public void updateHashtag(Hashtag hashtag) {
        hashtagDao.updateHashtag(hashtag);
    }

    @Override
    public Hashtag getHashtagById(int hashtagId) {
        return hashtagDao.getHashtagById(hashtagId);
    }

    @Override
    public List<Hashtag> getAllHashtags() {
        return hashtagDao.getAllHashtags();
    }

    @Override
    public List<Hashtag> getHashtagByBlogId(int blogId) {
        return hashtagDao.getHashtagByBlogId(blogId);
    }

    @Override
    public int addStaticPage(StaticPage staticPage) {
        return staticPageDao.addStaticPage(staticPage);
    }

    @Override
    public void deleteStaticPage(int staticPageId) {
        staticPageDao.deleteStaticPage(staticPageId);
    }

    @Override
    public void updateStaticPage(StaticPage staticPage) {
        staticPageDao.updateStaticPage(staticPage);
    }

    @Override
    public StaticPage getStaticPageById(int staticPageId) {
        return staticPageDao.getStaticPageById(staticPageId);
    }

    @Override
    public List<StaticPage> getAllStaticPages() {
        return staticPageDao.getAllStaticPages();
    }

    @Override
    public int addComment(Comment comment) {
        return commentDao.addComment(comment);
    }

    @Override
    public void deleteComment(int commentId) {
        commentDao.deleteComment(commentId);
    }

    @Override
    public void updateComment(Comment comment) {
        commentDao.updateComment(comment);
    }

    @Override
    public Comment getCommentById(int commentId) {
        return commentDao.getCommentById(commentId);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public List<Comment> getCommentsByBlogId(int blogId) {
        return commentDao.getCommentsByBlogId(blogId);
    }

    @Override
    public List<Blog> getFiveMostRecentBlogs() {
        return blogDao.getFiveMostRecentBlogs();
    }

    @Override
    public List<Blog> getAllApprovedBlogs() {
        return blogDao.getAllApprovedBlogs();
    }

    @Override
    public List<Hashtag> getAllHashtagsForApprovedBlogs() {
        return hashtagDao.getAllHashtagsForApprovedBlogs();
    }

    @Override
    public List<Category> getAllCategorysForApprovedBlogs() {
        return categoryDao.getAllCategorysForApprovedBlogs();
    }

    @Override
    public List<Blog> getBlogsByUser(String user) {
        return blogDao.getBlogsByUser(user);
    }

}
