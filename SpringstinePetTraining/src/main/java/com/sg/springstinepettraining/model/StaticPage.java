/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.model;

import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class StaticPage {

  private int staticPageId;
  private String pageType;
  private String title;
  private String content;

  public int getStaticPageId() {
    return staticPageId;
  }

  public void setStaticPageId(int staticPageId) {
    this.staticPageId = staticPageId;
  }

  public String getPageType() {
    return pageType;
  }

  public void setPageType(String pageType) {
    this.pageType = pageType;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 67 * hash + this.staticPageId;
    hash = 67 * hash + Objects.hashCode(this.pageType);
    hash = 67 * hash + Objects.hashCode(this.title);
    hash = 67 * hash + Objects.hashCode(this.content);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final StaticPage other = (StaticPage) obj;
    if (this.staticPageId != other.staticPageId) {
      return false;
    }
    if (!Objects.equals(this.pageType, other.pageType)) {
      return false;
    }
    if (!Objects.equals(this.title, other.title)) {
      return false;
    }
    if (!Objects.equals(this.content, other.content)) {
      return false;
    }
    return true;
  }
}
