/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.controller;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import com.sg.springstinepettraining.model.StaticPage;
import com.sg.springstinepettraining.service.PetTrainerServiceLayer;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class MainController {

  private PetTrainerServiceLayer service;

  @Inject
  public MainController(PetTrainerServiceLayer service) {
    this.service = service;
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String displayHomePage(Model model) {

    // Get all the Blogs from the ServiceLayer
    List<Blog> blogList = service.getAllBlogs();
    List<Hashtag> hashtags = service.getAllHashtags();
    List<Category> categories = service.getAllCategorys();
    List<Blog> recentBlogs = service.getFiveMostRecentBlogs();
    List<StaticPage> pageList = service.getAllStaticPages();

    //Put the List of Blogs on the Model
    model.addAttribute("blogList", blogList);
    model.addAttribute("hashtags", hashtags);
    model.addAttribute("categories", categories);
    model.addAttribute("recentBlogs", recentBlogs);
    model.addAttribute("pageList", pageList);

    // Return the name of our View
    return "home";
  }

  @RequestMapping(value = "/displayAdminPage", method = RequestMethod.GET)
  public String displayAdminPage(Model model) {

    // Get all the Blogs from the ServiceLayer
    List<Blog> blogList = service.getAllBlogs();
    List<Hashtag> hashtags = service.getAllHashtags();
    List<Category> categories = service.getAllCategorys();
    List<StaticPage> pageList = service.getAllStaticPages();

    //Put the List of Blogs on the Model
    model.addAttribute("blogList", blogList);
    model.addAttribute("hashtags", hashtags);
    model.addAttribute("categories", categories);
    model.addAttribute("pageList", pageList);

    // Return the name of our View
    return "admin";
  }

  @RequestMapping(value = "/displayLoginPage", method = RequestMethod.GET)
  public String displayLoginPage(Model model) {

    // Get all the Blogs from the ServiceLayer
    List<Blog> blogList = service.getAllBlogs();
    List<Hashtag> hashtags = service.getAllHashtags();
    List<Category> categories = service.getAllCategorys();
    List<StaticPage> pageList = service.getAllStaticPages();

    //Put the List of Blogs on the Model
    model.addAttribute("blogList", blogList);
    model.addAttribute("hashtags", hashtags);
    model.addAttribute("categories", categories);
    model.addAttribute("pageList", pageList);

    // Return the name of our View
    return "login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String showLoginForm() {
    return "login";
  }
}
