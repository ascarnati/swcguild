/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.controller;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import com.sg.springstinepettraining.model.StaticPage;
import com.sg.springstinepettraining.service.PetTrainerServiceLayer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class CategoryController {

    private PetTrainerServiceLayer service;

    @Inject
    public CategoryController(PetTrainerServiceLayer service) {
        this.service = service;
    }

    @RequestMapping(value = "/blogsByCategory", method = RequestMethod.GET)
    public String blogsByCategory(HttpServletRequest request, Model model) {

        try {
            String categoryIdParameter = request.getParameter("categoryId");
            int id = Integer.parseInt(categoryIdParameter);

            List<StaticPage> pageList = service.getAllStaticPages();
            model.addAttribute("pageList", pageList);

            List<Blog> blogList = service.getBlogsByCategoryId(id);
            model.addAttribute("blogList", blogList);

            List<Category> categoryList = service.getAllCategorysForApprovedBlogs();
            model.addAttribute("categoryList", categoryList);

            Set<Category> categorySet = new HashSet<>();
            for (Category currentCat : categoryList) {
                categorySet.add(currentCat);
            }

            // unique list
            List<Category> categories = new ArrayList<>();
            for (Category currentCat : categorySet) {
                categories.add(currentCat);
            }

            model.addAttribute("categories", categories);

            List<Hashtag> hashtagList = service.getAllHashtagsForApprovedBlogs();

            Set<Hashtag> hashtagSet = new HashSet<>();
            for (Hashtag currentHash : hashtagList) {
                hashtagSet.add(currentHash);
            }

            // unique list
            List<Hashtag> hashtags = new ArrayList<>();
            for (Hashtag currentHash : hashtagSet) {
                hashtags.add(currentHash);
            }

            model.addAttribute("hashtags", hashtags);

            return "allBlogs";

        } catch (NumberFormatException e) {
            return "redirect:displayAllBlogsPage";
        }

    }

}
