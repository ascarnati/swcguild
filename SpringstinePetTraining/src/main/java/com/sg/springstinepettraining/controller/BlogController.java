/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.controller;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Hashtag;
import com.sg.springstinepettraining.model.StaticPage;
import com.sg.springstinepettraining.service.PetTrainerServiceLayer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Anthony
 */
@Controller
public class BlogController {

    private PetTrainerServiceLayer service;

    @Inject
    public BlogController(PetTrainerServiceLayer service) {
        this.service = service;
    }

    @RequestMapping(value = "/displayAllBlogsPage", method = RequestMethod.GET)
    public String displayAllBlogsPage(HttpServletRequest request, Model model) {
        // Get username for blog entry page
        String blogAuthor = request.getParameter("blogAuthor");
        model.addAttribute("blogAuthor", blogAuthor);

        // Get all the Blogs from the ServiceLayer
        List<Blog> blogList = service.getAllApprovedBlogs();

        List<Hashtag> hashtagList = service.getAllHashtagsForApprovedBlogs();

        Set<Hashtag> hashtagSet = new HashSet<>();
        for (Hashtag currentHash : hashtagList) {
            hashtagSet.add(currentHash);
        }

        // unique list
        List<Hashtag> hashtags = new ArrayList<>();
        for (Hashtag currentHash : hashtagSet) {
            hashtags.add(currentHash);
        }

        model.addAttribute("hashtags", hashtags);

        List<Category> categoryList = service.getAllCategorysForApprovedBlogs();
        model.addAttribute("categoryList", categoryList);

        Set<Category> categorySet = new HashSet<>();
        for (Category currentCat : categoryList) {
            categorySet.add(currentCat);
        }

        // unique list
        List<Category> categories = new ArrayList<>();
        for (Category currentCat : categorySet) {
            categories.add(currentCat);
        }

        model.addAttribute("categories", categories);

        List<StaticPage> pageList = service.getAllStaticPages();

        //Put the List of Blogs on the Model
        model.addAttribute("blogList", blogList);
        model.addAttribute("pageList", pageList);

        // Return the name of our View
        return "allBlogs";
    }

    @RequestMapping(value = "/displayBlogEntryPage", method = RequestMethod.GET)
    public String displayBlogEntryPage(HttpServletRequest request, Model model) {
        String user = request.getParameter("user");
        List<Blog> blogs = service.getBlogsByUser(user);
        List<StaticPage> pageList = service.getAllStaticPages();
        model.addAttribute("blogs", blogs);
        model.addAttribute("pageList", pageList);

        List<Category> categoryList = service.getAllCategorysForApprovedBlogs();
        model.addAttribute("categoryList", categoryList);

        Set<Category> categorySet = new HashSet<>();
        for (Category currentCat : categoryList) {
            categorySet.add(currentCat);
        }

        // unique list
        List<Category> categories = new ArrayList<>();
        for (Category currentCat : categorySet) {
            categories.add(currentCat);
        }

        model.addAttribute("categories", categories);
        // Return the name of our View
        return "blogEntry";

    }

    @RequestMapping(value = "/saveBlog", method = RequestMethod.POST)
    public String saveBlog(HttpServletRequest request) {
        // setStatus is the only diff b/w saveBlog and submitBlog
        Blog blog = new Blog();
        blog.setTitle(request.getParameter("blogTitle"));
        blog.setAuthor(request.getParameter("blogAuthor"));

        String publishDateParameter = request.getParameter("publishDate");
        LocalDate publishDate = LocalDate.parse(publishDateParameter, DateTimeFormatter.ISO_DATE);
        blog.setPublishDate(publishDate);

        String expirationDateParameter = request.getParameter("expirationDate");
        LocalDate expirationDate = LocalDate.parse(expirationDateParameter, DateTimeFormatter.ISO_DATE);
        blog.setExpirationDate(expirationDate);

        String blogCategoryParameter = request.getParameter("blogCategory");
        int categoryId = Integer.parseInt(blogCategoryParameter);
        Category category = service.getCategoryById(categoryId);
        blog.setCategory(category);

        blog.setContent(request.getParameter("blogContent"));

        // On SAVE, set status of entry to "Pending Approval"
        blog.setStatus("Pending Approval");

        service.addBlog(blog);

        return "redirect:displayAllBlogsPage";
    }

    @RequestMapping(value = "/deleteBlog", method = RequestMethod.GET)
    public String deleteBlog(HttpServletRequest request) {
        String blogIdParameter = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdParameter);
        service.deleteBlog(blogId);
        return "redirect:displayAdminPage";
    }

    @RequestMapping(value = "/displayBlogDetails", method = RequestMethod.GET)
    public String displayBlogDetails(HttpServletRequest request, Model model) {
        
        try {
        String blogIdParameter = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdParameter);
        List<StaticPage> pageList = service.getAllStaticPages();

        Blog blog = service.getBlogById(blogId);
        model.addAttribute("blog", blog);
        model.addAttribute("pageList", pageList);

        //build set of status
        List<String> statusList = new ArrayList<>();

        String blogStatus = blog.getStatus();
        if (blogStatus.equalsIgnoreCase("Pending Approval")) {
            statusList.add("Approved");
            statusList.add("Denied");
        } else if (blogStatus.equalsIgnoreCase("Approved")) {
            statusList.add("Pending Approval");
            statusList.add("Denied");
        } else {
            statusList.add("Pending Approval");
            statusList.add("Approved");
        }

        // list of statuses for jsp
        model.addAttribute("statusList", statusList);

        return "blogDetails";
        
        } catch (NumberFormatException | NullPointerException e){
            return "redirect:displayAllBlogsPage";
        }
        
    }

    @RequestMapping(value = "/adminUpdateBlog", method = RequestMethod.POST)
    public String adminUpdateBlog(HttpServletRequest request) {
        String blogIdParameter = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdParameter);
        Blog blog = service.getBlogById(blogId);

        blog.setStatus(request.getParameter("updateStatus"));

        String pubDateParam = request.getParameter("publishDate");
        LocalDate pubDate = LocalDate.parse(pubDateParam, DateTimeFormatter.ISO_DATE);
        blog.setPublishDate(pubDate);

        String expDateParam = request.getParameter("expirationDate");
        LocalDate expDate = LocalDate.parse(expDateParam, DateTimeFormatter.ISO_DATE);
        blog.setExpirationDate(expDate);

        service.updateBlog(blog);

        return "redirect:displayAdminPage";
    }

    @RequestMapping(value = "/displayUserBlogDetails", method = RequestMethod.GET)
    public String displayUserBlogDetails(HttpServletRequest request, Model model) {
        String blogIdParameter = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdParameter);
        List<StaticPage> pageList = service.getAllStaticPages();

        Blog blog = service.getBlogById(blogId);
        model.addAttribute("blog", blog);
        model.addAttribute("pageList", pageList);

        //build set of status
        List<String> statusList = new ArrayList<>();

        String blogStatus = blog.getStatus();
        if (blogStatus.equalsIgnoreCase("Pending Approval")) {
        } else if (blogStatus.equalsIgnoreCase("Approved")) {
            statusList.add("Pending Approval");
        } else {
            statusList.add("Pending Approval");
        }

        // list of statuses for jsp
        model.addAttribute("statusList", statusList);

        return "blogDetailsForUser";
    }

    @RequestMapping(value = "/userUpdateBlog", method = RequestMethod.POST)
    public String userUpdateBlog(HttpServletRequest request, Model model) {
        String blogIdParameter = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdParameter);

        model.addAttribute("user", request.getParameter("user"));
        List<Blog> blogs = service.getBlogsByUser(request.getParameter("user"));

        model.addAttribute("blogs", blogs);

        Blog blog = service.getBlogById(blogId);

        blog.setContent(request.getParameter("blogContent"));

        blog.setTitle(request.getParameter("title"));

        blog.setStatus("Pending Approval");

        String pubDateParam = request.getParameter("publishDate");
        LocalDate pubDate = LocalDate.parse(pubDateParam, DateTimeFormatter.ISO_DATE);
        blog.setPublishDate(pubDate);

        String expDateParam = request.getParameter("expireDate");
        LocalDate expDate = LocalDate.parse(expDateParam, DateTimeFormatter.ISO_DATE);
        blog.setExpirationDate(expDate);

        service.updateBlog(blog);

        return "redirect:displayBlogEntryPage";
    }

}
