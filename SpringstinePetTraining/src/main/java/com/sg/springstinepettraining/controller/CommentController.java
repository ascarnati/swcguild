/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.controller;

import com.sg.springstinepettraining.model.Blog;
import com.sg.springstinepettraining.model.Category;
import com.sg.springstinepettraining.model.Comment;
import com.sg.springstinepettraining.model.Hashtag;
import com.sg.springstinepettraining.model.StaticPage;
import com.sg.springstinepettraining.service.PetTrainerServiceLayer;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class CommentController {

  private PetTrainerServiceLayer service;

  public CommentController(PetTrainerServiceLayer service) {
    this.service = service;
  }

  @RequestMapping(value = "/displayCommentForm", method = RequestMethod.GET)
  public String displayCommentForm(HttpServletRequest request, Model model) {
    int blogId;
    try {
      String blogIdParameter = request.getParameter("blogId");
      blogId = Integer.parseInt(blogIdParameter);
      Blog currentBlog = service.getBlogById(blogId);
      model.addAttribute("currentBlog", currentBlog);

    } catch (NumberFormatException | NullPointerException e) {
      return "redirect:allBlogs";
    }

    try {
      List<Comment> commentsForBlog = service.getCommentsByBlogId(blogId);
      model.addAttribute("commentsForBlog", commentsForBlog);
    } catch (NullPointerException e) {
      // do nothing
    }

    List<Hashtag> hashtags = service.getAllHashtagsForApprovedBlogs();
    List<Category> categories = service.getAllCategorysForApprovedBlogs();
    List<StaticPage> pageList = service.getAllStaticPages();

    model.addAttribute("hashtags", hashtags);
    model.addAttribute("categories", categories);
    model.addAttribute("pageList", pageList);

    return "comment";
  }

  @RequestMapping(value = "/addComment", method = RequestMethod.POST)
  public String addComment(HttpServletRequest request, Model model) {
    // get blog comment belongs to
    Blog b = service.getBlogById(Integer.parseInt(request.getParameter("blogId")));

    // create new comment, and set blog
    Comment c = new Comment();
    c.setBlog(b);
    c.setAuthor(request.getParameter("author"));
    c.setComment(request.getParameter("comment"));
    service.addComment(c);

    return "redirect:displayAllBlogsPage";
  }

}
