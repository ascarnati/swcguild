/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springstinepettraining.controller;

import com.sg.springstinepettraining.model.StaticPage;
import com.sg.springstinepettraining.service.PetTrainerServiceLayer;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class StaticPageController {

  private PetTrainerServiceLayer service;

  @Inject
  public StaticPageController(PetTrainerServiceLayer service) {
    this.service = service;
  }

  @RequestMapping(value = "/displayStaticPage", method = RequestMethod.GET)
  public String displayStaticPage(HttpServletRequest request, Model model) {

    try {
      String staticPageIdParameter = request.getParameter("staticPageId");
      int staticPageId = Integer.parseInt(staticPageIdParameter);

      List<StaticPage> pageList = service.getAllStaticPages();
      model.addAttribute("pageList", pageList);

      StaticPage staticPage = service.getStaticPageById(staticPageId);
      model.addAttribute("staticPage", staticPage);

      return "staticPage";

    } catch (NumberFormatException | NullPointerException e) {
      return "redirect:displayAllStaticPages";
    }
  }

  @RequestMapping(value = "/displayAllStaticPages", method = RequestMethod.GET)
  public String displayAllStaticPages(HttpServletRequest request, Model model) {

    List<StaticPage> pageList = service.getAllStaticPages();
    model.addAttribute("pageList", pageList);

    return "allStaticPages";
  }

  @RequestMapping(value = "/displayAdminStaticPages", method = RequestMethod.GET)
  public String displayAdminStaticPages(HttpServletRequest request, Model model) {

    List<StaticPage> pageList = service.getAllStaticPages();
    model.addAttribute("pageList", pageList);

    return "adminStaticPages";
  }

  @RequestMapping(value = "/displayCreateStaticPage", method = RequestMethod.GET)
  public String displayCreateStaticPage(HttpServletRequest request, Model model) {

    List<StaticPage> pageList = service.getAllStaticPages();
    model.addAttribute("pageList", pageList);

    return "createStaticPage";
  }

  @RequestMapping(value = "/savePage", method = RequestMethod.POST)
  public String savePage(HttpServletRequest request) {

    StaticPage page = new StaticPage();
    page.setTitle(request.getParameter("pageTitle"));
    page.setPageType(request.getParameter("pageType"));
    page.setContent(request.getParameter("pageContent"));

    service.addStaticPage(page);

    return "redirect:displayAllStaticPages";
  }

  @RequestMapping(value = "/deletePage", method = RequestMethod.GET)
  public String deletePage(HttpServletRequest request) {
    String pageIdParameter = request.getParameter("staticPageId");
    int pageId = Integer.parseInt(pageIdParameter);
    service.deleteStaticPage(pageId);
    return "redirect:displayAdminStaticPages";
  }
}
