<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Home Page</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick-theme.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/hover.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
    <script src="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/pets.js"></script>
    <style>
      @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
    </style>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
          return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=54234065685";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

    <div class="container">
      <h1>Aldolphis Springstine's Spectacular Pet Training & More</h1>
      <%@include file="menu.jsp" %>
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <h2>Welcome!</h2>
            <p>My name is Aldolphis Springstine and I am a pet trainer specializing in chinchillas, poodles, and ferrets.  </p>
            <p>You may have seen some of my animals on the big screen!  Some of the pets I have trained have appeared in movies including The Beastmaster and UHF.</p>
            <p>If you are interesting in learning more about my business, please feel free to explore my site.  I post blogs daily to keep you up to date on my current trainings.  I also have pages that showcase my pets.</p>
          </div>
        </div>
        <h2>What's Happening...</h2>
        <p style="text-align: center">Explore the 5 most recent blog posts below.</p>
        <div class="row col-md-12 petSlider">
          <c:forEach var="currentBlog" items="${recentBlogs}">
            <div class="blogSliderDiv">
              <h1><a href="${pageContext.request.contextPath}/displayCommentForm?blogId=${currentBlog.blogId}" style="color: #333"><c:out value="${currentBlog.title}"/></a></h1>
              <div class="form-group">
                <span>
                  <c:forEach var="currentHashtag" items="${currentBlog.hashtags}">
                    <a href="${pageContext.request.contextPath}/blogsByHashtag?hashtagId=${currentHashtag.hashtagId}"
                       class="hashtagButton btn btn-default hvr-buzz"><c:out value="${currentHashtag.name}"/></a>
                  </c:forEach>
                </span>
              </div>
              <h4>
                by <c:out value="${currentBlog.author}"/>
              </h4>
              <h5>
                created <c:out value="${currentBlog.createDate}"/>
              </h5>
            </div>
          </c:forEach>

        </div>
      </div>
      <div class="col-md-4">
        <div class="fb-page col-md-12" data-href="https://www.facebook.com/LouisvilleDogWizard/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
          <blockquote cite="https://www.facebook.com/thesoftwareguild/" class="fb-xfbml-parse-ignore">
            <a href="https://www.facebook.com/thesoftwareguild/">The Software Guild</a>
          </blockquote>
        </div>
      </div>
    </div>

</html>
