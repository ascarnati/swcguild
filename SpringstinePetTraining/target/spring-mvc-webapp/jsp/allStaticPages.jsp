<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>All Static Pages</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/list.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pets.js"></script>
        <script src="//listjs.com/assets/javascripts/list.min.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Aldophis Springstine's Spectacular Pet Training</h1>
            <%@include file="menu.jsp" %>
            <h2>All Pet Pages</h2>
            <div id="allPages">
                <div class="row">
                    <div class="col-md-4">
                        <input class="search" placeholder="Enter Search Terms" />
                    </div>
                </div>
                <table class="table table-hover">
                    <tr>
                        <th class="sort btn col-md-2" data-sort="title">Page Title</th>
                        <th class="sort btn col-md-2" data-sort="type">Type</th>
                    </tr>
                    <tbody class="list">
                        <c:forEach var="currentPage" items="${pageList}">
                            <tr>
                                <td class="title">
                                    <a href="displayStaticPage?staticPageId=${currentPage.staticPageId}"><c:out value="${currentPage.title}"/></a>
                                </td>
                                <td class="type">
                                    <c:out value="${currentPage.pageType}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <script type="text/javascript">
            var options = {
                valueNames: ['title', 'type']
            };
            var allPagesList = new List('allPages', options);
        </script>
    </body>
</html>