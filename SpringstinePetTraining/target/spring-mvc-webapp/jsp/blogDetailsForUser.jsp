<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Blog Details Page</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/slick-1.6.0/slick/slick-theme.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-dropdownhover.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/pets.css">
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap-dropdownhover.min.js"></script>
        <script src="${pageContext.request.contextPath}/slick-1.6.0/slick/slick.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pets.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Permanent+Marker');
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Adolphis Springstine's Spectacular Pet Training</h1>
            <%@include file="menu.jsp" %>
            <div class="row">
                <h2><c:out value="${blog.title}"/></h2>
                <div class="blogDetailHeader">
                    <h4>by <c:out value="${blog.author}"/></h4>
                    <h5>created <c:out value="${blog.createDate}"/></h5>
                </div>
            </div>
            <div class="row">
                <form class="form-vertical" role="form" method="POST" action="userUpdateBlog">
                    <input type="text" class="hidden" id="blogId" name="blogId" value="${blog.blogId}"/>
                    <input type="text" class="hidden" id="user" name="user" value="${pageContext.request.userPrincipal.name}"/>
                    <!-- Input for TinyMCE -->
                    <div class="form-group">
                        <div class="form-group">
                            <label for="add-blogContent" class="col-md-4 control-label"></label>
                            <textarea class="form-control" name="blogContent" id="blogContent" required><c:out escapeXml="false" value="${blog.content}" /></textarea>
                        </div>
                    </div>
                    <!-- end input for TinyMCE -->
                    <div class="form-group">
                        <label for="add-title" class="col-md-1 control-label">Title: </label>
                        <div class="col-md-2">
                            <input class="form-control" id="title" name="title" value="${blog.title}" type="text" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-publishDate" class="col-md-1 control-label">Publish Date: </label>
                        <div class="col-md-2">
                            <input class="form-control" id="publishDate" name="publishDate" value="${blog.publishDate}" placeholder="YYYY-MM-DD" type="text" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-expireDate" class="col-md-1 control-label">Expiration Date: </label>
                        <div class="col-md-2">
                            <input class="form-control" id="expireDate" name="expireDate" value="${blog.expirationDate}" placeholder="YYYY-MM-DD" type="text" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <a href="deleteBlog?blogId=${blog.blogId}" class="btn btn-default col-md-1">Delete</a>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-default col-md-1" value="Save"/>
                    </div>
                    <div class="form-group">
                        <a href="${pageContext.request.contextPath}/displayBlogEntryPage?user=${pageContext.request.userPrincipal.name}" class="btn btn-default col-md-1">
                            Back
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/datepicker.js"></script>
        <script type="text/javascript" src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=v4ch1mqjinfuchsoyz81b5oq9gwaxg5p1dhag26ptvtz5rjs'></script>
        <script type="text/javascript">
            tinymce.init({
                selector: '#blogContent',
                theme: 'modern',
                width: 1100,
                height: 300,
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
            });
        </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    </body>
</html>
