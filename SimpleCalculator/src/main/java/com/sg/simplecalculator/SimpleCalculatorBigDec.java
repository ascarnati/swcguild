/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.simplecalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Michael McCardel
 */
public class SimpleCalculatorBigDec {
    
    public BigDecimal adder(BigDecimal value1, BigDecimal value2) {
        return value1.add(value2);
    }

    public BigDecimal subtractor(BigDecimal value1, BigDecimal value2) {
        return value1.subtract(value2);
    }

    public BigDecimal multiplier(BigDecimal value1, BigDecimal value2) {
        return value1.multiply(value2);
    }

    public BigDecimal divider(BigDecimal value1, BigDecimal value2) {
        return value1.divide(value2, 2, RoundingMode.HALF_UP);
    }

}