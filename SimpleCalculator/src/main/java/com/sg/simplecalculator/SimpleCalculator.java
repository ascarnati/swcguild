/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.simplecalculator;

/**
 *
 * @author Michael McCardel
 */
public class SimpleCalculator {

    public double adder(double value1, double value2) {
        return value1 + value2;
    }

    public double subtractor(double value1, double value2) {
        return value1 - value2;
    }

    public double multiplier(double value1, double value2) {
        return value1 * value2;
    }

    public double divider(double value1, double value2) {
        return value1 / value2;
    }

}