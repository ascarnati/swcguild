
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.simplecalculator;

import java.util.Scanner;

/**
 *
 * @author Anthony Scarnati
 */
public class App {

    public static void main(String[] args) {

        boolean runCalculator = true;
        String operatorValue = " ",
                stingInputValue1 = " ",
                stingInputValue2 = " ";
        double inputValue1 = 0.0,
                inputValue2 = 0.0,
                calculatedResult = 0.0;

        System.out.println("Welcome to the Simple Calculator");

        Scanner myScanner = new Scanner(System.in);
        SimpleCalculator myCalc = new SimpleCalculator();

//*****************************************************************************
// Start the App and prompt user to enter input for calculation
//*****************************************************************************
        do {
            System.out.println("Please enter the operation you would like to "
                    + "complete: ");
            System.out.println("A = Addition");
            System.out.println("S = Subtraction");
            System.out.println("M = Multiplication");
            System.out.println("D = Division");
            System.out.println("X = Exit the calculator");
            System.out.print("Enter your value here:  ");
            operatorValue = myScanner.nextLine();

            // Check for exit of calculator input by user, return, and
            // print "Thank you!"   
            if (operatorValue.equalsIgnoreCase("x")) {
                System.out.println("********************************");
                System.out.println("Thank you!");
                return;
            }

            System.out.print("Enter the first value for the calculator: ");
            stingInputValue1 = myScanner.nextLine();
            inputValue1 = Double.parseDouble(stingInputValue1);

// add some error handling here

            System.out.print("Enter the second value for the calculator: ");
            stingInputValue2 = myScanner.nextLine();
            inputValue2 = Double.parseDouble(stingInputValue2);
// add some error handling here

            System.out.println(" ");

            switch (operatorValue) {
                case "A":
                case "a":
                    // execute code if expression == constant
                    calculatedResult
                            = myCalc.adder(inputValue1, inputValue2);
                    break;
                case "S":
                case "s":
                    // execute code if expression == constant
                    calculatedResult
                            = myCalc.subtractor(inputValue1, inputValue2);
                    break;
                case "M":
                case "m":
                    // execute code if expression == constant
                    calculatedResult
                            = myCalc.multiplier(inputValue1, inputValue2);
                    break;
                case "D":
                case "d":
                    // execute code if expression == constant
                    calculatedResult
                            = myCalc.divider(inputValue1, inputValue2);
                    break;
                case "X":
                    // execute code if expression == constant
                    break;
                default:
                    //execute code if no match found
                    break;
            }

            System.out.println("The calculated result is: "
                    + calculatedResult);
            System.out.println(" ");
            inputValue1 = 0.0;
            inputValue2 = 0.0;
            calculatedResult = 0.0;

        } while (runCalculator);

    }

}