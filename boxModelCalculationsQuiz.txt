

1. Total height = 212px	(20+1+10+150+10+1+20)
2. Total width= 462px	(20+1+10+400+10+1+20)

(margin+border+padding+content+padding+border+margin)

3. Browser calc. height = 172px 	(1+10+150+10+1)
4. Browser calc. width = 422px		(1+10+400+10+1)

(border-top+padding-top+content h+padding-bottom+border-bottom)
(border-left+padding-left+content w+padding-right+border-right

------------------------------------------

Sample element:
#div1 {
	height: 150px:
	width: 400px;
	margin: 20px;
	border: 1px solid red;
	padding: 10px;
}

For this object, calculate:
* Total height
* Total width
* Browser calculated height
* Browser calculated width
