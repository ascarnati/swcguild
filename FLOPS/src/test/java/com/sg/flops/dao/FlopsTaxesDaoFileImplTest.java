/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Taxes;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aScarnati 
 */
public class FlopsTaxesDaoFileImplTest {
    
    private FlopsTaxesDao dao = new FlopsTaxesDaoFileImpl();
    
    public FlopsTaxesDaoFileImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws FlopsPersistenceException {
        List<Taxes> taxesList = dao.getAllTaxes();
        for (Taxes tax : taxesList) {
            dao.getTaxes(tax.getTaxState());
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addTaxes and getTaxes method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testAddGetTaxes() throws Exception {
        // add 1 tax record
        Taxes state1 = new Taxes("PA");
        state1.setTaxRate(new BigDecimal("6.50"));
        
        dao.addTaxes("PA", state1);
        
        // confirm that the record was created and we can get it back
        // by verifying the taxRate property and the taxState property
        // even tho State is the key, its possible that because I'm passing
        // this as an object, the actual value of the state name could differ
        assertEquals("6.50", dao.getTaxes("PA").getTaxRate().toString());
        assertEquals("PA", dao.getTaxes("PA").getTaxState());
        
    }

    /**
     * Test of editTaxes method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testEditTaxes() throws Exception {
        
        // add 1 tax record
        Taxes state1 = new Taxes("PA");
        state1.setTaxRate(new BigDecimal("6.50").setScale(2));
        
        dao.addTaxes("PA", state1);
        
        // then change the taxRate property for that record
        state1.setTaxRate(new BigDecimal("4.00").setScale(2));
        
        // now confirm that the taxRate has changed from a 6.50 to 4.00
        assertEquals("4.00", dao.getTaxes("PA").getTaxRate().toString());
        
    }

    /**
     * Test of getAllTaxes method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testGetAllTaxes() throws Exception {
        // add two tax records
        Taxes state1 = new Taxes("PA");
        state1.setTaxRate(new BigDecimal("6.50"));
        
        dao.addTaxes("PA", state1);

        Taxes state2 = new Taxes("OH");
        state1.setTaxRate(new BigDecimal("7.25"));
        
        dao.addTaxes("OH", state2);

        // confirm that there are 2 objects in the list
        assertEquals(2, dao.getAllTaxes().size());
        
    }

    /**
     * Test of removeTaxes method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testRemoveTaxes() throws Exception {
        // add two tax records
        Taxes state1 = new Taxes("PA");
        state1.setTaxRate(new BigDecimal("6.50"));
        
        dao.addTaxes("PA", state1);

        Taxes state2 = new Taxes("OH");
        state1.setTaxRate(new BigDecimal("7.25"));
        
        dao.addTaxes("OH", state2);
        
        // then, remove record 1
        dao.removeTaxes(state1.getTaxState());
        
        // make sure there's just 1 item left in the list
        assertEquals(1, dao.getAllTaxes().size());
        
        // make sure that we get a null value if we try to get record 1 now
        assertNull(dao.getTaxes(state1.getTaxState()));
        
    }

    /**
     * Test of openTaxesDao method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testOpenTaxesDao() throws Exception {
    }

    /**
     * Test of closeTaxesDao method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testCloseTaxesDao() throws Exception {
    }

    /**
     * Test of writeTaxes method, of class FlopsTaxesDaoFileImpl.
     */
    @Test
    public void testWriteTaxes() throws Exception {
    }
    
}
