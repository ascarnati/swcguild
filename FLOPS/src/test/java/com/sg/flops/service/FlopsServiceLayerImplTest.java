/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.service;

import com.sg.flops.dao.FlopsOrderDao;
import com.sg.flops.dao.FlopsProductDao;
import com.sg.flops.dao.FlopsTaxesDao;
import com.sg.flops.dto.Order;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aScarnati
 */
public class FlopsServiceLayerImplTest {
    
    private FlopsServiceLayer service;
    
    public FlopsServiceLayerImplTest() {

    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAllOrders method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetAllOrders() throws Exception {
    }

    /**
     * Test of getAllOrdersForDate method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetAllOrdersForDate() throws Exception {
        
                // add 2 order records
        Order order1 = new Order(00001);
        order1.setCustomerName("Wise");
        order1.setCustomerState("OH");
        order1.setTaxRate(new BigDecimal("6.25"));
        order1.setProductType("Wood");
        order1.setArea(new BigDecimal("100"));
        order1.setLaborCost(new BigDecimal("475"));
        order1.setMaterialCost(new BigDecimal("515"));
        order1.setLaborCpSf(new BigDecimal("4.75"));
        order1.setMaterialCpSf(new BigDecimal("5.15"));
        order1.setTax(new BigDecimal("61.88"));
        order1.setTotal(new BigDecimal("1051.88"));
        
        // dao.addOrder(order1);

        Order order2 = new Order(00002);
        order2.setCustomerName("Hope");
        order2.setCustomerState("PA");
        order2.setTaxRate(new BigDecimal("7.00"));
        order2.setProductType("Laminate");
        order2.setArea(new BigDecimal("100"));
        order2.setLaborCost(new BigDecimal("475"));
        order2.setMaterialCost(new BigDecimal("515"));
        order2.setLaborCpSf(new BigDecimal("4.75"));
        order2.setMaterialCpSf(new BigDecimal("5.15"));
        order2.setTax(new BigDecimal("61.88"));
        order2.setTotal(new BigDecimal("1051.88"));
        
        // dao.addOrder(order2);
    }

    /**
     * Test of getAllProducts method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetAllProducts() throws Exception {
    }

    /**
     * Test of addOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testAddOrder() throws Exception {
    }

    /**
     * Test of getOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGetOrder() throws Exception {
    }

    /**
     * Test of generateQuote method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGenerateQuote() throws Exception {
    }

    /**
     * Test of generateOrderNumber method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testGenerateOrderNumber() {
    }

    /**
     * Test of editOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testEditOrder() {
    }

    /**
     * Test of removeOrder method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testRemoveOrder() {
    }

    /**
     * Test of openOrderDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testOpenOrderDao() throws Exception {
    }

    /**
     * Test of closeOrderDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testCloseOrderDao() throws Exception {
    }

    /**
     * Test of openProductDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testOpenProductDao() throws Exception {
    }

    /**
     * Test of openTaxesDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testOpenTaxesDao() throws Exception {
    }

    /**
     * Test of closeProductDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testCloseProductDao() throws Exception {
    }

    /**
     * Test of closeTaxesDao method, of class FlopsServiceLayerImpl.
     */
    @Test
    public void testCloseTaxesDao() throws Exception {
    }
    
}
