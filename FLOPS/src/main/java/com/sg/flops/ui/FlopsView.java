/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.ui;

import com.sg.flops.dto.Order;
import com.sg.flops.dto.Product;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class FlopsView {

    public UserIO io;

    public FlopsView(UserIO io) {
        this.io = io;
    }

    public void printSystemGreeting() {
        io.print("             ~*~ ");
        io.print("        Welcome to FLOPS  ");
        io.print("  FLooring Order Process System");
        io.print("             ~*~ ");
        io.print("");
        io.print("");
    }

    public int printMenuAndGetSelection() {
        io.print("=====> Main Menu <=====");
        io.print("1. Display Orders");
        io.print("2. Add an Order");
        io.print("3. Edit an Order");
        io.print("4. Remove an Order");
        io.print("5. Save Current Work");
        io.print("6. Quit");

        return io.readInt("Please select from the above choices.", 1, 6);
    }

    //public void displayAllOrders(List<Order> orderList) {}
    //public Order editOrder(Order orderNumber) {}
    //public String getUserConfirmation() {}
    //public void displayAllProducts(List<Product> productList) {}
    //public Integer promptForOrderNumber() {}
    public void displayCreateNewOrderBanner() {
        io.print("=====> Create New Order <=====");
    }

    public Order getCustomerInfo() {
        String tempDate = io.readString("Please input the Order Date. (MM/dd/yyyy)");
        LocalDate orderDate = LocalDate.parse(tempDate, DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        String customerName = io.readString("Please enter the Customer Name.");
        // insert list of valid states here (passed in from controller)
        String customerState = io.readString("Please enter the Customer State.");
        // insert list of valid products here (passed in from controller)
        String productType = io.readString("Please enter the Product Type.");
        BigDecimal area = io.readBigDecimal("Please enter the Area (SqFt) to be covered.");
        Order currentOrder = new Order(0);
        currentOrder.setCustomerName(customerName);
        currentOrder.setCustomerState(customerState);
        currentOrder.setProductType(productType);
        currentOrder.setArea(area);
        currentOrder.setOrderDate(orderDate);
        return currentOrder;
    }

    public String displayAndConfirmPendingOrder(Order pendingOrder) {

        io.print("");
        io.print("=====> Pending Order Summary <=====");
        io.print("Please review the quoted price for your order below.");

        io.print("Order Date: " + pendingOrder.getOrderDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        io.print("Customer Name: " + pendingOrder.getCustomerName());
        io.print("Customer State: " + pendingOrder.getCustomerState());
        io.print("Product Type: " + pendingOrder.getProductType());
        io.print("Area: " + pendingOrder.getArea().toString());
        io.print("Labor Cost/SqFt $: " + pendingOrder.getLaborCpSf());
        io.print("Material Cost/SqFt $: " + pendingOrder.getMaterialCpSf());
        io.print("State Tax Rate: " + pendingOrder.getTaxRate() + "%");
        io.print("Labor Cost $: " + pendingOrder.getLaborCost());
        io.print("Material Cost $: " + pendingOrder.getMaterialCost());
        io.print("Tax Cost $: " + pendingOrder.getTax());
        io.print("Total Cost $: " + pendingOrder.getTotal());

        io.print("");
        io.print("=====> Order Confirnmation <=====");
        return io.readString("Please type 'Y' to Save this order or 'N' to discard changes. "
                + "Then, hit enter.");

    }

    public void displayErrorMessage(String errorMsg) {
        io.print("***** ERROR *****");
        io.print(errorMsg);
    }

    public String displayNewOrderSuccessfulBanner() {
        return io.readString("Your order has been created.  Press enter to continue.");
    }

    public void displayNewOrderCancelledBanner() {
        io.print("~ Your order has been cancelled.  Returning to Main Menu. ~");
        io.print("");
    }

    public void displayListOfOrdersBanner() {
        io.print("=====> List of Orders <=====");
        io.print("");
    }

    public void displayOrdersList(List<Order> orderList) {
        for (Order currentOrder : orderList) {

            io.print("- Order Number: " + currentOrder.getOrderNumber() + " - ");
            io.print("Customer Name: " + currentOrder.getCustomerName() + " | Product Type: " + currentOrder.getProductType() + " | Total Cost: $" + currentOrder.getTotal());
            //io.print("Order Date: " + order.getOrderDate());
            io.print("");
        }
        io.readString("~ Please press <enter> to continue. ~");
    }

    public void displayDisplayOrderBanner() {
        io.print("=====> Display Order <=====");
    }

    public Integer displayOrderNumberSelection() {
        return io.readInt("~ Please enter the Order Number to view details ~");
    }

    public void displayOrder(Order order) {
        if (order != null) {

            io.print("");
            io.print("=====> Order Details <=====");
            System.out.println("Here's the information you requested for Order Number: "
                    + order.getOrderNumber());

            io.print("Order Date: " + order.getOrderDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
            io.print("Customer Name: " + order.getCustomerName());
            io.print("Customer State: " + order.getCustomerState());
            io.print("State Tax Rate: " + order.getTaxRate() + "%");
            io.print("Product Type: " + order.getProductType());
            io.print("Area: " + order.getArea().toString());
            io.print("Labor Cost: $" + order.getLaborCost());
            io.print("Material Cost: $" + order.getMaterialCost());
            io.print("Tax Cost: $" + order.getTax());
            io.print("Total Cost: $" + order.getTotal());

        } else {
            io.print("No such Order found.");
        }
        io.readString("~ Please press <enter> to continue. ~");
    }

    public Order displayEditOrder(Order order) {

        Integer orderNumber = order.getOrderNumber();

        io.print("");
        io.print("=====> Order Details <=====");
        io.print("Order Date: " + order.getOrderDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        io.print("Customer Name: " + order.getCustomerName());
        io.print("Customer State: " + order.getCustomerState());
        io.print("Product Type: " + order.getProductType());
        io.print("Total Cost: $" + order.getTotal());
        io.print("");
        io.print("~ Update Order Info ~");
        io.print("For each field, press enter to accept the current value.");

        LocalDate orderDate = order.getOrderDate();

        try {
            String tempDate = io.readString("Order Date: " + order.getOrderDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
            orderDate = LocalDate.parse(tempDate, DateTimeFormatter.ofPattern("MM/dd/yyyy"));

        } catch (DateTimeParseException e) {
            orderDate = order.getOrderDate();
        }

        String editedCustomerName = io.readString("Customer Name: " + order.getCustomerName() + ":");
        if (editedCustomerName.isEmpty()) {
            editedCustomerName = order.getCustomerName();
        }

        // insert list of valid states here (passed in from controller)
        String editedCustomerState = io.readString("Customer State: " + order.getCustomerState() + ":");
        if (editedCustomerState.isEmpty()) {
            editedCustomerState = order.getCustomerState();
        }

        // insert list of valid products here (passed in from controller)
        String editedProductType = io.readString("Product Type: " + order.getProductType() + ":");
        if (editedProductType.isEmpty()) {
            editedProductType = order.getProductType();
        }

        BigDecimal editedArea = order.getArea();
        try {
            editedArea = io.readBigDecimal("Area (SqFt) to be covered: " + order.getArea() + ":");
        } catch (NumberFormatException e) {
            editedArea = order.getArea();
        }

        Order editedOrder = new Order(orderNumber);
        editedOrder.setCustomerName(editedCustomerName);
        editedOrder.setCustomerState(editedCustomerState);
        editedOrder.setProductType(editedProductType);
        editedOrder.setArea(editedArea);
        editedOrder.setOrderDate(orderDate);
        return editedOrder;

    }

    public void displayOrderEditedBanner() {
        io.print("The order has been edited successfully.  Returning to Main Menu.");
    }

    public LocalDate promptForOrderDate() {

        String tempDate = io.readString("~ Please input the Order Date. (MM/dd/yyyy) ~ ");
        LocalDate searchDate = LocalDate.parse(tempDate, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        return searchDate;
    }

    public void displayOrderRemovedBanner(Integer orderNumber) {
        io.print("~ Order " + orderNumber + " removed successfully. ~");
    }

    public String displayConfirmDeletion(Integer orderNumber) {

        io.print("");
        io.print("=====> Confirm Order Deletion <=====");
        return io.readString("~ Please type 'Y' to delete this order or 'N' to keep it.  "
                + "Then, hit enter. ~");
    }

    public void displayCurrentWorkSavedBanner() {
        io.print("Your current work has been saved.  Returning to Main Menu.");
    }
    
    public void displayUserEnvironmentBanner(boolean trainingEnvironment) {
        
        if (trainingEnvironment == true) {
            io.print("~~");
            io.print("You are in the TRAINING environment.");
            io.print("Your changes will NOT be saved to a permanent record.");
            io.print("~~");
            return;
        }
        io.print("~~");
        io.print("You are in the PRODUCTION environment.");
        io.print("Your changes may be saved to the permanent record.");
        io.print("~~");
    }

}
