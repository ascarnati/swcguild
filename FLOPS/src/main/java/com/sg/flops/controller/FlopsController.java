/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.controller;

import com.sg.flops.dao.FlopsPersistenceException;
import com.sg.flops.dto.Order;
import com.sg.flops.service.FlopsOrderNotFoundException;
import com.sg.flops.service.FlopsServiceLayer;
import com.sg.flops.ui.FlopsView;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public class FlopsController {

    private FlopsView view;
    private FlopsServiceLayer service;
    //private boolean isTrainingMode;

    public FlopsController(FlopsServiceLayer service, FlopsView view) {
        this.service = service;
        this.view = view;
    }

    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;

        try {
            service.openOrderDao();
            service.openProductDao();
            service.openTaxesDao();

        } catch (FlopsPersistenceException e) {
            view.displayErrorMessage(e.getMessage());
            keepGoing = false;
        }

        view.printSystemGreeting();

        while (keepGoing) {

            try {

                boolean trainingEnvironment = service.getIsTrainingEnvironConfig();
                view.displayUserEnvironmentBanner(trainingEnvironment);

                menuSelection = getMenuSelection();

                switch (menuSelection) {
                    case 1:
                        displayOrder();
                        break;
                    case 2:
                        addOrder();
                        break;
                    case 3:
                        editOrder();
                        break;
                    case 4:
                        removeOrder();
                        break;
                    case 5:
                        saveWork(trainingEnvironment);
                        break;
                    case 6:
                        exit(trainingEnvironment);
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();
                }

            } catch (FlopsPersistenceException | FlopsOrderNotFoundException e) {
                view.displayErrorMessage(e.getMessage());
            }

        }

    }

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    private void addOrder() throws FlopsPersistenceException {
        view.displayCreateNewOrderBanner();

        // UI to collect partial (customer) order info
        Order pendingOrder = view.getCustomerInfo();

        // pass customerState, productType, and area to Service to get calculated order info ("get quote")
        service.generateQuote(pendingOrder);

        // pass pendingOrder to view to display and determine if user wishes to save order
        String savePendingOrder = view.displayAndConfirmPendingOrder(pendingOrder);

        try {

            if (savePendingOrder.equalsIgnoreCase("y")) {
                // pass pendingOrder to Service to generate and set orderNumber for currentOrder
                service.generateOrderNumber(pendingOrder);

                // pass to Service to put order in Hashmap
                Order newOrder = service.addOrder(pendingOrder);

                // finally, pass to the view to display to user
                view.displayNewOrderSuccessfulBanner();

            } else {
                view.displayNewOrderCancelledBanner();
                getMenuSelection();
                return;
            }
        } catch (FlopsPersistenceException e) {
            view.displayErrorMessage(e.getMessage());

        }

        getMenuSelection();
        return;

    }

    private void displayOrder() throws FlopsPersistenceException,
            FlopsOrderNotFoundException {

        // prompt user for a date to search by
        LocalDate searchDate = view.promptForOrderDate();

        // display banner
        view.displayListOfOrdersBanner();

        // display list of orders (summarized)
        List<Order> orderList = service.getAllOrdersForDate(searchDate);
        view.displayOrdersList(orderList);
        System.out.println("");

        // get Order Number from user
        Integer orderNumber = view.displayOrderNumberSelection();

        // call the service to locate the Order
        Order order = service.getOrder(orderNumber);

        // call the view to display the Order
        view.displayOrder(order);

    }

    private void editOrder() throws FlopsPersistenceException,
            FlopsOrderNotFoundException {

        // reuse code from displayOrder method
        LocalDate searchDate = view.promptForOrderDate();
        // display banner
        view.displayListOfOrdersBanner();
        // display list of orders (summarized)
        List<Order> orderList = service.getAllOrdersForDate(searchDate);
        view.displayOrdersList(orderList);
        System.out.println("");
        // get Order Number from user
        Integer orderNumber = view.displayOrderNumberSelection();
        // call the service to locate the Order
        Order order = service.getOrder(orderNumber);
        // call the view to have the user edit the order
        Order editedOrder = view.displayEditOrder(order);
        // call the service to generate a new quote
        service.generateQuote(editedOrder);
        // call the service to have the DAO update the order
        service.editOrder(orderNumber, editedOrder);
        // call the view to display the success message to user
        view.displayOrder(editedOrder);
        view.displayOrderEditedBanner();

    }

    private void removeOrder() throws FlopsPersistenceException,
            FlopsOrderNotFoundException {

        // prompt user for a date to search by
        LocalDate searchDate = view.promptForOrderDate();

        // display banner
        view.displayListOfOrdersBanner();

        // display list of orders (summarized)
        List<Order> orderList = service.getAllOrdersForDate(searchDate);
        view.displayOrdersList(orderList);
        System.out.println("");

        // get Order Number from user
        Integer orderNumber = view.displayOrderNumberSelection();

        // call the service to locate the Order
        Order order = service.getOrder(orderNumber);

        // call the view to display the Order
        view.displayOrder(order);
        String confirmDeletion = view.displayConfirmDeletion(orderNumber);

        if (confirmDeletion.equalsIgnoreCase("y")) {
            // remove Order
            service.removeOrder(orderNumber);
            view.displayOrderRemovedBanner(orderNumber);
            System.out.println("Returning to Main Menu");
            getMenuSelection();
            return;

        } else {
            getMenuSelection();
            return;
        }

    }

    private void unknownCommand() {

    }

    private void saveWork(boolean trainingEnvironment) throws FlopsPersistenceException {
        if (trainingEnvironment == false) {
            service.closeOrderDao();
            view.displayCurrentWorkSavedBanner();
            getMenuSelection();
            return;
        } else {
            System.out.println("You cannot save TRAINING work to a permanent record.");
        }
        return;

    }

    private void readEnvironFile() {

    }

    private void exit(boolean trainingEnvironment) throws FlopsPersistenceException {
        if (trainingEnvironment == false) {
            service.closeOrderDao();
            service.closeProductDao();
            service.closeTaxesDao();
            System.out.println("Thank you.  Your changes have been saved to a permanent record.");
            System.out.println("Goodbye.");
        } else {
            System.out.println("Changes made in TRAINING have not been saved.");
            System.out.println("Goodbye.");
        }

    }

}
