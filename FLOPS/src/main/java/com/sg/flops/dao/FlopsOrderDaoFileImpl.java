/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author aScarnati
 */
public class FlopsOrderDaoFileImpl implements FlopsOrderDao {

    private Map<Integer, Order> orderMap = new HashMap<>();

    public static String ORDER_PATH = "Orders/";
    public static String tempOrderFileName = "Orders_07042016.txt";
    public static String ORDER_INVENTORY = tempOrderFileName;
    public static final String DELIMITER = ",";
    public static final String USER_ENVIRON = "Config/configFile.txt";
    private Integer maxOrderNumber = 0;
    private boolean isTrainingEnvironment = false;

    @Override
    public boolean getIsTrainingEnvironment() throws FlopsPersistenceException {
        loadUserEnvironConfigFile();
        return isTrainingEnvironment;
    }
    
    @Override
    public Integer getMaxOrderNumber() {
        return maxOrderNumber;
    }

    @Override
    public void setMaxOrderNumber(Integer maxOrderNumber) {
        this.maxOrderNumber = maxOrderNumber;
    }

    @Override
    public Order addOrder(Order pendingOrder) throws FlopsPersistenceException {
        Integer orderNumber = pendingOrder.getOrderNumber();
        return orderMap.put(orderNumber, pendingOrder);
    }

    @Override
    public Order editOrder(Integer orderNumber, Order editedOrder)
            throws FlopsPersistenceException {
        return orderMap.put(orderNumber, editedOrder);
    }

    @Override
    public Order getOrder(Integer orderNumber) throws FlopsPersistenceException {
        return orderMap.get(orderNumber);
    }

    @Override
    public List<Order> getAllOrders() throws FlopsPersistenceException {
        return new ArrayList<Order>(orderMap.values());
    }

    @Override
    public Order removeOrder(Integer orderNumber) throws FlopsPersistenceException {
        return orderMap.remove(orderNumber);
    }

    public boolean loadUserEnvironConfigFile() throws FlopsPersistenceException {
        Scanner scanner;

        try {
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(USER_ENVIRON)));
        } catch (FileNotFoundException e) {
            throw new FlopsPersistenceException(
                    "-_- Could not load Training environment.", e);
        }
        
        String currentLine;
        String [] currentTokens;
        
        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(":");
            
            if (currentTokens[1].trim().equalsIgnoreCase("y")) {
                isTrainingEnvironment = true;
            } else isTrainingEnvironment = false;
            
        }
        return isTrainingEnvironment;

    }

    @Override
    public void openOrderDao() throws FlopsPersistenceException {
        loadOrderInventory();
    }

    @Override
    public void loadOrderInventory() throws FlopsPersistenceException {
        Scanner scanner;

        File ordersRepo = new File(ORDER_PATH);
        File[] listOfFiles = ordersRepo.listFiles();

        for (File file : listOfFiles) {

            String date = file.getName().substring(7, 15);
            String updatedDate = date.substring(0, 2) + "-" + date.substring(2, 4)
                    + "-" + date.substring(4);

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
            LocalDate formattedDate = LocalDate.parse(updatedDate, dtf);

            try {
                scanner = new Scanner(
                        new BufferedReader(
                                new FileReader(file)));
            } catch (FileNotFoundException e) {
                throw new FlopsPersistenceException(
                        "-_- Could not place Order data into memory.", e);
            }

            String currentLine;

            String[] currentTokens;

            // INFO: Headers for Orders data
            // OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,LaborCostPerSquareFoot,
            // MaterialCost,LaborCost,Tax,Total, OrderDate
            while (scanner.hasNextLine()) {
                currentLine = scanner.nextLine();
                currentTokens = currentLine.split(DELIMITER);
                Order currentOrder = new Order(Integer.parseInt(currentTokens[0]));

                // capture the maxOrder number while going through all orders
                if (currentOrder.getOrderNumber() > maxOrderNumber) {
                    maxOrderNumber = currentOrder.getOrderNumber();
                }

                currentOrder.setCustomerName(currentTokens[1].trim());
                currentOrder.setCustomerState(currentTokens[2].trim());
                currentOrder.setTaxRate(new BigDecimal(currentTokens[3].trim()));
                currentOrder.setProductType(currentTokens[4].trim());
                currentOrder.setArea(new BigDecimal(currentTokens[5].trim()));
                currentOrder.setMaterialCpSf(new BigDecimal(currentTokens[6].trim()));
                currentOrder.setLaborCpSf(new BigDecimal(currentTokens[7].trim()));
                currentOrder.setMaterialCost(new BigDecimal(currentTokens[8].trim()));
                currentOrder.setLaborCost(new BigDecimal(currentTokens[9].trim()));
                currentOrder.setTax(new BigDecimal(currentTokens[10].trim()));
                currentOrder.setTotal(new BigDecimal(currentTokens[11].trim()));
                currentOrder.setOrderDate(formattedDate);

                orderMap.put(Integer.parseInt(currentTokens[0]), currentOrder);
            }

            scanner.close();

        }
        // while loading all orders, set the new maxOrderNumber to use for 
        // generating new orderNumbers
//        List<Integer> orderNumberList = new ArrayList<>();
//
//        for (Order currentRecord : this.getAllOrders()) {
//            orderNumberList.add(currentRecord.getOrderNumber());
//            if (currentRecord.getOrderNumber() > maxOrderNumber) {
//                maxOrderNumber += maxOrderNumber;
//                
//            }
//        }

    }

    @Override
    public void closeOrderDao() throws FlopsPersistenceException {
        writeOrderInventory();
    }

    @Override
    public void writeOrderInventory() throws FlopsPersistenceException {

        File ordersRepo = new File(ORDER_PATH);
        File[] files = ordersRepo.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("Orders_") && name.endsWith(".txt");
            }
        });

        for (File file : files) {
            file.delete();
        }

        PrintWriter out;

        // create a SET to place all orderDates in
        Set<LocalDate> dateSet = new HashSet<>();

        // for-loop through the files to get the date from the file name
        for (Order currentRecord : this.getAllOrders()) {
            dateSet.add(currentRecord.getOrderDate());
        }
        // iterate through the SET, building a new string from each unique Date
        for (LocalDate currentDate : dateSet) {
            // format date to string and build fileName
            String stringDate = currentDate.format(DateTimeFormatter.ofPattern("MMddyyyy"));
            String fileName = "Orders_" + stringDate + ".txt";

            try {
                // modify FileWriter to new filename variable
                out = new PrintWriter(new FileWriter(new File(ordersRepo, fileName)));
            } catch (IOException e) {
                throw new FlopsPersistenceException("Could not save Order data to inventory.");
            }

            List<Order> orderList = this.getAllOrders();
            for (Order currentRecord : orderList) {
                // verify that the order date of currentOrder is the correct date
                if (currentRecord.getOrderDate().equals(currentDate)) {

                    out.println(currentRecord.getOrderNumber() + DELIMITER
                            + currentRecord.getCustomerName() + DELIMITER
                            + currentRecord.getCustomerState() + DELIMITER
                            + currentRecord.getTaxRate() + DELIMITER
                            + currentRecord.getProductType() + DELIMITER
                            + currentRecord.getArea() + DELIMITER
                            + currentRecord.getMaterialCpSf() + DELIMITER
                            + currentRecord.getLaborCpSf() + DELIMITER
                            + currentRecord.getMaterialCost() + DELIMITER
                            + currentRecord.getLaborCost() + DELIMITER
                            + currentRecord.getTax() + DELIMITER
                            + currentRecord.getTotal() + DELIMITER
                            + currentRecord.getOrderDate() + DELIMITER
                            + "EOL" + DELIMITER);

                    out.flush();
                }
            }
            out.close();

        }
    }

}
