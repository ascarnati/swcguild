/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Taxes;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public interface FlopsTaxesDao {
            
    Taxes addTaxes (String taxState, Taxes taxes) throws FlopsPersistenceException;
    
    Taxes editTaxes(String taxState, Taxes taxes) throws FlopsPersistenceException;

    Taxes getTaxes (String taxState) throws FlopsPersistenceException;
    
    List<Taxes> getAllTaxes() throws FlopsPersistenceException;

    Taxes removeTaxes (String taxState) throws FlopsPersistenceException;
    
    void openTaxesDao() throws FlopsPersistenceException;
    
    void closeTaxesDao() throws FlopsPersistenceException;
        
    void writeTaxesInventory() throws FlopsPersistenceException;
    
}
