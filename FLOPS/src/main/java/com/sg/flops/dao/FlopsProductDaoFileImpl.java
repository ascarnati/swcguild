/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import static com.sg.flops.dao.FlopsTaxesDaoFileImpl.TAXES_INVENTORY;
import com.sg.flops.dto.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author aScarnati
 */
public class FlopsProductDaoFileImpl implements FlopsProductDao {

    private Map<String, Product> productsMap = new HashMap<>();

    public static final String PRODUCT_INVENTORY = "Products.txt";
    public static final String DELIMITER = ",";
    
    // INFO: Headers for Products.txt data
    // ProductType,MaterialCostPerSquareFoot,LaborCostPerSquareFoot
    //
    
    public FlopsProductDaoFileImpl() {        
    };
    
    @Override
    public Product addProduct(String productType, Product product) throws FlopsPersistenceException {
        return productsMap.put(productType, product);
    }

    @Override
    public Product editProduct(String productType, Product product) throws FlopsPersistenceException {
        return productsMap.put(productType, product);
    }

    @Override
    public Product getProduct(String productType) throws FlopsPersistenceException {
        return productsMap.get(productType);
    }

    @Override
    public List<Product> getAllProducts() throws FlopsPersistenceException {
        return new ArrayList<Product>(productsMap.values());
    }

    @Override
    public Product removeProduct(String productType) throws FlopsPersistenceException {
        return productsMap.remove(productType);
    }

    @Override
    public void openProductDao() throws FlopsPersistenceException {
        loadProductInventory();
    }

    public void loadProductInventory() throws FlopsPersistenceException {
        Scanner scanner;
        
        try {
            scanner = new Scanner (
                    new BufferedReader(
                            new FileReader(PRODUCT_INVENTORY)));            
        } catch (FileNotFoundException e) {
            throw new FlopsPersistenceException(
                    "-_- Could not place Tax data into memory.", e);
        }
        
        String currentLine;
        
        String[] currentTokens;
        // INFO: Headers for Products.txt data
        // ProductType,MaterialCostPerSquareFoot,LaborCostPerSquareFoot
        //
       
        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            Product currentProduct = new Product(currentTokens[0]);
            currentProduct.setMaterialCpSf(new BigDecimal(currentTokens[1]));
            currentProduct.setLaborCpSf(new BigDecimal(currentTokens[2]));
           
            
            productsMap.put(currentProduct.getProductType(), currentProduct);
        }

        scanner.close();
    }
    
    @Override
    public void closeProductDao() throws FlopsPersistenceException {
        writeProductInventory();
    }
    
    @Override
    public void writeProductInventory() throws FlopsPersistenceException {
        PrintWriter out;
        
        try {
            out = new PrintWriter(new FileWriter(PRODUCT_INVENTORY));
        } catch (IOException e) {
            throw new FlopsPersistenceException("Could not save product data to inventory.");
        }
        
        List<Product> productsList = this.getAllProducts();
        for (Product currentRecord: productsList) {
            out.println(currentRecord.getProductType() + DELIMITER 
                    + currentRecord.getLaborCpSf() + DELIMITER
                    + currentRecord.getMaterialCpSf() + DELIMITER
                    + "EOL" + DELIMITER);
            
            out.flush();
        }
        
        out.close();
    }
       
    
}
