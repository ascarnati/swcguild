/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

/**
 *
 * @author aScarnati
 */
public class FlopsPersistenceException extends Exception {
    
    public FlopsPersistenceException (String message)   {
        super(message);
    }
    
    public FlopsPersistenceException (String message, Throwable cause)   {
        super(message, cause);
    }
    
}
