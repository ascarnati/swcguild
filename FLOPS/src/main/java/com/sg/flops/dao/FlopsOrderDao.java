/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Order;
import java.util.List;

/**
 *
 * @author aScarnati 
 */
public interface FlopsOrderDao {
    
    public boolean getIsTrainingEnvironment() 
            throws FlopsPersistenceException;
    
    Integer getMaxOrderNumber();

    void setMaxOrderNumber(Integer maxOrderNumber);
    
    Order addOrder(Order pendingOrder) 
            throws FlopsPersistenceException;
    
    Order editOrder(Integer orderNumber, Order editedOrder) 
            throws FlopsPersistenceException;
    
    Order getOrder(Integer orderNumber)
            throws FlopsPersistenceException;
    
    List<Order> getAllOrders()
            throws FlopsPersistenceException;

    Order removeOrder(Integer orderNumber)
            throws FlopsPersistenceException;
    
    void openOrderDao()
            throws FlopsPersistenceException;

    void loadOrderInventory()
            throws FlopsPersistenceException;
    
    void closeOrderDao()
            throws FlopsPersistenceException;
        
    void writeOrderInventory()
            throws FlopsPersistenceException;
    
}
