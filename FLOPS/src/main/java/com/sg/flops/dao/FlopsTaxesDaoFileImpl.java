/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dao;

import com.sg.flops.dto.Taxes;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author aScarnati
 */
public class FlopsTaxesDaoFileImpl implements FlopsTaxesDao {

    private Map<String, Taxes> taxesMap = new HashMap<>();

    public static final String TAXES_INVENTORY = "Taxes.txt";
    public static final String DELIMITER = ",";
    
    // INFO: Headers for Taxes.txt data
    // state, tax rate
    //

    public FlopsTaxesDaoFileImpl() {        
    };
    
    @Override
    public Taxes addTaxes(String taxState, Taxes taxes) throws FlopsPersistenceException {
        return taxesMap.put(taxState, taxes);
    }

    @Override
    public Taxes editTaxes(String taxState, Taxes taxes) throws FlopsPersistenceException {
        return taxesMap.put(taxState, taxes);
    }

    @Override
    public Taxes getTaxes(String taxState) throws FlopsPersistenceException {
        return taxesMap.get(taxState);
    }

    @Override
    public List<Taxes> getAllTaxes() throws FlopsPersistenceException {
        return new ArrayList<Taxes>(taxesMap.values());
    }

    @Override
    public Taxes removeTaxes(String taxState) throws FlopsPersistenceException {
        return taxesMap.remove(taxState);
    }

    @Override
    public void openTaxesDao() throws FlopsPersistenceException {
        loadTaxesInventory();
    }

    private void loadTaxesInventory() throws FlopsPersistenceException {
        Scanner scanner;

        try {
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(TAXES_INVENTORY)));
        } catch (FileNotFoundException e) {
            throw new FlopsPersistenceException(
                    "-_- Could not place Tax data into memory.", e);
        }

        String currentLine;

        String[] currentTokens;
        // INFO: Headers for Taxes.txt data
        // state, tax rate
        //

        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            Taxes currentRecord = new Taxes(currentTokens[0]);
            currentRecord.setTaxRate(new BigDecimal(currentTokens[1]));
            
            taxesMap.put(currentRecord.getTaxState(), currentRecord);

        }

        scanner.close();
    
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public void closeTaxesDao() throws FlopsPersistenceException {
        writeTaxesInventory();
    }
    
    @Override
    public void writeTaxesInventory() throws FlopsPersistenceException {
        PrintWriter out;
        
        try {
            out = new PrintWriter(new FileWriter(TAXES_INVENTORY));
        } catch (IOException e) {
            throw new FlopsPersistenceException("Could not save tax data to inventory.");
        }
        
        List<Taxes> taxesList = this.getAllTaxes();
        for (Taxes currentRecord : taxesList) {
            out.println(currentRecord.getTaxState() + DELIMITER
                    + currentRecord.getTaxRate() + DELIMITER
                    + "EOL" + DELIMITER);
            
            out.flush();
        }
        
        out.close();
    }

    
}
