/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.service;

import com.sg.flops.dao.FlopsOrderDao;
import com.sg.flops.dao.FlopsPersistenceException;
import com.sg.flops.dao.FlopsProductDao;
import com.sg.flops.dao.FlopsTaxesDao;
import com.sg.flops.dto.Order;
import com.sg.flops.dto.Product;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aScarnati
 */
public class FlopsServiceLayerImpl implements FlopsServiceLayer {

    private FlopsOrderDao orderDao;
    private FlopsProductDao productDao;
    private FlopsTaxesDao taxesDao;

    public FlopsServiceLayerImpl(FlopsOrderDao orderDao, FlopsProductDao productDao,
            FlopsTaxesDao taxesDao) {
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxesDao = taxesDao;

    }

    @Override
    public List<Order> getAllOrders() throws FlopsPersistenceException {
        return orderDao.getAllOrders();
    }

    @Override
    public List<Order> getAllOrdersForDate(LocalDate searchDate) throws
            FlopsPersistenceException, FlopsOrderNotFoundException {

        // get the list of all orders from the orderDao 
        List<Order> orderList = getAllOrders();
        // create a new list to contain the orders that meet our criteria
        ArrayList<Order> orderDateList = new ArrayList<>();

        for (Order currentRecord : orderList) {
            if (currentRecord.getOrderDate().equals(searchDate)) {
                // put this subset of orders in the new list
                orderDateList.add(currentRecord);
            }
            // return the subset list to the Controller            
        }
        if (orderDateList.isEmpty() == true) {
            throw new FlopsOrderNotFoundException("Returning to Main Menu.  No orders were found "
                    + "for date: " + searchDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        } else {
            return orderDateList;

        }
    }

    @Override
    public List<Product> getAllProducts() throws FlopsPersistenceException {
        return productDao.getAllProducts();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order addOrder(Order pendingorder) throws FlopsPersistenceException {
        return orderDao.addOrder(pendingorder);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order getOrder(Integer orderNumber) throws FlopsPersistenceException {
        return orderDao.getOrder(orderNumber);
    }

    @Override
    public Order generateQuote(Order pendingOrder) throws FlopsPersistenceException {
        // create variables to be calculated and later set on the object
        BigDecimal quotedLaborCost;
        BigDecimal quotedMaterialCost;
        BigDecimal quotedLaborCpSf;
        BigDecimal quotedMaterialCpSf;
        BigDecimal quotedSubTotalCost;
        BigDecimal quotedTotalCost;
        BigDecimal quotedTaxRate;
        BigDecimal quotedTaxCost;
        BigDecimal area;
        BigDecimal taxAdjustment = new BigDecimal("100");

        //receive pendingOrder object from Controller
        String customerState = pendingOrder.getCustomerState();
        String productType = pendingOrder.getProductType();
        area = pendingOrder.getArea();
        quotedTaxRate = taxesDao.getTaxes(customerState).getTaxRate();
        quotedLaborCpSf = productDao.getProduct(productType).getLaborCpSf().setScale(2, RoundingMode.HALF_UP);
        quotedMaterialCpSf = productDao.getProduct(productType).getMaterialCpSf().setScale(2, RoundingMode.HALF_UP);

        //use customer info to calculate costs
        quotedLaborCost = (productDao.getProduct(productType).getLaborCpSf().multiply(area)).setScale(2, RoundingMode.HALF_UP);
        quotedMaterialCost = (productDao.getProduct(productType).getMaterialCpSf().multiply(area)).setScale(2, RoundingMode.HALF_UP);
        quotedSubTotalCost = (quotedLaborCost.add(quotedMaterialCost)).setScale(2, RoundingMode.HALF_UP);
        quotedTaxCost = (quotedSubTotalCost.multiply(quotedTaxRate).divide(taxAdjustment)).setScale(2, RoundingMode.HALF_UP);
        quotedTotalCost = (quotedLaborCost.add(quotedMaterialCost).add(quotedTaxCost)).setScale(2, RoundingMode.HALF_UP);

        // Then set cost properties on object
        pendingOrder.setTaxRate(quotedTaxRate);
        pendingOrder.setTax(quotedTaxCost);
        pendingOrder.setLaborCpSf(quotedLaborCpSf);
        pendingOrder.setMaterialCpSf(quotedMaterialCpSf);
        pendingOrder.setLaborCost(quotedLaborCost);
        pendingOrder.setMaterialCost(quotedMaterialCost);
        pendingOrder.setTotal(quotedTotalCost);

        //return pendingOrder object to Controller        
        return pendingOrder;
    }

    @Override
    public Order generateOrderNumber(Order pendingOrder) {
        // get maxOrderNumber from DAO
        // set the nextOrderNumber = maxOrderNumber + 1;
        Integer nextOrderNumber = orderDao.getMaxOrderNumber() + 1;
        // set the orderNumber on the pendingOrder Object
        pendingOrder.setOrderNumber(nextOrderNumber);
        // set the orderNumber on the DAO maxOrderNumber (for the next time I get it)
        orderDao.setMaxOrderNumber(nextOrderNumber);
        return pendingOrder;
    }

    @Override
    public Order editOrder(Integer orderNumber, Order editedOrder) throws FlopsPersistenceException {
        return orderDao.editOrder(orderNumber, editedOrder);
    }

    @Override
    public void removeOrder(Integer orderNumber) throws FlopsPersistenceException {
        orderDao.removeOrder(orderNumber);
    }
    
    @Override
    public boolean getIsTrainingEnvironConfig() throws FlopsPersistenceException {
        return orderDao.getIsTrainingEnvironment();
    }

    @Override
    public void openOrderDao() throws FlopsPersistenceException {
        orderDao.openOrderDao();
    }

    @Override
    public void closeOrderDao() throws FlopsPersistenceException {
        orderDao.closeOrderDao();
    }

    @Override
    public void openProductDao() throws FlopsPersistenceException {
        productDao.openProductDao();
    }

    @Override
    public void openTaxesDao() throws FlopsPersistenceException {
        taxesDao.openTaxesDao();
    }

    @Override
    public void closeProductDao() throws FlopsPersistenceException {
        productDao.closeProductDao();
    }

    @Override
    public void closeTaxesDao() throws FlopsPersistenceException {
        taxesDao.closeTaxesDao();
    }

}
