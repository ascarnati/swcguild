/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.service;

/**
 *
 * @author aScarnati 
 */
public class FlopsOrderNotFoundException extends Exception {
    
    public FlopsOrderNotFoundException (String message) {
        super(message);
    }
    
    public FlopsOrderNotFoundException (String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
