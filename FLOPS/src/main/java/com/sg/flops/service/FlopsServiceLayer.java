/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.service;

import com.sg.flops.dao.FlopsPersistenceException;
import com.sg.flops.dto.Order;
import com.sg.flops.dto.Product;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author aScarnati 
 */
public interface FlopsServiceLayer {
    
    
    public boolean getIsTrainingEnvironConfig() throws FlopsPersistenceException;
    
    List<Order> getAllOrders() 
            throws FlopsPersistenceException;
    
    List<Order> getAllOrdersForDate(LocalDate searchDate) 
            throws FlopsPersistenceException, FlopsOrderNotFoundException;
    
    List<Product> getAllProducts() 
            throws FlopsPersistenceException;
    
    Order addOrder(Order order) 
            throws FlopsPersistenceException;
    
    Order getOrder(Integer orderNumber) 
            throws FlopsPersistenceException;
    
    Order generateQuote(Order pendingOrder) 
            throws FlopsPersistenceException;
    
    Order generateOrderNumber(Order pendingOrder) 
            throws FlopsPersistenceException;
    
    Order editOrder(Integer orderNumber, Order editedOrder) throws FlopsPersistenceException;
    
    void removeOrder(Integer orderNumber) throws FlopsPersistenceException;
    
    void openOrderDao() throws FlopsPersistenceException;
    
    void closeOrderDao() throws FlopsPersistenceException;
    
    void openProductDao() throws FlopsPersistenceException;
    
    void openTaxesDao() throws FlopsPersistenceException;
    
    void closeProductDao() throws FlopsPersistenceException;
    
    void closeTaxesDao() throws FlopsPersistenceException;
    
}
