/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dto;

import java.math.BigDecimal;

/**
 *
 * @author aScarnati
 */
public class Product {
    
    private String productType;
    private BigDecimal materialCpSf;
    private BigDecimal laborCpSf;

    public Product(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getMaterialCpSf() {
        return materialCpSf;
    }

    public void setMaterialCpSf(BigDecimal materialCpSf) {
        this.materialCpSf = materialCpSf;
    }

    public BigDecimal getLaborCpSf() {
        return laborCpSf;
    }

    public void setLaborCpSf(BigDecimal laborCpSf) {
        this.laborCpSf = laborCpSf;
    }
    
}
