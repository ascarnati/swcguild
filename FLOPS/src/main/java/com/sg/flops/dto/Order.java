/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 *
 * @author aScarnati
 */
public class Order {
    
    Integer orderNumber;
    LocalDate orderDate;
    String customerName;
    String customerState;
    BigDecimal taxRate;
    String productType;
    BigDecimal area;
    BigDecimal laborCost;
    BigDecimal materialCost;
    BigDecimal laborCpSf;
    BigDecimal materialCpSf;
    BigDecimal tax;
    BigDecimal total;

    public Order(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }
    
    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerState() {
        return customerState;
    }

    public void setCustomerState(String customerState) {
        this.customerState = customerState;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public BigDecimal getLaborCost() {
        return laborCost;
    }

    public void setLaborCost(BigDecimal laborCost) {
        this.laborCost = laborCost;
    }

    public BigDecimal getMaterialCost() {
        return materialCost;
    }

    public void setMaterialCost(BigDecimal materialCost) {
        this.materialCost = materialCost;
    }

    public BigDecimal getLaborCpSf() {
        return laborCpSf;
    }

    public void setLaborCpSf(BigDecimal laborCpSf) {
        this.laborCpSf = laborCpSf;
    }

    public BigDecimal getMaterialCpSf() {
        return materialCpSf;
    }

    public void setMaterialCpSf(BigDecimal materialCpSf) {
        this.materialCpSf = materialCpSf;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
   
}
