/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flops.dto;

import java.math.BigDecimal;

/**
 *
 * @author aScarnati
 */
public class Taxes {
    
    private String taxState;
    private BigDecimal taxRate;

    public Taxes(String taxState) {
        this.taxState = taxState;
    }
    
    public String getTaxState() {
        return taxState;
    }

    public void setTaxState(String taxState) {
        this.taxState = taxState;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }
            
}
