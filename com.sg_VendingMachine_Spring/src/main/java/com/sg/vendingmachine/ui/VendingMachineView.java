/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.ui;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Snack;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public class VendingMachineView {

    private UserIO io;

    public VendingMachineView(UserIO io) {
        this.io = io;
    }

    public String printAddExitMenuSelection(List<Snack> itemList) {
        io.print("~~~~~~ Welcome to the VendoMatic 3000 ~~~~~~");
        for (int i = 0; i < itemList.size(); i++) {

            io.print(i + 1 + ". " + itemList.get(i).getItemName() + ": "
                    + itemList.get(i).getItemCost() + ": "
                    + itemList.get(i).getItemInventory());

        }
        io.print("You must add credits to proceed.");
        return io.readString("Please type 'A' to continue or 'X' to exit, and hit enter.");
    }
    
    public String printMenuAndGetSelection(List<Snack> itemList) {
        io.print("Main Menu");
        for (int i = 0; i < itemList.size(); i++) {

            //io.print(i + 1 + ". " + itemList.get(i).getItemID() + ": "

            io.print(itemList.get(i).getItemID() + ": "
                    + itemList.get(i).getItemName() + ": "
                    + itemList.get(i).getItemCost() + ": "
                    + itemList.get(i).getItemInventory());

        }

        //io.print(itemList.size() + 1 + ". Add Credits");
        //io.print(itemList.size() + 2 + ". Exit");

        //return io.readInt("Please select from the above choices.",1,itemList.size());
        return io.readString("Please select from the above choices.");
    }

    public void displayItem(Snack itemToDispense)   {
        if (itemToDispense != null)  {
            io.print(itemToDispense.getItemName());
            io.print("Item cost: " + itemToDispense.getItemCost());
            io.print("Items in stock: " + itemToDispense.getItemInventory());
        } else {
            io.print("No itemID entered.");
        }
    }

    public BigDecimal addCreditsBanner() {
        return new BigDecimal(io.readString("How much would you like to add?"));
    }

    public void addCreditsSuccessfulBanner(BigDecimal balance, BigDecimal creditsInput) {
        io.print("Your deposit of " + creditsInput + " has been added successfully."
                + "Your balance is now " + balance + ".");
    }

    public void displayRefundBanner(BigDecimal userRefund) {
        io.print("Please remember to take your change: " + userRefund);
    }

    public void displayRefundInCoinsBanner(Change changeInCoins) {
        io.print("That's... ");
        if (changeInCoins.getQuarter().compareTo(BigDecimal.ZERO) != 0)   {
          io.print(changeInCoins.getQuarter() + " quarters, ");  
        } 
        
        if (changeInCoins.getDime().compareTo(BigDecimal.ZERO) != 0)  {
            io.print(changeInCoins.getDime() + " dimes, ");  
        } 
        
        if (changeInCoins.getNickel().compareTo(BigDecimal.ZERO) != 0)    {
          io.print(changeInCoins.getNickel() + " nickels, ");  
        } 
        
        if (changeInCoins.getPenny().compareTo(BigDecimal.ZERO) != 0) {
          io.print(changeInCoins.getPenny() + " pennies.");  
        }
    }
            
    
    public void displayExitBanner() {
        io.print("Good Bye!!!");
    }

    public void displayUnknownCommandBanner() {
        io.print("Unknown Command!!!");
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("~~~ ERROR ~~~");
        io.print(errorMsg);
    }

    public String displayUserChoiceBanner() {
        return io.readString("Please select from the above choices.");
    }

    public void displayDispenseSuccessfulBanner() {
        io.print("Thanks for your purchase. Enjoy your snack.");
    }

    public void displayDispenseUnsuccessfulBanner() {
        io.readInt("Sorry.  Please make another selection.");
    }

}
