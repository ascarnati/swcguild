/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.advice;

import com.sg.vendingmachine.dao.VendingMachineAuditDao;
import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.service.VendingMachineInsufficientFundsException;
import com.sg.vendingmachine.service.VendingMachineNoItemInventoryException;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author aScarnati
 */
public class LoggingAdvice {

    VendingMachineAuditDao auditDao;

    public LoggingAdvice(VendingMachineAuditDao auditDao) {
        this.auditDao = auditDao;
    }

    public void createAuditEntry(JoinPoint jp) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";

        for (Object currentArg : args) {
            auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry("(" + auditEntry + ")");
        } catch (VendingMachinePersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }

    public void createExceptionEntry(Exception ex) {
        String auditEntry = "(" + ex.getClass() + " | " + ex.getMessage() + ")";
        
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (VendingMachinePersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
        
    }
    
    public void createExceptionAuditEntry (JoinPoint jp, Exception ex) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";
        
        for (Object currentArg : args) {
                auditEntry = auditEntry + currentArg + " | " ;
                //auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry("(" + ex.getClass() + " | " + ex.getMessage() + " | " + auditEntry + ")");         
            // add'l info or formats to print if desired
            // auditDao.writeAuditEntry(auditEntry);
            // auditDao.writeAuditEntry(ex.getMessage());
            // auditDao.writeAuditEntry(ex.getClass() + " | " + auditEntry.toString());
        } catch (VendingMachinePersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }
    
}
