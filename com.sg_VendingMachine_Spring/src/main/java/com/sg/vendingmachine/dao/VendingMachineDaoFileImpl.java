/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Snack;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author aScarnati
 */
public class VendingMachineDaoFileImpl implements VendingMachineDao {

    private Map<String, Snack> snackMap = new HashMap<>();

    public static final String SNACK_INVENTORY = "snackInventory.txt";
    public static final String DELIMITER = "::";

    public VendingMachineDaoFileImpl() {
    };
    
    @Override
    // creates and returns a hashmap list from the items in the inventory.txt
    public List<Snack> getAllSnacks() throws VendingMachinePersistenceException {
        loadSnackInventory();
        return new ArrayList<Snack>(snackMap.values());
    }

    @Override
    public Snack getSnack(String itemID) throws VendingMachinePersistenceException {
        return snackMap.get(itemID);
    }

    @Override
    public Snack createSnack(String itemID, Snack item) throws VendingMachinePersistenceException {
        return snackMap.put(itemID, item);
    }

    @Override
    public Snack updateSnack(String itemID, Snack item) throws VendingMachinePersistenceException {
        return snackMap.put(itemID, item);
    }

    @Override
    public Snack removeSnack(String itemID) throws VendingMachinePersistenceException {
        return snackMap.remove(itemID);
    }

    @Override
    public void openDao() throws VendingMachinePersistenceException {
        loadSnackInventory();
    }

    private void loadSnackInventory() throws VendingMachinePersistenceException {
        Scanner scanner;

        try {
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(SNACK_INVENTORY)));
        } catch (FileNotFoundException e) {
            throw new VendingMachinePersistenceException(
                    "-_- Could not Snack data into memory.", e);
        }

        String currentLine;

        String[] currentTokens;

        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            Snack currentSnack = new Snack(currentTokens[0]);
            currentSnack.setItemName(currentTokens[1]);
            currentSnack.setItemCost(new BigDecimal(currentTokens[2]));
            currentSnack.setItemInventory(Integer.parseInt(currentTokens[3]));

            snackMap.put(currentSnack.getItemID(), currentSnack);

        }

        scanner.close();
    }

    @Override
    public void closeDao() throws VendingMachinePersistenceException {
        writeSnackInventory();
    }

    private void writeSnackInventory() throws VendingMachinePersistenceException {
        PrintWriter out;

        try {
            out = new PrintWriter(new FileWriter(SNACK_INVENTORY));
        } catch (IOException e) {
            throw new VendingMachinePersistenceException("Could not save item data to inventory.");
        }

        List<Snack> snackList = this.getAllSnacks();
        for (Snack currentSnack : snackList) {
            out.println(currentSnack.getItemID() + DELIMITER
                    + currentSnack.getItemName() + DELIMITER
                    + currentSnack.getItemCost() + DELIMITER
                    + currentSnack.getItemInventory() + DELIMITER
                    + "EOL" + DELIMITER);

            out.flush();
        }

        out.close();
    }

}
