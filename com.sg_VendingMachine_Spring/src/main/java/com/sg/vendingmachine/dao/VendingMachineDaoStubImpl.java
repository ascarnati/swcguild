/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Snack;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public class VendingMachineDaoStubImpl implements VendingMachineDao {
   
    Snack snack1;
    List<Snack> snackList = new ArrayList<>();
    
    public VendingMachineDaoStubImpl() throws VendingMachinePersistenceException {
        snack1 = new Snack("99");
        snack1.setItemName("snickers");
        snack1.setItemCost(new BigDecimal("1.50"));
        snack1.setItemInventory(5);
        
        snackList.add(snack1);

        Snack snack2 = new Snack("88");
        snack2.setItemName("kitkat");
        snack2.setItemCost(new BigDecimal("1.75"));
        snack2.setItemInventory(5);
        
        snackList.add(snack2);
        
    }
    
    @Override
    public Snack getSnack(String itemID) throws VendingMachinePersistenceException {
        if (itemID.equals(snack1.getItemID()))  {
            return snack1;
        } else {
            return null;
        }
    }

    @Override
    public Snack createSnack(String itemID, Snack item) throws VendingMachinePersistenceException {
        if (itemID.equals(snack1.getItemID()))  {
            return snack1;
        } else {
            return null;
        }
    }

    @Override
    public Snack updateSnack(String itemID, Snack item) throws VendingMachinePersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Snack removeSnack(String itemID) throws VendingMachinePersistenceException {
        if (itemID.equals(snack1.getItemID()))  {
            return snack1;
        } else {
            return null;
        }
    }

    @Override
    public List<Snack> getAllSnacks() throws VendingMachinePersistenceException {
        return snackList;
    }

    @Override
    public void openDao() throws VendingMachinePersistenceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void closeDao() throws VendingMachinePersistenceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
