/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Snack;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public interface VendingMachineDao {

    Snack getSnack(String itemID) throws
            VendingMachinePersistenceException;

    Snack createSnack(String itemID, Snack item) throws 
            VendingMachinePersistenceException;

    Snack updateSnack(String itemID, Snack item) throws 
            VendingMachinePersistenceException;
        
    Snack removeSnack(String itemID) throws VendingMachinePersistenceException;
    
    List<Snack> getAllSnacks() throws
            VendingMachinePersistenceException;
    
    void openDao() throws VendingMachinePersistenceException;
    
    void closeDao() throws VendingMachinePersistenceException;
    
}
