/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.VendingMachineAuditDao;
import com.sg.vendingmachine.dao.VendingMachineDao;
import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.service.VendingMachineInsufficientFundsException;
import com.sg.vendingmachine.service.VendingMachineNoItemInventoryException;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Snack;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author aScarnati
 */
public class VendingMachineServiceLayerImpl implements VendingMachineServiceLayer {

    private VendingMachineDao dao;
    private VendingMachineAuditDao auditDao;
    private BigDecimal balance;

    public VendingMachineServiceLayerImpl(VendingMachineDao dao, VendingMachineAuditDao auditDao) {
        this.dao = dao;
        balance = new BigDecimal("0.00");
    }

    @Override
    public List<Snack> getAllSnacks() throws VendingMachinePersistenceException {
        return dao.getAllSnacks();
    }

    @Override
    public Snack getSnack(String itemID) throws VendingMachinePersistenceException {
        return dao.getSnack(itemID);
    }

    @Override
    public BigDecimal setUserCredits(BigDecimal creditsInput) throws VendingMachinePersistenceException {
        balance = balance.add(creditsInput);
        return balance;
    }

    @Override
    public boolean verifyItemID(String itemID) throws VendingMachinePersistenceException {
        if (dao.getSnack(itemID) == null) {
            throw new VendingMachinePersistenceException("Sorry.  That's not a valid item ID.");
        }
        return true;
    }

    @Override
    public void updateInventory(String itemID) throws VendingMachinePersistenceException {
        Snack item = dao.getSnack(itemID);
        int updatedItemInventory = item.getItemInventory() - 1;
        item.setItemInventory(updatedItemInventory);
        dao.updateSnack(item.getItemID(), item);

        // below is an alternate way of writing the above ... 
        // int updatedItemInventory = ((dao.getSnack(itemID).getItemInventory()) - 1);
        // dao.getSnack(itemID).setItemInventory(updatedItemInventory);
    }

    @Override
    public boolean dispenseItem(String itemID, BigDecimal balance) throws VendingMachinePersistenceException,
            VendingMachineNoItemInventoryException, VendingMachineInsufficientFundsException {
        if ((checkForInventory(itemID)) && (checkForSufficientFunds(itemID, balance))) {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkForInventory(String itemID) throws VendingMachinePersistenceException,
            VendingMachineNoItemInventoryException {
        if (dao.getSnack(itemID).getItemInventory() <= 0) {
            throw new VendingMachineNoItemInventoryException("Sorry. That item is out of stock.");
        }
        return true;
    }

    @Override
    public boolean checkForSufficientFunds(String itemID, BigDecimal balance) throws VendingMachinePersistenceException,
            VendingMachineInsufficientFundsException {
        if (dao.getSnack(itemID).getItemCost().compareTo(balance) == 1) {
            throw new VendingMachineInsufficientFundsException("You don't have enough credits.");
        }
        return true;
    }

    @Override
    public Change convertToCoins(BigDecimal userRefund) throws VendingMachinePersistenceException {
        return new Change(userRefund);
    }

    @Override
    public void openDao() throws VendingMachinePersistenceException {
        dao.openDao();
    }

    @Override
    public void closeDao() throws VendingMachinePersistenceException {
        dao.closeDao();
    }

}
