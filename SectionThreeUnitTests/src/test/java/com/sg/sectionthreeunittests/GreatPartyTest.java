/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sectionthreeunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GreatPartyTest {
    
        // need to instantiate the class that we're testing, and then call
        // the method in that class
        GreatParty party = new GreatParty();
        
    
    public GreatPartyTest() {                
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of greatParty method, of class GreatParty.
     */
    @org.junit.Test
    public void test30False() {
        assertFalse(party.greatParty(30, false));
    }
    
    @org.junit.Test
    public void test50False() {
        assertTrue(party.greatParty(50, false));
    }

    @org.junit.Test
    public void test70true() {
        assertTrue(party.greatParty(70, true));
    }
    
    @org.junit.Test
    public void test39False() {
        assertFalse(party.greatParty(39, true));
    }
    
    @org.junit.Test
    public void test40False() {
        assertTrue(party.greatParty(40, false));
    }
}
