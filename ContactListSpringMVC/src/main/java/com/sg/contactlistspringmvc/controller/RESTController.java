/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.contactlistspringmvc.controller;

import com.sg.contactlistspringmvc.dao.ContactListDao;
import com.sg.contactlistspringmvc.model.Contact;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@CrossOrigin
@Controller
public class RESTController {
    
    private ContactListDao dao;
    
    @Inject
    public RESTController(ContactListDao dao) {
        this.dao = dao;
    }
    
    // In the method below... The {id} portion of the mapping is the path variable.  
    // This is how we let the Spring Framework know 
    // that {id} will be replaced with an actual value 
    // on each request and that we're interested in what the value is.
    // @PathVariable("id") long id tells the Spring Framework to extract the 
    // value associated with the path variable {id}, convert it to a long 
    // and then pass it into our method as the parameter called id.
    @RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Contact getContact(@PathVariable("id") long id) {
        return dao.getContactById(id);
    }
    
    
    // In the method below... The @RequestBody annotation lets the Spring Framework 
    // know that we expect JSON data in the request body and that we want 
    // Spring to convert the JSON data into a Contact object for us.
    // Our code persists the incoming Contact by passing it to the addContact method 
    // on the DAO.  It then returns the Contact object returned by the DAO (this 
    // Contact object now has a newly assigned contact id).  The @ResponseBody 
    // annotation tells the Spring Framework that we want it to convert the Contact 
    // object returned by our method into JSON format, place it in the Response Body, 
    // and send it to the client.  The @ResponseStatus annotation tells the Spring 
    // Framework to return a 401 Created HTTP status code back with the response.
    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Contact createContact(@RequestBody Contact contact) {
        return dao.addContact(contact);
    }
    
    
    // In the method below... Notice that this is the same URL pattern used to 
    // retrieve a contact from the system.  We can reuse a URL pattern in 
    //several endpoints as long as the HTTP method associated with each is unique. 
    @RequestMapping(value = "/contact/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteContact(@PathVariable("id") long id) {
        dao.removeContact(id);
    }
    
    // In the method below... If the contact id in the path variable differs 
    // from that in the incoming object our code favors the value in the path variable.
    // This method has a void return type and we use the No Content HTTP status 
    // code to indicate this to the client.
    @RequestMapping(value = "/contact/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateContact(@PathVariable("id") long id, @RequestBody Contact 
            contact) {
        // favor the path variable over the id in the object if they differ
        contact.setContactId(id);
        dao.updateContact(contact);
    }
    
    
    // In the method below... We use the @ResponseBody annotation, as we have 
    // for other methods, to let the Spring Framework know we would like it to 
    // convert the returned list of Contacts to JSON, put it on the response 
    // body, and send it back to the client.
    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    @ResponseBody
    public List<Contact> getAllContacts() {
        return dao.getAllContacts();
    }
    
    
}
