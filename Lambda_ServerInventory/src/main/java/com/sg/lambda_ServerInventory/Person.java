/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.lambda_ServerInventory;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

/**
 *
 * @author apprentice
 */
public class Person {

    private String name;
    private int age;

    public Person() {
        this.oldEnoughToVote = (Person p) -> {
            return p.getAge() >= 18;
        };
    }
    
    public String getName() {
        return name;        
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    
    // ** PREDICATE <T> ** 
    // THIS IS A REGULAR METHOD to tell us if a given person
    // is old enough to vote, we'd use the following.  But this would need enclosed
    // in a class, AND we can't assign it directly to a variable.
    
    public boolean test(Person person)  {
        return person.getAge() >= 18;
    }
    
    // This first version of Lambda code doesn't take full advantage of the 
    // syntactic shortcuts available...
    
    Predicate<Person> oldEnoughToVote = (Person p) -> {
        return p.getAge() >= 18;
    }; 
    
    // This second version of Lambda code takes advantage of the shortcuts
    Predicate<Person> oldEnoughToVote = p -> p.getAge() >= 18;
    
    // The curly braces aren't needed in the second example b/c there's only one
    // statement; if the Lambda body has more than one statement, the braces are
    // required.
    
    // ** CONSUMER<T> ** 
    // THIS IS A REGULAR METHOD to take in one parameter and print the name 
    // and age of the Person to the screen.
    
    public void accept(Person person) {
        System.out.println(person.getName()
                + "is"
                + person.getAge()
                + " years old.");
    }
    
    // This is the first version of Lambda code for the above...
    
    Consumer<Person> howOld = (Person p) -> {
        System.out.println(p.getName()
            + " is "
            + p.getAge()
            + " years old.");
    };
    
    // This is optimized version with shortcuts
    
    Consumer<Person> howOld = p -> System.out.println(p.getName() + " is "
    + p.getAge() + " years old.");
    
    
    // ** ToIntFunction <T> **
    
    // THIS IS A REGULAR METHOD
    
    public int applyAsInt(Person p) {
        return p.getAge();
    }
    
    // this is the first Lambda version...
    
    ToIntFunction<Person> toAge = (Person p) -> {
        return p.getAge();
    };
    
    // this is the optimized Lambda version...
    
    ToIntFunction<Person> toAge = p -> p.getAge();
    ToIntFunction<Person> toAge = Person::getAge;
    
}
