/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class VendingMachineItemDaoDbImpl implements VendingMachineItemDao {
    
    private static final String SQL_INSERT_ITEM
            = "insert into items "
            + "(name, price, quantity) "
            + "values (?, ?, ?)";
    
    private static final String SQL_DELETE_ITEM
            = "delete from items where item_id = ?";
    
    private static final String SQL_SELECT_ITEM
            = "select * from items where item_id = ?";
    
    private static final String SQL_UPDATE_ITEM
            = "update items set "
            + "name = ?, price = ?, quantity = ? "
            + "where item_id = ?";
    
    private static final String SQL_UPDATE_ITEM_INVENTORY
            = "update items set "
            + "quantity = ? "
            + "where item_id = ?";

    private static final String SQL_SELECT_ALL_ITEMS
            = "select * from items";
    
    private static final String SQL_SELECT_ITEM_BY_ID
            = "select * from items where item_id = ?";
    
    // create a class-level variable to use
    private JdbcTemplate jdbcTemplate;
    
    // inject the JdbcTemplate here using setter injection
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Item addItem(Item item) {
        jdbcTemplate.update(SQL_INSERT_ITEM,
            item.getName(), 
            item.getPrice(),
            item.getQuantity());
            
        // query the db for the id that was just assigned to the new row
        int newId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        // set the new id value on the item object and return it
        item.setId(newId);
        return item;
    }

    @Override
    public void removeItem(int id) {
        jdbcTemplate.update(SQL_DELETE_ITEM, id);
    }

    @Override
    public void updateItem(Item item) {
        jdbcTemplate.update(SQL_UPDATE_ITEM,
                item.getName(),
                item.getPrice(),
                item.getQuantity(),
                item.getId());
    }
    
    @Override
    public void updateItemInventory(int quantity, int id) {
        jdbcTemplate.update(SQL_UPDATE_ITEM_INVENTORY, quantity, id);
    }

    @Override
    public List<Item> getAllItems() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ITEMS, new ItemMapper());
    }

    @Override
    public Item getItemById(int id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ITEM_BY_ID, 
                new ItemMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            // its better to return null in this case than an Exception to the user
            return null;
        }
    }
    
    private static final class ItemMapper implements RowMapper<Item> {
        
        public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
            
            // My Item DTO has a constructor that requires an item ID,
            // so I need to use the ResultSet to FIRST GET the item_id 
            // associated with that row of data, and then use that
            // and pass it into the constructor to create a new Item
            Item item = new Item(rs.getInt("item_id"));
            item.setName(rs.getString("name"));
            item.setPrice(rs.getBigDecimal("price"));
            item.setQuantity(rs.getInt("quantity"));
            return item;
        }
    }
    
}
