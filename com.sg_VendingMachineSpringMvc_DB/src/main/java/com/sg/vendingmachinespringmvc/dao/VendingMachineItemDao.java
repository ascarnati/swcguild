/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface VendingMachineItemDao {
    // add the given Item to the data store
    public Item addItem(Item item);

    // remove the Item with the given id from the data store
    public void removeItem(int id);

    // update the given Item in the data store
    public void updateItem(Item item);
    
    public void updateItemInventory(int quantity, int id);

    // retrieve all Items from the data store
    public List<Item> getAllItems();

    // retrieve the Item with the given id from the data store
    public Item getItemById(int id);
    
}
