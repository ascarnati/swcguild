/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class VendingMachineItemDaoTest {

    private VendingMachineItemDao dao;
    private JdbcTemplate jdbcTemplate;
    
    public VendingMachineItemDaoTest() {
        }

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("testDao", VendingMachineItemDao.class);
        
        // remove all of the Items
        List<Item> items = dao.getAllItems();
        for (Item currentItem : items) {
            dao.removeItem(currentItem.getId());
        }
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void addUpdateItem() {
        // the database will auto generate an item ID, but the constructor
        // requires that we pass an ID, so we spoof one first, then get the 
        // actual item_id from the dao when it creates the object
        Item newItem = new Item(1);
        newItem.setName("Coke");
        newItem.setPrice(new BigDecimal("1.25"));
        newItem.setQuantity(5);
        newItem = dao.addItem(newItem);
        
        newItem.setQuantity(4);
        dao.updateItem(newItem);
        Item fromDb = dao.getItemById(newItem.getId());
        assertEquals(fromDb, newItem);
    }
    
    @Test
    public void addGetDeleteItem() {
        Item newItem2 = new Item(2);
        newItem2.setName("Water");
        newItem2.setPrice(new BigDecimal("1.00"));
        newItem2.setQuantity(5);
        newItem2 = dao.addItem(newItem2);
        
        Item fromDb = dao.getItemById(newItem2.getId());
        assertEquals(fromDb, newItem2);
        dao.removeItem(newItem2.getId());
        assertNull(dao.getItemById(newItem2.getId()));
    }
    
    
// this test is for an InMemoryDao
//    @Test
//    public void addGetDeleteItem() {
//
//        // create new item
//        Item newItem = new Item(1);
//        newItem.setName("Toblerone");
//        newItem.setPrice(new BigDecimal("2.35"));
//        newItem.setQuantity(5);
//        dao.addItem(newItem);
//        
//        // confirm that we can get the item back from the dao
//        Item fromDb = dao.getItemById(newItem.getId());
//        assertEquals(fromDb, newItem);
//        
//        // confirm that we can remove the item from the dao
//        dao.removeItem(newItem.getId());
//        assertNull(dao.getItemById(newItem.getId()));
//    }

// this test is for an InMemoryDao
//    @Test
//    public void addUpdateItem() {
//        // create new item
//        Item newItem = new Item(2);
//        newItem.setName("Red Stripes");
//        newItem.setPrice(new BigDecimal("1.25"));
//        newItem.setQuantity(5);
//        dao.addItem(newItem);
//        
//        // change the quantity value
//        newItem.setQuantity(1);
//        dao.updateItem(newItem);
//        
//        // confirm that we the value was changed in the dao
//        Item fromDb = dao.getItemById(newItem.getId());
//        assertEquals(fromDb, newItem);
//    }
    
//    @Test
//    public void getAllItems() throws FileNotFoundException {
//        
//        // confirm the dao has correct number of items now
//        List<Item> itemList = dao.getAllItems();
//        assertEquals(itemList.size(), 11);        
//    }
}
