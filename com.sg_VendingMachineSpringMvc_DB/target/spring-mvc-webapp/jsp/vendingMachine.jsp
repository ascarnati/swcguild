<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vending Machine Page</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="css/anthonyStyles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container" class="mainDivBorder" id="vendingMachineContainer">
            <!--row 1 is the header only; offset 4 to center it in the container -->
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                    <h2>Vending Machine</h2>
                </div>
            </div>
            <hr>
            <!--Container for the 3x3 box; 9/12 of the page container-->
            <div class="row">
                <div class="row col-md-12">
                    <div class="col-md-offset-1 col-md-7" id="vendItemButtonsContainer">
                        <!--<table id="itemsTable" class="table table-hover">-->
                        <!--#3: This holds the list of items - we will add rows dynamically using jQuery -->
                        <!--<tbody id="contentRows"></tbody>--> 
                        <c:forEach var="currentItem" items="${inventoryList}">
                            <button type="btn" onclick=selectItem(${currentItem.id})
                                    value="selectItem" class="button col-md-4">
                                <p style="text-align: left">
                                    <c:out value="${currentItem.id}"/>
                                </p>
                                <p>
                                    <c:out value="${currentItem.name}"/>
                                </p>
                                <p>
                                    <c:out value="${currentItem.price}"/>
                                </p>
                                <p>
                                    <c:out value="${currentItem.quantity}"/>
                                </p>
                            </button>
                        </c:forEach><br>
                        <!--</table>-->      
                    </div>
                    <hr>
                    <!--Container for the 1x3 box that holds the Control forms; 3/12 of the page container -->
                    <div class="col-md-3">
                        <form 
                            role="form" method="POST"
                            action="makePurchase"  >                                
                            <!-- first row of control forms; 3 cols total -->
                            <div class="row" id="inputFormContainer">
                                <div>
                                    <h3><p>Total $ In</p></h3>
                                    <input type="text" class="form-control" id="displayTotalIn" value="${totalIn}" name="totalInForController" placeholder="$$$" readonly/>
                                    <button type="button" id="addDollar" class="btn btn-success">Add Dollar</button>
                                    <button type="button" id="addQuarter" class="btn btn-success">Add Quarter</button>
                                    <button type="button" id="addDime" class="btn btn-success">Add Dime</button>
                                    <button type="button" id="addNickel" class="btn btn-success">Add Nickel</button>
                                </div>
                            </div>
                            <hr>
                            <!--                     second row of control forms; 3 cols total -->
                            <div class="row" id="messageFormContainer">
                                <div>
                                    <h3><p>Messages</p></h3>
                                    <input type="text" class="form-control" id="displayMessages" value="${message}" name="messageForController" placeholder="Hello!" readonly/>
                                    <div class="form-group">
                                        <label for="displayItem">Item:</label>
                                        <input type="text" class="form-control" id="displayItem" placeholder="" readonly>
                                        <input type="submit" value="Make Purchase" id="makePurchaseButton" class="btn btn-success"/>
                                        <input type="hidden" id="itemID" value="${itemID}" name="itemID"/>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!--third row of control forms; 3 cols total -->
                            <div class="row" id="changeFormContainer">
                                <div>
                                    <h3><p>Change</p></h3>
                                    <input type="text" class="form-control" id="displayChangeReturn" placeholder="" value="${userRefund}" readonly/>
                                    <button type="button" value="changeReturn" id="changeReturnButton" class="btn btn-success">Change Return</button>
                                </div>
                            </div>
                        </form>    
                    </div>    
                </div>
            </div>
        </div>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vendingMachine.js"></script>

    </body>
</html>
