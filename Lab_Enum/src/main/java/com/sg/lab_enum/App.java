/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.lab_enum;

import java.time.DayOfWeek;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);
//        DayOfWeekEnum myEnum = DayOfWeekEnum.FRIDAY;

        String userInputDay;
        int numberOfDaysUntil;

        System.out.println("Please input any day of the week: ");
        userInputDay = myScanner.nextLine();
        System.out.println("You entered " + userInputDay);
        
        
        DayOfWeekEnum myEnum = DayOfWeekEnum.valueOf(userInputDay.toUpperCase());
                
//        DayOfWeek.values();
//        DayOfWeek.valueOf(userInputDay);
        
        switch (myEnum) {
            case MONDAY:
                System.out.println("4 more days till Friday.");
                break;
            case TUESDAY:
                System.out.println("3 more days till Friday.");
                break;
            case WEDNESDAY:
                System.out.println("2 more days till Friday.");
                break;
            case THURSDAY:
                System.out.println("1 more day till Friday.");
                break;
            case FRIDAY:
                System.out.println("You NEED a vacation.");
                break;
            case SATURDAY:
                System.out.println("6 more days till Friday.");
                break;
            case SUNDAY:
                System.out.println("5 more days till Friday.");
                break;
            default:
                throw new UnsupportedOperationException("Unknown DOW Operator");

        }

    }

}
