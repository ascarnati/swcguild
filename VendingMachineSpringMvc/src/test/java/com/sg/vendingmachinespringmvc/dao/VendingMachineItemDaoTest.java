/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class VendingMachineItemDaoTest {

    private VendingMachineItemDao dao;
    
    public VendingMachineItemDaoTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("testDao", VendingMachineItemDao.class);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void addGetDeleteItem() {
        // create new item
        Item newItem = new Item(1);
        newItem.setName("Toblerone");
        newItem.setPrice(new BigDecimal("2.35"));
        newItem.setQuantity(5);
        dao.addItem(newItem);
        
        // confirm that we can get the item back from the dao
        Item fromDb = dao.getItemById(newItem.getId());
        assertEquals(fromDb, newItem);
        
        // confirm that we can remove the item from the dao
        dao.removeItem(newItem.getId());
        assertNull(dao.getItemById(newItem.getId()));
    }
    
    @Test
    public void addUpdateItem() {
        // create new item
        Item newItem = new Item(2);
        newItem.setName("Red Stripes");
        newItem.setPrice(new BigDecimal("1.25"));
        newItem.setQuantity(5);
        dao.addItem(newItem);
        
        // change the quantity value
        newItem.setQuantity(1);
        dao.updateItem(newItem);
        
        // confirm that we the value was changed in the dao
        Item fromDb = dao.getItemById(newItem.getId());
        assertEquals(fromDb, newItem);
    }
    
    @Test
    public void getAllItems() throws FileNotFoundException {
        
        // confirm the dao has 4 items now
        List<Item> itemList = dao.getAllItems();
        assertEquals(itemList.size(), 11);        
    }
}
