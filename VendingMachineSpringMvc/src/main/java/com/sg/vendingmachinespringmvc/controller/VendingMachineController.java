/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.controller;

import com.sg.vendingmachinespringmvc.dao.VendingMachineItemDao;
import com.sg.vendingmachinespringmvc.dao.VendingMachineItemDaoInMemImpl;
import com.sg.vendingmachinespringmvc.model.Change;
import com.sg.vendingmachinespringmvc.model.Item;
import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class VendingMachineController {

    VendingMachineItemDao dao;
    List<String> inventoryList;
    
    
    // @Inject annotation injects the dao Impl bean from the spring-persistence.xml 
    // file
    @Inject
    public VendingMachineController(VendingMachineItemDao dao) {
        this.dao = dao;
    }

    // value = / after removing the greeting file list from the web.xml file, 
    // the "/" value in the RequestMapping indicates that ANY request for the 
    // root of the project will start here
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayVendingMachine(Model model) {
        // RETRIEVE ALL ITEMS and create a list
        List<Item> inventoryList = dao.getAllItems();

        // Put the list on the Model
        model.addAttribute("inventoryList", inventoryList);

        // Return the name of our view
        return "vendingMachine";
    }

    @RequestMapping(value = "/makePurchase", method = RequestMethod.POST)
    public String makePurchase(HttpServletRequest request, Model model) {
        String message;
        String itemIdParam = request.getParameter("itemID");
        int itemID = Integer.parseInt(itemIdParam);

        String balanceParam = request.getParameter("totalInForController");
        BigDecimal balance = new BigDecimal(balanceParam).setScale(2);
        
        // check for inventory
        if (dao.getItemById(itemID).getQuantity() <= 0) {
            message = "Sold Out!";
            // Put the inventory and other values that were passed in back
            // back on the Model bc we're reloading .jsp rather than redirecting first
            List<Item> inventoryList = dao.getAllItems();
            model.addAttribute("inventoryList", inventoryList);
            model.addAttribute("message", message);
            model.addAttribute("totalIn", balance);
            model.addAttribute("itemID", itemID);
            
            return "vendingMachine";
        }

        // check for sufficient funds
        if (dao.getItemById(itemID).getPrice().compareTo(balance) == 1) {
            BigDecimal itemPrice = dao.getItemById(itemID).getPrice();
            BigDecimal amountToInput = itemPrice.subtract(balance);

            message = "Please insert $" + amountToInput + ".";
            // Put the inventory and the other values that were passed in 
            // back on the Model bc we're reloading .jsp rather than redirecting first
            List<Item> inventoryList = dao.getAllItems();
            model.addAttribute("inventoryList", inventoryList);
            model.addAttribute("message", message);
            model.addAttribute("totalIn", balance);
            model.addAttribute("itemID", itemID);

            return "vendingMachine";
        }
        
        // if we've made it this far, its a successful vend, so
        // reset the totalIn display to blank
        String totalInReset = "";
        
        // decrement item quantity by 1
        int newQuantity = (dao.getItemById(itemID).getQuantity() - 1);
        dao.getItemById(itemID).setQuantity(newQuantity);

        // calculate the userRefund
        BigDecimal itemPrice = dao.getItemById(itemID).getPrice();
        BigDecimal userRefund = balance.subtract(itemPrice).setScale(2);
        
        // convert the userRefund to coins using the DTO object constructor
        Change customerChange = new Change(userRefund);
        String changeRefundString = 
                customerChange.getQuarters() + " quarters, " + 
                customerChange.getDimes() + " dimes, " + 
                customerChange.getNickels() + " nickels, " + 
                customerChange.getPennies() + " pennies";

        // set the message 
        message = "Thank You!";
        
        // need to have the refreshed list to present again to the View
        List<Item> inventoryList = dao.getAllItems();

        // Put these variables on the Model
        model.addAttribute("inventoryList", inventoryList);
        // add userRefund in coins to the model
        model.addAttribute("userRefund", changeRefundString);
        // add message to the model
        model.addAttribute("message", message);
        // add reset totalIn to the model
        model.addAttribute("totalIn", totalInReset);
        
        return "vendingMachine";
    }

}
