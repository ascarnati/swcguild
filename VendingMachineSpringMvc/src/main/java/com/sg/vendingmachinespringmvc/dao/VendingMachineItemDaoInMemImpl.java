/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 *
 * @author apprentice
 */
public class VendingMachineItemDaoInMemImpl implements VendingMachineItemDao {

    // holds all of our Item objects - simulates the database
    private Map<Integer, Item> itemMap = new HashMap<>();
    // used to assign ids to Contacts - simulates the auto increment
    // primary key for Contacts in a database
    private static int itemIdCounter = 0;
    
    public VendingMachineItemDaoInMemImpl() {
        
        Item newItem = new Item(1);
        newItem.setName("Snickers");
        newItem.setPrice(new BigDecimal("1.25"));
        newItem.setQuantity(10);
        itemMap.put(1, newItem);
        
        itemMap.put(2, new Item(2,"M&Ms",new BigDecimal("1.50"),5));
        itemMap.put(3, new Item(3,"Red Stripes",new BigDecimal("1.75"),2));
        itemMap.put(4, new Item(4,"Doritos",new BigDecimal("2.10"),0));
        itemMap.put(5, new Item(5,"Chips",new BigDecimal("1.70"),9));
        itemMap.put(6, new Item(6,"Pretzels",new BigDecimal("1.45"),6));
        itemMap.put(7, new Item(7,"Water",new BigDecimal("1.50"),4));
        itemMap.put(8, new Item(8,"Coke",new BigDecimal("2.25"),3));
        itemMap.put(9, new Item(9,"Gum",new BigDecimal("1.25"),10));
        itemMap.put(10, new Item(10,"Sprite",new BigDecimal("1.25"),10));
        itemMap.put(11, new Item(11,"Beer",new BigDecimal("1.25"),10));
        
    }
    
    @Override
    public Item addItem(Item item) {
        // assign the current counter values as the item id
        item.setId(itemIdCounter);
        // increment the counter so it is ready for use for the 
        // next contact
        itemIdCounter++;
        itemMap.put(item.getId(), item);
        return item;
        
    }

    @Override
    public void removeItem(int id) {
        itemMap.remove(id);
    }

    @Override
    public void updateItem(Item item) {
        itemMap.put(item.getId(), item);
    }

    @Override
    public List<Item> getAllItems() {
        return new ArrayList<Item>(itemMap.values());
    }

    @Override
    public Item getItemById(int id) {
        return itemMap.get(id);
    }
    
    
}
