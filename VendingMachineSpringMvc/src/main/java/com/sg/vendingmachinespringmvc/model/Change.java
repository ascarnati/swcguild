/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author apprentice
 */
public class Change {
    
    private BigDecimal quarters;
    private BigDecimal dimes;
    private BigDecimal nickels;
    private BigDecimal pennies;

    private BigDecimal penniesInADollar = new BigDecimal("100");
    private BigDecimal penniesInAQuarter = new BigDecimal("25");
    private BigDecimal penniesInADime = new BigDecimal("10");
    private BigDecimal penniesInANickel = new BigDecimal("5");
    
    BigDecimal runningBalance = new BigDecimal("0");
    
    public Change(BigDecimal userRefund) {
        BigDecimal allThePennies = userRefund.multiply(penniesInADollar);
        
        this.quarters = allThePennies.divide(penniesInAQuarter).setScale(0, RoundingMode.DOWN);
        runningBalance = allThePennies.remainder(penniesInAQuarter);

        this.dimes = runningBalance.divide(penniesInADime).setScale(0, RoundingMode.DOWN);
        runningBalance = runningBalance.remainder(penniesInADime);

        this.nickels = runningBalance.divide(penniesInANickel).setScale(0, RoundingMode.DOWN);
        runningBalance = runningBalance.remainder(penniesInANickel);

        this.pennies = runningBalance.setScale(0, RoundingMode.DOWN);
        
    }
    
    
    public BigDecimal getQuarters() {
        return quarters;
    }

    public void setQuarters(BigDecimal quarters) {
        this.quarters = quarters;
    }

    public BigDecimal getDimes() {
        return dimes;
    }

    public void setDimes(BigDecimal dimes) {
        this.dimes = dimes;
    }

    public BigDecimal getNickels() {
        return nickels;
    }

    public void setNickels(BigDecimal nickels) {
        this.nickels = nickels;
    }

    public BigDecimal getPennies() {
        return pennies;
    }

    public void setPennies(BigDecimal pennies) {
        this.pennies = pennies;
    }
    
    
    
}
