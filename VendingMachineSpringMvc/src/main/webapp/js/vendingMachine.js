$(document).ready(function() {

    // each time the page loads, need to ensure the make purchase button is
    // disabled until an item is selected;  but the page may reload as a result
    // of insufficient funds or inventory, in which case we want to reload it
    // with the previously selected itemID and balance
    var currentBalance = $('#displayTotalIn').val();
    // if there WAS a balance, display the current itemID value too    
    if (currentBalance !== "") {
        $('#displayItem').val($('#itemID').val());
    } else {
    // otherwise set the initial value of the field to 0.00 and disable the button
        $('#displayTotalIn').val(0.00);
        document.getElementById('makePurchaseButton').disabled=true;
    }

    $('#addDollar').click(function(event) {
        addDollar();
    });

    $('#addQuarter').click(function(event) {
        addQuarter();
    });

    $('#addDime').click(function(event) {
        addDime();
    });

    $('#addNickel').click(function(event) {
        addNickel();
    });

    // #changeReturnButton handler
    $('#changeReturnButton').click(function(event) {
        var balance = $('#displayTotalIn').val();
        changeReturn(balance);
    });

});

// #makePurchasebutton is disabled unless the user selects an item
function selectItem(vendItemId) {
    document.getElementById('makePurchaseButton').disabled=false;
    $('#itemID').val(vendItemId);
    $('#displayItem').val(vendItemId);
}

function addDollar() {
    // get the value from the field on the html page and use that to replace 
    // anywhere in JS that uses "balance" $('#displayTotalIn').
    var balance = parseFloat($('#displayTotalIn').val());
    var dollar = parseFloat(1.00);
    balance += dollar;
    // use javascript to update the current balance on the View (.jsp)
    displayTotalIn(balance);
}

function addQuarter() {
    var balance = parseFloat($('#displayTotalIn').val());
    var quarter = parseFloat(0.25);
    balance += quarter;
    displayTotalIn(balance);
}

function addDime() {
    var balance = parseFloat($('#displayTotalIn').val());
    var dime = parseFloat(0.10);
    balance += dime;
    displayTotalIn(balance);
}

function addNickel() {
    var balance = parseFloat($('#displayTotalIn').val());
    var nickel = parseFloat(0.05);
    balance += nickel;
    displayTotalIn(balance);
}


function displayTotalIn(balance) {
    $('#displayTotalIn').val(balance.toFixed(2));
}

// #changeReturnButton functionality
function changeReturn(balance) {

    var quarter = parseFloat(0.25);
    var dime = parseFloat(0.10);
    var nickel = parseFloat(0.05);
    var penny = parseFloat(0.01);
    var runningBalance = parseFloat(0.00);


    if (balance < .01) {
        $('#displayMessages').val("Beat it, freeloader!");
        $('#displayChangeReturn').val("");

    } else {
        $('#displayMessages').val("Don't forget your change!");
        

        if (balance >= .25) {
        var quartersRefund = Math.floor(balance / quarter);
        runningBalance = parseFloat(balance % quarter).toFixed(2);

        }

        //var dimesRefund = runningBalance divided by dime
        if (runningBalance >= .10) {
        var dimesRefund = Math.floor(runningBalance / dime);
        runningBalance = parseFloat(runningBalance % dime).toFixed(2);        
        }

        //var nickelsRefund = runningBalance divided by nickel
        if (runningBalance >= .05) {
        var nickelsRefund = Math.floor(runningBalance / nickel);
        runningBalance = parseFloat(runningBalance % nickel).toFixed(2);
        }

        // penniesRefund = runningBalance divided by penny
        var penniesRefund = Math.floor(runningBalance / penny);

        // create a string value to display in the VIEW

        var changeRefundString = quartersRefund + " quarters, " + dimesRefund + " dimes, " + nickelsRefund + " nickels, " + penniesRefund + " pennies";
        $('#displayChangeReturn').val(changeRefundString);
        $('#displayTotalIn').val("0.00");

    }
}

function clearAllDisplays() {
    $('#displayTotalIn').val("0.00");

    // clear displayed error message
    $('#displayMessages').val("");

    // clear item display
    $('#displayItem').val("");

    // clear change return
    $('#displayChangeReturn').val("");
}

function clearItemsTable() {
    $('#contentRows').empty();
}
