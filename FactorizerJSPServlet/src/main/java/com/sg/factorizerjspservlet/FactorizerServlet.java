/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.factorizerjspservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FactorizerServlet", urlPatterns = {"/FactorizerServlet"})
public class FactorizerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // declare a list of integers that will hold our factor list as the user
        // adds a number to factor
        List<Integer> factorList = new ArrayList<>();
        int factorSum = 0;
        boolean isPrime = false;
        boolean isPerfect = false;

        // Get the value that was submitted in the index.jsp form
        // ("numberToFactor" is the parameter of the getParameter method)
        // the number comes to us in the parameter AS A STRING so we need
        // to create a var for it, then convert it to an integer
        String input = request.getParameter("numberToFactor");
        int numberToFactor = Integer.parseInt(input);
        
        // create the logic that our app will use
        
        for (int i = 1; i < numberToFactor; i++) {
            if (numberToFactor % i == 0) {
                factorList.add(i);
                factorSum += i;
            }
        }
        
        if (factorSum == numberToFactor) {
            isPerfect = true;            
        }
        
        if (factorSum == 1) {
            isPrime = true;
        }
        
        // after declaring the variables, and writing the logic, we need
        // to set the attributes to be sent to the .jsp (aka View)

        // Set numberToFactor as an attribute on the request so that
        // it is available to result.jsp
        // the params in setAttribute act like a map; they're a key/value set;
        // i.e. the name of the thing, and the thing itself
        request.setAttribute("numberToFactor", numberToFactor);
        request.setAttribute("factors", factorList);
        request.setAttribute("isPrime", isPrime);
        request.setAttribute("isPerfect", isPerfect);
        
        // Get the Request Dispatcher for result.jsp (result.jsp is the parameter
        // of the getRequestDispatcher method)
        RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
        
        // and forward the request to result.jsp (parameter)
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
