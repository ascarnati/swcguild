/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps.exercises;

import java.util.List;
import misc.Dog;
import misc.MiscObjectFactory;
import misc.Person;

/**
 *
 * @author ahill
 */
public class Indexing {
    public static void main(String[] args) {
        
        // Exercise 1:
        // Index this list of people by full name into a hashmap
        // Then print out all the fullNames and ages.
        List<Person> peopleToIndexByName = MiscObjectFactory.makePeople(20);
        
        
        // Exercise 2:
        // Index this next list first by microchip ID, and then in another hashmap by name.
        // Print ou the size of both, what's the difference?
        List<Dog> dogsToIndex = MiscObjectFactory.makeDogs(500);
        
        
        
    }
}
