/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps;

import java.util.ArrayList;
import java.util.HashMap;
import misc.Fruit;
import misc.MiscObjectFactory;

/**
 *
 * @author ahill
 */
public class ForCollecting {

    public static void main(String[] args) {

        // HashMaps can be useful to sort results or lists into "bins"
        // Here I have a list of 50 random fruit of different colors.
        ArrayList<Fruit> bunchOfFruit = MiscObjectFactory.makeFruit(50);
        
        // I can sort them by color, by making a hashmap with a value of string
        // (their colors are strings) and a list as a value, and then adding
        // each fruit as it comes to the hashMap to a new list, and then each
        // other fruit of that color to the existing list for that color.
        
        HashMap<String, ArrayList<Fruit>> fruitByColorMap = new HashMap<>();
        
        for (Fruit fruitToSort: bunchOfFruit) {
            
            String fruitColor = fruitToSort.getColor();
            
            // Remember - hashmaps return null if the key/value pair you asked for
            // doesn't exist. So if it's null, we have to create a new ArrayList.
            if(fruitByColorMap.get(fruitColor) == null){
                fruitByColorMap.put(fruitColor, new ArrayList<Fruit>());
            }
            
            // Get the list of fruit of that color, then add the fruit to it
            // If it was just created it will have been empty, but will now have one
            // if it wasn't just created, it will already have fruit that color in
            // the list!
            ArrayList<Fruit> fruitThatColor = fruitByColorMap.get(fruitColor);
            fruitThatColor.add(fruitToSort);
            
        }
        
        // Print out the number of each color fruit found in the random list
        for(String fruitColor : fruitByColorMap.keySet()){
            System.out.println("# of " + fruitColor + " fruit: " 
                    + fruitByColorMap.get(fruitColor).size());
        }
        
        

    }

}
