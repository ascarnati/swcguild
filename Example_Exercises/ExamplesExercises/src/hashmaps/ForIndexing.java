/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ahill
 */
public class ForIndexing {
    public static void main(String[] args) {
        
        // Creating a quick retrieve index...
        HashMap<String, String> phoneNumbers = new HashMap();
        phoneNumbers.put("Sally", "982-5568");
        phoneNumbers.put("Ron", "823-3421");
        phoneNumbers.put("Joe", "112-1324");
        phoneNumbers.put("Linda", "233-4245");
        phoneNumbers.put("Lisa", "456-1234");
        phoneNumbers.put("Brad", "334-6478");
        
        // You can then quickly retrieve a person's number just by using their name
        System.out.println("Sally's # :" + phoneNumbers.get("Sally"));
        
        // However, you run into a problem if one person has multiple numbers!
        // ------- Lets try it again but with a LIST of numbers per person
        HashMap<String, ArrayList<String>> morePhoneNumbers = new HashMap();
        
        // To add a more complex object into a map, like an ArrayList, you have to create it first!
        // This means you have to call it's constructor.
        // You can either create an object and store it in a named variable while
        // you update it to contain information and THEN put it into the hashmap
        ArrayList<String> sallysNumbers = new ArrayList<>();
        sallysNumbers.add("982-5568");
        sallysNumbers.add("456-2345");
        morePhoneNumbers.put("Sally", sallysNumbers);
        
        // Or you can create a new ArrayList (or obj) and store it in the map directly
        // However, if you want to make changes, you'll have to "get" it from the map
        // to be able to update it.
        
        morePhoneNumbers.put("Ron", new ArrayList<>());
        // This retrieves the created list from above, and stores it in a named variable
        ArrayList<String> ronsNumbers = morePhoneNumbers.get("Ron"); 
        ronsNumbers.add("823-3421");
        
        System.out.println("All Sally's #'s");
        for(String phoneNumber : morePhoneNumbers.get("Sally")){
            System.out.println(phoneNumber);
        }
        
        // ----------------- If you can come up with a unique identifier of an obj
        // You can use it as an index in a hashmap. But it should be something that is
        // easily remembered or found. Dates are good. Names are good. IDs are good.
        // Random numbers, not as good.  
        
        HashMap<String, String> stateBirds = new HashMap<>();
        stateBirds.put("Kentucky", "Cardinal");
        stateBirds.put("Arizona", "Cactus Wren");
        stateBirds.put("Colorado", "Lark bunting");
        
        // Finding the state bird above is easy if you know the state name
        // and faster than iterating thru a list of all 50 birds to find the right state
        // Or having 50 different variables called things like ky_stateBird
        // HashMaps can grow or shrink more easily as new information gets added
        // as LONG as the key/value pairs stay unique.
        
        System.out.println("The state bird of Colorado " + stateBirds.get("Colorado"));
        
        
    }
}
