/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.util.Random;

/**
 *
 * @author ahill
 */
public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String id;

    public Person() {
    }

    public Person(String firstName, String lastName, int birthYear) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = birthYear;
        this.id = java.util.UUID.randomUUID().toString();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthYear() {
        return age;
    }

    public void setBirthYear(int birthYear) {
        this.age = birthYear;
    }
    
    public String getFullName(){
        return firstName + " " + lastName;
    }
    
    public String getID(){
        return id;
    }
    
}
