/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

/**
 *
 * @author ahill
 */
public abstract class Dog {
    private String color;
    private double weight;
    protected String name;
    private String microChip;
    
    public Dog(String name, String color, double weight){
        this.color = color;
        this.weight = weight;
        this.name = name;
        this.microChip = java.util.UUID.randomUUID().toString();
    }
    
    public void bark(){
        System.out.println("Woof!");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getMicroChip(){
        return microChip;
    }
    
}
