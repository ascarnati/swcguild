/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringcalculatorspringmvc;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class FlooringCalculatorController {
    
    @RequestMapping(value = "/FlooringCalculatorServlet",
            method = RequestMethod.POST)
    public String FlooringCalculatorServlet(HttpServletRequest request,
            Map<String, Object> model) {
    
        // *** declare my variables ***
        
        int length;
        int width;
        int area;
        double materialCpSf;
        double materialCost;
        double laborHours;
        double laborCost;               
        double totalCost;
        
        // *** get the values submitted in the index.jsp form as strings and convert
        
        String inputLength = request.getParameter("length");
        length = Integer.parseInt(inputLength);
        
        String inputWidth = request.getParameter("width");
        width = Integer.parseInt(inputWidth);
        
        String inputMaterialCpSf = request.getParameter("costPerSqFt");
        materialCpSf = Double.parseDouble(inputMaterialCpSf);
        
        
        // *** process all business logic ***
        // calculate area = length x width
        area = length * width;
        
        // given area, calculate materialCost
        materialCost = area * materialCpSf;
        
        // calculating laborHours: 
            // first calculate labor Periods (1 period = .25 hours)
            // if 20 sqft per hour, then 5 sqft per period
        double laborPeriods = Math.round(area / 5);
            // round this value up to the next period
        
            // divide this number by 4 to display as an laborHours
        laborHours = laborPeriods / 4;
                
        // given labor hours, calculate laborCost as laborHours x 86.00
        laborCost = laborHours * 86.00;
        
        // given laborCost and materialCost, calculate totalCost
        totalCost = laborCost + materialCost;
        
        // *** PUT the info into the OBJECT map provided by the DispatcherServlet
        // so they'll be available to the result.jsp
        // instead of setting attributes on the request (the Dispatcher will
        // do this for us) ***
        
        model.put("area", area);
        model.put("length", length);
        model.put("width", width);
        model.put("laborHours", laborHours);
        model.put("laborCost", laborCost);
        model.put("materialCost", materialCost);
        model.put("totalCost", totalCost);
        model.put("materialCpSf", materialCpSf);        
        
        return "result";
    }
    
}
