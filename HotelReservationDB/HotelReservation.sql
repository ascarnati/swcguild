DROP DATABASE HotelCalifornia;

CREATE DATABASE IF NOT EXISTS HotelCalifornia;

USE HotelCalifornia;


-- CREATE BASE tables
CREATE TABLE IF NOT EXISTS RoomType (
	RoomTypeID int(6) NOT NULL AUTO_INCREMENT,
    Description varchar(20) NOT NULL,
    Occupancy int(2) null,
    BaseRate decimal(6,2) null,
    PRIMARY KEY (RoomTypeID)
);

CREATE TABLE IF NOT EXISTS Amenity (
	AmenityID int(6) NOT NULL AUTO_INCREMENT,
    Description varchar(50) NOT NULL,
    AmenityFee decimal(6,2) null,
    PRIMARY KEY (AmenityID)
);

CREATE TABLE IF NOT EXISTS RateModifier (
	RateModifierID int(6) NOT NULL AUTO_INCREMENT,
    RateCategory varchar(25) NOT NULL,
    RateName varchar(25) NOT NULL,
    RateDescription varchar(50) null,
    RateFactor decimal (3,2) NOT NULL,
    RateFlatDiscount int(6) null,
    StartDate date null,
    EndDate date null,
    PRIMARY KEY (RateModifierID)
);

CREATE TABLE IF NOT EXISTS Service (
	ServiceID int(6) NOT NULL AUTO_INCREMENT,
    Description varchar(50) NOT NULL,
    ServiceFee decimal(5,2) null,
    PRIMARY KEY (ServiceID)
);

CREATE TABLE IF NOT EXISTS Room (
	RoomID int(6) NOT NULL AUTO_INCREMENT,
    RoomNum int(4) NOT NULL,
    FloorNum int(2) NOT NULL,
    IsReserved bit(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (RoomID),
    RoomTypeID int(2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Reservation (
	ReservationID int(10) NOT NULL AUTO_INCREMENT,
    StartDate date NOT NULL,
    EndDate date NOT NULL,
    PRIMARY KEY (ReservationID),
    CustomerID int(6),
    RateModifierID int(6),
    BillingDetailsID int(6)
);

CREATE TABLE IF NOT EXISTS Phone (
	PhoneID int(6) NOT NULL AUTO_INCREMENT,
    PhoneType varchar(20) null,
    PhoneNumber varchar(12) null,
    IsPreferred bit(1) null DEFAULT 0,
    PRIMARY KEY (PhoneID),
    CustomerID int(6) NOT NULL
);

CREATE TABLE IF NOT EXISTS Customer (
	CustomerID int(6) NOT NULL AUTO_INCREMENT,
    CustomerFirstName varchar(15) NOT NULL,
    CustomerLastName varchar(15) NOT NULL,
    CustomerEmail varchar(50) null,
    CustomerHomeStreet varchar(100) null,
    CustomerHomeCity varchar(30) null,
    CustomerHomeState varchar (2) null,
    CustomerHomeZip varchar (5) null,
    PRIMARY KEY (CustomerID)
);

CREATE TABLE IF NOT EXISTS Guest (
	GuestID int(6) NOT NULL AUTO_INCREMENT,
    GuestFirstName varchar(15) NOT NULL,
    GuestLastName varchar(15) NOT NULL,
    GuestAge int(2) NOT NULL,
    PRIMARY KEY (GuestID),
    ReservationID int(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS BillingSummary (
    BillingSummaryID int NOT NULL AUTO_INCREMENT,
    TotalDue decimal(7,2) null,
    SubTotalDue decimal(7,2) null,
    TaxDue decimal(7,2) null,
    ReservationID int,
    PRIMARY KEY (BillingSummaryID),
    CONSTRAINT fk_BillingSummary_Reservation FOREIGN KEY (ReservationID) REFERENCES Reservation(ReservationID)
);

CREATE TABLE IF NOT EXISTS BillingDetails (
	BillingDetailsID int(6) NOT NULL AUTO_INCREMENT,
    DateOfStay date NOT NULL,
    RoomRate decimal(7,2) null,
    ServiceFees decimal(7,2) null,
    IsServiceFeeWaived bit(1) DEFAULT 0,
    AmenityFees decimal(7,2) null,
    IsAmenityFeeWaived bit(1) DEFAULT 0,
    DiscountsApplied varchar(30) null,
    Notes varchar(255) null,
    BillingSummaryID int,
    PRIMARY KEY (BillingDetailsID),
    CONSTRAINT fk_BillingDetails_BillingSummary FOREIGN KEY (BillingSummaryID) REFERENCES BillingSummary(BillingSummaryID)
);

-- CREATE BRIDGE tables for many-to-many relationships
CREATE TABLE IF NOT EXISTS RoomAmenity (
	RoomID int NOT NULL,
    AmenityID int NOT NULL,
    PRIMARY KEY (RoomID, AmenityID),
    CONSTRAINT fk_RoomAmenity_Room FOREIGN KEY (RoomID) REFERENCES Room(RoomID),
    CONSTRAINT fk_RoomAmenity_Amenity FOREIGN KEY (AmenityID) REFERENCES Amenity(AmenityID)
);

CREATE TABLE IF NOT EXISTS RoomReservation (
	RoomID int NOT NULL,
    ReservationID int NOT NULL,
    PRIMARY KEY (RoomID, ReservationID),
    CONSTRAINT fk_RoomReservation_Room FOREIGN KEY (RoomID) REFERENCES Room(RoomID),
    CONSTRAINT fk_RoomReservation_Reservation FOREIGN KEY (ReservationID) REFERENCES Reservation(ReservationID)
);

CREATE TABLE IF NOT EXISTS ServiceReservation (
	ServiceID int NOT NULL,
    ReservationID int NOT NULL,
    DateOfService date null,
    isFeeWaived bit null DEFAULT 0,
    PRIMARY KEY (ServiceID, ReservationID),
    CONSTRAINT fk_ServiceReservation_Service FOREIGN KEY (ServiceID) REFERENCES Service(ServiceID),
    CONSTRAINT fk_ServiceReservation_Reservation FOREIGN KEY (ReservationID) REFERENCES Reservation(ReservationID)
);

-- ALTER TABLES to create many-to-one FK relationships
ALTER TABLE Phone
	ADD FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID);

ALTER TABLE Reservation  
	ADD FOREIGN KEY (CustomerID) REFERENCES Customer(CustomerID),
	ADD FOREIGN KEY (RateModifierID) REFERENCES RateModifier(RateModifierID)
;

ALTER TABLE Room 
	ADD FOREIGN KEY (RoomTypeID) REFERENCES RoomType(RoomTypeID)
;

ALTER TABLE Guest
	ADD FOREIGN KEY (ReservationID) REFERENCES Reservation(ReservationID)
;

-- INSERT DATA into Tables --
INSERT INTO RateModifier (RateCategory, RateName, RateDescription, RateFactor, RateFlatDiscount) VALUES ('Seasonal','Peak-Season','June thru Aug',1.25,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateDescription, RateFactor, RateFlatDiscount) VALUES ('Seasonal','Off-Season','Jan thru May',0.75,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateDescription, RateFactor, RateFlatDiscount) VALUES ('Seasonal','Reg-Season','Sep thru Dec',1.00,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateFactor, RateFlatDiscount) VALUES ('Conference','Conference1',0.95,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateFactor, RateFlatDiscount) VALUES ('Corporate','LibertyMutual',0.90,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateFactor, RateFlatDiscount) VALUES ('Corporate','AAA',0.95,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateFactor, RateFlatDiscount) VALUES ('Promotional','Standard',0.90,0.00);
INSERT INTO RateModifier (RateCategory, RateName, RateDescription, RateFactor, RateFlatDiscount) VALUES ('Promotional','LLama','Must show proof of Llama',0.0,25.00);

INSERT INTO Amenity (Description, AmenityFee) VALUES ('MiniFridge',000.00);
INSERT INTO Amenity (Description, AmenityFee) VALUES ('Spa Tub',000.00);
INSERT INTO Amenity (Description, AmenityFee) VALUES ('Wifi',000.00);
INSERT INTO Amenity (Description, AmenityFee) VALUES ('Cable TV',000.00);
INSERT INTO Amenity (Description, AmenityFee) VALUES ('MiniBar',50.00);
INSERT INTO Amenity (Description, AmenityFee) VALUES ('Pool Access',000.00);
INSERT INTO Amenity (Description, AmenityFee) VALUES ('BusinessClass Wifi',25.00);

INSERT INTO RoomType (Description, Occupancy, BaseRate) VALUES ('Single',4,200.00);
INSERT INTO RoomType (Description, Occupancy, BaseRate) VALUES ('Double Queen',4,250.00);
INSERT INTO RoomType (Description, Occupancy, BaseRate) VALUES ('King',4,275.00);
INSERT INTO RoomType (Description, Occupancy, BaseRate) VALUES ('Suite',6,400.00);
INSERT INTO RoomType (Description, Occupancy, BaseRate) VALUES ('VIP',10,1500.00);

INSERT INTO Service (Description, ServiceFee) VALUES ('InRoom Dining',10.00);
INSERT INTO Service (Description, ServiceFee) VALUES ('Premium Movie',10.00);
INSERT INTO Service (Description, ServiceFee) VALUES ('Phone',0.00);
INSERT INTO Service (Description, ServiceFee) VALUES ('Premium Housekeeping',250.00);
INSERT INTO Service (Description, ServiceFee) VALUES ('Spa Services',150.00);

INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (101,1,0,1);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (102,1,0,2);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (103,1,0,3);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (104,1,0,4);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (105,1,1,5);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (201,2,0,1);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (202,2,1,2);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (203,2,0,3);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (204,2,0,4);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (205,2,0,5);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (301,3,0,1);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (302,3,0,2);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (303,3,1,3);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (304,3,1,4);
INSERT INTO Room (RoomNum, FloorNum, IsReserved, RoomTypeID) VALUES (305,3,0,5);

INSERT INTO Customer (CustomerFirstName, CustomerLastName, CustomerHomeStreet, CustomerHomeCity, CustomerHomeState, CustomerHomeZip)
VALUES ('Jimi','Hendrix','123 Red House Drive','Seattle','WA','98101');
INSERT INTO Customer (CustomerFirstName, CustomerLastName, CustomerHomeStreet, CustomerHomeCity, CustomerHomeState, CustomerHomeZip)
VALUES ('Janis','Joplin','20341 Shady Lane','Port Arthur','TX','77640');
INSERT INTO Customer (CustomerFirstName, CustomerLastName, CustomerHomeStreet, CustomerHomeCity, CustomerHomeState, CustomerHomeZip)
VALUES ('Jim','Morrison','51 Moonlight Drive','Melbourne','FL','32901');
INSERT INTO Customer (CustomerFirstName, CustomerLastName, CustomerHomeStreet, CustomerHomeCity, CustomerHomeState, CustomerHomeZip)
VALUES ('Glenn','Frey','7 Bridges Road','Detroit','MI','48201');
INSERT INTO Customer (CustomerFirstName, CustomerLastName, CustomerHomeStreet, CustomerHomeCity, CustomerHomeState, CustomerHomeZip)
VALUES ('B.B.','King','21 Memory Lane','Itta Bena','MS','38941');

INSERT INTO Phone (PhoneType, PhoneNumber, IsPreferred, CustomerID) VALUES ('Mobile','555-555-1234',1,1);
INSERT INTO Phone (PhoneType, PhoneNumber, IsPreferred, CustomerID) VALUES ('Home','555-555-5678',0,1);
INSERT INTO Phone (PhoneType, PhoneNumber, IsPreferred, CustomerID) VALUES ('Mobile','555-555-0101',1,2);
INSERT INTO Phone (PhoneType, PhoneNumber, IsPreferred, CustomerID) VALUES ('Home','555-555-1234',1,3);

INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-06-01','2017-06-07',1,1);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-07-01','2017-07-15',1,4);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-08-20','2017-08-27',2,1);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-10-31','2017-11-01',3,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-07-03','2017-07-07',2,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-09-21','2017-09-27',2,1);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-12-01','2017-12-07',1,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-02-13','2017-02-15',2,2);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-02-09','2017-02-15',3,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-06-01','2017-06-07',4,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-07-01','2017-07-15',5,4);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-08-23','2017-08-27',1,1);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-10-25','2017-11-01',5,1);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-07-01','2017-07-04',2,5);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-09-21','2017-10-01',5,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-12-06','2017-12-07',2,8);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-02-13','2017-02-15',1,2);
INSERT INTO Reservation (StartDate, EndDate, CustomerID, RateModifierID) VALUES ('2017-02-10','2017-02-12',5,8);

INSERT INTO Guest (GuestFirstName, GuestLastName, GuestAge, ReservationID) VALUES ('Moe','Howard','51',1);
INSERT INTO Guest (GuestFirstName, GuestLastName, GuestAge, ReservationID) VALUES ('Curly','Howard','45',1);
INSERT INTO Guest (GuestFirstName, GuestLastName, GuestAge, ReservationID) VALUES ('Larry','Fine','48',3);
INSERT INTO Guest (GuestFirstName, GuestLastName, GuestAge, ReservationID) VALUES ('Shemp','Howard','55',4);

INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (5,1);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (7,2);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (13,3);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (14,4);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (5,5);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (13,6);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (14,7);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (5,8);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (7,9);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (14,10);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (14,11);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (5,12);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (5,13);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (7,14);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (14,15);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (14,16);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (5,17);
INSERT INTO RoomReservation (RoomID, ReservationID) VALUES (7,18);

INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (1,4);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (2,4);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (3,4);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (4,4);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (5,4);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (5,2);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (6,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (7,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (8,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (9,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (10,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (10,5);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (10,2);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (11,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (12,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (13,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (14,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (15,1);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (15,5);
INSERT INTO RoomAmenity (RoomID, AmenityID) VALUES (15,2);

    
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-01',1500.00,50.00,0,0,0,0,'Nice guest.  Kinda quiet.');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-02',1500.00,50.00,0,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-03',1500.00,50.00,0,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-04',1500.00,50.00,0,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-05',1500.00,50.00,1,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-06',1500.00,50.00,1,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-06-07',1500.00,50.00,1,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-10-31',400.00,50.00,0,0,0,0,'');
INSERT INTO BillingDetails (DateOfStay, RoomRate, ServiceFees, IsServiceFeeWaived, AmenityFees, IsAmenityFeeWaived, DiscountsApplied, Notes)
	VALUES ('2017-11-01',400.00,50.00,0,0,0,0,'The guy trashed the place.');


INSERT INTO BillingSummary (TotalDue, SubTotalDue, TaxDue, ReservationID) VALUES (12626.00,10700.00,1926.00,1);
INSERT INTO BillingSummary (TotalDue, SubTotalDue, TaxDue, ReservationID) VALUES (1062.00,900.00,162.00,4);