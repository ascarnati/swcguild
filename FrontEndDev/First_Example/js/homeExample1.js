//home.js
$(document).ready(function() {
    alert("I'm ready");

    //$('H1').hide(); //hides all H1 headings

    //$('.notOriginal').hide(); //hides all those headings with CLASS "notOriginal"

    //$('#second').hide(); //accesses only one particular element (that with id "second") and hides it

    //$('#third').remove(); //will remove only the element with id "third" from the display of the html document

    //$('#emptyDiv').append('p').text('A new paragraph of text...');//will create a new paragraph element onto the emptyDiv html document in the html doc

    $('#first').removeClass('text-center');//removing a class from existing html element

    //$('#newButton').addClass('btn btn-default'); //adding a class to an existing html element

    //$('#first').css('color','blue');//adding style to an existing element uses the CSS function; where 'color' is the KEY, and 'blue' is the VALUE

    // NOTE the above can be used individually, or in combination as in the following:
    //$('#third').remove();
    //$('#emptyDiv').append('p').text('A new paragraph of text...');
    //$('#first').css('color', 'blue');

});