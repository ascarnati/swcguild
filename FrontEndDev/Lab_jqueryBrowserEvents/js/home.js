$(document).ready(function () {
    
	// - PAGE Load -
	//1. Only the content in the Main section should display when the page is loaded.
	$('#mainInfoDiv').show();
	$('#akronInfoDiv').hide();
	$('#minneapolisInfoDiv').hide();
	$('#louisvilleInfoDiv').hide();

	// - Nav Button Behavior - 
	//1. When the Akron button is clicked, only the content in the Akron section should display; the weather information for Akron should be hidden initially.
    $('#akronButton').on('click', function() {
		$('#mainInfoDiv').hide();
		$('#akronInfoDiv').show();
		$('#minneapolisInfoDiv').hide();
		$('#louisvilleInfoDiv').hide();
	    $('#akronWeather').hide();
	//$('#akronWeather').fadeIn("slow");
	
	// - Show/Hide Weather Behavior -
	// When the Show/Hide Weather button is clicked, the page should display the associated weather information if it was hidden or hide the associated weather information
	// if it was showing. It should default to hidden.
	$('#akronWeatherButton').click(function() {
		$("#akronWeather").toggle()});

    });
    
	//2. When the Minneapolis button is clicked, only the content in the Minneapolis section should display; the weather information for Minneapolis should be hidden initially.
	$('#minneapolisButton').on('click', function() {
		$('#mainInfoDiv').hide();
		$('#akronInfoDiv').hide();
		$('#minneapolisInfoDiv').show();
		$('#louisvilleInfoDiv').hide();
	    $('#minneapolisWeather').hide();
	//$('#minneapolisWeather').fadeIn("slow");
    
    // - Show/Hide Weather Behavior -
	// When the Show/Hide Weather button is clicked, the page should display the associated weather information if it was hidden or hide the associated weather information
	// if it was showing. It should default to hidden.
    $('#minneapolisWeatherButton').click(function() {
	$("#minneapolisWeather").toggle()});

    });

	$('#mainButton').on('click', function() {
		$('#mainInfoDiv').show();
		$('#akronInfoDiv').hide();
		$('#minneapolisInfoDiv').hide();
		$('#louisvilleInfoDiv').hide();
	    $('#minneapolisWeather').hide();
	});


	//3. When the Louisville button is clicked, only the content in the Louisville section should display; the weather information for Louisville should be hidden initially.
	$('#louisvilleButton').on('click', function() {
		$('#mainInfoDiv').hide();
		$('#akronInfoDiv').hide();
		$('#minneapolisInfoDiv').hide();
		$('#louisvilleInfoDiv').show();
	    $('#louisvilleWeather').hide();
	//$('#louisvilleWeather').fadeIn();
	
	// - Show/Hide Weather Behavior -
	// When the Show/Hide Weather button is clicked, the page should display the associated weather information if it was hidden or hide the associated weather information
	// if it was showing. It should default to hidden.
    $('#louisvilleWeatherButton').click(function() {
	$("#louisvilleWeather").toggle()});

	});

	// - Table Row Behavior -
	// The background color of any table row should change to “WhiteSmoke” when the mouse pointer is hovering over the row.
	$('td').hover(function() {
		$('td').css("background-color", "WhiteSmoke");
	},
	function() {
		$('td').css("background-color", "");
	});
	


});