$(document).ready(function() {

            // Add Button onclick handler
            $('#getCurrentWeatherButton').click(function(event) {

                    // grab the the tbody element that will hold the rows of contact information
                    //var currentLocalData = $('#currentLocalData');
                    //var cityName = currCityName

                    $('#cityName').empty();
                    $('#cityName').empty();
                    $('#temperature').empty();
                    $('#humidity').empty();
                    $('#wind').empty();

                    $.ajax({
                        type: 'GET',
                        url: 'http://api.openweathermap.org/data/2.5/weather?zip=16105,us&units=imperial&APPID=9dd53be9422e7d389bbe725f77d89738',
                        success: function(currentData, status) {

                                var cityName = currentData.name;
                                var currTemperature = currentData.main.temp;
                                var currHumidity = currentData.main.humidity;
                                var currWind = currentData.wind.speed;
                                var image = currentData.weather.icon;

                                // alert("city " + weatherData.name);
                                // alert("temp " + weatherData.main.temp);
                                // alert("humidity " + weatherData.main.humidity);
                                // alert("windSpeed " + weatherData.wind.speed);
                                // alert("icon " + weatherData.weather.icon);

                                $('#cityName').append(cityName);
                                $('#temperature').append(currTemperature);
                                $('#humidity').append(currHumidity);
                                $('#wind').append(currWind);
                        },

                        error: function() {
                            $('#errorMessages')
                                .append($('<li>')
                                    .attr({
                                        class: 'list-group-item list-group-item-danger'
                                    })
                                    .text('Error calling web service.  Please try again later.'));
                        }
                    });

                })

                // Add Button onclick handler
                $('#getFiveDayWeatherButton').click(function(event) {

                        // grab the the tbody element that will hold the rows of contact information
                        //var currentLocalData = $('#currentLocalData');
                        //var cityName = currCityName

                        $('#day3data').empty();

                        $.ajax({
                            type: 'GET',
                            url: 'http://api.openweathermap.org/data/2.5/forecast/daily?zip=40202,us&cnt=5&units=imperial&APPID=9dd53be9422e7d389bbe725f77d89738',
                            success: function(forecastData, status) {
                                // $.each(forecastData, function(index, daily) {
                                console.log("Get request for five day came back ...!");
                                console.log(forecastData);
                                var contentRows = $('#contentRows');
                                for(var i = 0; i < forecastData.list.length; i++){
                                    var daily = forecastData.list[i];
                                    var forecastCityName = forecastData.city.name;
                                    var forecastDate = daily.dt * 1000;
                                    var formattedDate = new Date(forecastDate);
                                    var forecastDescription = daily.weather[0].main;
                                    var forecastLowTemp = daily.temp.min;
                                    var forecastHighTemp = daily.temp.max;

                                    var row = '<tr>';
                                        row += '<td>' + forecastCityName + '</td>';
                                        row += '<td>' + formattedDate + '</td>';
                                        row += '<td>' + forecastDescription + '</td>';
                                        row += '<td>' + forecastLowTemp + '</td>';
                                        row += '<td>' + forecastHighTemp + '</td>';
                                        row += '</tr>';
                                        // debugger;
                                        console.log(row);
                                    contentRows.append(row);
                                  }
                                // });

                                    // var forecastCityName = forecastData.city.name;
                                    // var forecastDate = forecastData.list[2].dt;
                                    // var forecastDescription = forecastData.list[2].weather.main;
                                    // var forecastLowTemp = forecastData.list[2].temp.min;
                                    // var forecastHighTemp = forecastData.list[2].temp.max;
                                    //
                                    //
                                    // // alert("city " + forecastData.city.name);
                                    // // alert("date " + forecastData.list[2].dt);
                                    // // alert("description " + forecastData.list[2].weather.main);
                                    // // alert("lowTemp " + forecastData.list[2].temp.min);
                                    // // alert("highTemp " + forecastData.list[2].temp.max);
                                    //
                                    // var day3dataString = forecastCityName + "_" + forecastDate + "_" + forecastDescription
                                    // + "_" + forecastLowTemp + "_" + forecastHighTemp;
                                    //
                                    // $('#day3data').append(day3dataString);

                            },
                            error: function() {
                                $('#errorMessages')
                                    .append($('<li>')
                                        .attr({
                                            class: 'list-group-item list-group-item-danger'
                                        })
                                        .text('Error calling web service.  Please try again later.'));
                            }
                        });

                    })


            })
