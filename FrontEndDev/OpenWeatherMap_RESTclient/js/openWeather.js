$(document).ready(function() {

            // Add Button onclick handler
            $('#getWeatherButton').click(function (event) {

                    // grab the the tbody element that will hold the rows of contact information
                    //var currentLocalData = $('#currentLocalData');
                    var cityName = currCityName

                    $.ajax({
                        type: 'GET',
                        url: 'http://api.openweathermap.org/data/2.5/weather?zip=40202,us&units=imperial&APPID=9dd53be9422e7d389bbe725f77d89738',
                        success: function(weatherData, status) {
                            $.each(weatherData, function(index, currentWeather) {
                                var currCityName = weatherData.name;
                                // var currTemperature = main.temp;
                                // var currHumidity = main.humidity;
                                // var currWind = wind.speed;
                                // var image = weather.icon;
                                //
                                // var row = '<tr>';
                                // row += '<td>' + currTemperature + '</td>';
                                // row += '<td>' + currHumidity + '</td>';
                                // row += '<td>' + currWind + '</td>';
                                // row += '</tr>';

                            });
                        },
                        error: function() {
                            $('#errorMessages')
                                .append($('<li>')
                                    .attr({
                                        class: 'list-group-item list-group-item-danger'
                                    })
                                    .text('Error calling web service.  Please try again later.'));
                        }
                    });


                    // This block calls the 16-day forecast which uses diff variable names;
                    var fiveDayForecast = $('#fiveDayForecast');

                    $.ajax({
                        type: 'GET',
                        url: 'http://api.openweathermap.org/data/2.5/forecast/daily',
                        success: function(data, status) {
                            $.each(data, function(index, contact) {
                                var forecastCityName = city.name;
                                var forecastDate = list.dt;
                                var forecastDescription = list.weather.main;
                                var forecastLowTemp = list.temp.min;
                                var forecastHighTemp = list.temp.max;

                                var row = '<tr>';
                                row += '<td>' + forecastDate + '</td>';
                                row += '<td>' + forecastDescription + '</td>';
                                row += '<td>' + forecastLowTemp + '</td>';
                                row += '<td>' + forecastHighTemp + '</td>';
                                row += '</tr>';

                            });
                        },
                        error: function() {
                            $('#errorMessages')
                                .append($('<li>')
                                    .attr({
                                        class: 'list-group-item list-group-item-danger'
                                    })
                                    .text('Error calling web service.  Please try again later.'));
                        }
                    });

                }

            }
