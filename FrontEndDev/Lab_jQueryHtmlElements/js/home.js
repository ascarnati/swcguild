$(document).ready(function () {

    //1. Center all H1 elements
    $('h1').addClass('text-center');
	
	//2. Center all H2 elements
    $('h2').addClass('text-center');

    //3. Replace the class that is on the element containing the text “Team Up!” with the class page-header

    //4. Change the text of “The Squad” to “Yellow Team”
    $('#yellowHeading').text("Yellow Team");

    //5. Change the background color for each team list to match the name of the team
    $('#orangeHeading').css(
    	{"background-color" : "orange"});
	$('#blueHeading').css(
    	{"background-color" : "blue"});
	$('#redHeading').css(
    	{"background-color" : "red"});
	$('#yellowHeading').css(
    	{"background-color" : "yellow"});

	//6. Add Joseph Banks and Simon Jones to the Yellow Team list
	$('#yellowTeamList').append([
		'<li>Joseph Banks</li>',
        '<li>Simon Jones</li>'
		]);

	//7. Hide the element containing the text “Hide Me!!!”
	$('#oops').hide();

	//8. Remove the element containing the text “Bogus Contact Info” from the footer
	$('#footerPlaceholder').remove();

	//9. Add a paragraph element containing your name and email to the footer. The text must be in Courier font and be 24 pixels in height
	$('#footer').append('p').text('Anthony Scarnati anthony.scarnati@libertymutual.com');//will create a new paragraph element
	$('#footer').css("font-family", "Courier");
	$('#footer').css("font-size", "24px");
	$('#footer').addClass('text-center');


});	