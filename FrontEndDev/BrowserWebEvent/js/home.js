//home.js
$(document).ready(function() {
	$('#toggleNumbers').on('click', function() {
		$('h2').toggle('slow');
	}); 
	// above code selects the element by ID using the CSS selector syntax #; then listens for an "on" event; 
	// then specifies the event name 'click', 
	// and when we get that specific event, we register a function with add'l code 

	$('#centerUp').on('click', function() {
		$('h1').addClass('text-center');
		$('h2').addClass('text-center');
		$('#buttonDiv').addClass('text-center'); // using a different technique here by grabbing everything 
		// in an entire div 
	});

	// next code demonstrates how to change the background color of a div when the mouse hovers over it
	// we grab 'div' because we want to effect the change on ALL divs on the html page

	$('#headingsBlue').on('click', function() {
		$('h1').css('color', 'blue');  // uses a .css function to style the selected element
	});

	$('div').hover(
		//in callback function
		function() {
			$(this).css('background-color', 'CornflowerBlue');
		},		
		// out callback function 
		function() {
			$(this).css('background-color', '');  //NOTE the value '' indicates default color in this case
		}
		);

	// next code demonstrates how the system can detect two hover events at once
	$('h2').hover(
		// in callback function
		function() {
			$(this).css('color', 'DarkOrange');
		},
		// out callback function
		function() {
			$(this).css('color', '');
		}
		);

});