$(document).ready(function () {

  loadContacts();  //1. define the load contacts method; note: we're just calling the method here, the method is defined below the .ready procedure

});


// we don't want code to RUN outside the document.ready function, but
// we're just defining the "loadContacts" function here; this makes the
// code a bit easier to read.  The function still RUNS inside the .ready

function loadContacts() {  //2. defining the method here
    var contentRows= $('#contentRows');

    $.ajax({   //3. write the ajax call, including the success and error callbacks
        type: 'Get',
        url: 'http://localhost:8080/contacts',
        success: function(contactArray)  {
          //$.each function allows us to iterate through every item in the array that we get back, acting on it with the code (function) in the second part of the parameter
          //$.each(what thing  we want to act on, what code we want to process on the thing)
          //the first position in the function is - always - the index, and the second is the actual thing that we're grabbing from the array
            $.each(contactArray, function(index, contact) {
                var name = contact.firstName + ' ' + contact.lastName;
                var company = contact.company;

          //we're going to dynamically create an html table with the info returned in the array
                var row = '<tr>';
                    row += '<td>' + name + '</td>';
                    row += '<td>' + company + '</td>';
                    row += '<td><a>Edit</a></td>';
                    row += '<td><a>Delete</a></td>';
                    row += '</tr>';

          //next we're going to append the rows created above into the "contentRows" variable created earlier
                contentRows.append(row);


            });

        },
        error: function() {
          //this last step is going to manipulate the "errorMessages" reference element in the HTML
          // so that we can let the user know about any errors
          //this is an alternate syntax for jQuery
            $('#errorMessages')
              .append($('<li>')
              .attr({class: 'list-group-item list-group-item-danger'})
              .text('Error calling web service.  Please try again later.'));
            }
          // in the above, we've set it up to append the ul with a list item, color that item red (using list-group-item-danger attribute),
          // and then append specific text to that
    });


}
