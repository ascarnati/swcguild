/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.rockpaperscissors;

import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class DogGenetics {

    public static void main(String[] args) {
       
        String dogName;

        Random randomizer = new Random();        
        Scanner inputReader = new Scanner(System.in);
        
        System.out.println("Hello.  What's the name of your dog?");
        dogName = inputReader.nextLine();
                
        System.out.println("Well, you're in luck! I happen to have a totally"
                + " reliable DNA report for " + dogName.toUpperCase()+ " right here.");
        
        int num1 = randomizer.nextInt(20)+1;
        int num2 = randomizer.nextInt(20)+1;
        int num3 = randomizer.nextInt(20)+1;
        int num4 = randomizer.nextInt(20)+1;
        int num5 = 100 - (num1 + num2 + num3 +num4);
        
        System.out.println(dogName.toUpperCase() + " is " + num1 + "% Irish Setter.");
        System.out.println(num2 + "% Border Terrier.");
        System.out.println(num3 + "% Chihuahua.");
        System.out.println(num4 + "% Husky.");
        System.out.println("And, last but not least, " + num5 + "% Blood Hound.");
        
        System.out.println("Congratulations!  You must be proud.");
        
    }
}
