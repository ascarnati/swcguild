/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.rockpaperscissors;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HealthyHearts {


    public static void main(String[] args) {
        
        int userAge;
        double healthyHeartRange;
        double targetHeartRateZoneMin;
        double targetHeartRateZoneMax;
        
        Scanner inputReader = new Scanner(System.in);
        
        System.out.println("Hello.");
        System.out.println("What's your age?");
        userAge = inputReader.nextInt();
        inputReader.nextLine();
        
        healthyHeartRange = 220 - userAge;
        targetHeartRateZoneMin = Math.round (healthyHeartRange / 2);
        targetHeartRateZoneMax = Math.round (healthyHeartRange * .85);
        
        System.out.println("Your max heart rate should be " + healthyHeartRange + ".");
        System.out.println("Your target HR Zone is " + 
                targetHeartRateZoneMin + " - " + targetHeartRateZoneMax + 
                " beats per minute.");
        
        
    }
    
}
