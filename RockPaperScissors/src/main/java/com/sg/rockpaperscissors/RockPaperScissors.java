/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sg.rockpaperscissors;

import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {

        int numberOfRounds;
        int currentRound = 1;
        int userSelection;
        int cpuSelection;
        String roundWinner;
        int userWins = 0;
        int userLosses = 0;
        int roundsTied = 0;
        boolean playOn = false;

        Scanner inputReader = new Scanner(System.in);
        Random randomizer = new Random();

        System.out.println("Welcome to Rock, Paper, Scissors!");

//      Initiate the main loop (do-while) for the game;
        do {
            int gameLimit = 0; // this allows us to exit the for-loop at games' end

//      Determine the length of the game
            System.out.println("How many rounds would you like to play?");
            numberOfRounds = inputReader.nextInt();

            if (numberOfRounds > 10) {
                System.out.println("Sorry, the limit is 10 rounds.");
                System.exit(0);  //this allows for a programmatic quit given the if-condition

            } else if (numberOfRounds == 1) {
                System.out.println(+numberOfRounds + " round?  Okay, lets get started.");
            } else {
                System.out.println(+numberOfRounds + " rounds? Okay, lets get started.");
            }

//      Begin loop (aka Round)
            for (int i = 1; i <= numberOfRounds; i++) {
                gameLimit++;  //increases gameLimit set above to determine when 
                //we reach the desired # of rounds

//          Collect user pick and generate cpu picks each round
                System.out.println("Please make your selection.");
                System.out.println("Indicate (1) for Rock, (2) for Paper, or (3) for"
                        + " Scissors.");
                userSelection = inputReader.nextInt();

                cpuSelection = randomizer.nextInt(3) + 1;
                
                if (userSelection == 1) {
                    if (cpuSelection == 1) {
                        System.out.println("You selected Rock.  I selected Rock.  We tied the round.");
                        roundsTied = roundsTied + 1;
                    } else if (cpuSelection == 2) {
                        System.out.println("You selected Rock.  I selected Paper. I win the round.");
                        userLosses = userLosses + 1;
                    } else if (cpuSelection == 3) {
                        System.out.println("You selected Rock.  I selected Scissors.  You win the round.");
                        userWins = userWins + 1;
                    }
                }
                
                // ANSCINDWRK-9 changed lines 74 and 87 to else-if
                else if (userSelection == 2) {
                    if (cpuSelection == 1) {
                        System.out.println("You selected Paper.  I selected Rock.  You win the round.");
                        userWins = userWins + 1;
                    } else if (cpuSelection == 2) {
                        System.out.println("You selected Paper.  I selected Paper. We tied the round.");
                        roundsTied = roundsTied + 1;
                    } else if (cpuSelection == 3) {
                        System.out.println("You selected Paper.  I selected Scissors.  I win the round.");
                        userLosses = userLosses + 1;
                    }
                }

                else if (userSelection == 3) {
                    if (cpuSelection == 1) {
                        System.out.println("You selected Scissors.  I selected Rock.  I win the round.");
                        userLosses = userLosses + 1;
                    } else if (cpuSelection == 2) {
                        System.out.println("You selected Scissors.  I selected Paper. You win the round.");
                        roundsTied = roundsTied + 1;
                    } else if (cpuSelection == 3) {
                        System.out.println("You selected Scissors.  I selected Scissors.  We tied that round.");
                        roundsTied = roundsTied + 1;
                    }
                }

                else if (userSelection > 3) {
                    System.out.println("That's not fair!  You forfeit that round.  Ha! Ha!.");
                    userLosses = userLosses + 1;
                }
            }

//          Determine if we've reached the desired limit of rounds
            if (gameLimit == numberOfRounds) {

                System.out.println("_________Game Results_______");
                System.out.println("You won " + userWins + " times.");
                System.out.println("You lost " + userLosses + " times.");
                System.out.println("We tied " + roundsTied + " times.");
                System.out.println("____________________________");
                
                //ANSCINDWRK-6 adjusted win vs loss logic
                if ((userWins + roundsTied) == userLosses) {
                    System.out.println("It's a tie!");
                } else if ((userWins + roundsTied) < userLosses) {
                    System.out.println("You lost the game.  Too bad.");
                } else if ((userWins + roundsTied) > userLosses) {
                    System.out.println("You won the game! Congrats!");
                }  //ANSCINDWRK-7 resolved rogue curly brace

                System.out.println("This was fun.  Would you like to play again? (y/n)");

                if (inputReader.next().equals("y")) {
                    playOn = true;
                } else {
                    playOn = false;
                    System.out.println("No? Ok. Maybe we'll play again later.");
                    //System.exit(0); ANSCINDWRK-8 removed unnecessary step
                }

            }  // begins a new round or ends the game do-while loop
        } while (playOn);

    }
}
