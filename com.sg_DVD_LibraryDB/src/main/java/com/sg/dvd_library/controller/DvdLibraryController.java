/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd_library.controller;

import com.sg.dvd_library.dao.DvdLibraryDao;
import com.sg.dvd_library.dao.DvdLibraryDaoException;
import com.sg.dvd_library.dto.Dvd;
import com.sg.dvd_library.ui.DvdLibraryView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Anthony Scarnati
 */
public class DvdLibraryController {

    DvdLibraryView view;
    DvdLibraryDao dao;

    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;
        try {
            while (keepGoing) {

                menuSelection = getMenuSelection();

                switch (menuSelection) {
                    case 1:
                        createDvd();
                        break;
                    case 2:
                        removeDvd();
                        break;
                    case 3:
                        editDvd();
                        break;
                    case 4:
                        listDvds();
                        break;
                    case 5:
                        searchByTitle();
                        break;
                    case 6:
                        viewDvd();
                        break;
                    case 7:
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();
                }
            }
            exitMessage();
        } catch (DvdLibraryDaoException e) {
            view.displayErrorMessage(e.getMessage());
        }
    }

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    private void createDvd() throws DvdLibraryDaoException {
        view.displayCreateDvdBanner();
        Dvd newDvd = view.getNewDvd();
        dao.addDvd(newDvd.getId(), newDvd);
        view.displayCreateSuccessBanner();
    }

    private void listDvds() throws DvdLibraryDaoException {
        view.displayDisplayAllBanner();
        List<Dvd> dvdList = dao.getAllDvds();
        view.displayDvdList(dvdList);
    }

    private void viewDvd() throws DvdLibraryDaoException {
        view.displayDisplayDvdBanner();
        String id = view.getDvdTitleChoice();
        Dvd dvd = dao.getDvd(id);
        view.displayDvd(dvd);
    }

    private void removeDvd() throws DvdLibraryDaoException {
        view.displayRemoveDvdBanner();
        String id = view.getDvdTitleChoice();
        Dvd dvd = dao.removeDvd(id);
        view.displayRemoveSuccessBanner();
    }

    private void unknownCommand() {
        view.displayUnknownCommandBanner();
    }

    private void exitMessage() {
        view.displayExitBanner();
    }

    private void searchByTitle() throws DvdLibraryDaoException {
        // ask the user for the title name X
        String titleSearch = view.displaySearchByTitle();
        // ask the dao for the list of DVD objects
        List<Dvd> dvdList = dao.getAllDvds();

        // create a new Array List that WILL contain matching movies
        List<Dvd> moviesContainingThatTitle = new ArrayList();
        // find the matching movies by iterating through the list with an
        // enhanced for-loop to populate the new list
        for (Dvd currentDisc : dvdList) {
            if (currentDisc.getTitle().toUpperCase().contains(titleSearch.toUpperCase())) {
                moviesContainingThatTitle.add(currentDisc);
            }
        } 
        System.out.println("These are the movies in your library "
                + "containing the word:" + titleSearch);
        view.displayDvdList(moviesContainingThatTitle);
        
        // ask user to input the ID that corresponds to the DVD they want
        // return the object properties for the corresponding DVD           
    }

    private void editDvd() throws DvdLibraryDaoException {
        view.displayEditDvdBanner();
        String id = view.getDvdTitleChoice();
        Dvd dvd = dao.getDvd(id);
        Dvd editedDvd = view.editDvd(dvd);
        dao.editDvd(id, editedDvd);
        view.displayEditSuccessBanner();
    }

    public DvdLibraryController(DvdLibraryDao dao, DvdLibraryView view) {
        this.dao = dao;
        this.view = view;
    }

}
