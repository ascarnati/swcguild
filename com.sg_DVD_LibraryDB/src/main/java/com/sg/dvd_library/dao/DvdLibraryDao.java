/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd_library.dao;

import com.sg.dvd_library.dto.Dvd;
import java.util.List;

/**
 *
 * @author Anthony Scarnati
 */
public interface DvdLibraryDao {
    
    /**
     * Adds the given DVD to the Library and associates it with the given title.
     * If there is already a DVD associated with the given title it will return 
     * that DVD object, otherwise it will return null.
     * 
     * @param title title with which the DVD is to be associated
     * @param dvd dvd to be added to the library
     * @return the DVD object previously associated with the given title if it 
     * exists, null otherwise
     */
    Dvd addDvd(String id, Dvd dvd) throws DvdLibraryDaoException;
    
    /**
     * Returns a String array containing the titles of all DVDs in the library.
     * @return String array containing the titles of all the DVDs in the roster
     */
    List<Dvd> getAllDvds() throws DvdLibraryDaoException;
    
    /**
     * Returns the DVD object associated with the given title.
     * Returns null if no such DVD exists.
     * 
     * @param title title of the DVD to retrieve
     * @return the DVD object associated with the given title, null if no such
     * DVD exists
     */
    Dvd getDvd(String id) throws DvdLibraryDaoException;
    
    /**
     * Removes from the library the DVD associated with the given title.
     * Returns the DVD object that is being removed or null if there is no
     * DVD associated with the given title.
     * 
     * @param title title of the DVD to be removed
     * @return DVD object that was removed or null if no DVD was associated
     * with the given title
     */    
    Dvd removeDvd(String id) throws DvdLibraryDaoException;
    
    /**
     * Edits an existing DVD object that is associated with the given title.
     * @param title title of the DVD to be removed
     * @return DVD object that was edited or null if no DVD was associated with
     * the given title.
     */
    Dvd editDvd(String id, Dvd dvd) throws DvdLibraryDaoException;
    
}
