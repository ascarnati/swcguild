/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd_library.dao;

import com.sg.dvd_library.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Anthony Scarnati
 */
public class DvdLibraryDaoFileImpl implements DvdLibraryDao {

    private Map<String, Dvd> dvdsMap = new HashMap<>();

    public static final String DVD_FILE = "dvdLibrary.txt";
    public static final String DELIMITER = "::";

    @Override
    public Dvd addDvd(String id, Dvd dvd) throws DvdLibraryDaoException {
        Dvd newDvd = dvdsMap.put(id, dvd);
        writeDvdLibrary();
        return newDvd;
    }

    @Override
    public List<Dvd> getAllDvds() throws DvdLibraryDaoException {
        loadDvdLibrary();
        return new ArrayList<Dvd>(dvdsMap.values());
    }

    @Override
    public Dvd getDvd(String id) throws DvdLibraryDaoException {
        loadDvdLibrary();
        return dvdsMap.get(id);
    }

    @Override
    public Dvd removeDvd(String id) throws DvdLibraryDaoException {
        Dvd removedDvd = dvdsMap.remove(id);
        writeDvdLibrary();
        return removedDvd;
    }

    @Override
    public Dvd editDvd(String id, Dvd dvd) throws DvdLibraryDaoException {
//        loadDvdLibrary();
//        Dvd editedDvd = dvdsMap.get(title);
        Dvd edited2Dvd = dvdsMap.put(id, dvd);
        writeDvdLibrary();
        return edited2Dvd;
    }

    private void loadDvdLibrary() throws DvdLibraryDaoException {
        Scanner scanner;

        try {
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(DVD_FILE)));
        } catch (FileNotFoundException e) {
            throw new DvdLibraryDaoException(
                    "-_- Could not load DVD data into memory.", e);
        }

        String currentLine;

        String[] currentTokens;

        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(DELIMITER, 7);
            Dvd currentDvd = new Dvd(currentTokens[0]);
            currentDvd.setTitle(currentTokens[1]);
            currentDvd.setReleaseDate(currentTokens[2]);
            currentDvd.setMpaaRating(currentTokens[3]);
            currentDvd.setDirectorName(currentTokens[4]);
            currentDvd.setStudio(currentTokens[5]);
            currentDvd.setUserNote(currentTokens[6]);

            dvdsMap.put(currentDvd.getId(), currentDvd);

        }

        scanner.close();
    }

    private void writeDvdLibrary() throws DvdLibraryDaoException {
        PrintWriter out;

        try {
            out = new PrintWriter(new FileWriter(DVD_FILE));
        } catch (IOException e) {
            throw new DvdLibraryDaoException(
                    "Could not save DVD data.", e);
        }

        List<Dvd> dvdList = this.getAllDvds();
        for (Dvd currentDvd : dvdList) {
            out.println(currentDvd.getId() + DELIMITER
                    + currentDvd.getTitle() + DELIMITER
                    + currentDvd.getReleaseDate() + DELIMITER
                    + currentDvd.getMpaaRating() + DELIMITER
                    + currentDvd.getDirectorName() + DELIMITER
                    + currentDvd.getStudio() + DELIMITER
                    + currentDvd.getUserNote() + DELIMITER + "EOL" + DELIMITER);

            out.flush();
        }

        out.close();
    }

}
