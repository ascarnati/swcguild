/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd_library.dao;

import com.sg.dvd_library.dto.Dvd;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoDbImpl implements DvdLibraryDao {

    private static final String SQL_INSERT_DVD
            = "insert into dvds "
            + "(title, releaseDate, mpaaRating, director, studio) "
            + "values (?, ?, ?, ?, ?)";
    
    private static final String SQL_DELETE_DVD
            = "delete from dvds where dvd_id = ?";
    
    private static final String SQL_SELECT_DVD
            = "select * from dvd where dvd_id = ?";
    
    private static final String SQL_UPDATE_DVD
            = "UPDATE dvds SET "
            + "title = ?, releaseDate = ?, mpaaRating = ?, director = ?, studio = ? "
            + "WHERE dvd_id = ?";
    
    private static final String SQL_SELECT_ALL_DVDS
            = "select * from dvds";
    
    private static final String SQL_SELECT_DVDS_BY_TITLE
            = "select * from dvds where title = ?";
    
    // injecting the Jdbc Template using setter injection

    
    @Override
    public Dvd addDvd(String id, Dvd dvd) throws DvdLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Dvd> getAllDvds() throws DvdLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Dvd getDvd(String id) throws DvdLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Dvd removeDvd(String id) throws DvdLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Dvd editDvd(String id, Dvd dvd) throws DvdLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
