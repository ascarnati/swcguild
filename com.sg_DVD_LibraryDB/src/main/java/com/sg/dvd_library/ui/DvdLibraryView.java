/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd_library.ui;

import com.sg.dvd_library.dto.Dvd;
import java.util.List;

/**
 *
 * @author Anthony Scarnati
 */
public class DvdLibraryView {

    public UserIO io; 

    public int printMenuAndGetSelection() {
        io.print("Main Menu");
        io.print("1. Add a DVD");
        io.print("2. Remove a DVD");
        io.print("3. Edit a DVD");
        io.print("4. List DVD Collection");
        io.print("5. Search by Title");
        io.print("6. View a DVD");
        io.print("7. Exit");

        return io.readInt("Please select from the above choices.", 1, 7);
    }

    public Dvd getNewDvd() {
        String id = io.readString("Please enter an ID for this DVD.");
        String title = io.readString("Please enter DVD title.");
        String releaseDate = io.readString("Please enter release date.");
        String mpaaRating = io.readString("Please enter MPAA rating.");
        String directorName = io.readString("Please enter the Director's name.");
        String studio = io.readString("Please enter the studio name.");
        String userNote = io.readString("Please enter your personal notes.");
        Dvd currentDvd = new Dvd(id);
        currentDvd.setTitle(title);
        currentDvd.setReleaseDate(releaseDate);
        currentDvd.setMpaaRating(mpaaRating);
        currentDvd.setDirectorName(directorName);
        currentDvd.setStudio(studio);
        currentDvd.setUserNote(userNote);
        return currentDvd;
    }

    public void displayCreateDvdBanner() {
        io.print("~~~ Create DVD ~~~");
    }

    public void displayCreateSuccessBanner() {
        io.readString(
                "~~~ DVD successfully created.  Please hit <enter> to continue. ~~~");
    }

    public void displayDvdList(List<Dvd> dvdList) {
        for (Dvd currentDvd : dvdList) {
            io.print(currentDvd.getId() + ", "
                    + currentDvd.getTitle() + ", " 
                    + currentDvd.getReleaseDate() + ", "
                    + currentDvd.getMpaaRating() + ", "
                    + currentDvd.getStudio() + ", "
                    + currentDvd.getDirectorName() + ", "
                    + currentDvd.getUserNote());
        }
        io.readString("~~~ Please hit <enter> to continue. ~~~");
    }

    public void displayDisplayAllBanner() {
        io.print("~~~ Display All DVDs ~~~");
    }

    public void displayDisplayDvdBanner() {
        io.print("~~~ Display DVD ~~~");
    }

    public String getDvdTitleChoice() {
        return io.readString("Please enter the ID of your DVD.");
    }

    public void displayDvd(Dvd dvd) {
        if (dvd != null) {
            System.out.println("Here's the information you requested for "
                    + dvd.getTitle());
            
            io.print(dvd.getId());
            io.print(dvd.getTitle());
            io.print(dvd.getReleaseDate());
            io.print(dvd.getMpaaRating());
            io.print(dvd.getStudio());
            io.print(dvd.getDirectorName());
            io.print(dvd.getUserNote());
            io.print("");
        } else {
            io.print("No such DVD in this library.");
        }
        io.readString("~~~ Please hit <enter> to continue. ~~~");
    }

    public void displayRemoveDvdBanner() {
        io.print("~~~ Remove DVD ~~~");
    }

    public void displayRemoveSuccessBanner() {
        io.readString("~~~ DVD successfully removed.  Please hit enter to continue. ~~~");
    }

    public void displayExitBanner() {
        io.print("Good Bye!!!");
    }

    public void displayUnknownCommandBanner() {
        io.print("Unknown Command!!!");
    }

    public void displayEditDvdBanner() {
        io.print("~~~ Edit DVD ~~~");
    }

    public void displayEditSuccessBanner() {
        io.print("~~~ DVD successfully edited. ~~~");
    }

    public String displaySearchByTitle() {
        return io.readString("Please enter the Title of the DVD.");
    }
    
    public Dvd editDvd(Dvd dvd) {

        System.out.println("This is the current info for the DVD:");
        io.print(dvd.getId());
        io.print(dvd.getTitle());
        io.print(dvd.getReleaseDate());
        io.print(dvd.getMpaaRating());
        io.print(dvd.getStudio());
        io.print(dvd.getDirectorName());
        io.print(dvd.getUserNote());

        String id = io.readString("Please enter the ID.");
        String title = io.readString("Please re-enter the Title.");
        String releaseDate = io.readString("Please re-enter Release date.");
        String mpaaRating = io.readString("Please re-enter MPAA rating.");
        String directorName = io.readString("Please re-enter the Director's name.");
        String studio = io.readString("Please re-enter the Studio name.");
        String userNote = io.readString("Please re-enter your personal notes.");

        Dvd editedDvd = new Dvd(id);
        editedDvd.setTitle(title);
        editedDvd.setReleaseDate(releaseDate);
        editedDvd.setMpaaRating(mpaaRating);
        editedDvd.setDirectorName(directorName);
        editedDvd.setStudio(studio);
        editedDvd.setUserNote(userNote);
        return editedDvd;

    }

    public DvdLibraryView(UserIO io) {
        this.io = io;
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("~~~ ERROR ~~~");
        io.print(errorMsg);
    }

}
