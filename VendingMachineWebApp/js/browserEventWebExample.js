//browserEventWebExample.js
$(document).ready(function() {
	$('#toggleNumbers').on('click', function() {
		$('h2').toggle('slow');
	}); 
	// above code selects the element by ID using the CSS selector syntax #; then listens for an "on" event; 
	// then specifies the event name 'click', 
	// and when we get that specific event, we register a function with add'l code 

});