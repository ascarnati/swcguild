$(document).ready(function() {

    // load data for all items (ID, name, cost, quantity)
    // and display data for each item
    loadAllItems();


    clearAllDisplays();


    $('#addDollar').click(function(event) {
        addDollar();
    })

    $('#addQuarter').click(function(event) {
        addQuarter();
    })

    $('#addDime').click(function(event) {
        addDime();
    })

    $('#addNickel').click(function(event) {
        addNickel();
    })

    // #makePurchaseButton handler
    $('#makePurchaseButton').click(function(event) {
        var vendItemId = $('#displayItem').val()
        if (vendItemId < 1) {
            $('#displayMessages').val("You must select an item.");
        } else {
            makePurchase(vendItemId);
        }

    })

    // #changeReturnButton handler
    $('#changeReturnButton').click(function(event) {
        var balance = $('#displayTotalIn').val()
        changeReturn(balance);
    })

});

function selectItem(vendItemId) {

    $('#displayItem').val(vendItemId);
}


// #makePurchaseButton functionality
function makePurchase(vendItemId) {

    // call the dispenseItem function and pass it amount and item ID
    var balance = parseFloat($('#displayTotalIn').val());
    dispenseItem(balance, vendItemId);

    loadAllItems();
    $('#displayTotalIn').val("0.00");
    $('#displayItem').val("");

}


function addDollar() {
    // get the value from the field on the html page and use that to replace 
    // anywhere in JS that uses "balance" $('#displayTotalIn').
    var balance = parseFloat($('#displayTotalIn').val());
    var dollar = parseFloat(1.00);
    balance += dollar;
    displayTotalIn(balance);
}

function addQuarter() {
    var balance = parseFloat($('#displayTotalIn').val());
    var quarter = parseFloat(0.25);
    balance += quarter;
    displayTotalIn(balance);
}

function addDime() {
    var balance = parseFloat($('#displayTotalIn').val());
    var dime = parseFloat(0.10);
    balance += dime;
    displayTotalIn(balance);
}

function addNickel() {
    var balance = parseFloat($('#displayTotalIn').val());
    var nickel = parseFloat(0.05);
    balance += nickel;
    displayTotalIn(balance);
}


function displayTotalIn(balance) {
    $('#displayTotalIn').val(balance.toFixed(2));
}

// #changeReturnButton functionality
function changeReturn(balance) {

    var quarter = parseFloat(0.25);
    var dime = parseFloat(0.10);
    var nickel = parseFloat(0.05);
    var penny = parseFloat(0.01);
    var runningBalance = parseFloat(0.00);


    if (balance < .01) {
        $('#displayMessages').val("Beat it, freeloader!");
        $('#displayChangeReturn').val("");

    } else {
        $('#displayMessages').val("Don't forget your change!");
        

        if (balance >= .25) {
        var quartersRefund = Math.floor(balance / quarter);
        runningBalance = parseFloat(balance % quarter).toFixed(2);

        }

        //var dimesRefund = runningBalance divided by dime
        if (runningBalance >= .10) {
        var dimesRefund = Math.floor(runningBalance / dime);
        runningBalance = parseFloat(runningBalance % dime).toFixed(2);        
        }

        //var nickelsRefund = runningBalance divided by nickel
        if (runningBalance >= .05) {
        var nickelsRefund = Math.floor(runningBalance / nickel);
        runningBalance = parseFloat(runningBalance % nickel).toFixed(2);
        }

        // penniesRefund = runningBalance divided by penny
        var penniesRefund = Math.floor(runningBalance / penny);

        // calculate the change refund and display to UI

        var changeRefundString = quartersRefund + " quarters, " + dimesRefund + " dimes, " + nickelsRefund + " nickels, " + penniesRefund + " pennies";
        $('#displayChangeReturn').val(changeRefundString);
        $('#displayTotalIn').val("0.00");

    }
}

function clearAllDisplays() {
    $('#displayTotalIn').val("0.00");

    // clear displayed error message
    $('#displayMessages').val("");

    // clear item display
    $('#displayItem').val("");

    // clear change return
    $('#displayChangeReturn').val("");
}



function loadAllItems() {
    // we need to clear the previous content so we don't append to it
    clearItemsTable();

    // grab the tbody element that will hold the rows of contact information
    var contentRows = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/items',
        success: function(data, status) {
            $.each(data, function(index, items) {
                var id = items.id;
                var name = items.name;
                var price = items.price;
                var quantity = items.quantity;

                var row = '<tr class="col-md-4 border2" onclick=selectItem(' + id + ') id="vendItem + items.id">';
                row += '<td>' + id + '</td>';
                row += '<td>' + name + '</td>';
                row += '<td>' + price + '</td>';
                row += '<td>' + quantity + '</td>';
                row += '<td></td>';
                row += '</tr>';
                contentRows.append(row);
            });
        },
        error: function(data) {
            $('#displayMessages')
                .append($('<li>')
                    .attr({ class: 'list-group-item list-group-item-danger' })
                    .text('Error calling web service.  Please try again later.'));
        }
    });
}

function clearItemsTable() {
    $('#contentRows').empty();
}

function dispenseItem(balance, vendItemId) {

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/money/' + balance + '/item/' + vendItemId,
        success: function(data, status) {
            var quartersReturned = data.quarters;
            var dimesReturned = data.dimes;
            var nickelsReturned = data.nickels;
            var penniesReturned = data.pennies;

            var changeString = quartersReturned + " quarters, " + dimesReturned + " dimes, " + nickelsReturned + " nickels, " + penniesReturned + " pennies";

            var successfulVend = "Thank You!!!"
            $('#displayMessages').val(successfulVend);

            // receive the change from the api service and display to UI
            $('#displayChangeReturn').val(changeString);

        },
        error: function(data) {
            var errorMessage = data.responseJSON.message;

            $('#displayMessages').val(errorMessage);
        }
    });
}
