/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.advice;

import com.sg.classroster.dao.ClassRosterAuditDao;
import com.sg.classroster.dao.ClassRosterPersistenceException;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author apprentice
 */
public class LoggingAdvice {
    
    ClassRosterAuditDao auditDao;
    
    // this is the "aspect"
    public LoggingAdvice(ClassRosterAuditDao auditDao) {
        this.auditDao = auditDao;
    }
    
    // next we create a public method that won't return anything
    // we imported the aspectJ tools so that we can use the "joinPoint" tool
    // the JoinPoint carries is an object that carries the info about the particular
    // pointcut you're on - the name of the method, the parameters, e.g.
 
    // the purpose of this method is to create an Audit entry based upon the
    // name of the method that I'm auditing and the string representation of 
    // all of the parameters of that method; and in case something happens
    // and I can't create the audit entry I have to put some error handling in 
    // place because I don't want to cause the app to crash just because of my
    // logging
    
    public void createAuditEntry(JoinPoint jp)  {    
        // First, the arguments come back as an array of objects which we'll put into 
        // a variable called args
        Object[] args = jp.getArgs();
        // we create a string called auditEntry
        // which we ask the joinpoint to create for us
        String auditEntry = jp.getSignature().getName() + ": ";
        
        // by using Object currentArg : args in the below expression, we're 
        // able to use this same createAuditEntry method for both the "createStudent"
        // method which requires an Object (Student student) and for the 
        // remove student method which requires a String (Student studentId)
        for (Object currentArg : args) {
            auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (ClassRosterPersistenceException e) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }
    
}
