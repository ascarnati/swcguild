/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.dao;

import com.sg.classroster.dto.Student;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class ClassRosterDaoStubImpl implements ClassRosterDao {

    // First, add 2 class-level variables
    Student onlyStudent;  // declared, not initialized
    List<Student> studentList = new ArrayList<>();

    // Second, to initialize these class-level variables, we'll create a default
    // constructor (no params)
    public ClassRosterDaoStubImpl() {
        onlyStudent = new Student("0001");
        onlyStudent.setFirstName("John");
        onlyStudent.setLastName("Doe");
        onlyStudent.setCohort("Java-Jan-2015");

        studentList.add(onlyStudent);

    }

    // Third, add actual code to the addStudent method
    @Override
    public Student addStudent(String studentId, Student student) throws ClassRosterPersistenceException {
        if (studentId.equals(onlyStudent.getStudentId())) {
            return onlyStudent;
        } else {
            return null;
        }
    }

    // Fourth, add actual code to the getAllStudents so that no matter what we do
    // this stub DAO is only going to give you a list size of 1 because we're not 
    // concerned with testing the persistence (which happens in the DAO Test)
    @Override
    public List<Student> getAllStudents() throws ClassRosterPersistenceException {
        return studentList;
    }

    @Override
    public Student getStudent(String studentId) throws ClassRosterPersistenceException {
        if (studentId.equals(onlyStudent.getStudentId())) {
            return onlyStudent;
        } else {
            return null;
        }
    }

    @Override
    public Student removeStudent(String studentId) throws ClassRosterPersistenceException {
        if (studentId.equals(onlyStudent.getStudentId())) {
            return onlyStudent;
        } else {
            return null;
        }
    }

}
