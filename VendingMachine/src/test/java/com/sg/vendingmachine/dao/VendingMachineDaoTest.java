/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Snack;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoTest {
    
    private VendingMachineDao dao = new VendingMachineDaoFileImpl();

    
    public VendingMachineDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws VendingMachinePersistenceException {
        List<Snack>snackList = dao.getAllSnacks();
        for (Snack snack : snackList) {
            dao.removeSnack(snack.getItemID());
        }
        
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of createSnack method, of class VendingMachineDao.
     */
    @Test
    public void testCreateSnack() throws Exception {
        Snack snack1 = new Snack("99");
        snack1.setItemName("Snacky99");
        snack1.setItemCost(new BigDecimal("1.50"));
        snack1.setItemInventory(5);    
        
    }

    /**
     * Test of updateSnack method, of class VendingMachineDao.
     */
    @Test
    public void testUpdateSnack() throws Exception {
        
        // first add 1 snack
        Snack snack1 = new Snack("99");
        snack1.setItemName("Snacky99");
        snack1.setItemCost(new BigDecimal("1.50"));
        snack1.setItemInventory(5);
        
        dao.createSnack(snack1.getItemID(), snack1);
        
        // then change the inventory property for that snack
        snack1.setItemInventory(3);
        
        // now confirm that the itemInventory has changed from 5 to 3
        assertEquals(3, dao.getSnack(snack1.getItemID()).getItemInventory());
        
    }

    /**
     * Test of removeSnack method, of class VendingMachineDao.
     */
    @Test
    public void testRemoveSnack() throws Exception {
        
        //first add 2 snacks
        Snack snack1 = new Snack("99");
        snack1.setItemName("snickers");
        snack1.setItemCost(new BigDecimal("1.50"));
        snack1.setItemInventory(5);
        
        dao.createSnack(snack1.getItemID(), snack1);
        
        Snack snack2 = new Snack("88");
        snack2.setItemName("kitkat");
        snack2.setItemCost(new BigDecimal("1.75"));
        snack2.setItemInventory(5);    

        dao.createSnack(snack2.getItemID(), snack2);
        
        // then, remove snack#1
        dao.removeSnack(snack1.getItemID());
        
        // make sure there's just 1 item left in the list
        assertEquals(1, dao.getAllSnacks().size());
        
        // make sure that we get a null value if we try to get snack#1 now
        assertNull(dao.getSnack(snack1.getItemID()));
                
    }

    /**
     * Test of getAllSnacks method, of class VendingMachineDao.
     */
    @Test
    public void testGetAllSnacks() throws Exception {
        
        // first add three snacks
        Snack snack1 = new Snack("99");
        snack1.setItemName("snickers");
        snack1.setItemCost(new BigDecimal("1.50"));
        snack1.setItemInventory(5);
        
        dao.createSnack(snack1.getItemID(), snack1);
        
        Snack snack2 = new Snack("88");
        snack2.setItemName("kitkat");
        snack2.setItemCost(new BigDecimal("1.75"));
        snack2.setItemInventory(5);    

        dao.createSnack(snack2.getItemID(), snack2);
        
        Snack snack3 = new Snack("77");
        snack3.setItemName("water");
        snack3.setItemCost(new BigDecimal("1.00"));
        snack3.setItemInventory(5);    

        dao.createSnack(snack3.getItemID(), snack3);
        
        // now confirm that there are 3 objects in the list
        assertEquals(3, dao.getAllSnacks().size());
        
    }

    /**
     * Test of openDao method, of class VendingMachineDao.
     */
    @Test
    public void testOpenDao() throws Exception {
    }

    /**
     * Test of closeDao method, of class VendingMachineDao.
     */
    @Test
    public void testCloseDao() throws Exception {
    }
    
}
