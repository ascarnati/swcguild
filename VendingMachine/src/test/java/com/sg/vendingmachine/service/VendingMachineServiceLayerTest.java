/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.VendingMachineDao;
import com.sg.vendingmachine.dao.VendingMachineDaoStubImpl;
import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Snack;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aScarnati
 */
public class VendingMachineServiceLayerTest {

    //declare class-level variable for service
    private VendingMachineServiceLayer service;

    // then initialize it in the default constructor and wire it to use
    // the stub implementation of the dao
    public VendingMachineServiceLayerTest() {
        VendingMachineDao dao = new VendingMachineDaoStubImpl();

        service = new VendingMachineServiceLayerImpl(dao);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAllSnacks method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testGetAllSnacks() throws Exception {
        // this test verifies that the Dao stub correctly added 1 item to the test list
        assertEquals(1, service.getAllSnacks().size());
    }

    /**
     * Test of getSnack method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testGetSnack() throws Exception {
    }

    /**
     * Test of setUserCredits method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testSetUserCredits() throws Exception {
    }

    /**
     * Test of getItemID method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testGetItemID() throws Exception {
    }

    /**
     * Test of updateInventory method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testUpdateInventory() throws Exception {
    }

    /**
     * Test of dispenseItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testDispenseItem() throws Exception {
    }

    /**
     * Test of checkForInventory method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testCheckForInventoryInStock() throws Exception {
        service.checkForInventory("99");

    }

    @Test
    public void testCheckForInventoryOutOfStock() throws Exception {
        Snack snack = service.getSnack("99");
        assertNotNull(snack);

        service.getSnack("99").setItemInventory(0);

        try {
            service.checkForInventory("99");
            fail("VendingMachineNoItemInventoryException was expected but not thrown.");
        } catch (VendingMachineNoItemInventoryException e) {
            return;
        }

    }

    /**
     * Test of checkForSufficientFunds method, of class
     * VendingMachineServiceLayer.
     */
    @Test
    public void testCheckForSufficientFunds() throws Exception {
        BigDecimal testBalanceHigh = new BigDecimal("10.00");

        service.checkForSufficientFunds("99", testBalanceHigh);
    }

    @Test
    public void testCheckForInsufficientFunds() throws Exception {
        BigDecimal testBalanceLow = new BigDecimal("0.49");

        try {
            service.checkForSufficientFunds("99", testBalanceLow);
            fail("VendingMachineInsufficientFundsException was expected but not thrown.");
        } catch (VendingMachineInsufficientFundsException e) {
            return;
        }
    }

    /**
     * Test of convertToCoins method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testConvertToCoins() throws Exception {
    }

    /**
     * Test of openDao method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testOpenDao() throws Exception {
    }

    /**
     * Test of closeDao method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testCloseDao() throws Exception {
    }

}
