/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.controller;

import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Snack;
import com.sg.vendingmachine.service.VendingMachineInsufficientFundsException;
import com.sg.vendingmachine.service.VendingMachineNoItemInventoryException;
import com.sg.vendingmachine.service.VendingMachineServiceLayer;
import com.sg.vendingmachine.ui.VendingMachineView;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author aScarnati 
 */
public class VendingMachineController {

    //ASCINDWRK-18 defect fixed by making VendingMachineView property private
    private VendingMachineView view;
    private VendingMachineServiceLayer service;

    public VendingMachineController(VendingMachineServiceLayer service, VendingMachineView view) {
        this.service = service;
        this.view = view;
    }

    public void run() {
        boolean keepGoing = true;
        String greetingMenuSelection;
        boolean itemInStock;
        boolean sufficientFunds;
        BigDecimal balance = new BigDecimal("0.00");
        BigDecimal userRefund = new BigDecimal("0.00");
        String menuSelection;
        
        try {
            // opening the Dao here (ie loading the file) will catch and throw 
            // exception without getting stuck in the while-loop below
            service.openDao();
            
        } catch (VendingMachinePersistenceException e) {
            view.displayErrorMessage(e.getMessage());
            keepGoing = false;
        }
        
        while (keepGoing) {
            
            try {
                //display the main menu only
                greetingMenuSelection = getAddExitMenuSelection();
                if (greetingMenuSelection.equalsIgnoreCase("a")) {
                    balance = addCredits();
                } else {
                    exitMessage();
                    return;
                }

                // display the Snack items menu and get user selection
                String itemID = getMenuSelection();
                
                // convert user selection to an itemID
                //String itemID = getItemID(menuSelection);
                
                // dispense the Snack if its in stock and user has enough credits
                dispenseItem(balance, itemID);

                exitMessage();
                break;

            } catch (VendingMachinePersistenceException |
                    VendingMachineNoItemInventoryException |
                    VendingMachineInsufficientFundsException e) {
                view.displayErrorMessage(e.getMessage());
            }

        }

    }

    private String getAddExitMenuSelection() throws VendingMachinePersistenceException {
        List<Snack> itemList = service.getAllSnacks();
        return view.printAddExitMenuSelection(itemList);
    }

    private String getMenuSelection() throws VendingMachinePersistenceException {
        List<Snack> itemList = service.getAllSnacks();
        return view.printMenuAndGetSelection(itemList);
    }
    
    private BigDecimal addCredits() throws VendingMachinePersistenceException {

        // tell view to ask user how much to add
        BigDecimal creditsInput = view.addCreditsBanner();
        // give amount from view to service for service to provide DAO
        BigDecimal balance = service.setUserCredits(creditsInput);
        // tell user that credits have been added
        view.addCreditsSuccessfulBanner(balance, creditsInput);
        return balance;
    }
    
    private void dispenseItem(BigDecimal balance, String itemID) throws VendingMachinePersistenceException,
            VendingMachineNoItemInventoryException, VendingMachineInsufficientFundsException {
        
        // verify that menuSelection is a valid itemID
        service.verifyItemID(itemID);
        
        // then begin the dispense item process (verify funds, inventory)
        service.dispenseItem(itemID, balance);
        
        // tell Service to subtract itemCost from userCredits
        BigDecimal userRefund = balance.subtract(service.getSnack(itemID).getItemCost());
        
        // tell Service to subtract 1 from itemInventory
        service.updateInventory(itemID);
        
        // tell View to display userRefund amount
        view.displayRefundBanner(userRefund);
        
        // convert userRefund and display to user in coins
        Change changeInCoins = service.convertToCoins(userRefund);
        view.displayRefundInCoinsBanner(changeInCoins);
        
        // tell View to display Success banner
        view.displayDispenseSuccessfulBanner();
    }

    // tell Service to send DaoImpl the itemID and get itemInventory and itemCost
    // DaoImpl returns itemName, itemCost, itemInventory
    private boolean checkForInventory(String itemID) throws
            VendingMachinePersistenceException,
            VendingMachineNoItemInventoryException {
        return service.checkForInventory(itemID);
    }

    // tell Service to send DaoImpl the itemID and get itemInventory and itemCost
    // DaoImpl returns itemName, itemCost, itemInventory
    private boolean checkForSufficientFunds(String itemID, BigDecimal newTotal) throws
            VendingMachinePersistenceException,
            VendingMachineInsufficientFundsException {
        return service.checkForSufficientFunds(itemID, newTotal);
    }

    private void unknownCommand() {
        view.displayUnknownCommandBanner();
    }

    private void exitMessage() throws VendingMachinePersistenceException {
        service.closeDao();
        view.displayExitBanner();
    }

}
