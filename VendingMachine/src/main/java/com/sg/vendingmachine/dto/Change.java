/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author aScarnati
 */
public class Change {

    private BigDecimal quarter;
    private BigDecimal dime;
    private BigDecimal nickel;
    private BigDecimal penny;

    private BigDecimal penniesInADollar = new BigDecimal("100");
    private BigDecimal penniesInAQuarter = new BigDecimal("25");
    private BigDecimal penniesInADime = new BigDecimal("10");
    private BigDecimal penniesInANickel = new BigDecimal("5");
    
    BigDecimal runningBalance = new BigDecimal("0");
    
    public Change(BigDecimal userRefund) {
        BigDecimal allThePennies = userRefund.multiply(penniesInADollar);
        
        this.quarter = allThePennies.divide(penniesInAQuarter).setScale(0, RoundingMode.DOWN);
        runningBalance = allThePennies.remainder(penniesInAQuarter);

        this.dime = runningBalance.divide(penniesInADime).setScale(0, RoundingMode.DOWN);
        runningBalance = runningBalance.remainder(penniesInADime);

        this.nickel = runningBalance.divide(penniesInANickel).setScale(0, RoundingMode.DOWN);
        runningBalance = runningBalance.remainder(penniesInANickel);

        this.penny = runningBalance.setScale(0, RoundingMode.DOWN);

    }

    public BigDecimal getQuarter() {
        return quarter;
    }

    public void setQuarter(BigDecimal quarter) {
        this.quarter = quarter;
    }

    public BigDecimal getDime() {
        return dime;
    }

    public void setDime(BigDecimal dime) {
        this.dime = dime;
    }

    public BigDecimal getNickel() {
        return nickel;
    }

    public void setNickel(BigDecimal nickel) {
        this.nickel = nickel;
    }

    public BigDecimal getPenny() {
        return penny;
    }

    public void setPenny(BigDecimal penny) {
        this.penny = penny;
    }

}
