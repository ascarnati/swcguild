/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Snack;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface VendingMachineServiceLayer {
    
    List<Snack> getAllSnacks() throws VendingMachinePersistenceException;
    
    Snack getSnack(String itemID) throws VendingMachinePersistenceException;

    BigDecimal setUserCredits(BigDecimal creditsInput) throws VendingMachinePersistenceException;
    
    boolean verifyItemID(String menuSelection) throws VendingMachinePersistenceException;
    
    void updateInventory(String itemID) throws VendingMachinePersistenceException;
    
    boolean dispenseItem(String itemID, BigDecimal newTotal) throws VendingMachinePersistenceException,
            VendingMachineNoItemInventoryException, VendingMachineInsufficientFundsException;

    boolean checkForInventory(String itemID)  throws VendingMachinePersistenceException, 
            VendingMachineNoItemInventoryException;
    
    boolean checkForSufficientFunds(String itemID, BigDecimal balance) throws VendingMachinePersistenceException, 
            VendingMachineInsufficientFundsException;
    
    public Change convertToCoins(BigDecimal userRefund) throws VendingMachinePersistenceException;
 
    void openDao() throws VendingMachinePersistenceException;
    
    void closeDao() throws VendingMachinePersistenceException;
    
}
