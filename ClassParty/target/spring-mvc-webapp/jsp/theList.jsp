<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>theList.jsp</title>
    </head>
    <body>
        <h1>These are the <c:out items="${inventoryList}"> of cool kids coming to the party:</h1>
            
        <c:forEach var="currentItem" items="${inventoryList}">
            <p style="text-align: left">
                <c:out value="${currentItem.guestName}"/>
            </p>
            <p>
                And they are bringing <#> of llamas in total!!!
            </p>
        
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vendingMachine.js"></script>
    
    </body>
</html>