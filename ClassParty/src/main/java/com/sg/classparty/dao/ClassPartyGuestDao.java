/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classparty.dao;

import com.sg.classparty.model.Guest;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface ClassPartyGuestDao {
 
    public Guest addGuest(Guest guest);
    
    public void removeGuest(int id);
    
    public void updateGuest(Guest guest);
    
    public List<Guest> getAllGuests();
    
}
