/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classparty.dao;

import com.sg.classparty.model.Guest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class ClassPartyGuestDaoInMemImpl implements ClassPartyGuestDao {

    private Map<Integer, Guest> guestList = new HashMap<>();
    private static int guestIdCounter = 0;
    
    public ClassPartyGuestDaoInMemImpl() {
        
        guestList.put(1, new Guest("Bob McLlama",1));
        guestList.put(2, new Guest("Mary McLlama",0));
        guestList.put(3, new Guest("Austyn Hill",5));
        guestList.put(4, new Guest("Alan Galloway",0));
        
    }
    
    @Override
    public Guest addGuest(Guest guest) {
                
        guest.setId(guestIdCounter);
        guestIdCounter++;        
        guestList.put(guest.getId(), guest);
        return guest;
    }

    @Override
    public void removeGuest(int id) {
        guestList.remove(id);
    }

    @Override
    public void updateGuest(Guest guest) {
        guestList.put(guest.getId(), guest);
    }

    @Override
    public List<Guest> getAllGuests() {
        return new ArrayList<Guest>(guestList.values());
    }
    
}
