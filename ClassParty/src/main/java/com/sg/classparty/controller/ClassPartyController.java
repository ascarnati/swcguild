/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classparty.controller;

import com.sg.classparty.dao.ClassPartyGuestDao;
import com.sg.classparty.dao.ClassPartyGuestDaoInMemImpl;
import com.sg.classparty.model.Guest;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class ClassPartyController {
    
    ClassPartyGuestDao dao = new ClassPartyGuestDaoInMemImpl();
    List<String> guestList;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayWelcome (Model model) {
                
        return "Welcome";
    }

    @RequestMapping(value = "/RSVP", method = RequestMethod.POST)
    public String displayRSVPform(Model model) {
    
        return "RSVP";
    }    
           
    @RequestMapping(value = "/RSVP", method = RequestMethod.GET)
    public String submitRSVPform(HttpServletRequest request, Model model) {
        
        String llamaCountInput = request.getParameter("llamaInput");
        int llamaCount = Integer.parseInt(llamaCountInput);
        
        Guest guest = new Guest(request.getParameter("nameInput"), llamaCount);
        
        dao.addGuest(guest);
        
        List<Guest> inventoryList = dao.getAllGuests();
        
        model.addAttribute("inventoryList", inventoryList);
        
        return "RSVP";
    }    
    
        @RequestMapping(value = "/theList", method = RequestMethod.GET)
    public String submitAnotherGuest(Model model) {
    
                
        return "theList";
    }
    
    @RequestMapping(value = "/theList", method = RequestMethod.GET)
    public String displayList(Model model) {
    
                
        return "RSVP";
    }
   
    
}

