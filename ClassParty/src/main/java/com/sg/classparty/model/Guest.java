/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classparty.model;

/**
 *
 * @author apprentice
 */
public class Guest {
    
    private int id;
    private String guestName;
    private int llamaCount;

    public Guest(String guestName, int llamaCount) {
        this.guestName = guestName;
        this.llamaCount = llamaCount;
        
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public int getLlamaCount() {
        return llamaCount;
    }

    public void setLlamaCount(int llamaCount) {
        this.llamaCount = llamaCount;
    }
    
}
