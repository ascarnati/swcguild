<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RSVP.jsp</title>
    </head>
    <body>
        <form>
            <div>
                <h1>Coming to my party?</h1>
                <input type="radio" name="isComing" value="yes"> Yes!
                <input type="radio" name="isComing" value="no"> no<br>
                
                <label for="nameInput">Name: </label>
                <input type="text" name="nameInput">
                
                <label for "llamaInput">#Llamas you are bringing </label>
                <select name="llamaInput">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                
                <input type="submit" value="RSVPbutton" class=""btn btn-success"/>
            </div>    
        </form>
        
        
    
        
        
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vendingMachine.js"></script>
    
    </body>
</html>
