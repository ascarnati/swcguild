<%-- 
    Document   : result
    Created on : Mar 21, 2017, 12:56:49 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>result</title>
    </head>
    <body>
        <h1>These are the results....</h1>
        
        <p>
            Principle Amount: $
            <c:out value="${principleAmount} "/>
        </p>
        <p>
            Duration of investment (years):
            <c:out value="${numYears} "/>
        </p>
        <p>
            <c:forEach var="year" items="${yearlyResultsList}">
                Report Year: <c:out value="${year.getReportYear()} "/><br>
                New Principle: <c:out value="${year.getNewPrinciple()} "/><br>
                Interest Earned: <c:out value="${year.getInterestEarned()} "/><br>
                New Net Value: <c:out value="${year.getNewNetValue()} "/><br>
            </c:forEach>
        </p>
        
        <p>
            <a href="index.jsp">Click here to calculate a new order.</a>
        </p>
        
    </body>
</html>
