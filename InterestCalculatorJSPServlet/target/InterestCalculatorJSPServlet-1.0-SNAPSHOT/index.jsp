<%-- 
    Document   : index
    Created on : Mar 21, 2017, 12:57:09 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator</title>
    </head>
    <body>
        <h1>Welcome to my Interest Calculator Web App</h1>

        <div>
            <h2>Input Form</h2>

            <form method="POST" action="InterestCalculatorServlet">
                <div class="form-group">
                    <label for="interestRate">
                        Annual Interest Rate:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="interestRate"
                               placeholder="%" />
                    </div>   
                </div>

                <div class="form-group">
                    <label for="principleAmount">
                        Initial Principle:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="principleAmount"
                               placeholder="$$$" />
                    </div>

                </div>
                <div class="form-group">
                    <label for="numYears">
                    Years to Invest:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="numYears"
                               placeholder="years" />
                    </div>                    
                </div>

                <input type="submit" value="Calculate">
            </form>

        </div>

    </body>
</html>
