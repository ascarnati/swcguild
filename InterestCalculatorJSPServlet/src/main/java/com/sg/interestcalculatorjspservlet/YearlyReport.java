/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculatorjspservlet;

import java.math.BigDecimal;

/**
 *
 * @author apprentice
 */
public class YearlyReport {

    private int reportYear;
    private BigDecimal newPrinciple;
    private BigDecimal interestEarned;
    private BigDecimal newNetValue;
    
    public YearlyReport(int reportYear) {
        this.reportYear = reportYear;
    }

    public int getReportYear() {
        return reportYear;
    }

    public void setReportYear(int reportYear) {
        this.reportYear = reportYear;
    }

    public BigDecimal getNewPrinciple() {
        return newPrinciple;
    }

    public void setNewPrinciple(BigDecimal newPrinciple) {
        this.newPrinciple = newPrinciple;
    }

    public BigDecimal getInterestEarned() {
        return interestEarned;
    }

    public void setInterestEarned(BigDecimal interestEarned) {
        this.interestEarned = interestEarned;
    }

    public BigDecimal getNewNetValue() {
        return newNetValue;
    }

    public void setNewNetValue(BigDecimal newNetValue) {
        this.newNetValue = newNetValue;
    }
    
    
}
