/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculatorjspservlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "InterestCalculatorServlet", urlPatterns = {"/InterestCalculatorServlet"})
public class InterestCalculatorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // *** DECLARE VARIABLES ***
        List<YearlyReport> yearlyResultsList = new ArrayList<>();
        int reportYear = 2017,
                numYears,
                currentYear = 2017;
        BigDecimal principleAmount,
                interestRate,
                newPrinciple,
                interestEarned,
                minimumAmount = new BigDecimal("0.01");

        // *** GET VALUES SUBMITTED IN THE INDEX FORM ***
        // remember to convert the values from strings
        String inputInterestRate = request.getParameter("interestRate");
        interestRate = new BigDecimal(inputInterestRate).setScale(2);

        String inputPrincipleAmount = request.getParameter("principleAmount");
        principleAmount = new BigDecimal(inputPrincipleAmount).setScale(2);

        String inputNumYears = request.getParameter("numYears");
        numYears = Integer.parseInt(inputNumYears);

        // *** PROCESS BUSINESS LOGIC ***
        newPrinciple = principleAmount;


        // need to iterate over the number of years provided by the user
        // and create an object for each year, then, 
        // put each group of calculations into a list with key=year
        for (int i = 0; i <= numYears; i++) {
            YearlyReport year = new YearlyReport(reportYear);

            do {
                BigDecimal newNetValue = calcInterest(newPrinciple, interestRate, numYears);
                interestEarned = newNetValue.subtract(principleAmount);

                //i++;
                newPrinciple = newNetValue;
                        
            } while (numYears < i);
                    
            year.setReportYear(reportYear);
            year.setNewPrinciple(newPrinciple);
            year.setInterestEarned(interestEarned);
            yearlyResultsList.add(year);
            reportYear = reportYear + 1;
        }

        // *** SET ATTRIBUTES SO THEY'RE AVAIL TO RESULT.JSP ***
        request.setAttribute("numYears", numYears);
        // report year object (will get the properties from the object 
        // in the result.jsp
        //request.setAttribute("reportYear", reportYear);
        request.setAttribute("yearlyResultsList", yearlyResultsList);

        // *** GET THE REQUEST DISPATCHER FOR RESULT.JSP ***
        RequestDispatcher rd = request.getRequestDispatcher("result.jsp");

        // *** FORWARD THE REQUEST TO RESULT.JSP ***
        rd.forward(request, response);
    }

    protected BigDecimal calcInterest(BigDecimal principle, BigDecimal interestRate, int numYears) {
        BigDecimal newPrincipleCalc;
        newPrincipleCalc = principle;
        int i = 1;

        do {
            // given the annual interest rate, calc the quarterly rate
            BigDecimal quarter = new BigDecimal(4.00);
            BigDecimal quarterlyIntRate = interestRate.divide(quarter);
            newPrincipleCalc = newPrincipleCalc.multiply(quarterlyIntRate);

            i++;
        } while (i < numYears);

        principle = newPrincipleCalc;

        return principle;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
