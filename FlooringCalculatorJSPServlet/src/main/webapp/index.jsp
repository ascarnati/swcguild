<%-- 
    Document   : index
    Created on : Mar 20, 2017, 3:52:28 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>FlooringCalculator</title>
    </head>
    <body>
        <h1>The Best Flooring Calculator!</h1>
        
        <div>
            <h2>Input Form</h2>
            
            <form method="POST" action="FlooringCalculatorServlet">
                <div class="form-group">
                    <label for="width">
                        Width of the Area:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="width"
                               placeholder="width" />
                    </div>   
                </div>
                
                <div class="form-group">
                    <label for="length">
                        Length of the Area:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="length"
                               placeholder="length" />
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="costPerSqFt">
                        Cost per Square Foot of Material:
                    </label>
                    <div>
                        <input type="text"
                               class="form-control"
                               name="costPerSqFt"
                               placeholder="material cost" />
                    </div>                    
                </div>
                
<!--            Width:<br>
                <input type="text" name="width">
                <br>
                Length:<br>
                <input type="text" name="length">
                <br>
                Cost Per Square Foot:<br>
                <input type="text" name="costPerSqFt">
                <br><br>-->
                <input type="submit" value="Calculate">
            </form>
            
        </div>
            
    </body>
</html>
