<%-- 
    Document   : result
    Created on : Mar 20, 2017, 3:56:25 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Result</title>
    </head>
    <body>
        <h1>Flooring Calculator Results!</h1>
        <p>
            You asked to calculate the cost of flooring for an area that measures
        </p>
        <p>
            ${length} feet long and ${width} feet wide, using materials that 
            cost $${materialCpSf} dollars per square foot.            
        </p>
        <p>
            Materials Cost: $
            <c:out value="${materialCost} "/>
        </p>
        <p>
            Labor Cost: $
            <c:out value="${laborCost} "/>
        </p>
        <p>
            Install Time (in hours):
            <c:out value="${laborHours} "/>
        </p>
        <p>
            Total Cost: $
            <c:out value="${totalCost} "/>
        </p>
        
        <p>
            <a href="index.jsp">Click here to calculate a new order.</a>
        </p>
        
    </body>
</html>
