/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringcalculatorjspservlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FlooringCalculatorServlet", urlPatterns = {"/FlooringCalculatorServlet"})
public class FlooringCalculatorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // *** declare my variables ***
        
        int length;
        int width;
        int area;
        double materialCpSf;
        double materialCost;
        double laborHours;
        double laborCost;               
        double totalCost;
        
        // *** get the values submitted in the index.jsp form as strings and convert
        
        String inputLength = request.getParameter("length");
        length = Integer.parseInt(inputLength);
        
        String inputWidth = request.getParameter("width");
        width = Integer.parseInt(inputWidth);
        
        String inputMaterialCpSf = request.getParameter("costPerSqFt");
        materialCpSf = Double.parseDouble(inputMaterialCpSf);
        
        
        // *** process all business logic ***
        // calculate area = length x width
        area = length * width;
        
        // given area, calculate materialCost
        materialCost = area * materialCpSf;
        
        // calculating laborHours: 
            // first calculate labor Periods (1 period = .25 hours)
            // if 20 sqft per hour, then 5 sqft per period
        double laborPeriods = Math.round(area / 5);
            // round this value up to the next period
        
            // divide this number by 4 to display as an laborHours
        laborHours = laborPeriods / 4;
                
        // given labor hours, calculate laborCost as laborHours x 86.00
        laborCost = laborHours * 86.00;
        
        // given laborCost and materialCost, calculate totalCost
        totalCost = laborCost + materialCost;
        
        // *** set attributes so they're available to result.jsp ***
        // attribute area to cover
        request.setAttribute("area", area);
        // attribute laborHours
        request.setAttribute("laborHours", laborHours);
        // attribute laborCost
        request.setAttribute("laborCost", laborCost);
        // attribute materialCost
        request.setAttribute("materialCost", materialCost);
        // attribute totalCost
        request.setAttribute("totalCost", totalCost);
        // attribute materialCpSf
        request.setAttribute("materialCpSf", materialCpSf);
        
        // *** get the Request Dispatcher for result.jsp and finally 
        RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
        // forward the request to result.jsp  ***
        rd.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
