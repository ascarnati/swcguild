/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd_library;

import com.sg.dvd_library.controller.DvdLibraryController;
import com.sg.dvd_library.dao.DvdLibraryDao;
import com.sg.dvd_library.dao.DvdLibraryDaoFileImpl;
import com.sg.dvd_library.ui.DvdLibraryView;
import com.sg.dvd_library.ui.UserIO;
import com.sg.dvd_library.ui.UserIOConsoleImpl;

/**
 *
 * @author Anthony Scarnati
 * 
 */
public class App {
    
    public static void main(String[] args) {
        
        UserIO myIo = new UserIOConsoleImpl();
        DvdLibraryView myView = new DvdLibraryView(myIo);
        DvdLibraryDao myDao = new DvdLibraryDaoFileImpl();
        DvdLibraryController controller = new DvdLibraryController(myDao, myView);
      
        controller.run();
    }
}
