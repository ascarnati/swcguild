<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Organization Details</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Organization Details</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                  <li role="presentation">
                        <a href="${pageContext.request.contextPath}">
                            Home
                        </a>
                  </li>
                  <li role="presentation">
                      <a href="${pageContext.request.contextPath}/displayOrganizationsPage">
                          Organizations
                      </a>
                  </li>
                </ul>    
            </div>
                <p>
                    <h4><em>Organization Info:</em></h4>
                </p>
                <p>
                    <strong>Name: </strong>
                    <c:out value="${organization.name}"/>
                </p>
                <p>
                    <strong>Description: </strong>
                    <c:out value="${organization.description}"/>
                </p>
                <p>
                    <strong>Alignment: </strong>
                    <c:out value="${organization.alignment}"/>
                </p>
                <p>
                    <strong>Contact Info: </strong>
                    <c:out value="${organization.contactInfo}"/>
                </p>
                <br>
                <p><h4><em>
                    Member Characters: 
                </em></h4></p>
                
                <c:forEach items="${organization.characters}" var="currentCharacter">
                    <p>
                        <strong>Character:</strong>
                        <c:out value="${currentCharacter.name}"/>
                        <strong>Character Powers:</strong>
                            <c:forEach items="${currentCharacter.powers}" var="currentPower">
                                <c:out value="${currentPower.name}"/>,
                            </c:forEach>
                    </p>
                </c:forEach>

                                    <br>
                <p><h4><em>
                    Locations: 
                </em></h4></p>
                
                <c:forEach items="${organization.locations}" var="currentLocation">
                    <p>
                        <strong>Location:</strong>
                        <c:out value="${currentLocation.name}"/>
                    </p>
                </c:forEach>
                
        </div>
                
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>