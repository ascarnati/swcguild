<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Characters</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/anthonyStyles.css" rel="stylesheet" type="text/css" />        
    </head>
    <body>
        <div class="container">
            <h1><center>Hero Spotter</center></h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                	<li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                	<li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayCharactersPage">Characters</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayLocationsPage">Locations</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayOrganizationsPage">Organizations</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displaySightingsPage">Sightings</a></li>                        
                </ul>    
            </div>

            <!-- Main Page Content Start -->
            <!-- Add a row to our container - this will hold the summary table and the new
                    characters form. -->
            <div class="row">
                <!-- 
                    Add a col to hold the summary table - have it take up half the row 
                -->
                <div class="col-md-6">
                    <h2>Heroes and Villains</h2>
                    <table id="contactTable" class="table table-hover">
                        <tr>
                            <th width="40%">Character Name</th>
                            <th width="30%">Alignment</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>
                        <c:forEach var="currentCharacter" items="${characterList}">
                            <tr>
                                <td>
                                    <a href="displayCharacterDetails?characterId=${currentCharacter.characterId}">
                                    <c:out value="${currentCharacter.name}"/>
                                </td>
                                <td>
                                    <c:out value="${currentCharacter.alignment}"/>
                                </td>
                                <td>
                                    <a href="displayEditCharacterForm?contactId=${currentCharacter.characterId}">
                                    Edit
                                    </a>
                                </td>
                                <td>
                                    <a href="deleteCharacter?characterId=${currentCharacter.characterId}">
                                    Delete
                                    </a>                                    
                                </td>
                            </tr>
                        </c:forEach>
                    </table>                    
                </div> <!-- End col div -->
                <!-- 
                    Add col to hold the new contact form - have it take up the other 
                    half of the row
                -->
                <div class="col-md-6">
                    <h2>Add New Character</h2>
                    <form class="form-horizontal" 
                          role="form" method="POST" 
                          action="createCharacter">

                        <div class="form-group">
                            <label for="add-characterName" class="col-md-4 control-label">Character Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="characterName" placeholder="Character Name"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-alias" class="col-md-4 control-label">Alias:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="alias" placeholder="Alias"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-alignment" class="col-md-4 control-label">Alignment:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="alignment" placeholder="Alignment"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-characterPower" class="col-md-4 control-label">Powers:</label>
                            <div class="col-md-8">
                                <select name="characterPower" multiple>
                                    <c:forEach items="${powerList}" var="currentPower">
                                        <option value="${currentPower.powerId}"><c:out value="${currentPower.name}" /></option>
                                    </c:forEach>
                                </select>                                
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Character"/>
                            </div>
                        </div>
                    </form>

                </div> <!-- End col div -->

            </div> <!-- End row div -->


            <!-- Main Page Content Stop -->    
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>