<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Sighting</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Edit Sighting</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}">
                            Home
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displaySightingsPage">
                            Sightings
                        </a>
                    </li>
                </ul>    
            </div>
            <form class="form-horizontal" 
                  role="form" method="POST"
                  modelAttribute="sighting" 
                  action="editSighting">

                <div class="form-group">
                    <label for="displayCurrentDate" class="col-md-4 control-label">Date:</label>
                    <div class="col-md-8">
                        <c:out value="${sighting.date}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="displayCurrentDescription" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <c:out value="${sighting.description}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="displayCurrentLocation" class="col-md-4 control-label">Location:</label>
                    <div class="col-md-8">
                        <c:out value="${location.name}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="displayCurrentCharacters" class="col-md-4 control-label">Characters:</label>
                    <div class="col-md-8">
                        <c:forEach items="${sighting.characters}" var="currentCharacter">
                            <option value="${currentCharacter.characterId}"><c:out value="${currentCharacter.name}" />
                            </c:forEach>
                    </div>
                </div>

                <hr/>

                <div class="form-group">
                    <label for="edit-sightingDate" class="col-md-4 control-label">Edited Date:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="sightingDate" placeholder="yyyy-MM-dd" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-sightingDescription" class="col-md-4 control-label">Edited Description:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="sightingDescription" required/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit-sightingLocation" class="col-md-4 control-label">Edited Location:</label>
                    <div class="col-md-8">
                        <select name="sightingLocation" value="${sighting.location}">
                            <!--<option value="${sighting.location}"></option>-->
                            <c:forEach items="${allLocationList}" var="currentLocation">
                                <option value="${currentLocation.locationId}"><c:out value="${currentLocation.name}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit-sightingCharacter" class="col-md-4 control-label">Edited Characters:</label>
                    <div class="col-md-8">
                        <select name="sightingCharacter" value="${sighting.characters}" multiple required>
                            <c:forEach items="${allCharacterList}" var="currentCharacter">
                                <option value="${currentCharacter.characterId}"><c:out value="${currentCharacter.name}"/></option>
                            </c:forEach>
                            <input type="hidden" id="sightingId" value="${sighting.sightingId}" name="sightingId" />
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <input type="submit" class="btn btn-default" value="Edit Sighting"/>
                    </div>
                </div>
            </form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>