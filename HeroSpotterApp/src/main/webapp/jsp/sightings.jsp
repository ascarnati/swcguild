<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sightings</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1><center>Hero Spotter</center></h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                	<li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayCharactersPage">Characters</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayLocationsPage">Locations</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayOrganizationsPage">Organizations</a></li>
                	<li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displaySightingsPage">Sightings</a></li>                        
                </ul>    
            </div>
            <!-- Main Page Content Start -->
            <!-- Add a row to our container - this will hold the summary table and the new
                    sightings form. -->
            <div class="row">
                <!-- 
                    Add a col to hold the summary table - have it take up half the row 
                -->
                <div class="col-md-6">
                    <h2>Sightings</h2>
                    <table id="sightingTable" class="table table-hover">
                        <tr>
                            <th width="10%">Sighting ID</th>
                            <th width="30%">Date</th>
                            <th width="30%">Description</th>
                            <th width="15%"></th>
                        </tr>
                        <c:forEach var="currentSighting" items="${sightingList}">
                            <tr>
                                <td>
                                    <a href="displaySightingDetails?sightingId=${currentSighting.sightingId}">
                                        <c:out value="${currentSighting.sightingId}"/>
                                </td>
                                <td>
                                    <c:out value="${currentSighting.date}"/>
                                </td>
                                <td>
                                    <c:out value="${currentSighting.description}"/>
                                </td>
                                <td>
                                    <a href="displayEditSightingForm?sightingId=${currentSighting.sightingId}">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    <a href="deleteSighting?sightingId=${currentSighting.sightingId}">
                                        Delete
                                    </a>                                    
                                </td>
                            </tr>
                        </c:forEach>
                    </table>                    
                </div> <!-- End col div -->
                <!-- 
                    Add col to hold the new location form - have it take up the other 
                    half of the row
                -->
                <div class="col-md-6">
                    <h2>Add New Sighting</h2>
                    <form class="form-horizontal" 
                          role="form" method="POST" 
                          action="createSighting">
                        <div class="form-group">
                            <label for="add-sightingDate" class="col-md-4 control-label">Date:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="sightingDate" placeholder="MM/dd/yyyy"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-sightingDescription" class="col-md-4 control-label">Description:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="sightingDescription" placeholder="Description"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-sightingLocation" class="col-md-4 control-label">Sighting Location:</label>
                            <div class="col-md-8">
                                <select name="sightingLocation" >
                                    <c:forEach items="${locationList}" var="currentLocation">
                                        <option value="${currentLocation.locationId}"><c:out value="${currentLocation.name}"/></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="add-sightingCharacter" class="col-md-4 control-label">Characters:</label>
                            <div class="col-md-8">
                                <select name="sightingCharacter" multiple>
                                    <c:forEach items="${characterList}" var="currentCharacter">
                                        <option value="${currentCharacter.characterId}"><c:out value="${currentCharacter.name}"/></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Sighting"/>
                            </div>
                        </div>
                    </form>

                </div> <!-- End col div -->

            </div> <!-- End row div -->


            <!-- Main Page Content Stop -->    
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>