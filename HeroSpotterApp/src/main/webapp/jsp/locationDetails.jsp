<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Location Details</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Location Details</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                  <li role="presentation">
                        <a href="${pageContext.request.contextPath}">
                            Home
                        </a>
                  </li>
                  <li role="presentation">
                      <a href="${pageContext.request.contextPath}/displayLocationsPage">
                          Locations
                      </a>
                  </li>
                </ul>    
            </div>
                <p>
                    <strong>Name: </strong>
                    <c:out value="${location.name}"/>
                </p>
                <p>
                    <strong>Description: </strong>
                    <c:out value="${location.description}"/>
                </p>
                <p>
                    <strong>Street: </strong>
                    <c:out value="${location.addressStreet}"/>
                </p>
                <p>
                    <strong>City: </strong>
                    <c:out value="${location.addressCity}"/>
                </p>
                <p>
                    <strong>State: </strong>
                    <c:out value="${location.addressState}"/>
                </p>
                <p>
                    <strong>Zip Code: </strong>
                    <c:out value="${location.addressZip}"/>
                </p>
                <p>
                    <strong>Latitude: </strong>
                    <c:out value="${location.mapLatitude}"/>
                </p>                
                <p>
                    <strong>Longitude: </strong>
                    <c:out value="${location.mapLongitude}"/>
                </p>                
        </div>
                
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>