<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Locations</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1><center>Hero Spotter</center></h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                	<li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayCharactersPage">Characters</a></li>
                	<li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayLocationsPage">Locations</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displayOrganizationsPage">Organizations</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/displaySightingsPage">Sightings</a></li>                        
                </ul>    
            </div>
            <!-- Main Page Content Start -->
            <!-- Add a row to our container - this will hold the summary table and the new
                    locations form. -->
            <div class="row">
                <!-- 
                    Add a col to hold the summary table - have it take up half the row 
                -->
                <div class="col-md-6">
                    <h2>Locations</h2>
                    <table id="locationTable" class="table table-hover">
                        <tr>
                            <th width="40%">Location Name</th>
                            <th width="30%">Description</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>
                        <c:forEach var="currentLocation" items="${locationList}">
                            <tr>
                                <td>
                                    <a href="displayLocationDetails?locationId=${currentLocation.locationId}">
                                    <c:out value="${currentLocation.name}"/>
                                </td>
                                <td>
                                    <c:out value="${currentLocation.description}"/>
                                </td>
                                <td>
                                    <a href="displayEditLocationForm?locationId=${currentLocation.locationId}">
                                    Edit
                                    </a>
                                </td>
                                <td>
                                    <a href="deleteLocation?locationId=${currentLocation.locationId}">
                                    Delete
                                    </a>                                    
                                </td>
                            </tr>
                        </c:forEach>
                    </table>                    
                </div> <!-- End col div -->
                <!-- 
                    Add col to hold the new location form - have it take up the other 
                    half of the row
                -->
                <div class="col-md-6">
                    <h2>Add New Location</h2>
                    <form class="form-horizontal" 
                          role="form" method="POST" 
                          action="createLocation">
                        <div class="form-group">
                            <label for="add-locationName" class="col-md-4 control-label">Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="locationName" placeholder="Location Name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationDescription" class="col-md-4 control-label">Description:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="locationDescription" placeholder="Description"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationAddressStreet" class="col-md-4 control-label">Street Address:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="locationAddressStreet" placeholder="Street Address"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationAddressCity" class="col-md-4 control-label">City:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="locationAddressCity" placeholder="City"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationAddressState" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="locationAddressState" placeholder="State"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationAddressZip" class="col-md-4 control-label">Zipcode:</label>
                            <div class="col-md-8">
                                <input type="text" onkeypress='validate(event)' class="form-control" name="locationAddressZip" placeholder="Zipcode"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationMapLatitude" class="col-md-4 control-label">Latitude:</label>
                            <div class="col-md-8">
                                <input type="text" onkeypress='validate(event)' class="form-control" name="locationMapLatitude" placeholder="Latitude"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-locationMapLongitude" class="col-md-4 control-label">Longitude:</label>
                            <div class="col-md-8">
                                <input type="text" onkeypress='validate(event)' class="form-control" name="locationMapLongitude" placeholder="Longitude"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Location"/>
                            </div>
                        </div>
                    </form>

                </div> <!-- End col div -->

            </div> <!-- End row div -->


            <!-- Main Page Content Stop -->    
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/HeroSpotter.js"></script>

    </body>
</html>