/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.mappers.CharacterMapper;
import com.sg.herospotter.mappers.PowerMapper;
import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Power;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HeroSpotterCharacterPowerDaoImpl implements HeroSpotterCharacterPowerDao {

    private JdbcTemplate jdbcTemplate;

    // instantiate Mapper Classes for the Character and Power tables
    private CharacterMapper CharacterMapper = new CharacterMapper();
    private PowerMapper PowerMapper = new PowerMapper();

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //**************************************************************************
    // add Character-related Prepared Statements
    //**************************************************************************
    private static final String SQL_INSERT_CHARACTER
            = "INSERT INTO `Character` (Name, Alias, Alignment) VALUES (?, ?, ?)";

    private static final String SQL_DELETE_CHARACTER
            = "DELETE FROM `Character` WHERE CharacterId = ?";

    private static final String SQL_UPDATE_CHARACTER
            = "UPDATE `Character` SET Name = ?, Alias = ?, Alignment = ? WHERE CharacterId = ?";

    private static final String SQL_SELECT_CHARACTER
            = "SELECT * FROM `Character` WHERE CharacterId = ?";

    private static final String SQL_SELECT_ALL_CHARACTERS
            = "SELECT * FROM `Character`";

    //**************************************************************************
    // add Mapper for Character table
    //**************************************************************************
//    private static final class CharacterMapper implements RowMapper<Characters> {
//
//        @Override
//        public Characters mapRow(ResultSet rs, int i) throws SQLException {
//            Characters newChar = new Characters();
//            newChar.setName(rs.getString("name"));
//            newChar.setAlias(rs.getString("alias"));
//            newChar.setAlignment(rs.getString("alignment"));
//            newChar.setCharacterId(rs.getInt("characterId"));
//            return newChar;
//        }
//    }
    //**************************************************************************
    // Character-specific methods
    //**************************************************************************
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addCharacter(Characters character) {
        // insert a new row into Character table, then get newly generated id
        jdbcTemplate.update(SQL_INSERT_CHARACTER,
                character.getName(),
                character.getAlias(),
                character.getAlignment());

        int characterId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        character.setCharacterId(characterId);
        // finally update the Character_Power bridge table
        insertCharacterPowers(character);
    }

    @Override
    public void deleteCharacter(int characterId) {
        // delete Character_Power relationship for this character
        jdbcTemplate.update(SQL_DELETE_CHARACTER_POWERS, characterId);
        // now delete Character
        jdbcTemplate.update(SQL_DELETE_CHARACTER, characterId);
    }

    @Override
    public void updateCharacter(Characters character) {

        jdbcTemplate.update(SQL_UPDATE_CHARACTER,
                character.getName(),
                character.getAlias(),
                character.getAlignment());
    }

    @Override
    public Characters getCharacterById(int characterId) {
        try {
            Characters character = jdbcTemplate.queryForObject(SQL_SELECT_CHARACTER,
                    CharacterMapper,
                    characterId);
            character.setPowers(findPowersForCharacter(character));

            return character;

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Characters> getAllCharacters() {

        List<Characters> charList = jdbcTemplate.query(SQL_SELECT_ALL_CHARACTERS, CharacterMapper);
        return associatePowersWithCharacter(charList);
    }

    //**************************************************************************
    // add Power-related Prepared Statements
    //**************************************************************************
    private static final String SQL_INSERT_POWER
            = "INSERT INTO Power (Name) VALUE (?)";

    private static final String SQL_DELETE_POWER
            = "DELETE FROM Power WHERE PowerId = ?";

    private static final String SQL_UPDATE_POWER
            = "UPDATE Power SET Name = ? WHERE PowerId = ?";

    private static final String SQL_SELECT_POWER
            = "SELECT * FROM Power WHERE PowerId = ?";

    private static final String SQL_SELECT_ALL_POWERS
            = "SELECT * FROM Power";

    //**************************************************************************
    // add Mapper for Power table
    //**************************************************************************    
//    private static final class PowerMapper implements RowMapper<Power> {
//        
//        @Override
//        public Power mapRow(ResultSet rs, int i) throws SQLException {
//            Power power = new Power();
//            power.setName(rs.getString("name"));
//            power.setPowerId(rs.getInt("powerId"));
//            return power;
//        }
//    }
    //**************************************************************************
    // Power-specific methods
    //**************************************************************************
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addPower(Power power) {
        jdbcTemplate.update(SQL_INSERT_POWER,
                power.getName());

        int powerId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);

        power.setPowerId(powerId);
    }

    @Override
    public void deletePower(int powerId) {
        jdbcTemplate.update(SQL_DELETE_POWER, powerId);
    }

    @Override
    public void updatePower(Power power) {
        jdbcTemplate.update(SQL_UPDATE_POWER,
                power.getName(),
                power.getPowerId());

    }

    @Override
    public Power getPowerById(int powerId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_POWER,
                    PowerMapper,
                    powerId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Power> getAllPowers() {
        return jdbcTemplate.query(SQL_SELECT_ALL_POWERS,
                PowerMapper);
    }

    //**************************************************************************
    // *** add Character_Power Bridge Table-related Prepared Statements ***
    //**************************************************************************
    private static final String SQL_INSERT_CHARACTER_POWERS
            = "INSERT INTO Character_Power (CharacterId, PowerId) VALUES (?, ?)";

    private static final String SQL_DELETE_CHARACTER_POWERS
            = "DELETE FROM Character_Power WHERE CharacterId = ?";

//    private static final String SQL_GET_POWERS_BY_CHARACTER_ID
//            = "SELECT c.Name, p.Name FROM `Character` c "
//            + "INNER JOIN Character_Power cp ON cp.CharacterId = c.CharacterId "
//            + "INNER JOIN Power p ON p.PowerId = cp.PowerId "
//            + "WHERE c.CharacterId = ?";
//    
//    private static final String SQL_GET_CHARACTERS_BY_POWER_ID
//            = "SELECT c.Name, cp.PowerId "
//            + "FROM Character_Power cp "
//            + "INNER JOIN `Character` c ON c.CharacterId = cp.CharacterId "
//            + "WHERE PowerId = ?";
    private static final String SQL_GET_ALL_POWERS_BY_CHARACTER_ID
            = "SELECT p.PowerId, p.Name FROM `Character` c "
            + "INNER JOIN Character_Power cp ON cp.CharacterId = c.CharacterId "
            + "INNER JOIN Power p ON p.PowerId = cp.PowerId "
            + "WHERE c.CharacterId = ?";

    //**************************************************************************
    // Bridge-table related methods
    //**************************************************************************
    // *** create an entry in Character_Power Bridge Table ***
    private void insertCharacterPowers(Characters character) {
        final int characterId = character.getCharacterId();
        final List<Power> powers = character.getPowers();

        // if list of powers is null, don't attempt to insert them or will
        // result in a null pointer exception
        if (powers != null) {
            // update the bridge table with an entry for each power the char has
            for (Power currentPower : powers) {
                jdbcTemplate.update(SQL_INSERT_CHARACTER_POWERS,
                        characterId,
                        currentPower.getPowerId());
            }

        }

    }

    private List<Power> findPowersForCharacter(Characters character) {
        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_CHARACTER_ID,
                PowerMapper, character.getCharacterId());
    }
    
// ******** TESTING PURPOSES ONLY to try to solve when getting a character without Powers
//    private List<Power> findPowersForCharacter(Characters character) {
//        
//        final List<Power> powers = jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_CHARACTER_ID,
//                PowerMapper, character.getCharacterId());
//        
//        if (powers == null) {
//            // do something so that the List returns EMPTY instead of NULL
//            return Collections.emptyList();
//            
//        } return powers;
//    }

    // this helper method uses the findPowersForCharacter helper method to
    // associate the powers with each character in the given List.    
    // USE THIS both when inserting a character into the Character_Powers bridge
    // AND when getting a character back from the table
    private List<Characters> associatePowersWithCharacter(List<Characters> charList) {
        for (Characters currentCharacter : charList) {
            currentCharacter.setPowers(findPowersForCharacter(currentCharacter));
        }
        return charList;
    }

    // retrieves all Powers for a given Character by joining the character 
    // and power table
//    private List<Power> getAllPowersForCharacter(Characters character) {
//        return jdbcTemplate.query(SQL_GET_POWERS_BY_CHARACTER_ID,
//                new PowerMapper(),
//                character.getCharacterId());
//    }
}
