/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Power;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface HeroSpotterCharacterPowerDao {
    
    // crud Character DTO
    public void addCharacter(Characters character);
    
    public void deleteCharacter(int characterId);
    
    public void updateCharacter(Characters character);
    
    public Characters getCharacterById(int characterId);
    
    public List<Characters> getAllCharacters();
    
    // crud Power DTO
    public void addPower(Power power);
    
    public void deletePower(int powerId);
    
    public void updatePower(Power power);
    
    public Power getPowerById(int powerId);
    
    public List<Power> getAllPowers();
    
    
}
