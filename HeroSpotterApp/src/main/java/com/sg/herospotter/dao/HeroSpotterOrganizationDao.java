/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.model.Organization;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface HeroSpotterOrganizationDao {
    
    // crud Organization DAO
    public void addOrganization(Organization organization);
    
    public void deleteOrganization(int organizationId);
    
    public void updateOrganization(Organization organization);
    
    public Organization getOrganizationById(int organizationId);
    
    public List<Organization> getAllOrganizations();
    
}
