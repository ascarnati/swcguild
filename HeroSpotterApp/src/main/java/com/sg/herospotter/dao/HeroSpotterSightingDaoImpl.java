/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.mappers.CharacterMapper;
import com.sg.herospotter.mappers.LocationMapper;
import com.sg.herospotter.mappers.PowerMapper;
import com.sg.herospotter.mappers.SightingMapper;
import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Power;
import com.sg.herospotter.model.Sighting;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HeroSpotterSightingDaoImpl implements HeroSpotterSightingDao {

    private JdbcTemplate jdbcTemplate;
    private LocationMapper LocationMapper = new LocationMapper();
    private SightingMapper SightingMapper = new SightingMapper();
    private CharacterMapper CharacterMapper = new CharacterMapper();
    private PowerMapper PowerMapper = new PowerMapper();

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //**************************************************************************
    // add basic Sighting-specific Prepared Statements AND METHODS
    //**************************************************************************
    private static final String SQL_INSERT_SIGHTING
            = "INSERT INTO Sighting (Date, Description, LocationId) "
            + "VALUES (?, ?, ?)";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addSighting(Sighting sighting) {
        // insert basic info into Sighting table and get newly generated Sighting_id
        jdbcTemplate.update(SQL_INSERT_SIGHTING,
                sighting.getDate().toString(),
                sighting.getDescription(),
                sighting.getLocation().getLocationId());

        int sightingId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        sighting.setSightingId(sightingId);

        // now update the Character_Sighting table
        insertSightingsCharacter(sighting);
    }

    private static final String SQL_DELETE_SIGHTING
            = "DELETE FROM Sighting WHERE SightingId = ?";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteSighting(int sightingId) {
        // delete Character_Sighting relationship for this Sighting
        jdbcTemplate.update(SQL_DELETE_CHARACTERS_SIGHTINGS, sightingId);
        // now delete the Sighting
        jdbcTemplate.update(SQL_DELETE_SIGHTING, sightingId);
    }

    private static final String SQL_UPDATE_SIGHTING
            = "UPDATE Sighting SET Date = ?, Description = ?, LocationId = ? "
            + "WHERE SightingId = ?";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_UPDATE_SIGHTING,
                sighting.getDate().toString(),
                sighting.getDescription(),
                sighting.getLocation().getLocationId(),
                sighting.getSightingId());

        // delete character_sighting relationships and then reset them
        jdbcTemplate.update(SQL_DELETE_CHARACTERS_SIGHTINGS, sighting.getSightingId());
        // delete location from Sighting and then reset it
        

        insertSightingsCharacter(sighting);
        
    }

    private static final String SQL_SELECT_SIGHTING
            = "SELECT * FROM Sighting WHERE SightingId = ?";

    @Override
    public Sighting getSightingById(int sightingId) {
        try {
            Sighting sighting = jdbcTemplate.queryForObject(SQL_SELECT_SIGHTING,
                    SightingMapper,
                    sightingId);
            // before returning the Sighting object, you must first get
            // its associated Location and Characters
            sighting.setCharacters(findCharactersForSighting(sighting));
            sighting.setLocation(findLocationForSighting(sighting));
            return sighting;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_SELECT_ALL_SIGHTINGS
            = "SELECT * FROM Sighting";

    @Override
    public List<Sighting> getAllSightings() {
        List<Sighting> sightingList = jdbcTemplate.query(SQL_SELECT_ALL_SIGHTINGS,
                SightingMapper);
        // -- create and use helper method for associating Chars and Loc
        return associateCharactersAndLocationWithSighting(sightingList);
    }

    //**************************************************************************
    // A Sighting consists of 1 Location and 1-to-many Characters so
    // adding related Prepared Statements AND METHODS here
    //**************************************************************************
    private static final String SQL_SELECT_SIGHTINGS_BY_LOCATION_ID
            = "SELECT * FROM Sighting WHERE LocationId = ?";

    // getSightingsByLocationId(int sightingId) (like getBooksByPublisherId)
    public List<Sighting> getSightingsByLocationId(int locationId) {
        // get the sightings at this location
        List<Sighting> sightingList
                = jdbcTemplate.query(SQL_SELECT_SIGHTINGS_BY_LOCATION_ID,
                        SightingMapper,
                        locationId);
        // set the location and list of characters for each sighting
        return associateCharactersAndLocationWithSighting(sightingList);
    }

    private static final String SQL_SELECT_SIGHTING_BY_CHARACTER_ID
            = "SELECT cs.CharacterId, Date, Description, LocationId "
            + "FROM Sighting s "
            + "INNER JOIN Character_Sighting cs ON s.SightingId = cs.SightingId "
            + "WHERE CharacterId = ?";

    // getSightingsByCharacterId (like getBooksByAuthorId, will use to build the 
    // Sightings object when GETTING it from the bridge table)
    public List<Sighting> getSightingsByCharacterId(int characterId) {
        // get the sightings of this character
        List<Sighting> sightingList
                = jdbcTemplate.query(SQL_SELECT_SIGHTING_BY_CHARACTER_ID,
                        SightingMapper,
                        characterId);
        // set the location and list of characters for each sighting
        return associateCharactersAndLocationWithSighting(sightingList);
    }

    private static final String SQL_SELECT_CHARACTERS_BY_SIGHTING_ID
            = "SELECT cs.SightingId, c.CharacterId, c.Name, c.Alias, c.Alignment "
            + "FROM Character_Sighting cs "
            + "INNER JOIN `Character` c ON c.CharacterId = cs.CharacterId "
            + "WHERE SightingID = ?";

    // findCharactersForSighting (like findAuthorsForBook, will use when ADDING to 
    // the bridge table)
    private List<Characters> findCharactersForSighting(Sighting sighting) {
        // first get a list of Characters associated with the Sighting
        final List<Characters> charList = jdbcTemplate.query(SQL_SELECT_CHARACTERS_BY_SIGHTING_ID,
                CharacterMapper,
                sighting.getSightingId());
        // before returning the list of Characters, I need to get the associated
        // Powers and set them on the characters object for EACH Character
        for (Characters currentCharacter : charList) {
            currentCharacter.setPowers(findPowersForCharacter(currentCharacter));
        }
        // then, return the list of Characters WITH their powers
        return charList;
    }

    @Override
    public List<Characters> findCharactersBySightingId(Sighting sighting) {
        return findCharactersForSighting(sighting);
    }
        
    private static final String SQL_SELECT_LOCATION_BY_SIGHTING_ID
            = "SELECT s.SightingId, l.LocationId, l.Name, l.Description, l.AddressStreet, "
            + "l.AddressCity, l.AddressState, l.AddressZip, l.MapLatitude, l.MapLongitude "
            + "FROM Location l "
            + "INNER JOIN Sighting s ON l.LocationId = s.LocationId "
            + "WHERE s.SightingId = ?";

    // findLocationForSighting (like findPublisherForBook) retrieves the Location
    // info for the given Sighting by joining the Loc and Sighting tables
    private Location findLocationForSighting(Sighting sighting) {
        return jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_SIGHTING_ID,
                LocationMapper,
                sighting.getSightingId());
    }
//  NEED TO BE ABLE to use this method on the Edit Locations page to get the Locations 
//  back when all I have from the Request is the sightingID
    
    @Override
    public Location findLocationBySightingId(Sighting sighting) {
        return findLocationForSighting(sighting);
    }
    
    private static final String SQL_INSERT_CHARACTERS_SIGHTINGS
            = "INSERT INTO Character_Sighting (CharacterId, SightingId) "
            + "VALUES (?, ?)";

    // insertSightingsCharacter (like insertBooksAuthors, will insert a row in 
    // the CharacterSighting bridge for each Character)
    private void insertSightingsCharacter(Sighting sighting) {
        final int sightingId = sighting.getSightingId();
        final List<Characters> characters = sighting.getCharacters();

        // if list of Characters is null, don't attempt to insert them or will
        // result in a null pointer exception
        if (characters != null) {
            // update the Character_Sighting bridge table with an entry for
            // each character of this sighting
            for (Characters currentCharacter : characters) {
                jdbcTemplate.update(SQL_INSERT_CHARACTERS_SIGHTINGS,
                        currentCharacter.getCharacterId(),
                        sightingId);
            }

        }

    }

    private static final String SQL_DELETE_CHARACTERS_SIGHTINGS
            = "DELETE FROM Character_Sighting WHERE SightingId = ?";
    // NOTE, there is no corresponding method for this prepared statement;
    // like deleteBooksAuthors, this SQL is used in the UPDATE Sightings and
    // DELETE Sightings methods to delete a row in the bridge for each Sighting)

    //**************************************************************************
    // adding PREPARED STATEMENTS and HELPER METHODS for Character_Powers here
    // allows us to build a full Character Object in this dao and will eliminate
    // the need to build it via multiple DAO calls from the ServiceLayer
    //**************************************************************************
    private static final String SQL_GET_ALL_POWERS_BY_CHARACTER_ID
            = "SELECT p.PowerId, p.Name FROM `Character` c "
            + "INNER JOIN Character_Power cp ON cp.CharacterId = c.CharacterId "
            + "INNER JOIN Power p ON p.PowerId = cp.PowerId "
            + "WHERE c.CharacterId = ?";

    private List<Power> findPowersForCharacter(Characters character) {
        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_CHARACTER_ID,
                PowerMapper, character.getCharacterId());
    }

    private static final String SQL_SELECT_ALL_CHARACTERS
            = "SELECT * FROM `Character`";

    public List<Characters> getAllCharacters() {

        List<Characters> charList = jdbcTemplate.query(SQL_SELECT_ALL_CHARACTERS, CharacterMapper);
        return associatePowersWithCharacter(charList);
    }

    private List<Characters> associatePowersWithCharacter(List<Characters> charList) {
        for (Characters currentCharacter : charList) {
            currentCharacter.setPowers(findPowersForCharacter(currentCharacter));
        }
        return charList;
    }

    // associateCharactersAndLocationWithSighting (like associatePublisherAndAuthorsWithBooks,
    // will use with helper methods when adding AND getting an Organization object from the bridge
    private List<Sighting> associateCharactersAndLocationWithSighting(List<Sighting> sightingList) {
        // set the complete list of characters for each Sighting
        for (Sighting currentSighting : sightingList) {
            currentSighting.setCharacters(findCharactersForSighting(currentSighting));
            // set the Location to current Sighting
            currentSighting.setLocation(currentSighting.getLocation());
        }
        return sightingList;
    }

    private static final String SQL_SELECT_RECENT_SIGHTINGS
            = "SELECT * FROM Sighting ORDER BY SightingID DESC LIMIT 10";
    
    public List<Sighting> getRecentSightings() {
        List<Sighting> sightingList = jdbcTemplate.query(SQL_SELECT_RECENT_SIGHTINGS, 
                SightingMapper);
        return sightingList;
    }
    
}
