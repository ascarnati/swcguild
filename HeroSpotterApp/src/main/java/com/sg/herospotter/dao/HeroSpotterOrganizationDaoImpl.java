/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.mappers.CharacterMapper;
import com.sg.herospotter.mappers.LocationMapper;
import com.sg.herospotter.mappers.OrganizationMapper;
import com.sg.herospotter.mappers.PowerMapper;
import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Organization;
import com.sg.herospotter.model.Power;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class HeroSpotterOrganizationDaoImpl implements HeroSpotterOrganizationDao {

    private JdbcTemplate jdbcTemplate;
    private PowerMapper PowerMapper = new PowerMapper();
    private CharacterMapper CharacterMapper = new CharacterMapper();
    private LocationMapper LocationMapper = new LocationMapper();
    private OrganizationMapper OrganizationMapper = new OrganizationMapper();

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //**************************************************************************
    // add basic Organization-specific Prepared Statements AND METHODS
    //**************************************************************************
    private static final String SQL_INSERT_ORGANIZATION
            = "INSERT INTO Organization "
            + "(Name, Description, Alignment, ContactInfo) "
            + "VALUES (?, ?, ?, ?)";

    @Override
    public void addOrganization(Organization organization) {
        // insert into Org table and get newly generated Org_id
        jdbcTemplate.update(SQL_INSERT_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getAlignment(),
                organization.getContactInfo());

        int organizationId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);

        organization.setOrganizationId(organizationId);
        // now update the Organization_Location table
        insertOrganizationsLocations(organization);
        // and update the Character_Organization table
        insertOrganizationsCharacters(organization);
    }

    private static final String SQL_DELETE_ORGANIZATION
            = "DELETE FROM Organization WHERE OrganizationId = ?";

    @Override
    public void deleteOrganization(int organizationId) {
        // delete Organization_Location relationship for this Org
        jdbcTemplate.update(SQL_DELETE_ORGANIZATIONS_LOCATIONS, organizationId);
        // delete Organization_Character relationship for this Org
        jdbcTemplate.update(SQL_DELETE_ORGANIZATIONS_CHARACTERS, organizationId);
        // now delete the Organization
        jdbcTemplate.update(SQL_DELETE_ORGANIZATION, organizationId);
    }

    private static final String SQL_UPDATE_ORGANIZATION
            = "UPDATE Organization "
            + "SET Name = ?, Description = ?, Alignment = ?, ContactInfo = ?, LocationId = ? "
            + "WHERE OrganizationId = ?";

    @Override
    public void updateOrganization(Organization organization) {
        jdbcTemplate.update(SQL_UPDATE_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getAlignment(),
                organization.getContactInfo());
    }

    private static final String SQL_SELECT_ORGANIZATION
            = "SELECT * FROM Organization WHERE OrganizationId = ?";

    @Override
    public Organization getOrganizationById(int organizationId) {
        try {
            Organization organization = jdbcTemplate.queryForObject(SQL_SELECT_ORGANIZATION,
                    OrganizationMapper,
                    organizationId);
            // before returning the Organization object, you must first get
            // its associated locations and characters
            organization.setLocations(findLocationsForOrganization(organization));
            organization.setCharacters(findCharactersForOrganization(organization));

            return organization;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_SELECT_ALL_ORGANIZATIONS
            = "SELECT * FROM Organization";

    @Override
    public List<Organization> getAllOrganizations() {
        List<Organization> orgList = jdbcTemplate.query(SQL_SELECT_ALL_ORGANIZATIONS,
                OrganizationMapper);
        return associateCharactersAndLocationsWithOrganization(orgList);
    }

    //**************************************************************************
    // An Org consists of 1-to-Many Characters and 1-to-Many Locations so adding
    // related Prepared Statements AND METHODS here; 
    //**************************************************************************
    private static final String SQL_SELECT_CHARACTERS_BY_ORGANIZATION_ID
            = "SELECT co.OrganizationId, co.CharacterId, c.Name, c.Alias, c.Alignment "
            + "FROM Character_Organization co "
            + "INNER JOIN `Character` c ON co.CharacterId = c.CharacterId "
            + "WHERE co.OrganizationId = ?;";

    // findCharactersForOrganization (like findAuthorsForBook, will use when ADDING to the bridge table)
    private List<Characters> findCharactersForOrganization(Organization organization) {
        // first get a list of Characters associated with the Org
        final List<Characters> charList = jdbcTemplate.query(SQL_SELECT_CHARACTERS_BY_ORGANIZATION_ID,
                CharacterMapper, organization.getOrganizationId());

        // before returning the list of Characters, I need to get 
        // the associated Powers and set them on the Characters object for EACH CHARACTER
        for (Characters currentCharacter : charList) {
            currentCharacter.setPowers(findPowersForCharacter(currentCharacter));
        }
        // then, return the list of Characters WITH their powers
        return charList;
    }

    private static final String SQL_SELECT_LOCATIONS_BY_ORGANIZATION_ID
            = "SELECT ol.OrganizationId, ol.LocationId, l.Name, l.Description, "
            + "l.AddressStreet, l.AddressCity, l.AddressState, l.AddressZip, "
            + "l.MapLatitude, l.MapLongitude "
            + "FROM Organization_Location ol "
            + "INNER JOIN Location l ON ol.LocationId = l.LocationId "
            + "WHERE ol.OrganizationId = ?";

    // findLocationsForOrganization (like findAuthorsForBook, will use when ADDING to the bridge table)
    private List<Location> findLocationsForOrganization(Organization organization) {
        return jdbcTemplate.query(SQL_SELECT_LOCATIONS_BY_ORGANIZATION_ID,
                LocationMapper, organization.getOrganizationId());
    }

    private static final String SQL_SELECT_ORGANIZATION_BY_LOCATION_ID
            = "SELECT ol.LocationId, o.OrganizationId, o.Name, o.Description, "
            + "o.Alignment, o.ContactInfo "
            + "FROM Organization_Location ol "
            + "INNER JOIN Organization o ON ol.OrganizationId = o.OrganizationId "
            + "WHERE ol.LocationId = ?;";

    // getOrganizationByLocationId (like getBooksByAuthorId, will use to build the Organization object when GETTING it from the bridge table)
    public List<Organization> getOrganizationByLocationId(int locationId) {
        // get the Organizations by this location
        List<Organization> orgList
                = jdbcTemplate.query(SQL_SELECT_ORGANIZATION_BY_LOCATION_ID,
                        OrganizationMapper,
                        locationId);
        // set the Locations and Characters for each Organization
        return associateCharactersAndLocationsWithOrganization(orgList);
    }

    private static final String SQL_INSERT_ORGANIZATIONS_LOCATIONS
            = "INSERT INTO Organization_Location (OrganizationId, LocationId) "
            + "VALUES (?, ?)";

    // insertOrganizationsLocations (like insertBooksAuthors, will insert a row in the bridge for each Organization)
    private void insertOrganizationsLocations(Organization organization) {
        final int organizationId = organization.getOrganizationId();
        final List<Location> locations = organization.getLocations();

        // if list of locations is null, don't attempt to insert them or will
        // result in a null pointer exception
        if (locations != null) {
            // update the Organizations_Locations bridge table with an entry for 
            // each location of this Org
            for (Location currentLocation : locations) {
                jdbcTemplate.update(SQL_INSERT_ORGANIZATIONS_LOCATIONS,
                        organizationId,
                        currentLocation.getLocationId());
            }

        }

    }

    private static final String SQL_DELETE_ORGANIZATIONS_LOCATIONS
            = "DELETE FROM Organization_Location WHERE OrganizationId = ?";
    // NOTE, there is no corresponding method for this prepared statement;
    // like deleteBooksAuthors, this SQL is used in the UPDATE Org and 
    // DELETE ORG methods to delete a row in the bridge for each Organization)

    private static final String SQL_SELECT_ORGANIZATION_BY_CHARACTER_ID
            = "SELECT co.CharacterId, co.OrganizationId, o.Name, o.Description, "
            + "o.Alignment, o.ContactInfo "
            + "FROM Character_Organization co "
            + "INNER JOIN Organization o ON co.OrganizationId = o.OrganizationId "
            + "WHERE co.CharacterId = ?";

    // getOrganizationByCharacterId (like getBooksByAuthorId, will use to build the Organization object when GETTING it from the bridge table)
    public List<Organization> getOrganizationsByCharacterId(int characterId) {
        // get the Organizations by this character
        List<Organization> orgList
                = jdbcTemplate.query(SQL_SELECT_ORGANIZATION_BY_CHARACTER_ID,
                        OrganizationMapper,
                        characterId);
        // set the Locations and Characters for each Organization
        return associateCharactersAndLocationsWithOrganization(orgList);
    }

    private static final String SQL_INSERT_ORGANIZATIONS_CHARACTERS
            = "INSERT INTO Character_Organization (CharacterId, OrganizationId) "
            + "VALUES (?, ?)";

    // insertOrganizationsCharacters (like insertBooksAuthors, will insert a row 
    // in the bridge for each Organization)
    private void insertOrganizationsCharacters(Organization organization) {
        final int organizationId = organization.getOrganizationId();
        final List<Characters> characters = organization.getCharacters();

        // if list of Characters is null, don't attempt to insert them or will
        // result in a null pointer exception
        if (characters != null) {
            // update the Character_Organization bridge table with an entry for
            // each character of this Org
            for (Characters currentCharacter : characters) {
                jdbcTemplate.update(SQL_INSERT_ORGANIZATIONS_CHARACTERS,
                        currentCharacter.getCharacterId(),
                        organizationId);
            }

        }
    }
    private static final String SQL_DELETE_ORGANIZATIONS_CHARACTERS
            = "DELETE FROM Character_Organization WHERE OrganizationId = ?";
    // NOTE, there is no corresponding method for this prepared statement; 
    // like deleteBooksAuthors, this SQL is used in the UPDATE Org and 
    // DELETE org methods to delete a row in the bridge for each Organization)

    //**************************************************************************
    // adding PREPARED STATEMENTS and HELPER METHODS for Character_Powers here
    // allows us to build a full Character Object in this dao and will eliminate
    // the need to build it via multiple DAO calls from the ServiceLayer
    //**************************************************************************
    private static final String SQL_GET_ALL_POWERS_BY_CHARACTER_ID
            = "SELECT p.PowerId, p.Name FROM `Character` c "
            + "INNER JOIN Character_Power cp ON cp.CharacterId = c.CharacterId "
            + "INNER JOIN Power p ON p.PowerId = cp.PowerId "
            + "WHERE c.CharacterId = ?";

    private List<Power> findPowersForCharacter(Characters character) {
        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_CHARACTER_ID,
                PowerMapper, character.getCharacterId());
    }

    private static final String SQL_SELECT_ALL_CHARACTERS
            = "SELECT * FROM `Character`";

    public List<Characters> getAllCharacters() {

        List<Characters> charList = jdbcTemplate.query(SQL_SELECT_ALL_CHARACTERS, CharacterMapper);
        return associatePowersWithCharacter(charList);
    }

    private List<Characters> associatePowersWithCharacter(List<Characters> charList) {
        for (Characters currentCharacter : charList) {
            currentCharacter.setPowers(findPowersForCharacter(currentCharacter));
        }
        return charList;
    }

    // associateCharactersAndLocationsWithOrganization (like associatePublisherAndAuthorsWithBooks, 
    // will use with helper methods when adding AND getting an Organization object from the bridge
    private List<Organization> associateCharactersAndLocationsWithOrganization(List<Organization> orgList) {
        // set the complete list of locationIds for each Org
        for (Organization currentOrg : orgList) {
            // add locations to current Org
            currentOrg.setLocations(findLocationsForOrganization(currentOrg));
            // add characters to current Org
            currentOrg.setCharacters(findCharactersForOrganization(currentOrg));
        }
        return orgList;
    }

}
