/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.mappers.LocationMapper;
import com.sg.herospotter.model.Location;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HeroSpotterLocationDaoImpl implements HeroSpotterLocationDao {

    private JdbcTemplate jdbcTemplate;
    
    // instantiate Mapper Class for the Location Table 
    private LocationMapper LocationMapper = new LocationMapper();
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    //**************************************************************************
    // add Location-related Prepared Statements
    //**************************************************************************
    
    private static final String SQL_INSERT_LOCATION
            = "INSERT INTO Location "
            + "(Name, Description, AddressStreet, AddressCity, AddressState, "
            + "AddressZip, MapLatitude, MapLongitude) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_DELETE_LOCATION
            = "DELETE FROM Location WHERE LocationId = ?";
    
    private static final String SQL_UPDATE_LOCATION
            = "UPDATE Location SET "
            + "Name = ?, Description = ?, AddressStreet = ?, AddressCity = ?, "
            + "AddressState = ?, AddressZip = ?, MapLatitude = ?, MapLongitude = ? "
            + "WHERE LocationId = ?";
    
    private static final String SQL_SELECT_LOCATION
            = "SELECT * FROM Location WHERE LocationId = ?";
    
    private static final String SQL_GET_ALL_LOCATIONS
            = "SELECT * FROM Location";
    
    
    //**************************************************************************
    // Location methods
    //**************************************************************************    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addLocation(Location location) {
        // insert a new row into Location table, then get newly generated id
        jdbcTemplate.update(SQL_INSERT_LOCATION,
                location.getName(),
                location.getDescription(),
                location.getAddressStreet(),
                location.getAddressCity(),
                location.getAddressState(),
                location.getAddressZip(),
                location.getMapLatitude(),
                location.getMapLongitude());
        
                int locationId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
                
        location.setLocationId(locationId);        
    }

    @Override
    public void deleteLocation(int locationId) {
        jdbcTemplate.update(SQL_DELETE_LOCATION, locationId);
    }

    @Override
    public void updateLocation(Location location) {
        jdbcTemplate.update(SQL_UPDATE_LOCATION,
                location.getName(),
                location.getDescription(),
                location.getAddressStreet(),
                location.getAddressCity(),
                location.getAddressState(),
                location.getAddressZip(),
                location.getMapLatitude(),
                location.getMapLongitude(),
                location.getLocationId());
    }

    @Override
    public Location getLocationById(int locationId) {
        try {
            Location location = jdbcTemplate.queryForObject(SQL_SELECT_LOCATION,
                    LocationMapper,
                    locationId);
            return location;
            
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Location> getAllLocations() {

        List<Location> locationList = jdbcTemplate.query(SQL_GET_ALL_LOCATIONS, 
                LocationMapper);
        return locationList;
    }
    
}
