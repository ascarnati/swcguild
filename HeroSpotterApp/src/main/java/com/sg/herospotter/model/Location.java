/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.model;

import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class Location {
    
    private int locationId;
    private String name;
    private String description;
    private String addressStreet;
    private String addressCity;
    private String addressState;
    private String addressZip;
    private double mapLatitude;
    private double mapLongitude;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public double getMapLatitude() {
        return mapLatitude;
    }

    public void setMapLatitude(double mapLatitude) {
        this.mapLatitude = mapLatitude;
    }

    public double getMapLongitude() {
        return mapLongitude;
    }

    public void setMapLongitude(double mapLongitude) {
        this.mapLongitude = mapLongitude;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.locationId;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.description);
        hash = 83 * hash + Objects.hashCode(this.addressStreet);
        hash = 83 * hash + Objects.hashCode(this.addressCity);
        hash = 83 * hash + Objects.hashCode(this.addressState);
        hash = 83 * hash + Objects.hashCode(this.addressZip);
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.mapLatitude) ^ (Double.doubleToLongBits(this.mapLatitude) >>> 32));
        hash = 83 * hash + (int) (Double.doubleToLongBits(this.mapLongitude) ^ (Double.doubleToLongBits(this.mapLongitude) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Location other = (Location) obj;
        if (this.locationId != other.locationId) {
            return false;
        }
        if (Double.doubleToLongBits(this.mapLatitude) != Double.doubleToLongBits(other.mapLatitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.mapLongitude) != Double.doubleToLongBits(other.mapLongitude)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.addressStreet, other.addressStreet)) {
            return false;
        }
        if (!Objects.equals(this.addressCity, other.addressCity)) {
            return false;
        }
        if (!Objects.equals(this.addressState, other.addressState)) {
            return false;
        }
        if (!Objects.equals(this.addressZip, other.addressZip)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Location{" + "locationId=" + locationId + ", name=" + name + ", description=" + description + ", addressStreet=" + addressStreet + ", addressCity=" + addressCity + ", addressState=" + addressState + ", addressZip=" + addressZip + ", mapLatitude=" + mapLatitude + ", mapLongitude=" + mapLongitude + '}';
    }
    
    
    
}
