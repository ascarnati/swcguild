/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.mappers;

import com.sg.herospotter.model.Characters;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class CharacterMapper implements RowMapper<Characters> {

    @Override
    public Characters mapRow(ResultSet rs, int i) throws SQLException {
        Characters newChar = new Characters();
        newChar.setName(rs.getString("name"));
        newChar.setAlias(rs.getString("alias"));
        newChar.setAlignment(rs.getString("alignment"));
        newChar.setCharacterId(rs.getInt("characterId"));
        return newChar;
    }
}
