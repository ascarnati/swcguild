/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.mappers;

import com.sg.herospotter.model.Sighting;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class SightingMapper implements RowMapper<Sighting> {
    
    @Override
    public Sighting mapRow(ResultSet rs, int i) throws SQLException {
        Sighting newSighting = new Sighting();
        newSighting.setDate(rs.getTimestamp("date").
                toLocalDateTime().toLocalDate());
        newSighting.setDescription(rs.getString("description"));
        newSighting.setSightingId(rs.getInt("sightingId"));
                
        return newSighting;
    }
}
