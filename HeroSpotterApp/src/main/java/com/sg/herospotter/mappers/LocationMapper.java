/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.mappers;

import com.sg.herospotter.model.Location;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class LocationMapper implements RowMapper<Location> {
    
    @Override
    public Location mapRow(ResultSet rs, int i) throws SQLException {
        Location newLocation = new Location();
        newLocation.setName(rs.getString("name"));
        newLocation.setDescription(rs.getString("description"));
        newLocation.setAddressStreet(rs.getString("addressStreet"));
        newLocation.setAddressCity(rs.getString("addressCity"));
        newLocation.setAddressState(rs.getString("addressState"));
        newLocation.setAddressZip(rs.getString("addressZip"));
        newLocation.setMapLatitude(rs.getDouble("mapLatitude"));
        newLocation.setMapLongitude(rs.getDouble("mapLongitude"));
        newLocation.setLocationId(rs.getInt("locationId"));
        
        return newLocation;
    }
}
