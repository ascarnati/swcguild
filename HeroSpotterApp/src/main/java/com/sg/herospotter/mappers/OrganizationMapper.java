/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.mappers;

import com.sg.herospotter.model.Organization;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class OrganizationMapper implements RowMapper<Organization> {

    @Override
    public Organization mapRow(ResultSet rs, int i) throws SQLException {
        Organization newOrg = new Organization();
        newOrg.setName(rs.getString("name"));
        newOrg.setDescription(rs.getString("description"));
        newOrg.setAlignment(rs.getString("alignment"));
        newOrg.setContactInfo(rs.getString("contactInfo"));
        newOrg.setOrganizationId(rs.getInt("organizationId"));
        return newOrg;
    }
}
