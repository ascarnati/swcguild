/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.service;

import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Organization;
import com.sg.herospotter.model.Power;
import com.sg.herospotter.model.Sighting;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface HeroSpotterServiceLayer {
    
    //**************************************************************************
    // crud Character DTO
    //**************************************************************************
    public void addCharacter(Characters character);
    
    public void deleteCharacter(int characterId);
    
    public void updateCharacter(Characters character);
    
    public Characters getCharacterById(int characterId);
    
    public List<Characters> getAllCharacters();
    
    //**************************************************************************
    // crud Power DTO
    //**************************************************************************
    public void addPower(Power power);
    
    public void deletePower(int powerId);
    
    public void updatePower(Power power);
    
    public Power getPowerById(int powerId);
    
    public List<Power> getAllPowers();
    
    //**************************************************************************
    // crud Location DTO
    //**************************************************************************
    public void addLocation(Location location);
    
    public void deleteLocation(int locationId);
    
    public void updateLocation(Location location);
    
    public Location getLocationById(int locationId);
    
    public List<Location> getAllLocations();
    
    //**************************************************************************
    // crud Organization DAO    
    //**************************************************************************
    public void addOrganization(Organization organization);
    
    public void deleteOrganization(int organizationId);
    
    public void updateOrganization(Organization organization);
    
    public Organization getOrganizationById(int organizationId);
    
    public List<Organization> getAllOrganizations();
    
    
    //**************************************************************************
    // crud Sighting DTO
    //**************************************************************************
    public void addSighting(Sighting sighting);
    
    public void deleteSighting(int sightingId);
    
    public void updateSighting(Sighting sighting);
    
    public Sighting getSightingById(int sightingId);
    
    public List<Sighting> getAllSightings();

    public List<Characters> findCharactersBySightingId(Sighting sighting);
    
    public Location findLocationBySightingId(Sighting sighting);    
    
    public List<Sighting> getRecentSightings();

}
