/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.service;

import com.sg.herospotter.dao.HeroSpotterCharacterPowerDao;
import com.sg.herospotter.dao.HeroSpotterLocationDao;
import com.sg.herospotter.dao.HeroSpotterOrganizationDao;
import com.sg.herospotter.dao.HeroSpotterSightingDao;
import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Organization;
import com.sg.herospotter.model.Power;
import com.sg.herospotter.model.Sighting;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author apprentice
 */
public class HeroSpotterServiceLayerImpl implements HeroSpotterServiceLayer {

    private HeroSpotterCharacterPowerDao charPowerDao;
    private HeroSpotterLocationDao locationDao;
    private HeroSpotterOrganizationDao orgDao;
    private HeroSpotterSightingDao sightingDao;
    
    @Inject
    public HeroSpotterServiceLayerImpl(HeroSpotterCharacterPowerDao charPowerDao,
            HeroSpotterLocationDao locationDao,
            HeroSpotterOrganizationDao orgDao,
            HeroSpotterSightingDao sightingDao) {
        this.charPowerDao = charPowerDao;
        this.locationDao = locationDao;
        this.orgDao = orgDao;
        this.sightingDao = sightingDao;
    }
    
    //**************************************************************************
    // crud Character DTO
    //**************************************************************************
    @Override
    public void addCharacter(Characters character) {
        charPowerDao.addCharacter(character);
    }

    @Override
    public void deleteCharacter(int characterId) {
        charPowerDao.deleteCharacter(characterId);
    }

    @Override
    public void updateCharacter(Characters character) {
        charPowerDao.updateCharacter(character);
    }

    @Override
    public Characters getCharacterById(int characterId) {
        return charPowerDao.getCharacterById(characterId);
    }

    @Override
    public List<Characters> getAllCharacters() {
        return charPowerDao.getAllCharacters();
    }

    //**************************************************************************
    // crud Power DTO
    //**************************************************************************
    @Override
    public void addPower(Power power) {
        charPowerDao.addPower(power);
    }

    @Override
    public void deletePower(int powerId) {
        charPowerDao.deletePower(powerId);
    }

    @Override
    public void updatePower(Power power) {
        charPowerDao.updatePower(power);
    }

    @Override
    public Power getPowerById(int powerId) {
        return charPowerDao.getPowerById(powerId);
    }

    @Override
    public List<Power> getAllPowers() {
        return charPowerDao.getAllPowers();
    }

    //**************************************************************************
    // crud Location DTO
    //**************************************************************************    
    
    @Override
    public void addLocation(Location location) {
        locationDao.addLocation(location);
    }

    @Override
    public void deleteLocation(int locationId) {
        locationDao.deleteLocation(locationId);
    }

    @Override
    public void updateLocation(Location location) {
        locationDao.updateLocation(location);
    }

    @Override
    public Location getLocationById(int locationId) {
        return locationDao.getLocationById(locationId);
    }

    @Override
    public List<Location> getAllLocations() {
        return locationDao.getAllLocations();
    }

    //**************************************************************************
    // crud Organization DTO
    //**************************************************************************
    
    @Override
    public void addOrganization(Organization organization) {
        orgDao.addOrganization(organization);
    }

    @Override
    public void deleteOrganization(int organizationId) {
        orgDao.deleteOrganization(organizationId);
    }

    @Override
    public void updateOrganization(Organization organization) {
        orgDao.updateOrganization(organization);
    }

    @Override
    public Organization getOrganizationById(int organizationId) {
        return orgDao.getOrganizationById(organizationId);
    }

    @Override
    public List<Organization> getAllOrganizations() {
        return orgDao.getAllOrganizations();
    }
    

    //**************************************************************************
    // crud Sighting DTO
    //**************************************************************************
    
    @Override
    public void addSighting(Sighting sighting) {
        sightingDao.addSighting(sighting);
    }

    @Override
    public void deleteSighting(int sightingId) {
        sightingDao.deleteSighting(sightingId);
    }

    @Override
    public void updateSighting(Sighting sighting) {
        sightingDao.updateSighting(sighting);
    }

    @Override
    public Sighting getSightingById(int sightingId) {
        return sightingDao.getSightingById(sightingId);
    }

    @Override
    public List<Sighting> getAllSightings() {
        return sightingDao.getAllSightings();
    }
    
    @Override
    public List<Characters> findCharactersBySightingId(Sighting sighting) {
        return sightingDao.findCharactersBySightingId(sighting);
    }
    
    @Override
    public Location findLocationBySightingId(Sighting sighting) {
        return sightingDao.findLocationBySightingId(sighting);
    }
    
    @Override
    public List<Sighting> getRecentSightings() {
        return sightingDao.getRecentSightings();
    };

}
