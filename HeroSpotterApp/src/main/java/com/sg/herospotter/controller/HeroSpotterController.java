/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.controller;

import com.sg.herospotter.dao.HeroSpotterCharacterPowerDao;
import com.sg.herospotter.dao.HeroSpotterLocationDao;
import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Organization;
import com.sg.herospotter.model.Power;
import com.sg.herospotter.model.Sighting;
import com.sg.herospotter.service.HeroSpotterServiceLayer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HeroSpotterController {

    private HeroSpotterServiceLayer service;

    @Inject
    public HeroSpotterController(HeroSpotterServiceLayer service) {
        this.service = service;
    }

    //**************************************************************************
    //          Index.jsp 
    //**************************************************************************
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHomePage(Model model) {
        // Get the list of sightings from the Dao)
//        List<Sighting> sightingList = service.getAllSightings();
        List<Sighting> sightingList = service.getRecentSightings();
        // put the list of Sightings on the Model
        model.addAttribute(sightingList);

        return "index";
    }

    //**************************************************************************
    //          Characters.jsp controls
    //**************************************************************************
    @RequestMapping(value = "/displayCharactersPage", method = RequestMethod.GET)
    public String displayCharactersPage(Model model) {
        // Get all the Characters and Powers from the Dao)
        List<Characters> characterList = service.getAllCharacters();
        List<Power> powerList = service.getAllPowers();

        // Put the Lists of Characters and Powers on the Model
        model.addAttribute("characterList", characterList);
        model.addAttribute("powerList", powerList);

        // Return the name of our View
        return "characters";
    }

    @RequestMapping(value = "/createCharacter", method = RequestMethod.POST)
    public String createCharacter(HttpServletRequest request) {

        Characters character = new Characters();
        character.setName(request.getParameter("characterName"));
        character.setAlias(request.getParameter("alias"));
        character.setAlignment(request.getParameter("alignment"));

        // Get the string of values selected from the view
        String[] characterPowerParameterList = request.getParameterValues("characterPower");

        List<Power> powerList = new ArrayList<>();

        for (String currentPower : characterPowerParameterList) {
            int powerId = Integer.parseInt(currentPower);
            Power power = service.getPowerById(powerId);

            powerList.add(power);
        }

        character.setPowers(powerList);
        service.addCharacter(character);

        return "redirect:displayCharactersPage";
    }

    @RequestMapping(value = "/displayCharacterDetails", method = RequestMethod.GET)
    public String displayCharacterDetails(HttpServletRequest request, Model model) {
        String characterIdParameter = request.getParameter("characterId");
        int characterId = Integer.parseInt(characterIdParameter);

        Characters character = service.getCharacterById(characterId);

        model.addAttribute("character", character);

        return "characterDetails";
    }

    @RequestMapping(value = "/deleteCharacter", method = RequestMethod.GET)
    public String deleteCharacter(HttpServletRequest request) {
        String characterIdParameter = request.getParameter("characterId");
        int characterId = Integer.parseInt(characterIdParameter);
        service.deleteCharacter(characterId);
        return "redirect:displayCharactersPage";
    }
    
    @RequestMapping(value = "/displayEditCharacterForm", method = RequestMethod.GET)
    public String displayEditCharacterForm(HttpServletRequest request, Model model) {
        String characterIdParameter = request.getParameter("characterId");
        int characterId = Integer.parseInt(characterIdParameter);
        Characters character = service.getCharacterById(characterId);

        // get the powers for the character from the serviceLayer 
        
        // and put them in a list for the model
        
        return "editSightingForm";
    }

    //**************************************************************************
    //          Locations.jsp controls
    //**************************************************************************
    @RequestMapping(value = "/displayLocationsPage", method = RequestMethod.GET)
    public String displayLocationsPage(Model model) {
        // Get all the Locations from the Dao
        List<Location> locationList = service.getAllLocations();

        // Put the List of Locations on the Model
        model.addAttribute("locationList", locationList);

        return "locations";
    }

    @RequestMapping(value = "/createLocation", method = RequestMethod.POST)
    public String createLocation(HttpServletRequest request) {

        Location location = new Location();
        location.setName(request.getParameter("locationName"));
        location.setDescription(request.getParameter("locationDescription"));
        location.setAddressStreet(request.getParameter("locationAddressStreet"));
        location.setAddressCity(request.getParameter("locationAddressCity"));
        location.setAddressZip(request.getParameter("locationAddressZip"));

        if (request.getParameter("locationMapLatitude").isEmpty()) {
            double mapLatitude = 0.0;
            location.setMapLatitude(mapLatitude);

        } else {
            String mapLatitudeParameter = request.getParameter("locationMapLatitude");
            double mapLatitude = Double.parseDouble(mapLatitudeParameter);
            location.setMapLatitude(mapLatitude);
        }
        if (request.getParameter("locationMapLongitude").isEmpty()) {
            double mapLongitude = 0.0;
            location.setMapLongitude(mapLongitude);

        } else {
            String mapLongitudeParameter = request.getParameter("locationMapLongitude");
            double mapLongitude = Double.parseDouble(mapLongitudeParameter);
            location.setMapLongitude(mapLongitude);
        }

        service.addLocation(location);
        return "redirect:displayLocationsPage";
    }

    @RequestMapping(value = "/displayLocationDetails", method = RequestMethod.GET)
    public String displayLocationDetails(HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("locationId");
        int locationId = Integer.parseInt(locationIdParameter);

        Location location = service.getLocationById(locationId);

        model.addAttribute("location", location);

        return "locationDetails";
    }

    @RequestMapping(value = "/deleteLocation", method = RequestMethod.GET)
    public String deleteLocation(HttpServletRequest request) {
        String locationIdParameter = request.getParameter("locationId");
        int locationId = Integer.parseInt(locationIdParameter);
        service.deleteLocation(locationId);
        return "redirect:displayLocationsPage";
    }

    @RequestMapping(value = "/displayEditLocationForm", method = RequestMethod.GET)
    public String displayEditLocationForm(HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("locationId");
        int locationId = Integer.parseInt(locationIdParameter);
        Location location = service.getLocationById(locationId);
        model.addAttribute("location", location);
        return "editLocationForm";
    }

    @RequestMapping(value = "/editLocation", method = RequestMethod.POST)
    public String editLocation(@Valid @ModelAttribute("location") Location location, BindingResult result) {
        // @Valid indicates that the incoming Contact object must be validated
        // @ModelAttribute is the counterpart from the ModelAttribute on the editForm
        // 'BindingResult' object parameter allows us to check for validation errors;
        // If there are validation errors, we return the JSP that contains the Edit Contact form so that
        // its displayed again; validation ERRORS will show up on the form through the sf:error tags we
        // we added previously
        if (result.hasErrors()) {
            return "editLocationForm";
        }

        service.updateLocation(location);
        return "redirect:displayLocationsPage";
    }

    //**************************************************************************
    //          Organizations.jsp controls
    //**************************************************************************
    @RequestMapping(value = "/displayOrganizationsPage", method = RequestMethod.GET)
    public String displayOrganizationsPage(Model model) {
        // Get all the Orgs, Characters, and Locations from the Dao
        List<Organization> organizationList = service.getAllOrganizations();
        List<Characters> characterList = service.getAllCharacters();
        List<Location> locationList = service.getAllLocations();

        // Put the lists of Orgs, Chars, and Locations on the Model
        model.addAttribute("organizationList", organizationList);
        model.addAttribute("characterList", characterList);
        model.addAttribute("locationList", locationList);

        return "organizations";
    }

    @RequestMapping(value = "/createOrganization", method = RequestMethod.POST)
    public String createOrganization(HttpServletRequest request) {

        Organization organization = new Organization();
        organization.setName(request.getParameter("organizationName"));
        organization.setDescription(request.getParameter("organizationDescription"));
        organization.setAlignment(request.getParameter("organizationAlignment"));
        organization.setContactInfo(request.getParameter("organizationContact"));

        // Get the string of values selected from the view
        String[] organizationCharacterParameterList = request.getParameterValues("organizationCharacter");

        List<Characters> characterList = new ArrayList<>();

        for (String currentCharacter : organizationCharacterParameterList) {
            int characterId = Integer.parseInt(currentCharacter);
            Characters character = service.getCharacterById(characterId);

            characterList.add(character);
        }

        // Get the string values selected from the view
        String[] organizationLocationParameterList = request.getParameterValues("organizationLocation");

        List<Location> locationList = new ArrayList<>();

        for (String currentLocation : organizationLocationParameterList) {
            int locationId = Integer.parseInt(currentLocation);
            Location location = service.getLocationById(locationId);

            locationList.add(location);
        }

        organization.setCharacters(characterList);
        organization.setLocations(locationList);
        service.addOrganization(organization);
        return "redirect:displayOrganizationsPage";
    }

    @RequestMapping(value = "/displayOrganizationDetails", method = RequestMethod.GET)
    public String displayOrganizationDetails(HttpServletRequest request, Model model) {
        String organizationIdParameter = request.getParameter("organizationId");
        int organizationId = Integer.parseInt(organizationIdParameter);

        Organization organization = service.getOrganizationById(organizationId);

        model.addAttribute("organization", organization);

        return "organizationDetails";
    }

    @RequestMapping(value = "/deleteOrganization", method = RequestMethod.GET)
    public String deleteOrganization(HttpServletRequest request) {
        String organizationIdParameter = request.getParameter("organizationId");
        int organizationId = Integer.parseInt(organizationIdParameter);
        service.deleteOrganization(organizationId);
        return "redirect:displayOrganizationsPage";
    }

    //**************************************************************************
    //          Sightings.jsp controls
    //**************************************************************************
    @RequestMapping(value = "/displaySightingsPage", method = RequestMethod.GET)
    public String displaySightingsPage(Model model) {
        // Get all the Sightings from the Dao
        List<Sighting> sightingList = service.getAllSightings();

        List<Location> locationList = service.getAllLocations();

        List<Characters> characterList = service.getAllCharacters();

        // Put the list of Sightings on the Model
        model.addAttribute("sightingList", sightingList);
        model.addAttribute("locationList", locationList);
        model.addAttribute("characterList", characterList);

        return "sightings";
    }

    @RequestMapping(value = "/createSighting", method = RequestMethod.POST)
    public String createSighting(HttpServletRequest request) {

        Sighting sighting = new Sighting();

        String sightingDateParameter = request.getParameter("sightingDate");
        LocalDate sightingDate = LocalDate.parse(sightingDateParameter, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        sighting.setDate(sightingDate);

        sighting.setDescription(request.getParameter("sightingDescription"));

        String sightingLocationParameter = request.getParameter("sightingLocation");
        int locationId = Integer.parseInt(sightingLocationParameter);
        Location location = service.getLocationById(locationId);
        sighting.setLocation(location);

        // Get the string of values selected from the view
        String[] characterSightingParameterList = request.getParameterValues("sightingCharacter");

        List<Characters> characterList = new ArrayList<>();

        for (String currentCharacter : characterSightingParameterList) {
            int characterId = Integer.parseInt(currentCharacter);
            Characters character = service.getCharacterById(characterId);

            characterList.add(character);
        }

        // put the list on the Sighting object
        sighting.setCharacters(characterList);

        service.addSighting(sighting);

        return "redirect:displaySightingsPage";
    }

    @RequestMapping(value = "/displaySightingDetails", method = RequestMethod.GET)
    public String displaySightingDetails(HttpServletRequest request, Model model) {
        String sightingIdParameter = request.getParameter("sightingId");
        int sightingId = Integer.parseInt(sightingIdParameter);

        Sighting sighting = service.getSightingById(sightingId);

        model.addAttribute("sighting", sighting);

        return "sightingDetails";
    }

    @RequestMapping(value = "/deleteSighting", method = RequestMethod.GET)
    public String deleteSighting(HttpServletRequest request) {
        String sightingIdParameter = request.getParameter("sightingId");
        int sightingId = Integer.parseInt(sightingIdParameter);
        service.deleteSighting(sightingId);
        return "redirect:displaySightingsPage";
    }

    @RequestMapping(value = "/displayEditSightingForm", method = RequestMethod.GET)
    public String displayEditSightingForm(HttpServletRequest request, Model model) {
        String sightingIdParameter = request.getParameter("sightingId");
        int sightingId = Integer.parseInt(sightingIdParameter);
        Sighting sighting = service.getSightingById(sightingId);

        Location location = service.findLocationBySightingId(sighting);
        List<Location> allLocationList = service.getAllLocations();
        // Get the string of values selected from the view

        List<Characters> selectedCharacterList = service.findCharactersBySightingId(sighting);
        List<Characters> allCharacterList = service.getAllCharacters();

        model.addAttribute("sighting", sighting);
        model.addAttribute("location", location);
        model.addAttribute("allLocationList", allLocationList);
        model.addAttribute("selectedCharacterList", selectedCharacterList);
        model.addAttribute("allCharacterList", allCharacterList);
        return "editSightingForm";
    }

    @RequestMapping(value = "/editSighting", method = RequestMethod.POST)
    //public String editSighting(@Valid @ModelAttribute("sighting") Sighting sighting, 
    public String editSighting(HttpServletRequest request) {

//        if (result.hasErrors()) {
//            return "editSightingForm";
//        }

        String sightingIdParameter = request.getParameter("sightingId");
        int sightingId = Integer.parseInt(sightingIdParameter);
        Sighting sighting = service.getSightingById(sightingId);
        
        String sightingDateParameter = request.getParameter("sightingDate");
        LocalDate sightingDate = LocalDate.parse(sightingDateParameter, DateTimeFormatter.ISO_DATE);
        sighting.setDate(sightingDate);
                
        sighting.setDescription(request.getParameter("sightingDescription"));

        String sightingLocationParameter = request.getParameter("sightingLocation");
        int locationId = Integer.parseInt(sightingLocationParameter);
        Location location = service.getLocationById(locationId);
        sighting.setLocation(location);

        // Get the string of values selected from the view
        String[] characterSightingParameterList = request.getParameterValues("sightingCharacter");

        List<Characters> characterList = new ArrayList<>();

        for (String currentCharacter : characterSightingParameterList) {
            int characterId = Integer.parseInt(currentCharacter);
            Characters character = service.getCharacterById(characterId);

            characterList.add(character);
        }

        // put the list on the Sighting object
        sighting.setCharacters(characterList);

//        service.addSighting(sighting);



        service.updateSighting(sighting);
        return "redirect:displaySightingsPage";
    }

}
