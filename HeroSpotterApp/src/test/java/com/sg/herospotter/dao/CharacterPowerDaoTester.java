/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Power;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class CharacterPowerDaoTester {

    private HeroSpotterCharacterPowerDao dao;

    public CharacterPowerDaoTester() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        dao = ctx.getBean("HeroSpotterCharacterPowerDao", HeroSpotterCharacterPowerDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("DELETE FROM Character_Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Organization_Location WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Power WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Sighting WHERE 1=1");        
        cleaner.execute("DELETE FROM Sighting WHERE 1=1");
        cleaner.execute("DELETE FROM Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Location WHERE 1=1");
        cleaner.execute("DELETE FROM Power WHERE 1=1");
        cleaner.execute("DELETE FROM `Character` WHERE 1=1");
        
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void addGetCharacter() {
        
        Power power = new Power();
        power.setName("Super Smell");

        dao.addPower(power);
                
        Characters character = new Characters();
        character.setName("Spawn");
        character.setAlias("Al Simmons");
        character.setAlignment("good");

        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character.setPowers(powers);
        
        dao.addCharacter(character);

        Characters fromDao = dao.getCharacterById(character.getCharacterId());
        assertEquals(fromDao, character);

    }

        @Test
    public void addCharacterWithoutPowers() {
        
        Characters character = new Characters();
        character.setName("The Tick");
        character.setAlias("John Smith");
        character.setAlignment("good");
        
        List<Power> powers = new ArrayList<>();
        character.setPowers(powers);

        dao.addCharacter(character);

        Characters fromDao = dao.getCharacterById(character.getCharacterId());
        assertEquals(fromDao, character);

    }
    
    @Test
    public void deleteCharacter() {
        Power power = new Power();
        power.setName("Super Smell");

        dao.addPower(power);
        
        Characters character = new Characters();
        character.setName("Spawn");
        character.setAlias("Al Simmons");
        character.setAlignment("good");

        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character.setPowers(powers);
        
        dao.addCharacter(character);

        Characters fromDao = dao.getCharacterById(character.getCharacterId());
        assertEquals(fromDao, character);
        dao.deleteCharacter(character.getCharacterId());
        assertNull(dao.getCharacterById(character.getCharacterId()));
    }

    @Test
    public void addGetPower() {
        Power power = new Power();
        power.setName("Super Smell");

        dao.addPower(power);

        Power fromDao = dao.getPowerById(power.getPowerId());
        assertEquals(fromDao, power);
        
    }

    @Test
    public void deletePower() {
        Power power = new Power();
        power.setName("Super Smell");

        dao.addPower(power);

        Power fromDao = dao.getPowerById(power.getPowerId());
        assertEquals(fromDao, power);

        dao.deletePower(power.getPowerId());
        assertNull(dao.getPowerById(power.getPowerId()));

    }
    
    
}
