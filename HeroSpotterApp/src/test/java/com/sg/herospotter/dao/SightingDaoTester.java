/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Power;
import com.sg.herospotter.model.Sighting;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class SightingDaoTester {
    
    private HeroSpotterCharacterPowerDao charPowerDao;
    private HeroSpotterLocationDao locationDao;
    private HeroSpotterSightingDao sightingDao;
    
    public SightingDaoTester() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx 
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        
        charPowerDao = ctx.getBean("HeroSpotterCharacterPowerDao", HeroSpotterCharacterPowerDao.class);
        locationDao = ctx.getBean("HeroSpotterLocationDao", HeroSpotterLocationDao.class);
        sightingDao = ctx.getBean("HeroSpotterSightingDao", HeroSpotterSightingDao.class);
    
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("DELETE FROM Character_Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Organization_Location WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Power WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Sighting WHERE 1=1");        
        cleaner.execute("DELETE FROM Sighting WHERE 1=1");
        cleaner.execute("DELETE FROM Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Location WHERE 1=1");
        cleaner.execute("DELETE FROM Power WHERE 1=1");
        cleaner.execute("DELETE FROM `Character` WHERE 1=1");
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void addGetSighting() {
        
                // create a new super Power
        Power power = new Power();
        power.setName("Super Smell");
        
        charPowerDao.addPower(power);
        
        // create a new Character
        Characters character1 = new Characters();
        character1.setName("Spawn");
        character1.setAlias("Al Simmons");
        character1.setAlignment("good");
        
        Characters character2 = new Characters();
        character2.setName("Malebolgia");
        character2.setAlias("none");
        character2.setAlignment("evil");
        
        // add the superpower to the new Character object
        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character1.setPowers(powers);
        character2.setPowers(powers);
        
        // then add the character (with superpower) to the dao
        charPowerDao.addCharacter(character1);
        charPowerDao.addCharacter(character2);
        
        // for the Assert purposes, ALSO add the new character to this temp list
        List<Characters> characters = new ArrayList<>();
        characters.add(character1);
        characters.add(character2);

        // create a new Location
        Location location = new Location();
        location.setName("The Electric Company");
        location.setDescription("Its shocking.");
        location.setAddressStreet("1800 Main Street");
        location.setAddressCity("Townsville");
        location.setAddressState("KY");
        location.setAddressZip("40202");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        // add the location to the dao
        locationDao.addLocation(location);
        //List<Location> locations = new ArrayList<>();
        //locations.add(location);

        // create a new Sighting object
        Sighting sighting = new Sighting();
        sighting.setDate(LocalDate.parse("2017-04-12",
                DateTimeFormatter.ISO_DATE));
        sighting.setDescription("Epic Battle Took Place Here");
        
        // before adding the Sighting to the dao, add the new Characters and 
        // new Location to the Sighting object
        sighting.setLocation(location);
        sighting.setCharacters(characters);
        sightingDao.addSighting(sighting);
    
        Sighting fromDao = sightingDao.getSightingById(sighting.getSightingId());
        assertEquals(fromDao, sighting);
        
    }
    
    @Test
    public void deleteSighting() {
        
        // create a new super Power
        Power power = new Power();
        power.setName("Super Smell");
        
        charPowerDao.addPower(power);
        
        // create a new Character
        Characters character1 = new Characters();
        character1.setName("Spawn");
        character1.setAlias("Al Simmons");
        character1.setAlignment("good");
        
        Characters character2 = new Characters();
        character2.setName("Malebolgia");
        character2.setAlias("none");
        character2.setAlignment("evil");
        
        // add the superpower to the new Character object
        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character1.setPowers(powers);
        character2.setPowers(powers);
        
        // then add the character (with superpower) to the dao
        charPowerDao.addCharacter(character1);
        charPowerDao.addCharacter(character2);
        
        // for the Assert purposes, ALSO add the new character to this temp list
        List<Characters> characters = new ArrayList<>();
        characters.add(character1);
        characters.add(character2);

        // create a new Location
        Location location = new Location();
        location.setName("The Electric Company");
        location.setDescription("Its shocking.");
        location.setAddressStreet("1800 Main Street");
        location.setAddressCity("Townsville");
        location.setAddressState("KY");
        location.setAddressZip("40202");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        // add the location to the dao
        locationDao.addLocation(location);
        //List<Location> locations = new ArrayList<>();
        //locations.add(location);

        // create a new Sighting object
        Sighting sighting = new Sighting();
        sighting.setDate(LocalDate.parse("2017-04-12",
                DateTimeFormatter.ISO_DATE));
        sighting.setDescription("Epic Battle Took Place Here");
        
        // before adding the Sighting to the dao, add the new Characters and 
        // new Location to the Sighting object
        sighting.setLocation(location);
        sighting.setCharacters(characters);
        sightingDao.addSighting(sighting);
    
        // assert that what we created matches what we put into the Dao
        Sighting fromDao = sightingDao.getSightingById(sighting.getSightingId());
        assertEquals(fromDao, sighting);
        
        // assert that what we deleted is no longer in the Dao
        sightingDao.deleteSighting(sighting.getSightingId());
        assertNull(sightingDao.getSightingById(sighting.getSightingId()));
        
    }
    
    @Test
    public void updateSighting() {
        
        // create a new super Power
        Power power = new Power();
        power.setName("Super Smell");
        
        charPowerDao.addPower(power);
        
        // create a new Character
        Characters character1 = new Characters();
        character1.setName("Spawn");
        character1.setAlias("Al Simmons");
        character1.setAlignment("good");
        
        Characters character2 = new Characters();
        character2.setName("Malebolgia");
        character2.setAlias("none");
        character2.setAlignment("evil");
        
        // add the superpower to the new Character object
        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character1.setPowers(powers);
        character2.setPowers(powers);
        
        // then add the character (with superpower) to the dao
        charPowerDao.addCharacter(character1);
        charPowerDao.addCharacter(character2);
        
        // for the Assert purposes, ALSO add the new character to this temp list
        List<Characters> characters = new ArrayList<>();
        characters.add(character1);
        characters.add(character2);

        // create a new Location
        Location location = new Location();
        location.setName("The Electric Company");
        location.setDescription("Its shocking.");
        location.setAddressStreet("1800 Main Street");
        location.setAddressCity("Townsville");
        location.setAddressState("KY");
        location.setAddressZip("40202");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        // add the location to the dao
        locationDao.addLocation(location);
        //List<Location> locations = new ArrayList<>();
        //locations.add(location);

        // create a new Sighting object
        Sighting sighting = new Sighting();
        sighting.setDate(LocalDate.parse("2017-04-12",
                DateTimeFormatter.ISO_DATE));
        sighting.setDescription("Epic Battle Took Place Here");
        
        // before adding the Sighting to the dao, add the new Characters and 
        // new Location to the Sighting object
        sighting.setLocation(location);
        sighting.setCharacters(characters);
        sightingDao.addSighting(sighting);
    
        // assert that what we created matches what we put into the Dao
        Sighting fromDao = sightingDao.getSightingById(sighting.getSightingId());
        assertEquals(fromDao, sighting);
        
        // now update the Sighting information
        sighting.setDescription("The battle was first noticed at 12:30pm ET");
        sightingDao.updateSighting(sighting);
        
        Sighting updatedDao = sightingDao.getSightingById(sighting.getSightingId());
        assertEquals(updatedDao, sighting);
        
    }
}
