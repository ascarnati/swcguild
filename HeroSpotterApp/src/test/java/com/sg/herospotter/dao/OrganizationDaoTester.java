/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.model.Characters;
import com.sg.herospotter.model.Location;
import com.sg.herospotter.model.Organization;
import com.sg.herospotter.model.Power;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class OrganizationDaoTester {
    
    private HeroSpotterOrganizationDao orgDao;
    private HeroSpotterCharacterPowerDao charPowerDao;
    private HeroSpotterLocationDao locationDao;
    
    public OrganizationDaoTester() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx 
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        
        orgDao = ctx.getBean("HeroSpotterOrganizationDao", HeroSpotterOrganizationDao.class);
        charPowerDao = ctx.getBean("HeroSpotterCharacterPowerDao", HeroSpotterCharacterPowerDao.class);
        locationDao = ctx.getBean("HeroSpotterLocationDao", HeroSpotterLocationDao.class);
    
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("DELETE FROM Character_Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Organization_Location WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Power WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Sighting WHERE 1=1");        
        cleaner.execute("DELETE FROM Sighting WHERE 1=1");
        cleaner.execute("DELETE FROM Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Location WHERE 1=1");
        cleaner.execute("DELETE FROM Power WHERE 1=1");
        cleaner.execute("DELETE FROM `Character` WHERE 1=1");
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void addGetOrganization() {
    
        // create a new super Power
        Power power = new Power();
        power.setName("Super Smell");
        
        charPowerDao.addPower(power);
        
        // create a new Character
        Characters character = new Characters();
        character.setName("Spawn");
        character.setAlias("Al Simmons");
        character.setAlignment("good");
        
        // add the superpower to the new Character object
        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character.setPowers(powers);
        
        // then add the character (with superpower) to the dao
        charPowerDao.addCharacter(character);
        
        // for the Assert purposes, ALSO add the new character to this temp list
        List<Characters> characters = new ArrayList<>();
        characters.add(character);
        
        // create a new Location
        Location location = new Location();
        location.setName("The Electric Company");
        location.setDescription("Its shocking.");
        location.setAddressStreet("1800 Main Street");
        location.setAddressCity("Townsville");
        location.setAddressState("KY");
        location.setAddressZip("40202");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        // add the location to the dao
        locationDao.addLocation(location);
        List<Location> locations = new ArrayList<>();
        locations.add(location);
    
        // create a new Organization object
        Organization organization = new Organization();
        organization.setName("Band of Gypsies");
        organization.setDescription("Just a bunch of weirdos");
        organization.setAlignment("neutral");
        organization.setContactInfo("Light a fire");
        
        // before adding the Organization to the dao, add the new Character and 
        // new Location to the Organization object
        organization.setCharacters(characters);
        organization.setLocations(locations);
        orgDao.addOrganization(organization);
        
        Organization fromDao = orgDao.getOrganizationById(organization.getOrganizationId());
        assertEquals(fromDao, organization);
       
    }
    
    @Test
    public void deleteOrganization() {
        // create a new super Power
        Power power = new Power();
        power.setName("Super Smell");
        
        charPowerDao.addPower(power);
        
        // create a new Character
        Characters character = new Characters();
        character.setName("Spawn");
        character.setAlias("Al Simmons");
        character.setAlignment("good");
        
        // add the superpower to the new Character object
        List<Power> powers = new ArrayList<>();
        powers.add(power);
        character.setPowers(powers);
        
        // then add the character (with superpower) to the dao
        charPowerDao.addCharacter(character);
        
        // for the Assert purposes, ALSO add the new character to this temp list
        List<Characters> characters = new ArrayList<>();
        characters.add(character);
        
        // create a new Location
        Location location = new Location();
        location.setName("The Electric Company");
        location.setDescription("Its shocking.");
        location.setAddressStreet("1800 Main Street");
        location.setAddressCity("Townsville");
        location.setAddressState("KY");
        location.setAddressZip("40202");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        // add the location to the dao
        locationDao.addLocation(location);
        List<Location> locations = new ArrayList<>();
        locations.add(location);
    
        // create a new Organization object
        Organization organization = new Organization();
        organization.setName("Band of Gypsies");
        organization.setDescription("Just a bunch of weirdos");
        organization.setAlignment("neutral");
        organization.setContactInfo("Light a fire");
        
        // before adding the Organization to the dao, add the new Character and 
        // new Location to the Organization object
        organization.setCharacters(characters);
        organization.setLocations(locations);
        orgDao.addOrganization(organization);
        
        Organization fromDao = orgDao.getOrganizationById(organization.getOrganizationId());
        assertEquals(fromDao, organization);
        
        orgDao.deleteOrganization(organization.getOrganizationId());
        assertNull(orgDao.getOrganizationById(organization.getOrganizationId()));
        
    }
}
