/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.herospotter.dao;

import com.sg.herospotter.model.Location;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class LocationDaoTester {
    
    private HeroSpotterLocationDao dao;
    
    public LocationDaoTester() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx =
                new ClassPathXmlApplicationContext("test-applicationContext.xml");
    
        dao = ctx.getBean("HeroSpotterLocationDao", HeroSpotterLocationDao.class);
        
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("DELETE FROM Character_Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Organization_Location WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Power WHERE 1=1");
        cleaner.execute("DELETE FROM Character_Sighting WHERE 1=1");        
        cleaner.execute("DELETE FROM Sighting WHERE 1=1");
        cleaner.execute("DELETE FROM Organization WHERE 1=1");
        cleaner.execute("DELETE FROM Location WHERE 1=1");
        cleaner.execute("DELETE FROM Power WHERE 1=1");
        cleaner.execute("DELETE FROM `Character` WHERE 1=1");
    
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void addGetLocation() {
    
        Location location = new Location();
        location.setName("K-Mart");
        location.setDescription("Housewares");
        location.setAddressStreet("12345 Elm Blvd");
        location.setAddressCity("Townsville");
        location.setAddressState("PA");
        location.setAddressZip("16101");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        dao.addLocation(location);
        
        Location fromDao = dao.getLocationById(location.getLocationId());
        assertEquals(fromDao, location);
    }
    
    
    @Test
    public void deleteLocation() {        
            
        Location location = new Location();
        location.setName("K-Mart");
        location.setDescription("Housewares");
        location.setAddressStreet("12345 Elm Blvd");
        location.setAddressCity("Townsville");
        location.setAddressState("PA");
        location.setAddressZip("16101");
        location.setMapLatitude(0.0);
        location.setMapLongitude(0.0);
        
        dao.addLocation(location);
        
        Location fromDao = dao.getLocationById(location.getLocationId());
        assertEquals(fromDao, location);
        
        dao.deleteLocation(location.getLocationId());
        assertNull(dao.getLocationById(location.getLocationId()));
    }
}
