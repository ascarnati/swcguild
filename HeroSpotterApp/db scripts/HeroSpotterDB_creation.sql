DROP DATABASE HeroSpotter;

CREATE DATABASE IF NOT EXISTS HeroSpotter;

USE HeroSpotter;

-- CREATE BASE TABLES 

CREATE TABLE IF NOT EXISTS `Character` (
	CharacterId int(6) NOT NULL AUTO_INCREMENT,
    Name varchar(30) NOT NULL,
    Alias varchar(30) null,
    Alignment varchar(7) null,
	PRIMARY KEY (CharacterId)
);

CREATE TABLE IF NOT EXISTS Power (
	PowerId int(6) NOT NULL AUTO_INCREMENT,
    Name varchar(30) NOT NULL,
	PRIMARY KEY (PowerId)
);

CREATE TABLE IF NOT EXISTS Location (
	LocationId int(6) NOT NULL AUTO_INCREMENT,
    Name varchar(30),
    Description varchar(255),
    AddressStreet varchar(100),
    AddressCity varchar(30),
    AddressState varchar(2),
    AddressZip varchar(5),
    MapLatitude decimal(5,2),
    MapLongitude decimal(5,2),
	PRIMARY KEY (LocationId)
    );

CREATE TABLE IF NOT EXISTS Organization (
	OrganizationId int(6) NOT NULL AUTO_INCREMENT,
    Name varchar(30),
    Description varchar(255),
    Alignment varchar(7) null,
    ContactInfo varchar(255),
    PRIMARY KEY (OrganizationId)
);

CREATE TABLE IF NOT EXISTS Sighting (
	SightingId int(6) NOT NULL AUTO_INCREMENT,
    Date date NOT NULL,
    Description varchar(255),
    LocationId int,
    PRIMARY KEY (SightingId),
    FOREIGN KEY (LocationId) REFERENCES Location(LocationId)
);

-- CREATE BRIDGE tables for many-to-many relationships
CREATE TABLE IF NOT EXISTS Character_Sighting (
	CharSightingId int(6) NOT NULL AUTO_INCREMENT,
    CharacterId int NOT NULL,
    SightingId int NOT NULL,
    PRIMARY KEY (CharSightingId),
    FOREIGN KEY (CharacterId) REFERENCES `Character`(CharacterId),
    FOREIGN KEY (SightingId) REFERENCES Sighting(SightingId)
);

CREATE TABLE IF NOT EXISTS Character_Organization (
	CharacterId int NOT NULL,
    OrganizationId int Not NULL,
    PRIMARY KEY (CharacterId, OrganizationId),
    FOREIGN KEY (CharacterId) REFERENCES `Character`(CharacterId),
    FOREIGN KEY (OrganizationId) REFERENCES Organization(OrganizationId)
);

CREATE TABLE IF NOT EXISTS Character_Power (
	CharacterId int NOT NULL,
    PowerId int NOT NULL,
    PRIMARY KEY (CharacterId, PowerId),
    FOREIGN KEY (CharacterId) REFERENCES `Character`(CharacterId),
	FOREIGN KEY (PowerId) REFERENCES Power(PowerId)
);

CREATE TABLE IF NOT EXISTS Organization_Location (
	OrganizationId int NOT NULL,
    LocationId int NOT NULL,
    PRIMARY KEY (OrganizationId, LocationId),
    FOREIGN KEY (OrganizationId) REFERENCES Organization(OrganizationId),
    FOREIGN KEY (LocationId) REFERENCES Location(LocationId)
);


-- INSERT DATA into tables
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Joker', 'Jack Napier', 'evil');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Batman', 'Bruce Wayne', 'good');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Superman', 'Clark Kent', 'good');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Flash', 'Barry Allen', 'good');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Punisher', 'Frank Castle', 'neutral');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Wolverine', 'Logan', 'good');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Doctor Octopus', 'Otto Octavius', 'evil');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Lex Luther', 'Lex Luther', 'evil');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Spiderman', 'Peter Parker', 'good');
INSERT INTO `Character` (Name, Alias, Alignment) VALUES ('Sabretooth', 'Victor Creed', 'evil');


INSERT INTO Power (Name) VALUE ('Object-Based');
INSERT INTO Power (Name) VALUE ('Strength');
INSERT INTO Power (Name) VALUE ('Flight');
INSERT INTO Power (Name) VALUE ('Intelligence');
INSERT INTO Power (Name) VALUE ('Healing Factor');
INSERT INTO Power (Name) VALUE ('Invisibility');
INSERT INTO Power (Name) VALUE ('Pain Resistance');
INSERT INTO Power (Name) VALUE ('Sorcery');
INSERT INTO Power (Name) VALUE ('Telepathy');
INSERT INTO Power (Name) VALUE ('Speed');

INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('Daily Planet', 'Newspaper Headquarters', '2308 Main Street', 'Louisville', 'KY', '40202');
INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('Stark Tower', 'Home offices of Tony Stark', '5500 Main Street', 'Louisville', 'KY', '40202');
INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('Daily Bugle', 'Newspaper Headquarters', '1300 Main Street', 'Louisville', 'KY', '40202');
INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('The Riverfront', 'river front', '1500 Main Street', 'Louisville', 'KY', '40202');
INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('Warehouse District', '', '1300 Dark Alley Way', 'Louisville', 'KY', '40202');
INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('Justice League Headquarters', 'Team Headquarters', '4010 Maple Street','Louisville','KY', '40202');
INSERT INTO Location (Name, Description, AddressStreet, AddressCity, AddressState, AddressZip)
	VALUES ('Injustice League Headquarters', 'Team Headquarters', '7728 Magna Street', 'Louisville', 'KY', '40202');

INSERT INTO Organization (Name, Alignment, ContactInfo)	VALUES ('Injustice League', 'evil', 'Dial 1-800-Get-Evil');
INSERT INTO Organization (Name, Alignment, ContactInfo) VALUES ('Justice League', 'good', 'Bat Signal, Commissioner Gordon Hotline');
INSERT INTO Organization (Name, Alignment, ContactInfo) VALUES ('Avengers', 'good', 'Call Tony Stark');
INSERT INTO Organization (Name, Alignment, ContactInfo) VALUES ('Rogue', 'neutral', "They'll find you.");

INSERT INTO Character_Power (CharacterId, PowerId) VALUES (1,7);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (2,1);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (2,7);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (7,9);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (1,1);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (3,2);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (3,3);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (4,10);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (5,1);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (6,5);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (8,4);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (9,2);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (10,2);
INSERT INTO Character_Power (CharacterId, PowerId) VALUES (10,5);

INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (1,1);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (7,1);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (8,1);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (10,1);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (2,2);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (3,2);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (4,2);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (5,2);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (6,2);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (9,2);
INSERT INTO Character_Organization (CharacterId, OrganizationId) VALUES (5,3);

INSERT INTO Sighting (Date, LocationId) VALUES ('2017-01-01', 1);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-01-02', 2);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-01-02', 3);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-02-14', 4);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-02-14', 5);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-03-15', 1);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-03-20', 3);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-04-01', 4);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-04-01', 5);
INSERT INTO Sighting (Date, LocationId) VALUES ('2017-02-09', 4);
INSERT INTO Sighting (Date, LocationId) VALUES ('2016-12-31', 7);

INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (1,1);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (2,1);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (4,1);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (10,2);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (7,3);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (5,4);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (5,5);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (6,6);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (10,6);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (9,7);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (7,7);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (3,8);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (3,9);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (3,10);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (2,7);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (3,7);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (4,7);
INSERT INTO Character_Sighting (CharacterId, SightingId) VALUES (5,7);

INSERT INTO Organization_Location (OrganizationId, LocationId) VALUES (1,7);
INSERT INTO Organization_Location (OrganizationId, LocationId) VALUES (2,6);
INSERT INTO Organization_Location (OrganizationId, LocationId) VALUES (3,2);