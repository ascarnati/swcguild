<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hero Spotter Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1><center>Hero Spotter</center></h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayCharactersPage">Characters</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayLocationsPage">Locations</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayOrganizationsPage">Organizations</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displaySightingsPage">Sightings</a></li>                        
                </ul>    
            </div>

            <!-- Add a row to our container to hold the list of sightings -->
            <div class="row">
                <h4><center>
                        <p>
                            Welcome to the official sightings database of the
                            Hero Education and Relationship Organization.
                        </p>
                        <p>
                            You can use this Hero Spotter application to log 
                            information when you spot a Hero or Villain among us.
                        </p>
                    </center></h4>
                <br>
            </div>
            <div class="col-md-7 col-md-offset-2">
                <h2><center>Recent Sightings!</center></h2>
                <table id="recentSightingTable" class="table table-hover">
                    <tr>
                        <th width="50%">Date</th>
                        <th width="50%">Description</th>
                    </tr>
                    <c:forEach var="currentSighting" items="${sightingList}">
                        <tr>
                            <td>
                                <a href="displaySightingDetails?sightingId=${currentSighting.sightingId}">
                                <c:out value="${currentSighting.date}"/>
                            </td>
                            <td>
                                <c:out value="${currentSighting.description}"/>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

