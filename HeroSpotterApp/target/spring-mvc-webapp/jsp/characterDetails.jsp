<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Character Details</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Character Details</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                  <li role="presentation">
                        <a href="${pageContext.request.contextPath}">
                            Home
                        </a>
                  </li>
                  <li role="presentation">
                      <a href="${pageContext.request.contextPath}/displayCharactersPage">
                          Characters
                      </a>
                  </li>
                </ul>    
            </div>
                <p>
                    <strong>Name: </strong>
                    <c:out value="${character.name}"/>
                </p>
                <p>
                    <strong>Alias: </strong>
                    <c:out value="${character.alias}"/>
                </p>
                <p>
                    <strong>Alignment: </strong>
                    <c:out value="${character.alignment}"/>
                </p>
                <br>
                <p><h4><em>
                    Powers:
                </em></h4></p>
                <c:forEach items="${character.powers}" var="currentPowers">
                    <p>
                        <c:out value="${currentPowers.name}"/>
                    </p>    
                </c:forEach>
                

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>