<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Locations</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Edit Location</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}">
                            Home
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayLocationsPage">
                            Contacts
                        </a>
                    </li>
                </ul>    
            </div>
            <sf:form class="form-horizontal" role="form" modelAttribute="location"
                     action="editLocation" method="POST">
                <div class="form-group">
                    <label for="add-locationName" class="col-md-4 control-label">Name:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationName" 
                                  path="name" placeholder="Location Name" />
                        <sf:errors path="name" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationDescription" class="col-md-4 control-label">Description:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationDescription"
                                  path="description" placeholder="Description" />
                        <sf:errors path="description" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationAddressStreet" class="col-md-4 control-label">Street Address:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationAddressStreet"
                                  path="addressStreet" placeholder="Street Address" />
                        <sf:errors path="addressStreet" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationAddressCity" class="col-md-4 control-label">City:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationAddressCity"
                                  path="addressCity" placeholder="City" />
                        <sf:errors path="addressCity" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationAddressState" class="col-md-4 control-label">State:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationAddressState"
                                  path="addressState" placeholder="State" />
                        <sf:errors path="addressState" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationAddressZip" class="col-md-4 control-label">Zipcode:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationAddressZip"
                                  path="addressZip" placeholder="Zipcode" />
                        <sf:errors path="addressZip" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationMapLatitude" class="col-md-4 control-label">Latitude:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationMapLatitude"
                                  path="mapLatitude" placeholder="Latitude" />
                        <sf:errors path="mapLatitude" cssClass="error"></sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-locationMapLongitude" class="col-md-4 control-label">Longitude:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-locationMapLongitude"
                                  path="mapLongitude" placeholder="Longitude" />
                        <sf:errors path="mapLongitude" cssClass="error"></sf:errors>
                        <sf:hidden path="locationId" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <input type="submit" class="btn btn-default" value="Update Location"/>
                    </div>
                </div>
            </sf:form>      
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>