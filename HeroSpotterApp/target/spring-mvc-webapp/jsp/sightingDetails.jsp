<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sighting Details</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Sighting Details</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                  <li role="presentation">
                        <a href="${pageContext.request.contextPath}">
                            Home
                        </a>
                  </li>
                  <li role="presentation">
                      <a href="${pageContext.request.contextPath}/displaySightingsPage">
                          Sightings
                      </a>
                  </li>
                </ul>    
            </div>
                <p>
                <h4><em>Sighting Info:</em></h4>
                </p>
                <p>
                    <strong>Date: </strong>
                    <c:out value="${sighting.date}"/>
                </p>
                <p>
                    <strong>Description: </strong>
                    <c:out value="${sighting.description}"/>
                </p>
                <br
                <p>
                    <h4><em>Location Info:</em></h4>
                </p>
                <p>
                    <strong>Name: </strong>
                    <c:out value="${sighting.location.name}"/>
                </p>
                <p>
                    <strong>Description:</strong>
                    <c:out value="${sighting.location.description}"/>
                </p>
                <p>
                    <strong>Address:</strong>
                    <c:out value="${sighting.location.addressStreet}"/>
                    <c:out value="${sighting.location.addressCity}"/>
                    <c:out value="${sighting.location.addressState}"/>
                    <c:out value="${sighting.location.addressZip}"/>
                </p>
                <p>
                    <strong>Latitude: </strong>
                    <c:out value="${sighting.location.mapLatitude}"/>
                </p>                
                <p>
                    <strong>Longitude: </strong>
                    <c:out value="${sighting.location.mapLongitude}"/>
                </p>
                <br
                <p><h4><em>
                    Characters Sighted:
                </em></h4></p>
                
                <c:forEach items="${sighting.characters}" var="currentCharacter">
                    <p>
                        <strong>Character:</strong>
                        <c:out value="${currentCharacter.name}"/>
                        <strong>Character Powers:</strong>
                            <c:forEach items="${currentCharacter.powers}" var="currentPower">
                                <c:out value="${currentPower.name}"/>,
                            </c:forEach>
                    </p>
                </c:forEach>

        </div>
                
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>