<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Character</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/anthonyStyles.css" rel="stylesheet" type="text/css" />        
    </head>
    <body>
        <div class="container">
            <h1><center>Hero Spotter</center></h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayCharactersPage">Characters</a></li>
                </ul>    
            </div>
            <form class="form-horizontal" 
                  role="form" method="POST" 
                  action="editCharacter">

                <div class="form-group">
                    <label for="displayCurrentName" class="col-md-4 control-label">Character Name:</label>
                    <div class="col-md-8">
                        <c:out value="${character.name}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="displayCurrentAlias" class="col-md-4 control-label">Alias:</label>
                    <div class="col-md-8">
                        <c:out value="${character.alias}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="displayCurrentAlignment" class="col-md-4 control-label">Alignment:</label>
                    <div class="col-md-8">
                        <c:out value="${character.alignment}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="displayCurrentPowers" class="col-md-4 control-label">Powers:</label>
                    <div class="col-md-8">
                        <c:forEach items="${powerList}" var="currentPower">
                            <option value="${currentPower.powerId}"><c:out value="${currentPower.name}" /></option>
                        </c:forEach>
                    </div>
                </div>

                <hr/>


                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <input type="submit" class="btn btn-default" value="Edit Character"/>
                    </div>
                </div>
            </form>

        </div> <!-- End col div -->

    </div> <!-- End row div -->


    <!-- Main Page Content Stop -->    
</div>
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>